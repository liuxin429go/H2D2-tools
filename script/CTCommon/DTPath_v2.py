#!/usr/bin/env python
# -*- coding: utf-8 -*-
## cython: profile=True
# cython: linetrace=True
#************************************************************************
# --- Copyright (c) INRS 2017-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Data path algo
One makes the assumption that data is LINEARLY interpolated in time
"""

import math         # for sqrt in a test
import logging
import numpy as np

try:
    from .DTPathError    import ERR_CODE as ErrCode
except ImportError:
    from .DTPathError_pp import ERR_CODE as ErrCode

LOGGER = logging.getLogger("INRS.H2D2.Tools.Data.Path")

class DTPathPoint:
    """
    A point in the particle path, with time stamp, position, data.
    DTDataPoint acts as a tuple. Cython does not allow to inherit form tuple.
    """
    def __init__(self, *args):
        self.__data = tuple(args)

    def __getitem__(self, k):
        return self.__data[k]

    def __iter__(self):
        return iter(self.__data)

    def __eq__(self, other):
        if self[0] != other[0]: return False
        if self[1] != other[1]: return False
        if self[2] != other[2]: return False
        return True

    def __ne__(self, other):
        return not self == other

    def __lt__(self, other):
        if self[0] < other[0]: return True
        if self[1] < other[1]: return True
        if self[2] < other[2]: return True
        return False

    def __le__(self, other):
        return not other < self

    def __gt__(self, other):
        return other < self

    def __ge__(self, other):
        return self < other

    def t(self):
        return self[0]

    def x(self):
        return self[1]

    def y(self):
        return self[2]

    def xy(self):
        return self[1], self[2]

"""
List of DTPathPoint
"""
DTPath = list

class PosInRefElem:
    """
    Position in the reference element
    """
    __slots__ = 'ele', 'isid', 'k', 'e'
    def __init__(self, ele=None, isid=-1, k=-10.0, e=-10.0):
        self.ele = ele
        self.isid = isid
        self.k, self.e = k, e

    def update(self, ele=None, isid=-1, k=-10.0, e=-10.0):
        self.ele = ele
        self.isid = isid
        self.k, self.e = k, e

class PathInfo:
    """
    Current position, display time and status
    """
    def __init__(self, x0, y0, tmin, tmax):
        self.x0 = x0
        self.y0 = y0
        self.tmin = tmin
        self.tmax = tmax

        self.point = DTPathPoint()  # Current DTPathPoint (t, x, y, ...)
        self.refp  = PosInRefElem() # Current PosInRefElem
        self.tdsp  = -1             # display time
        self.stts  = ErrCode.ERR_END_OF_PATH

    def update(self, point, refp, tdsp, stts):
        self.point = point          # Current DTPathPoint (t, x, y, ...)
        self.refp  = refp           # Current PosInRefElem
        self.tdsp  = tdsp           # display time
        self.stts  = stts           # Status (ErrCode)

    def isDone(self):
        return self.point.t() >= self.tmax

    def t(self):
        return self.point.t()

    def x(self):
        return self.point.x()

    def y(self):
        return self.point.y()

    def xy(self):
        return self.point.xy()

class DTAlgoPath:
    def __init__(self):
        # ---  Parameters
        self.field = None
        self.cols  = None
        self.tinj  = []
        self.dtdsp = 0.0
        self.dtclc = 0.0
        self.prgrs = []

        self.mesh  = None    # short-cut
        self.data  = None    # short-cut

        # ---  Computing variables
        self.paths  = []        # Paths, list of DTPath
        self.ptInfo = []        # Curent points
        self.time0  = -1        # Time at step i
        self.time1  = -1        # Time at step i+1
        self.data0  = None      # Data at step i
        self.data1  = None      # Data at step i+1
        self.currentPath = None # DTPath

    def getNextElement(self, refp):
        raise NotImplementedError

    def addToCurrentPath(self, p, s=''):
        LOGGER.debug('DTAlgoPath.addToCurrentPath: %.3f (%.3f, %.3f) %s', p.t(), p.x(), p.y(), s)
        self.currentPath.append(p)

    def xeqOneElement(self, ptcl, tfin):
        raise NotImplementedError

    def xeq1(self, ipath):
        """
        Compute one particle path
        Returns a DTPath
        """

        # ---  Update progress
        if self.prgrs: self.prgrs[1][0] = ipath  % self.prgrs[1][1] + 1 # Fragile!! Depend
        if self.prgrs: self.prgrs[2][0] = ipath // self.prgrs[1][1] + 1 # de l'ordre de parcours

        # --- Checks
        ptcl = self.ptInfo[ipath]
        if ptcl.stts != ErrCode.ERR_OK: return
        if ptcl.isDone(): return
        if ptcl.tmax < self.time0: return
        if ptcl.tmin > self.time1: return

        # --- Initialize current path
        ierr = ptcl.stts
        self.currentPath = self.paths[ipath]

        # --- Compute path
        ttot = max(ptcl.tmin, self.time0)
        tfin = min(ptcl.tmax, self.time1)
        while ttot < tfin and ierr == ErrCode.ERR_OK:
            if self.prgrs and self.prgrs[4]: break
            if self.prgrs: self.prgrs[3][0] = ptcl.t()-ptcl.tmin
            LOGGER.debug('DTAlgoPath.xeq1: enter element %i side %s t=%.3f xy=(%.3f,%.3f) ke=(%.3f,%.3f)', ptcl.refp.ele.ig, ptcl.refp.isid, ptcl.t(), ptcl.x(), ptcl.y(), ptcl.refp.k, ptcl.refp.e)
            ierr = self.xeqOneElement(ptcl, tfin)
            LOGGER.debug('DTAlgoPath.xeq1: leave element %i side %s t=%.3f xy=(%.3f,%.3f) ke=(%.3f,%.3f)', ptcl.refp.ele.ig, ptcl.refp.isid, ptcl.t(), ptcl.x(), ptcl.y(), ptcl.refp.k, ptcl.refp.e)

            if ierr == ErrCode.ERR_OK:
                ierr = self.getNextElement(ptcl.refp)
                ptcl.stts = ierr
            elif ierr == ErrCode.ERR_POINT_OUTSIDE:
                # ele = self.mesh.localizePoints( (x,), (y,) )[0]
                # if ele
                LOGGER.info('DTAlgoPath.xeq1: No continuation found')

        # --- Log exit code
        if ierr == ErrCode.ERR_OK:
            pass
        elif ierr == ErrCode.ERR_OUT_OF_ITER:
            LOGGER.info('DTAlgoPath.xeq1: Out of iteration: probably very slow velocities')
        elif ierr == ErrCode.ERR_OUT_OF_TIME:
            if ptcl.isDone():
                LOGGER.info('DTAlgoPath.xeq1: Path OK')
                if ptcl.tdsp > 0.0: self.addToCurrentPath(ptcl.point)
            else:
                ptcl.stts = ErrCode.ERR_OK
                # ierr = ptcl.stts
        elif ierr == ErrCode.ERR_ALL_ITER_OUTSIDE:
            LOGGER.info('DTAlgoPath.xeq1: No steps inside element')
        elif ierr == ErrCode.ERR_POINT_OUTSIDE:
            LOGGER.info('DTAlgoPath.xeq1: Algo locked: no start element found')
        elif ierr == ErrCode.ERR_END_OF_PATH:
            LOGGER.info('DTAlgoPath.xeq1: End of path: probably boundary reached')
        elif ierr == ErrCode.ERR_BROKEN_NEIGHBOUR_LIST:
            LOGGER.error('DTAlgoPath.xeq1: Broken neighbour list (should not happen!)')

        self.ptInfo[ipath] = ptcl
        self.currentPath = None

    def xeq_ini(self, X, Y, tinj, tstop):
        """
        Initialize all paths
        Returns the list of DTPath with starting point, and the list of current path info
        """

        paths  = []
        ptInfo = []

        # --- Get start elements
        E = self.mesh.localizePoints(X, Y)
        for x, y, ele in zip(X, Y, E):
            if not ele: raise ValueError("Point (%f, %f) could not be located in mesh" % (x, y))

        # --- Initialize each path
        for tini, tfin in zip(tinj, tstop):
            data = self.data.getDataAtTime(tini, cols=self.cols)

            for x, y, ele in zip(X, Y, E):
                # --- Initialize current path
                pth = DTPath()

                # --- Get start position
                k, e = ele.transformeInverse(x, y)
                d = ele.interpolateRef(k, e, data)
                refp = PosInRefElem(ele, -1, k, e)
                pnt  = DTPathPoint(tini, x, y, *d)

                # --- Add start point to path
                self.currentPath = pth
                self.addToCurrentPath(pnt, '(%.3f,%.3f)' % (k, e))
                self.currentPath = None

                # --- Particle info
                tdsp = 0.0
                ptcl = PathInfo(x, y, tini, tfin)
                ptcl.update(pnt, refp, tdsp, ErrCode.ERR_OK)

                paths.append(pth)
                ptInfo.append(ptcl)
                LOGGER.info('DTAlgoPath.xeq_ini: path %.3f (%.3f, %.3f)', tini, x, y)

        return paths, ptInfo

    def xeq(self, X, Y, field, cols, tinj, dtsim, dtdsp, dtclc, prgrs=[]):
        """
        Compute all the particle paths corresponding to the
        X, Y injection points.
        Returns a list of DTPath
        prgs is an exchange list for progress info:
            prgrs[0]: output: on exit will contain the paths
            prgrs[1]: output: [ixy, nxy]
            prgrs[2]: output: [inj, nnj]
            prgrs[3]: output: [t-t0, t1-t0]
            prgrs[4]: input:  True if computation is cancelled
        """
        self.field = field
        self.cols  = cols
        self.tinj  = tinj   # list of injection times
        self.dtdsp = dtdsp
        self.dtclc = dtclc
        self.prgrs = prgrs

        self.mesh = self.field.getGrid()
        self.data = self.field.getData()

        tend = [t+dtsim for t in tinj]
        self.paths, self.ptInfo = self.xeq_ini(X, Y, tinj, tend)

        tmin, tmax = min(tinj), max(tinj)+dtsim
        LOGGER.debug('DTAlgoPath.xeq: tmin, tmax (%.3f, %.3f)', tmin, tmax)

        # ---  Get indices bounding [tmin, tmax]
        allSteps = self.data.getTimes()
        if len(allSteps) == 1:      # Full range if just 1 time step
            allSteps = [tmin, tmax]
            allIndxs = [0, 0]
            imin, imax = 0, 1
        else:                       # Limit to the time serie
            allIndxs = list(range(len(allSteps)))
            if tmin <= allSteps[0]:
                imin = 0
            elif tmin >= allSteps[-1]:
                imin = len(allSteps)-1
            else:
                imin = next( iter([i for i, t in enumerate(allSteps) if t > tmin]) ) -1
            if tmax <= allSteps[0]:
                imax = 0
            elif tmax >= allSteps[-1]:
                imax = len(allSteps)-1
            else:
                imax = next( iter([i for i, t in enumerate(allSteps) if t > tmax]) )
        LOGGER.info('DTAlgoPath.xeq: imin, imax (%i, %i) (%.3f, %.3f)', imin, imax, allSteps[imin], allSteps[imax])

        if self.prgrs: self.prgrs[1] = [0, len(X)]
        if self.prgrs: self.prgrs[2] = [0, len(tinj)]
        for it in range(imin, imax):
            if self.prgrs and self.prgrs[4]: break
            LOGGER.debug('DTAlgoPath.xeq: (it, t) (%i, %.3f)', it, allSteps[it])
            it0 = allIndxs[it+0]
            it1 = allIndxs[it+1]
            self.time0 = allSteps[it+0]
            self.time1 = allSteps[it+1]
            self.data0 = self.data.getDataAtStep(it0, cols=self.cols)
            self.data1 = self.data.getDataAtStep(it1, cols=self.cols)
            if self.prgrs: self.prgrs[3] = [self.time0-tmin, tmax-tmin]

            for ip in range(len(self.paths)):
                if self.prgrs and self.prgrs[4]: break
                self.xeq1(ip)

        # ---  Compact and sort the paths
        self.paths = [p for p in self.paths if len(p) > 1]
        self.paths.sort(key=lambda item: item[0])

        if self.prgrs: self.prgrs[0] = self.paths
        return self.paths

# Should be @staticmethod, but static methods are not callable
def intersect_side0(k, e, dk, de):
    # intersection with side 0
    ret = False, 0, 0, 0
    #print('intersect_side0: k,e=(%.6e, %.6e); dk,de=(%.6e, %.6e)' % (k, e, dk, de))
    isValid = abs(de) > 1.0e-18
    if not isValid: return ret

    dki = - e * (dk / de)   # dki in [0, dk]
    isValid = (dki >= 0.0 and (0.0 <= dki <= dk)) or (dki < 0.0 and (dk <= dki <= 0.0))
    if not isValid: return ret

    ki = k + dki        # ki in [0, 1]
    isValid = 0.0 <= ki <= 1.0
    if not isValid: return ret

    dei = -e            # dei in [0, e]
    isValid = (dei >= 0.0 and (0.0 <= dei <= de)) or (dei < 0.0 and (de <= dei <= 0.0))
    if not isValid: return ret

    ei = 0.0            # ei in [0, 1]

    ds = math.hypot(dki,dei) / math.hypot(dk,de)
    doIntersect = True
    #print('intersect_side0: intersection at k,e=(%.6e, %.6e)' % (ki, ei), ds)
    return doIntersect, ki, ei, ds

def intersect_side1(k, e, dk, de):
    # intersection with side 1
    ret = False, 0, 0, 0
    #print('intersect_side1: k,e=(%.6e, %.6e); dk,de=(%.6e, %.6e)' % (k, e, dk, de))
    isValid = abs(dk + de) > 1.0e-18
    if not isValid: return ret

    if de == 0.0:
        ki = 1.0 - e        # ki in [0, 1]
    elif dk == 0.0:
        ki = k              # ki in [0, 1]
    else:
        ki = (dk + k * de - e * dk) / (dk + de)    # ki in [0, 1]
    isValid = 0.0 <= ki <= 1.0
    if not isValid: return ret

    dki = ki - k        # dki in [0, dk]
    isValid = (dki >= 0.0 and (0.0 <= dki <= dk)) or (dki <= 0.0 and (dk <= dki <= 0.0))
    if not isValid: return ret

    ei = 1.0 - ki       # ei in [0, 1]
    isValid = 0.0 <= ei <= 1.0
    if not isValid: return ret

    dei = ei - e        # dei in [0, e]
    isValid = (dei >= 0.0 and (0.0 <= dei <= de)) or (dei < 0.0 and (de <= dei <= 0.0))
    if not isValid: return ret

    ds = math.hypot(dki,dei) / math.hypot(dk,de)
    doIntersect = True
    #print('intersect_side1: intersection at k,e=(%.6e, %.6e)' % (ki, ei), ds)
    return doIntersect, ki, ei, ds

def intersect_side2(k, e, dk, de):
    # intersection with side 2
    ret = False, 0, 0, 0
    #print('intersect_side2: k,e=(%.6e, %.6e); dk,de=(%.6e, %.6e)' % (k, e, dk, de))
    isValid = abs(dk) > 1.0e-18
    if not isValid: return ret

    dei = - k * (de / dk)   # dei in [0, de]
    isValid = (dei >= 0.0 and (0.0 <= dei <= de)) or (dei < 0.0 and (de <= dei <= 0.0))
    if not isValid: return ret

    ei = e + dei        # ee in [0, 1]
    isValid = 0.0 <= ei <= 1.0
    if not isValid: return ret

    dki = -k            # dki in [0, k]
    isValid = (dki >= 0.0 and (0.0 <= dki <= dk)) or (dki < 0.0 and (dk <= dki <= 0.0))
    if not isValid: return ret

    ki = 0.0            # ki in [0, 1]

    ds = math.hypot(dki,dei) / math.hypot(dk,de)
    doIntersect = True
    #print('intersect_side2: intersection at k,e=(%.6e, %.6e)' % (ki, ei), ds)
    return doIntersect, ki, ei, ds

NPAS_MAX = 100000
INTERSECT_FUNCTIONS = (intersect_side1, intersect_side2, intersect_side0, intersect_side1, intersect_side2)

class DTAlgoPathT3(DTAlgoPath):
    def __init__(self):
        super(DTAlgoPathT3, self).__init__()

    def getNextElement(self, refp):
        """
        Get the neighbouring element.
        On exit, refp is updated and an error code is returned
        """
        ierr = ErrCode.ERR_OK

        el1, is1 = refp.ele, refp.isid
        ie2 = self.mesh.getElementNeighbour(el1.il, is1)
        is2, k2, e2 = -1, -10.0, -10.0  # Valeurs par défaut de PosInRefElem
        if ie2 < 0:
            el2 = None
            ierr = ErrCode.ERR_END_OF_PATH
        else:
            el2 = self.mesh.getElement(ie2)
            if   self.mesh.getElementNeighbour(el2.il, 0) == el1.il:
                is2 = 0
                if   is1 == 0:
                    k2, e2 = (1.0 - refp.k), 0.0
                elif is1 == 1:
                    k2, e2 = refp.k, 0.0
                elif is1 == 2:
                    k2, e2 = refp.e, 0.0
            elif self.mesh.getElementNeighbour(el2.il, 1) == el1.il:
                is2 = 1
                if   is1 == 0:
                    k2, e2 = refp.k, (1.0 - refp.k)
                elif is1 == 1:
                    k2, e2 = refp.e, refp.k
                elif is1 == 2:
                    k2, e2 = (1 - 0 - refp.e), refp.e
            elif self.mesh.getElementNeighbour(el2.il, 2) == el1.il:
                is2 = 2
                if   is1 == 0:
                    k2, e2 = 0.0, refp.k
                elif is1 == 1:
                    k2, e2 = 0.0, refp.e
                elif is1 == 2:
                    k2, e2 = 0.0, (1.0 - refp.e)
            else:
                is2 = -1
                ierr = ErrCode.ERR_BROKEN_NEIGHBOUR_LIST
        #================================
        # Debug code
        #================================
        if ierr == ErrCode.ERR_OK:
            x1, y1 = el1.transforme(refp.k, refp.e)
            x2, y2 = el2.transforme(k2, e2)
            d = math.hypot(x2-x1, y2-y1)
            if d > 1.0e-8:
                print(is1, is2)
                print((x1, y1), (x2, y2), d)
                print((refp.k, refp.e), (k2, e2))
                print((k2, e2), el2.transformeInverse(x1, y1))
                raise ValueError
        #================================
        # End debug code
        #================================
        refp.update(el2, is2, k2, e2)
        return ierr

    def xeqOneElement(self, ptcl, tfin):
        """
        Computes the path in one element. Except for errors,
        the path will either reach element border or timeout.
        On exit, ptcl is updated and the function returns an error code
        """

        ierr = ErrCode.ERR_OK

        # --- Initialize local variables
        refp = ptcl.refp
        x, y = ptcl.xy()
        t, tdsp = ptcl.t(), ptcl.tdsp

        # --- Entry point in ksi,eta coord
        el0 = refp.ele
        is0 = refp.isid
        vksi_0, veta_0 = refp.k, refp.e
        LOGGER.debug('DTAlgoPathT3.xeqOneElement: (%.3f,%.3f) in element %7i @ (%.3e,%.3e)', x, y, el0.ig, vksi_0, veta_0)
        vsum_0 = 1.0 - vksi_0 - veta_0
        elmin = min(vsum_0, vksi_0, veta_0)
        if elmin < -1.0e-6:
            ierr = ErrCode.ERR_POINT_OUTSIDE
            return ierr

        # --- Loop on time steps
        vksi_1, veta_1 = vksi_0, veta_0
        dt = min(self.dtclc, tfin - t)
        nout = 0
        it   = 0    # pylint
        for it in range(NPAS_MAX):     # Protection again small steps
            dt = min(dt, tfin - t)
            n1 = (self.time1-t) /(self.time1-self.time0)
            n2 = (t-self.time0) /(self.time1-self.time0)
            # --- Get velocity
            u = n1*el0.interpolateRef(vksi_1, veta_1, self.data0) + n2*el0.interpolateRef(vksi_1, veta_1, self.data1)
            # --- Derivatives
            dudx = n1*el0.ddx(x, y, self.data0) + n2*el0.ddx(x, y, self.data1)    # u,x & v,x
            dudy = n1*el0.ddy(x, y, self.data0) + n2*el0.ddy(x, y, self.data1)    # u,y & v,y
            # --- IF |V| TOO SMALL RETURN
            #vabs = u[0]**2 + u[1]**2
            #if vabs < 1.0e-6:
            #    vksi = vksi_1
            #    veta = veta_1
            #    ierr = 1
            #    goto 300
            # --- Convection
            cnvx = u[0] * dudx[0] + u[1] * dudy[0]
            cnvy = u[0] * dudx[1] + u[1] * dudy[1]
            # --- Delta - Taylor 2nd order
            dx = (u[0] + 0.5 * cnvx * dt) * dt
            dy = (u[1] + 0.5 * cnvy * dt) * dt
            dksi, deta = np.dot(el0.jacobian(), (dx, dy))
            vksi_2 = vksi_1 + dksi
            veta_2 = veta_1 + deta
            vsum_2 = 1.0 - vksi_2 - veta_2
            elmin = min(vsum_2, vksi_2, veta_2)

            # --- Already outside ?  break out of loop
            if elmin < 0.0:
                if nout == 5: break
                nout += 1
                dt   *= 0.5
                continue

            # --- Update position
            t += dt
            tdsp += dt
            vksi_1 = vksi_2
            veta_1 = veta_2
            # --- New point
            if tdsp >= self.dtdsp:
                tdsp = 0.0
                x, y = el0.transforme(vksi_1, veta_1)
                n1 = (self.time1-t) /(self.time1-self.time0)
                n2 = (t-self.time0) /(self.time1-self.time0)
                u = n1*el0.interpolateRef(vksi_1, veta_1, self.data0) + n2*el0.interpolateRef(vksi_1, veta_1, self.data1)
                self.addToCurrentPath(DTPathPoint(t, x, y, *u), '(%.3f,%.3f)' % (vksi_1, veta_1))
            # --- Check for time elapsed condition
            if t >= tfin: break

        # --- Error codes
        doClcIntersect = True
        if t >= tfin:
            vksi = vksi_1
            veta = veta_1
            ierr = ErrCode.ERR_OUT_OF_TIME
            doClcIntersect = False
        elif it >= NPAS_MAX:
            vksi = vksi_1
            veta = veta_1
            ierr = ErrCode.ERR_OUT_OF_ITER        # Out of iter
            doClcIntersect = False
        elif it == nout:
            ierr = ErrCode.ERR_ALL_ITER_OUTSIDE   # All iter are outside

        # --- Intersection with side of element
        isid = -1
        if doClcIntersect:
            doIntersect = False
            iss = is0 if is0 != -1 else 0
            for f in INTERSECT_FUNCTIONS[iss:iss + 3]:
                if not doIntersect:
                    doIntersect, vksi_2, veta_2, ds = f(vksi_1, veta_1, dksi, deta)
            if not doIntersect:                                 # Fall-back Mikado
                vksi_2 = min(max(vksi_2, 0.0), 1.0)
                veta_2 = min(max(veta_2, 0.0), 1.0)
                vsum_2 = 1.0 - vksi_2 - veta_2
                if vsum_2 < 0.0:
                    vksi_2 += 0.5 * vsum_2
                    veta_2 += 0.5 * vsum_2
                LOGGER.info('DTAlgoPathT3.xeqOneElement: No intersection with sides found, use fall-back')
            # --- Update position
            dt *= ds
            t += dt
            tdsp += dt
            vksi = vksi_2
            veta = veta_2
            vsum = 1.0 - vksi - veta
            vmin = min(vsum, vksi, veta)
            if vsum == vmin:
                isid = 1
            elif vksi == vmin:
                isid = 2
            else:
                isid = 0

        # --- Check for same out-segment as in-segment
        if ierr == ErrCode.ERR_ALL_ITER_OUTSIDE:
            if isid != is0:
                ierr = ErrCode.ERR_OK
            else:
                if isid == 1:
                    if vksi > veta:
                        isid = 0
                    else:
                        isid = 2
                elif isid == 2:
                    if veta > 0.5:
                        isid = 1
                    else:
                        isid = 0
                else:
                    if vksi > 0.5:
                        isid = 1
                    else:
                        isid = 2

        # --- Update end point
        if ierr != ErrCode.ERR_ALL_ITER_OUTSIDE:
            x, y = el0.transforme(vksi, veta)
            n1 = (self.time1-t) /(self.time1-self.time0)
            n2 = (t-self.time0) /(self.time1-self.time0)
            u = n1*el0.interpolateRef(vksi_1, veta_1, self.data0) + n2*el0.interpolateRef(vksi_1, veta_1, self.data1)

        refp = PosInRefElem(refp.ele, isid, vksi, veta)
        pnt  = DTPathPoint(t, x, y, *u)
        ptcl.update(pnt, refp, tdsp, ierr)
        return ierr

#pylint: disable
if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname(os.path.abspath(__file__))
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

    from . import FEMesh
    from . import FEMeshIO
    from . import DTData
    from . import DTDataIO
    from . import DTField
    #from IPImageProcessor import IPRaster
    def main():
        d = r'E:\Projets_simulation\LacErie\1x'
        #srs_proj = IPRaster.IPSpatialReference()
        #srs_grid = IPRaster.IPSpatialReference()
        #srs_proj.ImportFromFileAndGuess(os.path.join(d, 'erie_nad83.prj'))
        #srs_grid.ImportFromFileAndGuess(os.path.join(d, 'erie_utm.prj'))
        #grid2proj = IPRaster.IPCoordinateTransformation(srs_grid, srs_proj)
        #proj2grid = IPRaster.IPCoordinateTransformation(srs_proj, srs_grid)

        fpath = [os.path.join(d, 'erie_t6l.cor'), os.path.join(d, 'erie_t6l.ele')]
        #grid2proj = None
        m = FEMesh.FEMesh(fpath, withLocalizer=True)
        #m = FEMesh.FEMesh(fpath, projection=grid2proj, withLocalizer=True)

        d = r'E:\Projets_simulation\LacErie\1x\rdps\cw6'
        f = os.path.join(d, 'simul000.pst.sim')
        d = DTData.DTData(f)

        c = DTField.DT2DFiniteElementField(m, d)
        p = DTAlgoPathT3()
        tini  = 1472688600.0
        dtsim = 10*3600.0
        dtdsp = 60.0
        dtclc = 15.0
        #X = (-83.131,)
        #Y = (41.988,)
        X = ( 323360.229,)
        Y = (4653642.919,)
        LOGGER.info('main: (tmin, tmax) (%.3f, %.3f)', tini, tini+dtsim)
        #print(proj2grid.TransformPoints(zip(X, Y)))
        path = p.xeq(X, Y, c, [], [tini], dtsim, dtdsp, dtclc)
        print(path)

    #formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatter = logging.Formatter('%(asctime)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    #import cProfile
    #cProfile.run('main()')
    main()
