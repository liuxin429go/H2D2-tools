#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2012-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************
"""
Unstructured triangular finite element mesh IO.
"""

import os
import sys
selfDir = os.path.dirname(os.path.abspath(__file__))
supPath = os.path.normpath(os.path.join(selfDir, '..'))
if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

from . import FEMesh
from . import FEMeshIO

import logging
import os
import cProfile as profile
import timeit

LOGGER = logging.getLogger("INRS.H2D2.Tools")

def main():
    streamHandler = logging.StreamHandler()
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    #m = FEMeshH2D2T6L('test.cor', 'test.ele')
    #for n in m.getNodes():
    #    print(n)
    #for n in range(m.getNbrNodes()):
    #    print(m.getNode(n))

    #m = FEMeshModeleurT6LIO('data/mesh.mai')
    #for n in range(m.getNbrNodes()):
    #    print(m.getNode(n))
    d = r'E:\Projets_simulation\tide_potential\2000x500'
    c = os.path.normpath( os.path.join(d, 'test.cor') )
    e = os.path.normpath( os.path.join(d, 'test.ele') )
    m = FEMesh.FEMesh( (c, e), withLocalizer=True)

#a = timeit.Timer(main).timeit(number=1)
#print (a)
profile.run("main()", sort='tottime')
