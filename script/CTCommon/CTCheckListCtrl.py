#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx
from wx.lib.mixins.listctrl import CheckListCtrlMixin

class CTCheckListCtrl(wx.ListCtrl, CheckListCtrlMixin):
    def __init__(self, parent, *args, **kwds):
        try:
            kwds['style'] += wx.LC_REPORT
        except:
            kwds['style'] = wx.LC_REPORT
        wx.ListCtrl.__init__(self, parent, *args, **kwds)
        CheckListCtrlMixin.__init__(self)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnItemActivated)
        self.Bind(wx.EVT_CHAR, self.OnCharEvent)

        hlp = """\
Special commands:
CTRL-A    Check All items
CTRL-X    Check eXclusively selected item
CTRL-Z    Un-check all items
"""
        self.SetToolTip(hlp)

    def OnItemActivated(self, evt):
        self.ToggleItem(evt.m_itemIndex)

    def OnCharEvent(self, event):
        keycode  = event.GetKeyCode()
        ctrlDown = event.CmdDown()
        altDown  = event.AltDown()
        shiftDown= event.ShiftDown()

        if shiftDown: return
        if altDown: return

        n = self.GetItemCount()
        if (ctrlDown and keycode in [1]):     # Ctrl-A
            for i in range(n):
                self.CheckItem(i, True)
        elif (ctrlDown and keycode in [24]):  # Ctrl-X
            for i in range(n):
                self.CheckItem(i, False)
            i = self.GetFirstSelected()
            while i != -1:
                self.CheckItem(i, True)
                i = self.GetNextSelected(i)
        elif (ctrlDown and keycode in [26]):  # Ctrl-Z
            for i in range(n):
                self.CheckItem(i, False)
        else:
            event.Skip()

    def GetCheckedCount(self):
        return len( self.GetChecked() )

    def GetChecked(self):
        return [i for i in range(self.GetItemCount()) if self.IsChecked(i)]

    def IsAllChecked(self):
        return self.GetCheckedCount() == self.GetItemCount()

    def IsNoneChecked(self):
        return self.GetCheckedCount() == 0


#==============================================================================
#
#==============================================================================
if __name__ == "__main__":
    import sys
    class MyDialogBox(wx.Dialog):
        def __init__(self, *args, **kwargs):
            kwargs["style"] = wx.CAPTION|wx.CLOSE_BOX|wx.MINIMIZE_BOX|wx.MAXIMIZE_BOX|wx.SYSTEM_MENU|wx.RESIZE_BORDER # |wx.CLIP_CHILDREN
            super(MyDialogBox, self).__init__(*args, **kwargs)
            self.pnl = CTCheckListCtrl(self)
            self.btn_cancel = wx.Button(self, wx.ID_CANCEL, "")
            self.btn_ok     = wx.Button(self, wx.ID_OK, "")

            szr_frm = wx.BoxSizer(wx.VERTICAL)
            szr_frm.Add(self.pnl, 1, wx.EXPAND)

            szr_btn = wx.BoxSizer(wx.HORIZONTAL)
            szr_btn.Add(self.btn_cancel,0, wx.EXPAND, 0)
            szr_btn.Add(self.btn_ok,    0, wx.EXPAND, 0)
            szr_frm.Add(szr_btn, 0, wx.EXPAND, 0)

            self.SetSizer(szr_frm)
            self.Layout()

            self.pnl.InsertColumn(0, 'Layers')
            self.pnl.DeleteAllItems()
            for item, flag in zip(['a', 'b', 'c'], [True, False, True]):
                idx = self.pnl.InsertItem(sys.maxsize, '  %s' % item)
#                self.pnl.CheckItem(idx, flag)

    class MyApp(wx.App):
        def OnInit(self):
            dlg = MyDialogBox(None)
            #if dlg.ShowModal() == wx.ID_OK:
            dlg.ShowModal()
            print(dlg.pnl.GetChecked())
            return True

    app = MyApp(False)
    app.MainLoop()

