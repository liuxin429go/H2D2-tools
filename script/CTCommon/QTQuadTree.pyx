# distutils: language = c++
## # distutils: sources = QTQuadTree_c.cpp

from cpython       cimport array
from libcpp.vector cimport vector

## cdef extern from "QTQuadTree_c.h":
##     cdef cppclass QTTree:
##         QTTree()
##         QTTree(double[4])
##         QTTree(double[4], long, long)
##         
##         void         insert     (long, double[4])
##         void         remove     (long, double[4])
##         vector[long] intersect  (double[4])
## 
##         long         size       ()
##         long         maxItemsEff()
##         long         maxDepthEff()
##         long         maxItemsLmt()
##         long         maxDepthLmt()

# Create a Cython extension type which holds a C++ instance
# as an attribute and create a bunch of forwarding methods
# Python extension type.
cdef class QTQuadTree:
    # Attributes are declared in the .pxd file
    # cdef QTTree *head

    def __cinit__(self, bbox=None, long max_items = -1, long max_depth=-1):
        cdef array.array bb4
        if bbox:
            bb4 = array.array('d', bbox)
            if max_items == -1 or max_depth == -1:
                self.head = new QTTree(bb4.data.as_doubles)
            else:
                self.head = new QTTree(bb4.data.as_doubles, max_items, max_depth)
        else:
            self.head = new QTTree()
        
    #def __reduce__(self):
    #    return self.__class__, (self.bbox(), self.maxItemsLmt(), self.maxDepthLmt())
      
    def __dealloc__(self):
        del self.head
        self.head = NULL

    def insert(self, long item, bbox):
        cdef array.array bb4 = array.array('d', bbox)
        self.head.insert(item, bb4.data.as_doubles)

    def remove(self, long item, bbox):
        cdef array.array bb4 = array.array('d', bbox)
        self.head.remove(item, bb4.data.as_doubles)

    def intersect(self, bbox):
        cdef array.array bb4 = array.array('d', bbox)
        return self.head.intersect(bb4.data.as_doubles)

    def bbox(self):
        cdef const double* bb = self.head.bbox()
        return (bb[0], bb[1], bb[2], bb[3])
        
    def size(self):
        return self.head.size()

    def maxItemsEff(self):
        return self.head.maxItemsEff()

    def maxDepthEff(self):
        return self.head.maxDepthEff()

    def maxItemsLmt(self):
        return self.head.maxItemsLmt()

    def maxDepthLmt(self):
        return self.head.maxDepthLmt()


