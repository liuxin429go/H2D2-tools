#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
IO functionalities for H2D2 data files.
"""

from __future__ import absolute_import
from __future__ import print_function

from DTDataIO import constructReaderFromFile
import os
import cProfile

import logging
LOGGER = logging.getLogger("INRS.H2D2.Tools.Data.Data.IO")
streamHandler = logging.StreamHandler()
LOGGER.addHandler(streamHandler)
LOGGER.setLevel(logging.DEBUG)

def main():
    p = r'E:/Projets_simulation/EQ/Dry-Wet/Simulations/GLOBAL_01/Simulation/global01_0036'
    f = os.path.join(p, 'simul000-2.pst.sim')
    reader = constructReaderFromFile(f, cols = (2,4))
    blocs = reader.readStructNoStats()
    #blocs = reader.readStruct()
    for b in blocs:
        print(b.file)
        print('   data', b.data)
        print('   pos ', b.pos)
        print('   nrow', b.nrow)
        print('   ncol', b.ncol)
        print('   time', b.time)
        print('   vmin', b.vmin)
        print('   vmax', b.vmax)

#import timeit
#t = timeit.timeit('main()', setup="from __main__ import main", number=1)
#print t
cProfile.run('main()')
#main()

