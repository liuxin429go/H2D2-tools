@echo off
setlocal EnableDelayedExpansion

pushd external
rmdir /Q /S -rf build
call :delete_one_ext *.pyd
call :delete_one_ext h
call :delete_one_ext hpp
call :delete_one_ext cpp
call :delete_one_ext c
call :delete_one_ext html
popd

rmdir /Q /S -rf build
call :delete_one_ext *.pyd
call :delete_one_ext h
call :delete_one_ext hpp
call :delete_one_ext cpp
call :delete_one_ext c
call :delete_one_ext html
pause
goto :EOF
 
:delete_one_ext
FOR %%f IN ("*.setup.py") DO (
    set TMP_PATH=%%f
    del !TMP_PATH:setup.py=%1!
)
exit /b
