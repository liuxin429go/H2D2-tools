#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Data path algo
"""

import enum
import math         # for sqrt in a test
import logging
import numpy as np

LOGGER = logging.getLogger("INRS.H2D2.Tools.Data.Path")

class DTPathPoint(tuple):
    """
    A point in the particle path, with time stamp, position, data
    """
    def __init__(cls, *args):
        super(DTPathPoint, cls).__init__(cls, args)

    def t(self):
        return self[0]

    def x(self):
        return self[1]
        
    def y(self):
        return self[2]

    def xy(self):
        return self[1], self[2]

class DTPath(list):
    """
    List of DTPathPoint
    """
    def __init__(self, *args, **kwargs):
        super(DTPath, self).__init__(*args, **kwargs)

class PathInfo:
    """
    Utilitary class to hold for position and display time
    """
    def __init__(self, point, tdsp):
        self.point = point  # PathPoint
        self.tdsp  = tdsp   # display time

    def t(self):
        return self.point.t()

    def x(self):
        return self.point.x()
        
    def y(self):
        return self.point.y()

    def xy(self):
        return self.point.xy()

class PosInElem:
    """
    Position in the reference element
    """
    __slots__ = 'ele', 'isid', 'k', 'e'
    def __init__(self, ele=None, isid=-1, k=-10.0, e=-10.0):
        self.ele = ele
        self.isid = isid
        self.k, self.e = k, e

class DTAlgoPath:
    ErrCodes = enum.Enum('Status', 'OK ERR_OUT_OF_ITER ERR_OUT_OF_TIME ERR_ALL_ITER_OUTSIDE ERR_POINT_OUTSIDE, ERR_END_OF_PATH, ERR_BROKEN_NEIGHBOUR_LIST')

    def __init__(self):
        self.currentPath = DTPath()

        self.field = None
        self.cols  = None
        self.tinj  = []
        self.dtdsp = 0.0
        self.dtclc = 0.0

        self.mesh = None    # short-cut

    def getNextElement(self, refp):
        raise NotImplementedError

    def addToCurrentPath(self, p, s=''):
        LOGGER.debug('DTAlgoPathT3.addToCurrentPath: %.3f (%.3f, %.3f) %s' % (p.t(), p.x(), p.y(), s))
        self.currentPath.append(p)

    def xeqOneElement(self, refp, ptcl, tfin):
        raise NotImplementedError

    def xeq1(self, x, y, tini, tfin):
        """
        Compute one particle path
        Returns a DTPath
        """
        ierr = DTAlgoPath.ErrCodes.OK

        # --- Initialize current path
        self.currentPath = DTPath()

        # --- Get start element
        self.mesh = self.field.getGrid()
        eles = self.mesh.localizePoints((x,), (y,))
        ele = eles[0]
        #if not ele: return []
        if not ele: raise ValueError

        # --- Get start position
        k, e = ele.transformeInverse(x, y)
        refp = PosInElem(ele, -1, k, e)

        # --- Initialize particle
        tdsp = 0.0
        d = self.field.getDataAtTime(tsteps=(tini,), cols=self.cols)
        d = ele.interpolateRef(k, e, d)
        pnt  = DTPathPoint(tini, x, y, *d)
        ptcl = PathInfo(pnt, tdsp)
        self.addToCurrentPath(pnt, '(%.3f,%.3f)' % (k,e))

        # --- Compute path
        ttot = tini
        while ttot < tfin and ierr == DTAlgoPath.ErrCodes.OK:
            LOGGER.debug('DTAlgoPath.xeq1: enter element %i side %s t=%.3f xy=(%.3f,%.3f) ke=(%.3f,%.3f)' % (refp.ele.ie, refp.isid, ptcl.t(), ptcl.x(), ptcl.y(), refp.k, refp.e))
            ierr, refp, ptcl = self.xeqOneElement(refp, ptcl, tfin)
            LOGGER.debug('DTAlgoPath.xeq1: leave element %i side %s t=%.3f xy=(%.3f,%.3f) ke=(%.3f,%.3f)' % (refp.ele.ie, refp.isid, ptcl.t(), ptcl.x(), ptcl.y(), refp.k, refp.e))
            if  ierr == DTAlgoPath.ErrCodes.OK:
                ierr, refp = self.getNextElement(refp)
            elif ierr == DTAlgoPath.ErrCodes.ERR_POINT_OUTSIDE:
                #ele = self.mesh.localizePoints( (x,), (y,) )[0]
                #if ele
                LOGGER.info('DTAlgoPath.xeq1: No continuation found')
            if self.prgrs: self.prgrs[3] = (ptcl.t()-tini, tfin-tini)

        # --- Log exit code
        if ierr == DTAlgoPath.ErrCodes.OK:
            pass
        elif ierr == DTAlgoPath.ErrCodes.ERR_OUT_OF_ITER:
            LOGGER.info('DTAlgoPath.xeq1: Out of iteration: probably very slow velocities')
        elif ierr == DTAlgoPath.ErrCodes.ERR_OUT_OF_TIME:
            LOGGER.info('DTAlgoPath.xeq1: Path OK')
            if ptcl.tdsp > 0.0: self.addToCurrentPath(ptcl.point)
        elif ierr == DTAlgoPath.ErrCodes.ERR_ALL_ITER_OUTSIDE:
            LOGGER.info('DTAlgoPath.xeq1: No steps inside element')
        elif ierr == DTAlgoPath.ErrCodes.ERR_POINT_OUTSIDE:
            LOGGER.info('DTAlgoPath.xeq1: Algo locked: no start element found')
        elif ierr == DTAlgoPath.ErrCodes.ERR_END_OF_PATH:
            LOGGER.info('DTAlgoPath.xeq1: End of path: probably boundary reached')
        elif ierr == DTAlgoPath.ErrCodes.ERR_BROKEN_NEIGHBOUR_LIST:
            LOGGER.error('DTAlgoPath.xeq1: Broken neighbour list (should not happen!)')

        p = self.currentPath
        self.currentPath = DTPath()
        return p

    def xeq(self, X, Y, field, cols, tinj, dtsim, dtdsp, dtclc, prgrs=None):
        """
        Compute all the particle paths corresponding to the 
        X, Y injection points.
        Returns a list of DTPath
        """
        self.field = field
        self.cols = cols
        self.tinj = tinj   # list of injection times
        self.dtdsp = dtdsp
        self.dtclc = dtclc
        self.prgrs = prgrs

        pathes = []
        nxy = min(len(X), len(Y))
        ntm = len(tinj)
        ixy = 0
        for x, y, in zip(X,Y):
            ixy += 1
            if self.prgrs: self.prgrs[1] = (ixy, nxy)
            itm = 0
            for tinj_ini in tinj:
                itm += 1
                tinj_fin = tinj_ini + dtsim
                if self.prgrs: self.prgrs[2] = (itm, ntm)
                if self.prgrs: self.prgrs[3] = (0.0, tinj_fin-tinj_ini)
                pth = self.xeq1(x, y, tinj_ini, tinj_fin)
                pathes.append(pth)
        if self.prgrs: self.prgrs[0] = pathes

        return pathes

# Should be @staticmethod, but static methods are not callable
def intersect_side0(k, e, dk, de):
    # intersection with side 0
    ret = False, 0, 0, 0
    #print('intersect_side0: k,e=(%.6e, %.6e); dk,de=(%.6e, %.6e)' % (k, e, dk, de))
    isValid = abs(de) > 1.0e-18
    if not isValid: return ret

    dki = - e * (dk / de)   # dki in [0, dk]
    isValid = (dki >= 0.0 and (0.0 <= dki <= dk)) or (dki < 0.0 and (dk <= dki <= 0.0))
    if not isValid: return ret

    ki = k + dki        # ki in [0, 1]
    isValid = 0.0 <= ki <= 1.0
    if not isValid: return ret

    dei = -e            # dei in [0, e]
    isValid = (dei >= 0.0 and (0.0 <= dei <= de)) or (dei < 0.0 and (de <= dei <= 0.0))
    if not isValid: return ret

    ei = 0.0            # ei in [0, 1]

    ds = math.hypot(dki,dei) / math.hypot(dk,de)
    doIntersect = True
    #print('intersect_side0: intersection at k,e=(%.6e, %.6e)' % (ki, ei), ds)
    return doIntersect, ki, ei, ds

def intersect_side1(k, e, dk, de):
    # intersection with side 1
    ret = False, 0, 0, 0
    #print('intersect_side1: k,e=(%.6e, %.6e); dk,de=(%.6e, %.6e)' % (k, e, dk, de))
    isValid = abs(dk + de) > 1.0e-18
    if not isValid: return ret

    if de == 0.0:
        ki = 1.0 - e        # ki in [0, 1]
    elif dk == 0.0:
        ki = k              # ki in [0, 1]
    else:
        ki = (dk + k * de - e * dk) / (dk + de)    # ki in [0, 1]
    isValid = 0.0 <= ki <= 1.0
    if not isValid: return ret

    dki = ki - k        # dki in [0, dk]
    isValid = (dki >= 0.0 and (0.0 <= dki <= dk)) or (dki <= 0.0 and (dk <= dki <= 0.0))
    if not isValid: return ret

    ei = 1.0 - ki       # ei in [0, 1]
    isValid = 0.0 <= ei <= 1.0
    if not isValid: return ret

    dei = ei - e        # dei in [0, e]
    isValid = (dei >= 0.0 and (0.0 <= dei <= de)) or (dei < 0.0 and (de <= dei <= 0.0))
    if not isValid: return ret

    ds = math.hypot(dki,dei) / math.hypot(dk,de)
    doIntersect = True
    #print('intersect_side1: intersection at k,e=(%.6e, %.6e)' % (ki, ei), ds)
    return doIntersect, ki, ei, ds

def intersect_side2(k, e, dk, de):
    # intersection with side 2
    ret = False, 0, 0, 0
    #print('intersect_side2: k,e=(%.6e, %.6e); dk,de=(%.6e, %.6e)' % (k, e, dk, de))
    isValid = abs(dk) > 1.0e-18
    if not isValid: return ret

    dei = - k * (de / dk)   # dei in [0, de]
    isValid = (dei >= 0.0 and (0.0 <= dei <= de)) or (dei < 0.0 and (de <= dei <= 0.0))
    if not isValid: return ret

    ei = e + dei        # ee in [0, 1]
    isValid = 0.0 <= ei <= 1.0
    if not isValid: return ret

    dki = -k            # dki in [0, k]
    isValid = (dki >= 0.0 and (0.0 <= dki <= dk)) or (dki < 0.0 and (dk <= dki <= 0.0))
    if not isValid: return ret

    ki = 0.0            # ki in [0, 1]

    ds = math.hypot(dki,dei) / math.hypot(dk,de)
    doIntersect = True
    #print('intersect_side2: intersection at k,e=(%.6e, %.6e)' % (ki, ei), ds)
    return doIntersect, ki, ei, ds

NPAS_MAX = 100000
intersect_functions = (intersect_side1, intersect_side2, intersect_side0, intersect_side1, intersect_side2)

class DTAlgoPathT3(DTAlgoPath):
    def __init__(self):
        super(DTAlgoPathT3, self).__init__()

    def getNextElement(self, refp):
        """
        """
        ierr = DTAlgoPath.ErrCodes.OK

        el1, is1 = refp.ele, refp.isid
        ie2 = self.mesh.getElementNeighbour(el1.ie, is1)
        is2, k2, e2 = -1, -10.0, -10.0  # Valeurs par défaut de PosInElem
        if ie2 < 0:
            el2 = None
            ierr = DTAlgoPath.ErrCodes.ERR_END_OF_PATH
        else:
            el2 = self.mesh.getElement(ie2)
            if   self.mesh.getElementNeighbour(el2.ie, 0) == el1.ie:
                is2 = 0
                if   is1 == 0:
                    k2, e2 = (1.0 - refp.k), 0.0
                elif is1 == 1:
                    k2, e2 = refp.k, 0.0
                elif is1 == 2:
                    k2, e2 = refp.e, 0.0
            elif self.mesh.getElementNeighbour(el2.ie, 1) == el1.ie:
                is2 = 1
                if   is1 == 0:
                    k2, e2 = refp.k, (1.0 - refp.k)
                elif is1 == 1:
                    k2, e2 = refp.e, refp.k
                elif is1 == 2:
                    k2, e2 = (1 - 0 - refp.e), refp.e
            elif self.mesh.getElementNeighbour(el2.ie, 2) == el1.ie:
                is2 = 2
                if   is1 == 0:
                    k2, e2 = 0.0, refp.k
                elif is1 == 1:
                    k2, e2 = 0.0, refp.e
                elif is1 == 2:
                    k2, e2 = 0.0, (1.0 - refp.e)
            else:
                is2 = -1
                ierr = DTAlgoPath.ErrCodes.ERR_BROKEN_NEIGHBOUR_LIST
        #================================
        # Debug code
        #================================
        if ierr == DTAlgoPath.ErrCodes.OK:
            x1, y1 = el1.transforme(refp.k, refp.e)
            x2, y2 = el2.transforme(k2, e2)
            d = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            if d > 1.0e-8:
                print(is1, is2)
                print((x1, y1), (x2, y2), d)
                print((refp.k, refp.e), (k2, e2))
                print((k2, e2), el2.transformeInverse(x1, y1))
                raise ValueError
        #================================
        # End debug code
        #================================
        return ierr, PosInElem(el2, is2, k2, e2)

    def xeqOneElement(self, refp, ptcl, tfin):

        ierr = DTAlgoPath.ErrCodes.OK

        # --- Initialize local variables
        x, y = ptcl.xy()
        t, tdsp = ptcl.t(), ptcl.tdsp

        # --- Entry point in ksi,eta coord
        el0 = refp.ele
        is0 = refp.isid
        vksi_0, veta_0 = refp.k, refp.e
        LOGGER.debug('DTAlgoPathT3.xeqOneElement: (%.3f,%.3f) in element %7i @ (%.3e,%.3e)' % (x, y, el0.ie, vksi_0, veta_0))
        vsum_0 = 1.0 - vksi_0 - veta_0
        elmin = min(vsum_0, vksi_0, veta_0)
        if elmin < -1.0e-6:
            ierr = DTAlgoPathT3.ErrCodes.ERR_POINT_OUTSIDE
            return ierr, refp, ptcl

        # --- Derivatives
        data = self.field.getDataAtTime(tsteps=(t,), cols=self.cols)
        dudx = el0.ddx(x, y, data)    # u,x & v,x
        dudy = el0.ddy(x, y, data)    # u,y & v,y
        dataOK = True

        # --- Loop on time steps
        vksi_1, veta_1 = vksi_0, veta_0
        dt = min(self.dtclc, tfin - t)
        nout = 0
        for it in range(DTAlgoPathT3.NPAS_MAX):     # Protection again small steps
            dt = min(dt, tfin - t)
            if not dataOK:
                data = self.field.getDataAtTime(tsteps=(t,), cols=self.cols)
                dataOK = True
            # --- Get velocity
            u = el0.interpolateRef(vksi_1, veta_1, data)
            # --- IF |V| TOO SMALL RETURN
            #vabs = u[0]**2 + u[1]**2
            #if vabs < 1.0e-6:
            #    vksi = vksi_1
            #    veta = veta_1
            #    ierr = 1
            #    goto 300
            # --- Convection
            cnvx = u[0] * dudx[0] + u[1] * dudy[0]
            cnvy = u[0] * dudx[1] + u[1] * dudy[1]
            # --- Delta - Taylor 2nd order
            dx = (u[0] + 0.5 * cnvx * dt) * dt
            dy = (u[1] + 0.5 * cnvy * dt) * dt
            dksi, deta = np.dot(el0.jacobian(), (dx,dy))
            vksi_2 = vksi_1 + dksi
            veta_2 = veta_1 + deta
            vsum_2 = 1.0 - vksi_2 - veta_2
            elmin = min(vsum_2, vksi_2, veta_2)

            # --- Already outside ?  break out of loop
            if elmin < 0.0:
                if (nout == 5): break
                nout += 1
                dt   *= 0.5
                continue

            # --- Update position
            dataOK = False
            t += dt
            tdsp += dt
            vksi_1 = vksi_2
            veta_1 = veta_2
            # --- New point
            if tdsp >= self.dtdsp:
                tdsp = 0.0
                x, y = el0.transforme(vksi_1, veta_1)
                data = self.field.getDataAtTime(tsteps=(t,), cols=self.cols)
                u = el0.interpolateRef(vksi_1, veta_1, data)
                self.addToCurrentPath(DTPathPoint(t, x, y, *u), '(%.3f,%.3f)' % (vksi_1, veta_1))
                dataOK = True
            # --- Check for time elapsed condition
            if t >= tfin: break

        # --- Error codes
        doClcIntersect = True
        if t >= tfin:
            vksi = vksi_1
            veta = veta_1
            ierr = DTAlgoPathT3.ErrCodes.ERR_OUT_OF_TIME
            doClcIntersect = False
        elif it >= DTAlgoPathT3.NPAS_MAX:
            vksi = vksi_1
            veta = veta_1
            ierr = DTAlgoPathT3.ErrCodes.ERR_OUT_OF_ITER        # Out of iter
            doClcIntersect = False
        elif it == nout:
            ierr = DTAlgoPathT3.ErrCodes.ERR_ALL_ITER_OUTSIDE   # All iter are outside

        # --- Intersection with side of element
        isid = -1
        if doClcIntersect:
            doIntersect = False
            iss = is0 if is0 != -1 else 0
            for f in DTAlgoPathT3.intersect_functions[iss:iss + 3]:
                if not doIntersect:
                    doIntersect, vksi_2, veta_2, ds = f(vksi_1, veta_1, dksi, deta)
            if not doIntersect:                                 # Fall-back Mikado
                vksi_2 = min(max(vksi_2, 0.0), 1.0)
                veta_2 = min(max(veta_2, 0.0), 1.0)
                vsum_2 = 1.0 - vksi_2 - veta_2
                if vsum_2 < 0.0:
                    vksi_2 += 0.5 * vsum_2
                    veta_2 += 0.5 * vsum_2
                LOGGER.info('DTAlgoPathT3.xeqOneElement: No intersection with sides found, use fall-back')
            # --- Update position
            dt *= ds
            t += dt
            tdsp += dt
            vksi = vksi_2
            veta = veta_2
            vsum = 1.0 - vksi - veta
            vmin = min(vsum, vksi, veta)
            if vsum == vmin:
                isid = 1
            elif vksi == vmin:
                isid = 2
            else:
                isid = 0

        # --- Check for same out-segment as in-segment
        if ierr == DTAlgoPathT3.ErrCodes.ERR_ALL_ITER_OUTSIDE:
            if isid != is0:
                ierr = DTAlgoPathT3.ErrCodes.OK
            else:
                if isid == 1:
                    if vksi > veta:
                        isid = 0
                    else:
                        isid = 2
                elif isid == 2:
                    if veta > 0.5:
                        isid = 1
                    else:
                        isid = 0
                else:
                    if vksi > 0.5:
                        isid = 1
                    else:
                        isid = 2

        # --- Update end point
        if ierr != DTAlgoPathT3.ErrCodes.ERR_ALL_ITER_OUTSIDE:
            x, y = el0.transforme(vksi, veta)
            u = el0.interpolateRef(vksi, veta, data)

        #print('t=%.6f x,y=(%.6e,%.6e) dx=%.6e dt=%.6e' % (t, x, y, x - ptcl.x, t - ptcl.t)
        return (ierr, 
                PosInElem(refp.ele, isid, vksi, veta), 
                PathInfo(DTPathPoint(t, x, y, *u), tdsp))


if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

    from . import FEMesh
    from . import FEMeshIO
    from . import DTData
    from . import DTDataIO
    from . import DTField
    #from IPImageProcessor import IPRaster
    def main():
        d = r'E:\Projets_simulation\LacErie\1x'
        #srs_proj = IPRaster.IPSpatialReference()
        #srs_grid = IPRaster.IPSpatialReference()
        #srs_proj.ImportFromFileAndGuess(os.path.join(d, 'erie_nad83.prj'))
        #srs_grid.ImportFromFileAndGuess(os.path.join(d, 'erie_utm.prj'))
        #grid2proj = IPRaster.IPCoordinateTransformation(srs_grid, srs_proj)
        #proj2grid = IPRaster.IPCoordinateTransformation(srs_proj, srs_grid)
        
        fpath = [os.path.join(d, 'erie_t6l.cor'), os.path.join(d, 'erie_t6l.ele')]
        #grid2proj = None
        m = FEMesh.FEMesh(fpath, withLocalizer=True)
        #m = FEMesh.FEMesh(fpath, projection=grid2proj, withLocalizer=True)

        d = r'E:\Projets_simulation\LacErie\1x\rdps\cw6'
        f = os.path.join(d, 'simul000.pst.sim')
        d = DTData.DTData(f)

        c = DTField.DT2DFiniteElementField(m, d)
        p = DTAlgoPathT3()
        tini  = 1472688600.0
        dtsim = 10*3600.0
        dtdsp = 60.0
        dtclc = 15.0
        #X = (-83.131,)
        #Y = (41.988,)
        X = ( 323360.229,)
        Y = (4653642.919,)
        #print(proj2grid.TransformPoints(zip(X, Y)))
        path = p.xeq(X, Y, c, [], [tini], dtsim, dtdsp, dtclc)
        #print(path)

    #formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatter = logging.Formatter('%(asctime)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.INFO)

    import cProfile
    cProfile.run('main()')
    #main()
