#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import enum

REDUCTION_OPERATION = enum.Enum('REDUCTION_OPERATION',
    ('op_noop',
     'op_min', 'op_amin', 'op_max', 'op_amax', 'op_sum', 'op_mean',
     'op_delta_min', 'op_delta_amin', 'op_delta_max', 'op_delta_amax', 'op_delta_sum', 'op_delta_mean',
    ) )
