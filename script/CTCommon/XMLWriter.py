#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import codecs
import collections.abc
import sys
from xml.sax.saxutils import quoteattr

class XMLWriter:
    def __init__(self, fname = None):
        self.indent = 0
        self.tabsiz = 3
        self.elemstk = []

        if (fname):
            self.fout = codecs.open(fname, 'w', 'utf8')
        else:
            self.fout = sys.stdout

    def __del__(self):
        if self.fout:
            self.close()

    def close(self):
        if self.fout and self.fout != sys.stdout:
            self.fout.close()
        self.fout = None

    def __writeElem(self, elem, attrs, close=True):
        closing = '/>' if close else '>'
        if attrs:
            if isinstance(attrs, str):
                txt = [attrs]
            elif isinstance(attrs, dict):
                txt = [ '%s=%s' % (k, quoteattr(v)) for k, v in attrs.items() ]
            elif isinstance(attrs, collections.abc.Iterable):
                txt = attrs
            else:
                txt = '%s' % attrs
            if len(txt) <= 1:
                self.writeLine('<%s %s%s' % (elem, txt[0], closing))
            else:
                self.writeLine('<%s' % elem)
                self.indent += 1
                for l in txt[:-1]:
                    self.writeLine('%s' % l)
                self.writeLine('%s%s' % (txt[-1], closing))
                self.indent -= 1
        else:
            self.writeLine('<%s%s' % (elem, closing))

    def openElem(self, elem, attrs = None):
        self.elemstk.append(elem)
        self.__writeElem(elem, attrs, close=False)
        self.indent += 1

    def closeElem(self):
        self.indent -= 1
        elem = self.elemstk.pop()
        self.writeLine('</%s>' % elem)

    def writeElem(self, elem, attrs = None):
        self.__writeElem(elem, attrs, close=True)

    def writeLine(self, l):
        if (self.indent > 0):
            self.fout.write( '%s%s\n' % (' '*self.indent*self.tabsiz, l) )
        else:
            self.fout.write( '%s\n' % (l) )

    def writeTag(self, tag, txt):
        self.writeLine('<%s>%s</%s>' % (tag, txt, tag.split(' ')[0]))

    def writeHeader(self, copyright = ''):
        self.writeLine('<?xml version="1.0" encoding="UTF-8"?>')
        if copyright: self.writeLine(copyright)

    def writeFooter(self, closeAll=False):
        if (closeAll):
            try:
                while True: self.closeElem()
            except:
                pass
        else:
            self.closeElem()      # kml

