
#include "QTQuadTree_c.h"

QTTree::QTTree()
   : m_headP(NULL)
{
}

QTTree::QTTree(double bbox[4])
   : m_bbox({bbox[0], bbox[1], bbox[2], bbox[3]})
   , m_headP(NULL)
{
   double width  = bbox[2] - bbox[0];
   double height = bbox[3] - bbox[1];
   double midx = (bbox[2] + bbox[0]) / 2.0;
   double midy = (bbox[3] + bbox[1]) / 2.0;
   long depth = 0;
   m_headP = new QTNode(midx, midy, width, height, MAX_ITEMS, MAX_DEPTH, depth);
}

QTTree::QTTree(double bbox[4], long max_items, long max_depth)
   : m_bbox({bbox[0], bbox[1], bbox[2], bbox[3]})
   , m_headP(NULL)
{
   double width  = bbox[2] - bbox[0];
   double height = bbox[3] - bbox[1];
   double midx = (bbox[2] + bbox[0]) / 2.0;
   double midy = (bbox[3] + bbox[1]) / 2.0;
   long depth = 0;
   m_headP = new QTNode(midx, midy, width, height, max_items, max_depth, depth);
}

void QTTree::insert(TTInfo item, double bbox[4])
{
   TTBBox bb = { bbox[0], bbox[1], bbox[2], bbox[3] };
   this->m_headP->insert( QTItem(item, bb) );
}

void QTTree::remove(TTInfo item, double bbox[4])
{
   TTBBox bb = { bbox[0], bbox[1], bbox[2], bbox[3] };
   this->m_headP->insert(QTItem(item, bb));
}

#include <iostream>
TTResults QTTree::intersect(double bbox[4]) const
{
   TTBBox bb = { bbox[0], bbox[1], bbox[2], bbox[3] };
   TTResults results;
   TTUniquer uniq;
   this->m_headP->intersect(bb, results, uniq);
   // std::cout << "Intersect: " << results.size() << std::endl;
   return results;
}

const double* QTTree::bbox() const
{
   return this->m_bbox.data();
}

size_t QTTree::size() const
{
   return this->m_headP ? this->m_headP->size() : 0;
}

size_t QTTree::maxItemsEff() const
{
   return this->m_headP ? this->m_headP->maxItemsEff() : 0;
}

size_t QTTree::maxDepthEff() const
{
   return this->m_headP ? this->m_headP->maxDepthEff() : 0;
}

size_t QTTree::maxItemsLmt() const
{
   return this->m_headP ? this->m_headP->maxItemsLmt() : 0;
}

size_t QTTree::maxDepthLmt() const
{
   return this->m_headP ? this->m_headP->maxDepthLmt() : 0;
}

