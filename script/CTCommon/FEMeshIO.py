#!/usr/bin/env python
# -*- coding: utf-8 -*-
## cython: profile=True
## cython: linetrace=True
#************************************************************************
# --- Copyright (c) INRS 2012-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************
"""
Unstructured triangular finite element mesh IO.
"""

import logging
import numpy as np

from .CTException import CTException
#from .external.profile_each_line import profile_each_line

LOGGER = logging.getLogger("INRS.H2D2.Tools.Mesh.IO")

RBUF_DEF = -1
RBUF_BIG = 1 << 20

def isBinary(fname):
    """Check for binary file.
    Return True if the file is in a binary format."""
    fi = open(fname, 'rb')
    line = fi.read(256)
    fi.close()
    is_bin = False
    for c in line:
        if c not in b'0123456789.-+eE \t\r\n':
            is_bin = True
            break
    return is_bin

class FEMeshIO:
    """IO class, reader-writer for FE meshes"""
    def __init__(self, files):
        if isinstance(files, (list, tuple)):
            self.files = [ f for f in files ]
        else:
            self.files = [files]
        self.nbDim = 0
        self.nbNod = 0
        self.nbEle = 0

    def skipRecords(self, fi, ncol, n=1):
        for _i in range(n):
            if not fi.readline(): raise EOFError

    def isValid(self):
        raise NotImplementedError

    def readDim(self):
        raise NotImplementedError

    def readMesh(self, mesh):
        raise NotImplementedError

    def writeMesh(self, mesh):
        raise NotImplementedError


class FEMeshModeleurIO(FEMeshIO):
    def __init__(self, files):
        if self.__class__.__name__ == FEMeshModeleurIO.__name__:
            msg = 'Trying to instantiate an abstract class. Use a specialized class like FEMeshModeleurT3 instead'
            raise CTException(msg)
        FEMeshIO.__init__(self, files)
        if len(self.files) != 1: raise ValueError("Expected 1 file, got %i instead" % len(self.files))
        self.nSkip = -1

    def __readHeader(self, fi):
        line = fi.readline().strip()
        if line.split('=')[1] == 'cartographique':
            line = fi.readline()
            line = fi.readline()
            self.nSkip = 3
        elif line.split('=')[1] == 'geographique':
            line = fi.readline()
            line = fi.readline()
            self.nSkip = 3
        elif line.split('=')[1] == 'cartesienne':
            line = fi.readline().strip()
            if int(line.split('=')[1]) == 1:
                line = fi.readline()
                line = fi.readline()
                self.nSkip = 4
            else:
                self.nSkip = 2
        else:
            raise CTException('Error reading Modeleur projection')
        line = fi.readline()
        nbNod, nbEle, nbDim = line.split()
        return (int(nbDim), int(nbNod), int(nbEle))

    def readCoor(self, mesh):
        XY = np.genfromtxt(self.files[0],
                           skip_header=self.nSkip+1,  # saute headerRows+1
                           skip_footer=self.nbEle+7,  # nbTriang+entête triangle + 6
                           usecols=(2,3))             # 3 et 4e colonne
        assert len(XY) == self.nbNod
        X, Y = np.hsplit(XY, [1])
        X = np.reshape(X, -1)
        Y = np.reshape(Y, -1)
        mesh.setCoord(X, Y)

    def readElem(self, mesh):
        raise NotImplementedError

    def isValidWithDimCheck(self, indTyp):
        fi = None
        r  = True
        try:
            fi = open(self.files[0], 'r')
            ndim, nnt, nelt = self.__readHeader(fi)
            if ndim != 2: r = False
            if nnt  <= 0: r = False
            if nelt <= 0: r = False
            if r:
                self.skipRecords(fi, self.nSkip+1+self.nbNod+3+1)
                line = fi.readline()
                nel = line.split()
                if int(nel[indTyp]) != nelt: r = False
        except:
            r = False
        if fi: fi.close()
        return r

    def readDim(self):
        fi = open(self.files[0], 'r')
        nbDim, nbNod, nbEle = self.__readHeader(fi)
        fi.close()
        self.nbDim = nbDim
        self.nbNod = nbNod
        self.nbEle = nbEle
        return nbDim, nbNod, nbEle

    def readMesh(self, mesh):
        _nbDim, _nbNod, _nbEle = self.readDim()
        self.readCoor(mesh)
        self.readElem(mesh)

class FEMeshModeleurT3IO(FEMeshModeleurIO):
    def __init__(self, files):
        FEMeshModeleurIO.__init__(self, files)

    def isValid(self):
        return FEMeshModeleurIO.isValidWithDimCheck(self, 3)

    def readElem(self, mesh):
        connec = np.genfromtxt(self.files[0],
                               dtype='int',
                               skip_header=self.nSkip+1+self.nbNod+3+1,
                               skip_footer=3)
        tri = connec[:,(1,2,3)]
        assert len(tri) == self.nbEle
        tri -= 1
        mesh.setConnec(mesh)

class FEMeshModeleurT6LIO(FEMeshModeleurIO):
    def __init__(self, files):
        FEMeshModeleurIO.__init__(self, files)

    def isValid(self):
        return FEMeshModeleurIO.isValidWithDimCheck(self, 4)

    def readElem(self, mesh):
        connec = np.genfromtxt(self.files[0],
                               dtype='int',
                               skip_header=self.nSkip+1+self.nbNod+4+1,
                               skip_footer=2)
        ele1 = connec[:,(1,2,6)]
        ele2 = connec[:,(2,3,4)]
        ele3 = connec[:,(6,4,5)]
        ele4 = connec[:,(4,6,2)]
        tri = np.concatenate( (ele1, ele2, ele3, ele4) )
        assert len(tri) == 4*self.nbEle
        tri -= 1
        mesh.setConnec(mesh)

class FEMeshModeleurT6LasT3IO(FEMeshModeleurIO):
    def __init__(self, files):
        FEMeshModeleurIO.__init__(self, files)

    def isValid(self):
        return FEMeshModeleurIO.isValidWithDimCheck(self, 4)

    def readElem(self, mesh):
        connec = np.genfromtxt(self.files[0],
                               dtype='int',
                               skip_header=self.nSkip+1+self.nbNod+4+1,
                               skip_footer=2)
        tri = connec[:,(1,3,5)]
        assert len(tri) == self.nbEle
        tri -= 1
        mesh.setConnec(mesh)


class FEMeshH2D2IO(FEMeshIO):
    def __init__(self, files):
        if self.__class__.__name__ == FEMeshH2D2IO.__name__:
            msg = 'Trying to instantiate an abstract class. Use a specialized class like FEMeshH2D2TT3 instead'
            raise CTException(msg)
        FEMeshIO.__init__(self, files)
        if (len(self.files) not in [1, 2]): raise ValueError("Expected 1 or 2 files, got %i instead" % len(self.files))

    def getType(self):
        raise NotImplementedError

    def readHeader(self, fc, fe):
        """
        Read the header of both files returning the dimensions
        This is the generic version and can be inherited to specialize.
        """
        line = fc.readline()
        nbNod, nbDim, _time = line.split()
        if fe:
            line = fe.readline()
            nbEle, _nbNel, eltp, _time = line.split()
            if eltp != self.getType():
                raise CTException('Invalid element type: expected=%s, read=%s' % (self.getType(), eltp))
        else:
            nbEle = '0'
        return (int(nbDim), int(nbNod), int(nbEle))

    #@profile_each_line
    def readCoor(self, mesh):
        fi = open(self.files[0], 'rb', RBUF_BIG)
        nbNod = int(next(fi).split(None, 1)[0])
        X = np.zeros(nbNod)
        Y = np.zeros(nbNod)
        for i in range(nbNod):
            x, y = next(fi).split(None, 1)
            X[i] = float(x)
            Y[i] = float(y)
        fi.close()
        mesh.setCoord(X, Y)

    def readElem(self, mesh):
        raise NotImplementedError

    def isValid(self):
        fc = None
        fe = None
        r  = True
        try:
            fc = open(self.files[0], 'r')
            try:
                fe = open(self.files[1], 'r')
            except:
                pass
            ndim, nnt, nelt = self.readHeader(fc, fe)
            if ndim != 2: r = False
            if nnt  <= 0: r = False
            if nelt <  0: r = False
        except:
            r = False
        if fc: fc.close()
        if fe: fe.close()
        return r

    def readDim(self):
        fc = open(self.files[0], 'r')
        try:
            fe = open(self.files[1], 'r')
        except:
            fe = None
        nbDim, nbNod, nbEle = self.readHeader(fc, fe)
        if fc: fc.close()
        if fe: fe.close()
        self.nbDim = nbDim
        self.nbNod = nbNod
        self.nbEle = nbEle
        return nbDim, nbNod, nbEle

    def readMesh(self, mesh):
        _nbDim, _nbNod, _nbEle = self.readDim()
        self.readCoor(mesh)
        if len(self.files) == 2:
            self.readElem(mesh)

    def writeCoor(self, mesh):
        fo = open(self.files[0], 'wt')
        nbNod = mesh.getNbNodes()
        nbDim = 2
        time = 0.0
        fo.write(' %i %i %17.9e\n' % (nbNod, nbDim, time))
        for nod in mesh.getNodes():
            x, y = nod.getCoordinates()
            fo.write('%25.17e %25.17e\n' % (x,y))
        fo.close()

    def writeElem(self, mesh):
        raise NotImplementedError

    def writeMesh(self, mesh):
        self.writeCoor(mesh)
        if len(self.files) == 2:
            self.writeElem(mesh)

class FEMeshH2D2L2IO(FEMeshH2D2IO):
    def __init__(self, files):
        FEMeshH2D2IO.__init__(self, files)

    def getType(self):
        return '101'

    def readElem(self, mesh):
        fi = open(self.files[1], 'rb', RBUF_BIG)
        nbEle = int( next(fi).split()[0] )
        tri = np.zeros( (nbEle, 2), np.int)
        for ie in range(nbEle):
            s1, s2 = next(fi).split()[0:2]
            tri[ie] = (int(s1), int(s2))
        tri -= 1
        fi.close()
        mesh.setConnec(tri)

    def writeElem(self, mesh):
        fo = open(self.files[1], 'wt')
        nbEle = mesh.getNbElements()
        nbNel = 2
        eltp = self.getType()
        time = 0.0
        fo.write(' %i %i %s %17.9e\n' % (nbEle, nbNel, eltp, time))
        for ele in mesh.getElements():
            n1, n2 = ele.getConnectivities()
            fo.write(' %i %i\n' % (n1+1, n2+1))
        fo.close()

class FEMeshH2D2T3IO(FEMeshH2D2IO):
    def __init__(self, files):
        FEMeshH2D2IO.__init__(self, files)

    def getType(self):
        return '201'

    #@profile_each_line
    def readElem(self, mesh):
        fi = open(self.files[1], 'rb', RBUF_BIG)
        nbEle = int( next(fi).split()[0] )
        tri = np.zeros( (nbEle, 3), np.int)
        for ie in range(nbEle):
            connec = [ int(i) for i in next(fi).split() ]
            tri[ie] = (connec[0], connec[1], connec[2])
        tri -= 1
        fi.close()
        mesh.setConnec(tri)

    def writeElem(self, mesh):
        fo = open(self.files[1], 'wt')
        nbEle = mesh.getNbElements()
        nbNel = 3
        eltp = self.getType()
        time = 0.0
        fo.write(' %i %i %s %17.9e\n' % (nbEle, nbNel, eltp, time))
        for ele in mesh.getElements():
            n1, n2, n3 = ele.getConnectivities()
            fo.write(' %i %i %i\n' % (n1+1, n2+1, n3+1))
        fo.close()

class FEMeshH2D2T6LIO(FEMeshH2D2IO):
    def __init__(self, files):
        FEMeshH2D2IO.__init__(self, files)

    def getType(self):
        return '203'

    #@profile_each_line
    def readElem(self, mesh):
        fi = open(self.files[1], 'rb', RBUF_BIG)
        nbEle = int(next(fi).split(None, 1)[0])
        tri = np.zeros( (nbEle, 6), np.int)
        ie = 0
        for _il in range(nbEle):
            s1, s2, s3, s4, s5, s6 = next(fi).split(None, 6)
            n1, n2, n3, n4, n5, n6 = int(s1), int(s2), int(s3), int(s4), int(s5), int(s6)
            tri[ie] = (n1, n2, n3, n4, n5, n6)
            ie += 1
        tri -= 1
        fi.close()
        mesh.setConnec(tri)

    def writeElem(self, mesh):
        fo = open(self.files[1], 'wt')
        nbEle = mesh.getNbElements()
        nbNel = 6
        eltp = self.getType()
        time = 0.0
        fo.write(' %i %i %s %17.9e\n' % (nbEle, nbNel, eltp, time))
        for ele in mesh.getElements():
            n1, n2, n3, n4, n5, n6 = ele.getConnectivities()
            fo.write(' %i %i %i %i %i %i\n' % (n1+1, n2+1, n3+1, n4+1, n5+1, n6+1))
        fo.close()

class FEMeshH2D2T6LasT3IO(FEMeshH2D2IO):
    def __init__(self, files):
        FEMeshH2D2IO.__init__(self, files)

    def getType(self):
        return '203'

    #@profile_each_line
    def readElem(self, mesh):
        fi = open(self.files[1], 'rb', RBUF_BIG)
        nbEle = int( next(fi).split()[0] )
        tri = np.zeros( (nbEle, 3), np.int)
        for ie in range(nbEle):
            s1, _, s3, _, s5, _ = next(fi).split(None, 6)
            n1, n3, n5 = int(s1), int(s3), int(s5)
            tri[ie] = (n1, n3, n5)
        tri -= 1
        fi.close()
        mesh.setConnec(tri)

def constructReaderFromFile_isValid(r, m):
    if r.isValid():
        LOGGER.debug(m)
        return r
    return None

def constructReaderFromFile(fnames_):
    isLst = isinstance(fnames_, (list, tuple))
    fnames = [f for f in fnames_] if isLst else [fnames_]

    nBin = sum( [1 if isBinary(f) else 0 for f in fnames] )
    if nBin != 0 and nBin != len(fnames):
        raise CTException('Unsupported mode: mixed binary and ASCII file')

    LOGGER.debug('Construct reader for : %s', repr(fnames))
    if nBin == len(fnames):
        LOGGER.debug('   detected as binary file')
        raise CTException('Binary file are not (yet) supported: %s' % repr(fnames))
    else:
        LOGGER.debug('   detected as ASCII file')
        readers = [ (FEMeshModeleurT3IO,     '   detected as Modeleur T3'),
                    (FEMeshModeleurT6LIO,    '   detected as Modeleur T6L'),
                    (FEMeshModeleurT6LasT3IO,'   detected as Modeleur T6L read as T3'),
                    (FEMeshH2D2L2IO,         '   detected as H2D2 L2'),
                    (FEMeshH2D2T3IO,         '   detected as H2D2 T3'),
                    (FEMeshH2D2T6LIO,        '   detected as H2D2 T6L'),
                    (FEMeshH2D2T6LasT3IO,    '   detected as H2D2 T6L read as T3')]
        for c, m in readers:
            try:
                r = constructReaderFromFile_isValid(c(fnames), m)
                if r: return r
            except Exception as e:
                LOGGER.debug('While constructing : %s', c.__name__)
                LOGGER.debug('   exception: %s', str(e))
    raise CTException('No valid reader found for file: %s' % repr(fnames))


if __name__ == "__main__":
    import os
    def main():
        streamHandler = logging.StreamHandler()
        LOGGER.addHandler(streamHandler)
        LOGGER.setLevel(logging.DEBUG)

        #m = FEMeshH2D2T6L('test.cor', 'test.ele')
        #for n in m.getNodes():
        #    print(n)
        #for n in range(m.getNbrNodes()):
        #    print(m.getNode(n))

        #m = FEMeshModeleurT6LIO('data/mesh.mai')
        #for n in range(m.getNbrNodes()):
        #    print(m.getNode(n))
        d = 'E:/Modeleur1.0a07/simul_h2d2/sim00020/40l'
        c = os.path.normpath( os.path.join(d, 'simul000.cor') )
        e = os.path.normpath( os.path.join(d, 'simul000.ele') )
        r = constructReaderFromFile( (c,e) )
        
