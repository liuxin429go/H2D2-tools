# -*- coding: utf-8 -*-

# This is an automatically generated file.
# Manual changes will be merged and conflicts marked!
#
# Generated by py2pxd_ version 0.0.3 on 2018-04-28 16:29:35

import cython
from numpy cimport ndarray
from . cimport DTData
from . cimport FEMesh
from .DTReducOperation cimport REDUCTION_OPERATION as OperationPXD

cdef class DTField:
    cpdef object       getDataAtTime   (DTField self, OperationPXD reducOp=*, object tsteps=*, list cols=*)
    cpdef object       getDataAtStep   (DTField self, OperationPXD reducOp=*, object tsteps=*, list cols=*)
    cpdef object       getDataActual   (DTField self)
    cpdef object       getData         (DTField self)
    cpdef object       doInterpolate   (DTField self, object X, object Y)
    cpdef object       doProbe         (DTField self, object X, object Y)
    cpdef object       getGrid         (DTField self)
    cpdef object       getDataMin      (DTField self)
    cpdef object       getDataMax      (DTField self)

cdef class DT2DFiniteElementField(DTField):
    cdef public object        actu
    cdef public object        cols
    cdef public DTData.DTData data
    cdef public object        itrp
    cdef public FEMesh.FEMesh mesh
    cdef public object        ndgb
    cdef public object        prbe
    cdef public object        tstp
    #
    @cython.locals (op = object)
    cpdef object       getDataAtTime   (DT2DFiniteElementField self, OperationPXD reducOp=*, object tsteps=*, list cols=*)
    @cython.locals (op = object)
    cpdef object       getDataAtStep   (DT2DFiniteElementField self, OperationPXD reducOp=*, object tsteps=*, list cols=*)
    cpdef object       getDataActual   (DT2DFiniteElementField self)
    cpdef object       getData         (DT2DFiniteElementField self)
    @cython.locals (i = object)
    cpdef object       doInterpolate   (DT2DFiniteElementField self, object X, object Y)
    cpdef object       doProbe         (DT2DFiniteElementField self, object X, object Y)
    cpdef object       getGrid         (DT2DFiniteElementField self)
    cpdef object       getDataMin      (DT2DFiniteElementField self)
    cpdef object       getDataMax      (DT2DFiniteElementField self)

cdef class DT2DRegularGridField(DTField):
    cdef public object       actu
    cdef public object       rgrid
    cdef public object       source
    #
    cpdef object       getDataAtTime   (DT2DRegularGridField self, OperationPXD reducOp=*, object tsteps=*, list cols=*)
    cpdef object       getDataAtStep   (DT2DRegularGridField self, OperationPXD reducOp=*, object tsteps=*, list cols=*)
    cpdef ndarray      getData         (DT2DRegularGridField self)
    cpdef object       getDataActual   (DT2DRegularGridField self)
    cpdef              doInterpolate   (DT2DRegularGridField self, object X, object Y)
    cpdef              doProbe         (DT2DRegularGridField self, object X, object Y)
    cpdef object       getGrid         (DT2DRegularGridField self)
    cpdef object       getDataMin      (DT2DRegularGridField self)
    cpdef object       getDataMax      (DT2DRegularGridField self)

cdef class DT1DRegularGridField(DTField):
    cdef public object       actu
    cdef public object       rgrid
    cdef public object       source
    #
    cpdef object       getDataAtTime   (DT1DRegularGridField self, OperationPXD reducOp=*, object tsteps=*, list cols=*)
    cpdef object       getDataAtStep   (DT1DRegularGridField self, OperationPXD reducOp=*, object tsteps=*, list cols=*)
    cpdef ndarray      getData         (DT1DRegularGridField self)
    @cython.locals (nd = object, nx = long, ny = long, shp = tuple)
    cpdef ndarray      getDataActual   (DT1DRegularGridField self)
    cpdef              doInterpolate   (DT1DRegularGridField self, object X, object Y)
    cpdef              doProbe         (DT1DRegularGridField self, object X, object Y)
    cpdef object       getGrid         (DT1DRegularGridField self)
    cpdef object       getDataMin      (DT1DRegularGridField self)
    cpdef object       getDataMax      (DT1DRegularGridField self)

@cython.locals (blocs = object, f = object, p = str, reader = object)
cpdef              main            ()

