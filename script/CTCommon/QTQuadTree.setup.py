#!/usr/bin/env python
# -*- coding: utf-8 -*-
from distutils.core      import setup
from distutils.extension import Extension
from Cython.Build        import cythonize
from Cython.Distutils    import build_ext

# ---  As we are in a module, set path to parent
import os
import sys
os.chdir('..')
for iarg, arg in enumerate(sys.argv):
    if arg[:12] != "--build-lib=": continue
    pth = arg.split("=")[1]
    if pth[0] in ['"', "'"]: pth = eval(pth, None, None)
    pth = os.path.join(pth, '..')
    pth = os.path.normpath(pth)
    sys.argv[iarg] = '--build-lib=%s' % pth

# ---  Module definition
pkg_args = {'language' : 'c++'}
pkg_name = 'CTCommon'
mdl_file = ['QTQuadTree.pyx', 'QTQuadTree_c.cpp']
ext_modules=[
    Extension('%s.%s'  % (pkg_name, 'QTQuadTree'),
              ['%s/%s' % (pkg_name, mdl) for mdl in mdl_file ],
              **pkg_args,
             ),
]

# ---  Setup
setup(
  name = pkg_name,
  cmdclass = {'build_ext': build_ext},
  include_dirs=['CTCommon'],
  ext_modules = cythonize(ext_modules,
                          annotate      = True,
                          language_level=3),
)
