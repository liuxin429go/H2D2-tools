#!/usr/bin/env python
# -*- coding: utf-8 -*-
## cython: profile=True
## cython: linetrace=True
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Unstructured triangular finite element mesh.
"""

import logging
import math

import numpy as np
from scipy import stats as sp

try:
    from .FEMeshMetricKind    import ELEMENT_QUALITY_METRICS as EQM
except ImportError:
    from .FEMeshMetricKind_pp import ELEMENT_QUALITY_METRICS as EQM

LOGGER = logging.getLogger("INRS.H2D2.Tools.Mesh.Metrics")

class FEMeshMetric:
    def __init__(self):
        pass

    def clcOneElement(self, elem):
        raise NotImplementedError

    def getInitialValue(self):
        raise NotImplementedError

    def getCompareOp(self):
        raise NotImplementedError

    def getAcceptableRange(self):
        return '[-]'

    def clcCriterion(self, grid, doRanking=False):
        vini = self.getInitialValue()
        fcmp = self.getCompareOp()

        c_val = np.full(grid.getNbNodes(), vini)
        for elem in grid.iterElements():
            val = self.clcOneElement(elem)
            for i in elem.getConnectivities():
                c_val[i] = fcmp(c_val[i], val)
        return c_val if not doRanking else sp.rankdata(c_val, method='ordinal')

class FEMeshMetricArea(FEMeshMetric):
    """
    Quality metric
    Sandia 4.1
    """
    def clcOneElement(self, elem):
        a, b, c = elem.sideLength()
        p = 0.5*(a+b+c)
        s = math.sqrt(p*(p-a)*(p-b)*(p-c))  # area
        return s

    def getInitialValue(self):
        return -1.0e99

    def getCompareOp(self):
        return max

class FEMeshMetricAspectRatio(FEMeshMetric):
    """
    Quality metric
    Sandia 4.2
    Acceptable range [1, 1.3]
    """
    def clcOneElement(self, elem):
        lMax = max(elem.sideLength())
        r = elem.inCircleRadius()
        return lMax / (2.0*math.sqrt(3.0)*r)

    def getInitialValue(self):
        return -1.0e99

    def getCompareOp(self):
        return max

    def getAcceptableRange(self):
        return 'Sandia: [1, 1.3]'

class FEMeshMetricAspectFrobenius(FEMeshMetric):
    """
    Quality metric
    Sandia 4.3
    Acceptable range [1, 1.3]
    """
    def clcOneElement(self, elem):
        a, b, c = elem.sideLength()
        p = 0.5*(a+b+c)
        s = math.sqrt(p*(p-a)*(p-b)*(p-c))  # area
        return (a*a + b*b * c*c) / (4.0*math.sqrt(3.0)*s)

    def getInitialValue(self):
        return -1.0e99

    def getCompareOp(self):
        return max

    def getAcceptableRange(self):
        return 'Sandia: [1, 1.3]'

class FEMeshMetricCondition(FEMeshMetric):
    """
    Quality metric
    Sandia 4.4
    Acceptable range [1, 1.3]
    """
    def clcOneElement(self, elem):
        a, b, c = elem.sideLength()
        p = 0.5*(a+b+c)
        s = math.sqrt(p*(p-a)*(p-b)*(p-c)) # area
        return (b*b + a*a * a*b) / (2.0*math.sqrt(3.0)*s)

    def getInitialValue(self):
        return -1.0e99

    def getCompareOp(self):
        return max

    def getAcceptableRange(self):
        return 'Sandia: [1, 1.3]'

class FEMeshMetricEdgeRatio(FEMeshMetric):
    """
    Quality metric
    Sandia 4.6
    Acceptable range [1, 1.3]
    """
    def clcOneElement(self, elem):
        a, b, c = elem.sideLength()
        return max(a, b, c)/ min(a, b, c)

    def getInitialValue(self):
        return -1.0e99

    def getCompareOp(self):
        return max

    def getAcceptableRange(self):
        return 'Sandia: [1, 1.3]; Other: [1, 30]'

class FEMeshMetricMaxAngle(FEMeshMetric):
    """
    Quality metric
    Sandia 4.7
    Acceptable range [60, 90]
    """
    def clcOneElement(self, elem):
        return math.degrees(max(elem.angles()))

    def getInitialValue(self):
        return -1.0e99

    def getCompareOp(self):
        return max

    def getAcceptableRange(self):
        return 'Sandia: [60, 90]; Other: [60, 170]'

class FEMeshMetricMinAngle(FEMeshMetric):
    """
    Quality metric
    Sandia 4.8
    Acceptable range [30, 60]
    """
    def clcOneElement(self, elem):
        return math.degrees(min(elem.angles()))

    def getInitialValue(self):
        return 1.0e99

    def getCompareOp(self):
        return min

    def getAcceptableRange(self):
        return 'Sandia: [30, 60]; Other: [10, 60]'

class FEMeshMetricScaledJacobian(FEMeshMetric):
    """
    Quality metric
    Sandia 4.9
    Acceptable range [.5, 2sqrt(3)/3]
    """
    def clcOneElement(self, elem):
        a, b, c = elem.sideLength()
        lmax = max(a*b, a*c, b*c)
        return (2.0*math.sqrt(3.0)/3.0) * (elem.detJ() / lmax)

    def getInitialValue(self):
        return -1.0e99

    def getCompareOp(self):
        return max

    def getAcceptableRange(self):
        return 'Sandia: [0.5, %.2f]' % (2.0*math.sqrt(3.0)/3.0)

class FEMeshMetricRadiusRatio(FEMeshMetric):
    """
    Quality metric
    Sandia 4.10
    Acceptable range [1, 3]
    """
    def clcOneElement(self, elem):
        r = elem.inCircleRadius()
        R = elem.circumCircleRadius()
        return R / (2*r)

    def getInitialValue(self):
        return -1.0e99

    def getCompareOp(self):
        return max

    def getAcceptableRange(self):
        return 'Sandia: [1, 3]'

class FEMeshMetricAngularSkewness(FEMeshMetric):
    """
    Quality metric
    Acceptable range ???
    """
    def clcOneElement(self, elem):
        t060 = math.pi / 3
        t180 = math.pi
        t1, t2, t3 = elem.angles()
        tmin = min(t1, t2, t3)
        tmax = max(t1, t2, t3)
        return max((tmax-t060) / (t180-t060), (t060-tmin) / (t060))

    def getInitialValue(self):
        return -1.0e99

    def getCompareOp(self):
        return max

class FEMeshMetricSkewness(FEMeshMetric):
    """
    Quality metric
    Acceptable range ???
    """
    def clcOneElement(self, elem):
        a, b, c = elem.sideLength()
        p = 0.5*(a+b+c)
        s = math.sqrt(p*(p-a)*(p-b)*(p-c))  # area
        r = elem.circumCircleRadius()
        o = (3.0*math.sqrt(3.0)*r*r)/4.0    # optimal - equilateral
        return (o-s)/o

    def getInitialValue(self):
        return -1.0e99

    def getCompareOp(self):
        return max

    def getAcceptableRange(self):
        return '(< 0.8)'


def metricFactory(kind):
    def iseq(r1, r2):
        # La comparaison directe des item de l'enum ne retourne pas toujours
        # le bon résultat. La comparaison sur valeur est stable. Sauf que ...
        # Cython passe des int!
        try:
            return r1.value == r2.value
        except:
            return r1 == r2
    if iseq(kind, EQM.area):            return FEMeshMetricArea()
    if iseq(kind, EQM.aspectRatio):     return FEMeshMetricAspectRatio()
    if iseq(kind, EQM.aspectFrobenius): return FEMeshMetricAspectFrobenius()
    if iseq(kind, EQM.condition):       return FEMeshMetricCondition()
    if iseq(kind, EQM.edgeRatio):       return FEMeshMetricEdgeRatio()
    if iseq(kind, EQM.maxAngle):        return FEMeshMetricMaxAngle()
    if iseq(kind, EQM.minAngle):        return FEMeshMetricMinAngle()
    if iseq(kind, EQM.scaledJacobian):  return FEMeshMetricScaledJacobian()
    if iseq(kind, EQM.radiusRatio):     return FEMeshMetricRadiusRatio()
    if iseq(kind, EQM.angularSkewness): return FEMeshMetricAngularSkewness()
    if iseq(kind, EQM.skewness):        return FEMeshMetricSkewness()
    raise ValueError('Unknown metric kind: %s', kind)

def logAllMeshMetrics(mesh, doRanking=False):
    """
    Compute the quality metrics for all the metric.
    Log the result (min, max)
    """
    def __str_v(v):
        return '(%10.3e, %7i)' % v

    res = []
    for kind in EQM:
        mm = metricFactory(kind)
        mv = mm.clcCriterion(mesh, doRanking)
        res.append('%s: Acceptable range: %s' % (kind.name, mm.getAcceptableRange()))
        res.append('   min: %.6e - %s' % (np.min(mv), mesh.getNode(np.argmin(mv))))
        res.append('   max: %.6e - %s' % (np.max(mv), mesh.getNode(np.argmax(mv))))
    return res

def getMeshMetrics(mesh, metrics, doRanking=False):
    """
    Compute the quality metrics for all the metric in metrics
    Return a list of np.array of nodal values.
    """
    res = []
    for kind in metrics:
        mm = metricFactory(kind)
        mv = mm.clcCriterion(mesh, doRanking)
        res.append(mv)
    return res

def reduceMeshMetrics(mesh, metrics):
    """
    Compute the quality metrics for all the metric in metrics, and
    reduce the result.
    The individual metrics are ranked, and the mean of all the ranks
    is returned.
    """
    res = np.zeros(mesh.getNbNodes())
    for kind in metrics:
        mm = metricFactory(kind)
        mv = mm.clcCriterion(mesh, doRanking=True)
        res += mv
    res /= len(metrics)
    return res

if __name__ == "__main__":
    from .FEMesh import FEMesh

    def t_main():
        d = 'E:/Projets_simulation/LacErie/2x/'
        m = FEMesh(files=[d+'erie_2x_t6l.cor', d+'erie_2x_t6l.ele'])
        r = logAllMeshMetrics(m, doRanking=False)
        for l in r:
            print(l)
        mms = [
                EQM.area,
                #EQM.aspectRatio,
                EQM.aspectFrobenius,
                #EQM.condition,
                EQM.edgeRatio,
                #EQM.maxAngle,
                EQM.minAngle,
                #EQM.scaledJacobian,
                EQM.radiusRatio,
                EQM.angularSkewness,
                EQM.skewness,
              ]
        r = reduceMeshMetrics(m, mms)
        print(r)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)
    #cProfile.run('t_main()', sort='tottime')
    t_main()


