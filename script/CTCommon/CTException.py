#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
LOGGER = logging.getLogger("INRS.H2D2.Tools")

class CTException(Exception):
    def __init__(self, value):
        super(CTException, self).__init__()
        self.parameter = value

    def __str__(self):
        return repr(self.parameter)

class NoMorePlotSpaceException(Exception):
    def __init__(self, value):
        super(NoMorePlotSpaceException, self).__init__()
        self.parameter = value

    def __str__(self):
        return repr(self.parameter)
