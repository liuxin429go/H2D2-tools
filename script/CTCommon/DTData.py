# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2012-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
H2D2 data from a data file.
"""

import hashlib
import logging
import re
import sys
import numpy as np

from . import CTException
from . import DTDataIO
from . import DTDataBloc as DTBloc
try:
    from .DTReducOperation    import REDUCTION_OPERATION as Operation
except ImportError:
    from .DTReducOperation_pp import REDUCTION_OPERATION as Operation

np.set_printoptions(linewidth=120, precision=15)

LOGGER = logging.getLogger("INRS.H2D2.Tools.Data.Data")

if sys.version_info >= (3,):
    INT_TYPES = (int)
else:
    INT_TYPES = (int, int)

class DTDataOpBase:
    def getDataAtTime(self, time):
        raise NotImplementedError

    def getDataAtStep(self, i):
        raise NotImplementedError

    def getNodesAtTime(self, time, nodes=[], cols=[], compact=True):
        raise NotImplementedError

    def getNodesAtStep(self, i, nodes=[], cols=[], compact=True):
        raise NotImplementedError

class DTDataOp: #(DTDataOpBase)
    """
    Opération sur les données et appliquée juste après la lecture des données.
    """
    def __init__(self, c):
        self.locals = {}
        self.code   = c

        self.src = c
        self.src_1 = c.replace('{0}', 'values[:]')
        self.ast_1 = compile(self.src_1, '<source>', 'eval')
        # ---  Subst {n} with values[:,n]
        tmp = re.sub(r'\{\s*(?P<idx>\d+)\s*\}', r'values[:,\g<idx>]', c, flags=re.ASCII)
        self.src_n = tmp
        self.ast_n = compile(self.src_n, '<source>', 'eval')

    def setLocals(self, **kwargs):
        """
        Set the __dict__ used for locals,
        variables known for the calls
        """
        self.locals = kwargs

    def __getAst1(self):
        src = self.code.replace('{0}', 'getDataAtTime(time)')
        ast = compile(src, '<source>', 'eval')
        return ast

    def __getAstN(self):
        src = re.sub(r'\{\s*(?P<idx>\d+)\s*\}', r'getDataAtTime(time, cols=[\g<idx>])', self.code, flags=re.ASCII)
        ast = compile(src, '<source>', 'eval')
        return ast

    #def getDataAtTime(self, time):
    #    lcls = self.locals
    #    if len(np.shape(v_)) == 1:
    #       ast = self.__getAst1()
    #    else:
    #       ast = self.__getAstN()
    #    return eval(ast, None, lcls)

    def __call__(self, v_):
        """
        Evaluate the operation.
        """
        lcls = self.locals
        lcls['values'] = v_
        if len(np.shape(v_)) == 1:
            return eval(self.ast_1, None, lcls)
        else:
            return eval(self.ast_n, None, lcls)

class DTDataOpCol:
    """
    Op identité
    """
    def __init__(self, ic):
        self.col = ic
    def setLocals(self, **kwargs):
        pass
    def __call__(self, v):
        return v[:, self.col]

class DTDataOpId:
    """
    Op identité
    """
    def __init__(self):
        pass
    def setLocals(self, **kwargs):
        pass
    def __call__(self, v):
        return v

class DTData:
    def __init__(self, fnam=None, data=None, cols=None):
        self.nrow  = 0
        self.ncol  = 0
        self.blocs = []
        self.cache = []
        self.buf   = np.zeros(1)

        self.reset()
        if fnam is not None:
            self.appendFile(fnam, cols)
        elif data is not None:
            self.appendData(data)

    def reset(self):
        self.nrow = 0
        self.ncol = 0
        self.blocs = []
        self.buf = np.zeros(1)

    def appendData(self, data):
        LOGGER.info('DTData: append structure from data')
        # ---  Control dimensions
        shp = np.shape(data[0][1])
        if len(shp) == 1:
            nrow, ncol = (shp[0], 1)
        elif len(shp) == 2:
            nrow, ncol = shp
        else:
            raise CTException.CTException('Invalid data array shape: %s. Only 1 or 2 dimensions supported.' % str(shp))
        vmin = [(-1.0e99, -1) for j in range(ncol)]
        vmax = [( 1.0e99, -1) for j in range(ncol)]

        # ---  Get structure
        blocs = []
        for t, d in data:
            b = DTBloc.DTDataBloc(None, None, 0, d, nrow, ncol, t, vmin, vmax)
            blocs.append(b)
        nrow = blocs[0].nrow
        ncol = blocs[0].ncol

        # ---  Control coherence
        if self.blocs:
            errMsg = []
            if nrow != self.getNbNodes(): errMsg.append('Incompatible row count %i/%i'    % (nrow, self.getNbNodes()))
            if ncol != self.getNbVal():   errMsg.append('Incompatible column count %i/%i' % (ncol, self.getNbVal()))
            if blocs[0].time <= self.getTimeMax(): errMsg.append('Times must be globally increasing')
            if errMsg:
                errMsg.append('While appending data')
                raise CTException.CTException('\n'.join(errMsg))

        # ---  Append
        if self.blocs:
            self.blocs += blocs
        else:
            self.blocs= blocs
            self.nrow = nrow
            self.ncol = ncol
            if self.ncol == 1:
                self.buf = np.zeros([self.nrow])
            else:
                self.buf = np.zeros([self.nrow, self.ncol])
            self.cache = [[-1, None], [-1, None]]

    def appendFile(self, fnam, cols=None):
        LOGGER.info('DTData: append structure from file %s', fnam)
        # ---  Get structure
        readr= DTDataIO.constructReaderFromFile(fnam, cols)
        blocs= readr.readStructNoStats()
        nrow = blocs[0].nrow
        ncol = blocs[0].ncol

        # ---  Control coherence
        if self.blocs:
            errMsg = []
            if nrow != self.getNbNodes(): errMsg.append('Incompatible row count %i/%i'    % (nrow, self.getNbNodes()))
            if ncol != self.getNbVal():   errMsg.append('Incompatible column count %i/%i' % (ncol, self.getNbVal()))
            if blocs[0].time <= self.getTimeMax(): errMsg.append('Times must be globally increasing')
            if errMsg:
                errMsg.append('While reading file "%s"' % fnam)
                raise CTException.CTException('\n'.join(errMsg))

        # ---  Append
        if self.blocs:
            self.blocs += blocs
        else:
            self.blocs= blocs
            self.nrow = nrow
            self.ncol = ncol
            if self.ncol == 1:
                self.buf = np.zeros([self.nrow])
            else:
                self.buf = np.zeros([self.nrow, self.ncol])
            self.cache = [[-1, None], [-1, None]]

    def __loadDataAtStep(self, i):
        """
        Retrieve the whole table at time step 'i'
        The method returns a reference to internal storage.
        """
        try:
            if self.cache[0][1].shape[0] != self.nrow:
                self.cache = [ [-1, None], [-1, None] ]
        except AttributeError:
            pass

        if i == self.cache[0][0]:
            LOGGER.debug('DTData: Cache 0 hit at step %i', i)
            return self.cache[0][1]
        elif i == self.cache[1][0]:
            LOGGER.debug('DTData: Cache 1 hit at step %i', i)
            return self.cache[1][1]
        else:
            LOGGER.debug('DTData: Cache miss at step %i', i)
            bloc = self.blocs[i]
            if bloc.reader:
                self.buf = bloc.reader.readDataAtStep(bloc, self.buf)
            else:
                np.copyto(self.buf, bloc.data)

            #print '__loadDataAtStep: On entry'
            #print '   buff:  ', self.buf.ctypes.data, self.buf[0]
            #print '   cache0:', self.cache[0][1].ctypes.data if self.cache[0][1] is not None else None
            #print '   cache1:', self.cache[1][1].ctypes.data if self.cache[1][1] is not None else None
            # ---  Swap 1 and 0
            b0 = self.cache[0][1]
            self.cache[0][0] = self.cache[1][0]
            self.cache[0][1] = self.cache[1][1]
            self.cache[1][0] = -1
            self.cache[1][1] = b0
            # ---  Copy buf to 1
            self.cache[1][0] = i
            if self.cache[1][1] is None:
                self.cache[1][1] = np.array(self.buf)   # create array
            else:
                np.copyto(self.cache[1][1], self.buf)   # copy
            #print '__loadDataAtStep: On exit'
            #print '   buff:  ', self.buf.ctypes.data, self.buf[0]
            #print '   cache0:', self.cache[0][1].ctypes.data if self.cache[0][1] is not None else None
            #print '   cache1:', self.cache[1][1].ctypes.data if self.cache[1][1] is not None else None
            LOGGER.debug('DTData: Steps in cache %i %i ', self.cache[0][0], self.cache[1][0])
            try:
                LOGGER.debug('    vals[%i] (%s, %s) ', 0, self.cache[0][1][0], self.cache[1][1][0])
            except:
                pass
            return self.cache[1][1]

    def __loadDataAtTime(self, time):
        """
        Retrieve the whole table at time 'time'
        """
        if time <= self.getTimeMin():
            i1 = 0
            i2 = 0
        elif time >= self.getTimeMax():
            i1 = len(self.blocs)-1
            i2 = i1
        else:
            i1 = 0
            for i in range(len(self.blocs)):
                if self.blocs[i].time > time: break
                i1 = i
            i2 = i1+1
        LOGGER.debug('DTData: Required steps (%i, %i) time (%f, %f)', i1, i2, self.blocs[i1].time, self.blocs[i2].time)

        if i2 == i1:
            d1 = self.__loadDataAtStep(i1)
            np.copyto(self.buf, d1)
        elif i2 > i1:
            d1 = self.__loadDataAtStep(i1)
            d2 = self.__loadDataAtStep(i2)
            #print '   d1:  ', d1.ctypes.data, np.min(d1), np.max(d1)
            #print '   d2:  ', d2.ctypes.data, np.min(d2), np.max(d2)
            #print '  buf:  ', self.buf.ctypes.data
            t1 = self.blocs[i1].time
            t2 = self.blocs[i2].time
            alfa = (time-t1) / (t2-t1)
            np.copyto(self.buf, d2) # (1.0-alfa)*d1 + alfa*d2
            self.buf -= d1          # faster without temporaries
            self.buf *= alfa
            self.buf += d1
            LOGGER.debug('DTData: Interpolate between steps with alfa %f', alfa)
        return self.buf

    def __loadNodesAtStep(self, i, nodes, compact=True):
        """
        Retrieve the values at 'nodes' for time step 'i'.
        If compact is False, the returne table contains values for all nodes,
        but where only indices of 'nodes' are valid.
        If compact is True, the table contains only values for 'nodes'.
        """
        try:
            if self.cache[0][1].shape[0] != len(nodes):
                self.cache = [ [-1, None], [-1, None] ]
        except AttributeError:
            pass

        r = None
        if i == self.cache[0][0]:
            LOGGER.debug('DTData: Cache 0 hit at step %i', i)
            r = self.cache[0][1]
        elif i == self.cache[1][0]:
            LOGGER.debug('DTData: Cache 1 hit at step %i', i)
            r = self.cache[1][1]
        else:
            LOGGER.debug('DTData: Cache miss at step %i', i)
            bloc = self.blocs[i]
            if bloc.reader:
                self.buf = bloc.reader.readRowsAtStep(nodes, bloc, self.buf)
            else:
                self.buf = bloc.data
            if self.cache[0][1] is not None:
                _i, d_ = self.cache[0]
                self.cache[0] = [-1, None]
                del d_
            self.cache[0] = self.cache[1]
            self.cache[1] = [i, np.copy(self.buf)]
            LOGGER.debug('    steps %i %i ', self.cache[0][0], self.cache[1][0])
            try:
                LOGGER.debug('    vals[%i] (%f, %f) ', 0, self.cache[0][1][0], self.cache[1][1][0])
            except:
                pass
            r = self.cache[1][1]
        if compact:
            try:
                return r[nodes, :]
            except IndexError:
                return r[nodes]
        else:
            return r

    def __loadNodesAtTime(self, time, nodes, compact=True):
        """
        Retrieve the values at 'nodes' for 'time'.
        If compact is False, the returne table contains values for all nodes,
        but where only indices of 'nodes' are valid.
        If compact is True, the table contains only values for 'nodes'.
        """
        if time <= self.getTimeMin():
            i1 = 0
            i2 = 0
        elif time >= self.getTimeMax():
            i1 = len(self.blocs)-1
            i2 = i1
        else:
            i1 = 0
            for i in range(len(self.blocs)):
                if self.blocs[i].time > time: break
                i1 = i
            i2 = i1+1
        LOGGER.debug('    steps (%i, %i) time (%f, %f)', i1, i2, self.blocs[i1].time, self.blocs[i2].time)

        d1 = self.__loadNodesAtStep(i1, nodes, compact)
        if i2 > i1:
            d2 = self.__loadNodesAtStep(i2, nodes, compact)
            t1 = self.blocs[i1].time
            t2 = self.blocs[i2].time
            alfa = (time-t1) / (t2-t1)
            d1 = (1.0-alfa)*d1 + alfa*d2
            LOGGER.debug('    alfa %f', alfa)
        return d1

    def getNbNodes(self):
        return self.nrow

    def getNbVal(self):
        return self.ncol

    def getNbTimeSteps(self):
        return len(self.blocs)

    def getTimeMin(self):
        return self.blocs[0].time

    def getTimeMax(self):
        return self.blocs[-1].time

    def getTimes(self):
        times = [b.time for b in self.blocs]
        return times

    def __aa(self, d, cols):
        if len(cols) <= 0: return d
        if isinstance(cols[0], INT_TYPES):
            if self.ncol == 1: return d
            if len(cols) == 1: return d[:, cols].reshape(-1)
            if len(cols) >  1: return d[:, cols]
        elif all([isinstance(op, DTDataOpCol) for op in cols]):
            if self.ncol == 1: return d
            cols_  = [op.col for op in cols]
            if len(cols) == 1: return d[:, cols_].reshape(-1)
            if len(cols) >  1: return d[:, cols_]
        else:
            c = np.array(())    # array of size 0, for cython
            for i, op in enumerate(cols):
                r = op(d)
                if c.size == 0:
                    try:
                        c = np.empty( (np.shape(r)[0], len(cols)) )
                    except IndexError:
                        c = np.empty( (1, len(cols)) )
                c[:,i] = r
            if len(cols) == 1: return c.reshape(-1)
            if len(cols) >  1: return c

    def getDataMD5(self, d):
        md5 = hashlib.md5()
        md5.update(d.view(np.uint8))
        return md5.hexdigest()

    def getDataAtTime(self, time, cols=[]):
        LOGGER.debug('DTData: Get data at time %f', time)
        d = self.__loadDataAtTime(time)
        return self.__aa(d, cols)

    def getDataAtStep(self, i, cols=[]):
        LOGGER.debug('DTData: Get data at step %i', i)
        d = self.__loadDataAtStep(i)
        return self.__aa(d, cols)

    def getNodesAtTime(self, time, nodes=[], cols=[], compact=True):
        """
        Retrieve the values at 'nodes' for 'time'.
        If 'nodes' is empty, all values are returned.
        If compact is False, the returne table contains values for all nodes,
        but where only indices of 'nodes' are valid.
        If compact is True, the table contains only values for 'nodes'.
        """
        LOGGER.debug('DTData: get nodes at time %f', time)
        if not nodes:
            d = self.__loadDataAtTime(time)
        else:
            d = self.__loadNodesAtTime(time, nodes, compact)
        return self.__aa(d, cols)

    def getNodesAtStep(self, i, nodes=[], cols=[], compact=True):
        """
        Retrieve the values at 'nodes' for time step 'i'.
        If 'nodes' is empty, all values are returned.
        If compact is False, the returne table contains values for all nodes,
        but where only indices of 'nodes' are valid.
        If compact is True, the table contains only values for 'nodes'.
        """
        LOGGER.debug('DTData: get nodes at step %i', i)
        if not nodes:
            d = self.__loadDataAtStep(i)
        else:
            d = self.__loadNodesAtStep(i, nodes, compact)
        return self.__aa(d, cols)

    def getPointsAtTime(self, time, points, cols=[]):
        LOGGER.debug('DTData: get points at time %f', time)
        d = self.getDataAtTime(time, cols=cols)
        return time, points.interpolate(d)

    def getPointsAtStep(self, i, points, cols=[]):
        LOGGER.debug('DTData: get points at step %i', i)
        d = self.getDataAtStep(i, cols=cols)
        return self.blocs[i].time, points.interpolate(d)

    def getTimeSerieAtNodes(self, nodes=[], tmin=None, tmax=None, cols=[]):
        # retourne [ (t1, [v1, ..., vi]), (t2, [v1, ..., vi]) ]
        if not tmin: tmin = self.getTimeMin()
        if not tmax: tmax = self.getTimeMax()

        r = []
        # ---  Le temps min
        d = self.getNodesAtTime(tmin, nodes=nodes, cols=cols, compact=True)
        r.append((tmin, d))
        # ---  Les pas entre min et max
        t = tmin
        for ib in range(len(self.blocs)):
            if self.blocs[ib].time <= tmin: continue
            if self.blocs[ib].time >= tmax: break
            t = self.blocs[ib].time
            d = self.getNodesAtStep(ib, nodes=nodes, cols=cols, compact=True)
            r.append((t, d))
        # ---  Le temps max
        if tmax > t:
            d = self.getNodesAtTime(tmax, nodes=nodes, cols=cols, compact=True)
            r.append((tmax, d))
        return r

    def getTimeSerieAtPoints(self, points, tmin=None, tmax=None, cols=[]):
        # retourne [ (t1, [v1, ..., vi]), (t2, [v1, ..., vi]) ]
        if not tmin: tmin = self.getTimeMin()
        if not tmax: tmax = self.getTimeMax()

        r = []
        # ---  Le temps min
        d = self.getDataAtTime(tmin, cols=cols)
        r.append((tmin, points.interpolate(d)))
        # ---  Les pas entre min et max
        t = tmin
        for ib in range(len(self.blocs)):
            if self.blocs[ib].time <= tmin: continue
            if self.blocs[ib].time >= tmax: break
            t = self.blocs[ib].time
            d = self.getDataAtStep(ib, cols=cols)
            r.append((t, points.interpolate(d)))
        # ---  Le temps max
        if tmax > t:
            d = self.getDataAtTime(tmax, cols=cols)
            r.append((tmax, points.interpolate(d)))
        return r

    def write(self, writer):
        for i, b in enumerate(self.blocs):
            d = self.getDataAtStep(i)
            writer.writeDataBloc(b, d)

    def writeAsAscii(self, f):
        w = DTDataIO.DTDataIOAscii(f)
        self.write(w)

    def writeAsBinary(self, f):
        w = DTDataIO.DTDataIOBinary(f)
        self.write(w)

    def writeTimeSerie(self, writer, data):
        for t, v in data:
            writer.writeData(t, v)

    def writeTimeSerieAsAscii(self, f, data):
        w = DTDataIO.DTDataIOAscii(f)
        self.writeTimeSerie(w, data)

    def writeTimeSerieAsBinary(self, f, data):
        w = DTDataIO.DTDataIOBinary(f)
        self.writeTimeSerie(w, data)

    def splitAsAscii(self, root):
        LOGGER.info('DTData: split data, one ascii file per bloc')
        for i, b in enumerate(self.blocs):
            f = '%s_%012i.vno' % (root, int(b.time))
            w = DTDataIO.DTDataIOAscii(f)
            d = self.getDataAtStep(i)
            w.writeDataBloc(b, d)

    def splitAsBinary(self, root):
        LOGGER.info('DTData: split data, one binary file per bloc')
        for i, b in enumerate(self.blocs):
            f = '%s_%012i.vnob' % (root, int(b.time))
            w = DTDataIO.DTDataIOBinary(f)
            d = self.getDataAtStep(i)
            w.writeDataBloc(b, d)

class DTDataStats:
    def __init__(self):
        self.max = np.array(()) # array of size 0, for cython
        self.min = np.array(())
        self.s0  = np.array(())
        self.s1  = np.array(())
        self.s2  = np.array(())

    def compute(self, data, nodes=None, tsteps=None, cols=[]):
        times = []
        if not tsteps:
            times = data.getTimes()
        elif len(tsteps) > 0 and isinstance(tsteps[0], int):
            ttmp = data.getTimes()
            times = [ttmp[it] for it in tsteps]
        else:
            times = tsteps

        nVal = data.getNbVal() if cols == [] else len(cols)
        self.max = np.zeros([nVal]) - 1.0e99
        self.min = np.zeros([nVal]) + 1.0e99
        self.s0  = np.zeros([nVal], dtype=np.int)
        self.s1  = np.zeros([nVal])
        self.s2  = np.zeros([nVal])

        for t in times:
            d = data.getDataAtTime(t, cols)
            l = np.size(d, 0)
            #s = np.shape(d)
            if nodes and len(nodes) <= l:
                n = np.full((l,), False)
                n[nodes] = True
                d = np.compress(n, d, axis=0)
            try:
                self.max = list(map(lambda x, y: max(x,y), self.max, np.amax(d, axis=0)))
                self.min = list(map(lambda x, y: min(x,y), self.min, np.amin(d, axis=0)))
            except TypeError:    # arg 3 must support iteration
                self.max[0] = max(self.max[0], np.amax(d, axis=0))
                self.min[0] = min(self.min[0], np.amin(d, axis=0))
            self.s0 = self.s0 + np.size(d,0)
            self.s1 = self.s1 + np.sum(d, axis=0)
            self.s2 = self.s2 + np.sum(np.square(d), axis=0)

    def getMax(self):
        return self.max

    def getMin(self):
        return self.min

    def getMean(self):
        return self.s1 / self.s0

    def getStd(self):
        m = self.getMean()
        s2 = self.s2 / self.s0
        return np.sqrt(s2-m*m)

    def getCount(self):
        return self.s0

from pubsub import pub
class DTDataReduc:
    def __init__(self):
        self.val = None

    def compute(self, data, reducOp=Operation.op_noop, tsteps=None, cols=[]):
        assert isinstance(data, DTData)
        def iseq(r1, r2):
            # La comparaison directe des item de l'enum ne retourne pas toujours
            # le bon résultat. La comparaison sur valeur est stable. Sauf que ...
            # Cython passe des int!
            try:
                return r1.value == r2.value
            except:
                return r1 == r2

        times = []
        if not tsteps:
            times = list(range(data.getNbTimeSteps()))
            getDataFnc = data.getDataAtStep
        elif len(tsteps) > 0 and isinstance(tsteps[0], int):
            times = tsteps
            getDataFnc = data.getDataAtStep
        else:
            times = tsteps
            getDataFnc = data.getDataAtTime
        if iseq(reducOp, Operation.op_noop):
            times = times[:1]   # times[-1:]    !! Both are false

        doProgress = len(times) > 1
        if doProgress: pub.sendMessage('progress.start')

        acc = None
        self.val = None
        for it, t in enumerate(times):
            d = getDataFnc(t, cols=cols)
            if doProgress: pub.sendMessage('progress.set', pos=int(100*it/len(times)))
            #print t, reducOp
            #print 'd  ', d[:5], np.min(d), np.max(d)
            if self.val is None:
                self.val = np.copy(d)
            elif iseq(reducOp, Operation.op_noop):
                self.val = np.copy(d)
            elif iseq(reducOp, Operation.op_min):
                self.val = np.minimum(self.val, d)
            elif iseq(reducOp, Operation.op_amin):
                self.val = np.minimum(np.abs(self.val), np.abs(d))
            elif iseq(reducOp, Operation.op_max):
                self.val = np.maximum(self.val, d)
            elif iseq(reducOp, Operation.op_amax):
                self.val = np.maximum(np.abs(self.val), np.abs(d))
            elif iseq(reducOp, Operation.op_sum):
                self.val = np.add(self.val, d)
            elif iseq(reducOp, Operation.op_mean):
                self.val = np.add(self.val, d)
            elif iseq(reducOp, Operation.op_delta_min):
                if acc is None: acc = np.full(np.size(d), 1.0e+99)
                acc = np.minimum(acc, d-self.val)
                self.val = np.copy(d)
            elif iseq(reducOp, Operation.op_delta_amin):
                if acc is None: acc = np.full(np.size(d), 1.0e+99)
                acc = np.minimum(acc, np.abs(d-self.val))
                self.val = np.copy(d)
            elif iseq(reducOp, Operation.op_delta_max):
                if acc is None: acc = np.full(np.size(d), -1.0e+99)
                acc = np.maximum(acc, d-self.val)
                self.val = np.copy(d)
            elif iseq(reducOp, Operation.op_delta_amax):
                if acc is None: acc = np.full(np.size(d), -1.0e+99)
                acc = np.maximum(acc, np.abs(d-self.val))
                self.val = np.copy(d)
            elif iseq(reducOp, Operation.op_delta_sum):
                if acc is None: acc = np.zeros(np.size(d))
                acc = np.add(acc, np.subtract(d, self.val))
                self.val = np.copy(d)
            elif iseq(reducOp, Operation.op_delta_mean):
                #print 'Operation.op_delta_mean'
                if acc is None: acc = np.zeros(np.size(d))
                acc = np.add(acc, np.subtract(d, self.val))
                self.val = np.copy(d)
                #print 'acc', acc[:5], np.min(acc), np.max(acc)

        if iseq(reducOp, Operation.op_noop):
            pass
        elif iseq(reducOp, Operation.op_min):
            pass
        elif iseq(reducOp, Operation.op_amin):
            pass
        elif iseq(reducOp, Operation.op_max):
            pass
        elif iseq(reducOp, Operation.op_amax):
            pass
        elif iseq(reducOp, Operation.op_sum):
            pass
        elif iseq(reducOp, Operation.op_mean):
            if len(times) > 1: self.val /= len(times)
        elif iseq(reducOp, Operation.op_delta_min):
            self.val = acc
        elif iseq(reducOp, Operation.op_delta_amin):
            self.val = acc
        elif iseq(reducOp, Operation.op_delta_max):
            self.val = acc
        elif iseq(reducOp, Operation.op_delta_amax):
            self.val = acc
        elif iseq(reducOp, Operation.op_delta_sum):
            self.val = acc
        elif iseq(reducOp, Operation.op_delta_mean):
            if len(times) > 1: self.val = acc / (len(times)-1)

        if doProgress: pub.sendMessage('progress.stop')
        return self.val

    def getVal(self):
        return self.val


if __name__ == "__main__":
    def main():
        import os
        p = 'E:/Projets_simulation/LacErie/1x/rdps/cw6'
        f = os.path.join(p, 'simul000.pst.sim')
        d = DTData(fnam=f) #, cols = (3,))
        return

        times = d.getTimes()
        md5 = []
        for it, t in enumerate(times):
            dd = d.getDataAtStep(it)
            m1 = d.getDataMD5(dd)
            dd = d.getDataAtTime(t)
            m2 = d.getDataMD5(dd)
            md5.append( (it, m1) )
            print('%s' % md5[it])
            if m1 != m2:
                print('---------------------')
                print('%s %s' % (it, times[it]))
                print('%s %s' % (m1, m2))
                raise RuntimeError

        dr = DTDataReduc()
        for it in [2, 1, 0, 1, 2]:
            dd = dr.compute(d, tsteps=(times[it],) )
            m = d.getDataMD5(dd)
            print(m, md5[it][1])
            if m != md5[it][1]:
                print(it, times[it])
                print(m, md5[it][1])
                raise RuntimeError

#    stts = DTDataReduc()
#    stts.compute(d, Operation.op_delta_mean, tsteps=list(range(10)), cols=(3,))
#    v = stts.getVal()
#    v = np.minimum(v, 1.0)
#    v = np.maximum(v, 1.0e-99)

    # pylint: disable=all

    streamHandler = logging.StreamHandler()
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    import cProfile
    cProfile.run('main()', sort='tottime')
    #main()
