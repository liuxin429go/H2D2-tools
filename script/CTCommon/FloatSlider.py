# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx

def ScientificFormater(v):
    return '%g' % v

def FloatFormater(v):
    return '%f' % v

def IntegerFormater(v):
    return '%i' % int(v)

class FloatSlider(wx.Panel):
    #xwSlider::SetSelection     win95
    #wxSlider::GetThumbLength   win95
    #wxSlider::SetThumbLength   win95
    #wxSlider::GetTickFreq      no ticks
    #wxSlider::SetTick          no ticks
    #wxSlider::SetTickFreq      no ticks

    def __init__(self,
                 parent,
                 id=-1,
                 value=0,
                 minValue=0,
                 maxValue=100,
                 pos=wx.DefaultPosition,
                 size=wx.DefaultSize,
                 style=wx.SL_HORIZONTAL,
                 validator=wx.DefaultValidator,
                 name=wx.SliderNameStr,
                 resolution=1000,
                 formater=FloatFormater):

        unsupported_styles = wx.SL_VERTICAL | wx.SL_AUTOTICKS | \
                             wx.SL_LEFT | wx.SL_RIGHT | wx.SL_TOP | wx.SL_BOTTOM | \
                             wx.SL_SELRANGE | wx.SL_INVERSE
        # supported_styles   = wx.SL_HORIZONTAL | wx.SL_LABELS

        if style & unsupported_styles:
            raise ValueError("FloatSlider: construction with unsupported styles")

        self.label_min = None
        self.label_val = None
        self.label_max = None
        self.resolution= resolution
        self.formater  = formater

        pnl_style = style
        pnl_style = pnl_style | wx.TAB_TRAVERSAL
        super(FloatSlider, self).__init__(parent, id=id, pos=pos, size=size, style=pnl_style)

        if (style & wx.SL_LABELS):
            self.label_min = wx.StaticText(self, -1, formater(minValue))
            self.label_val = wx.StaticText(self, -1, formater(value),    style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
            self.label_max = wx.StaticText(self, -1, formater(maxValue), style=wx.ALIGN_RIGHT  | wx.ST_NO_AUTORESIZE)

        self.slider = wx.Slider(self, -1, style = wx.SL_HORIZONTAL)

        self.__do_layout()

        self.slider.Bind(wx.EVT_SCROLL_TOP, self.OnScroll)
        self.slider.Bind(wx.EVT_SCROLL_BOTTOM, self.OnScroll)
        self.slider.Bind(wx.EVT_SCROLL_LINEUP, self.OnScroll)
        self.slider.Bind(wx.EVT_SCROLL_LINEDOWN, self.OnScroll)
        self.slider.Bind(wx.EVT_SCROLL_PAGEUP, self.OnScroll)
        self.slider.Bind(wx.EVT_SCROLL_PAGEDOWN, self.OnScroll)
        self.slider.Bind(wx.EVT_SCROLL_THUMBTRACK, self.OnScroll)
        self.slider.Bind(wx.EVT_SCROLL_THUMBRELEASE, self.OnScroll)
        self.slider.Bind(wx.EVT_SCROLL_CHANGED, self.OnScroll)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_ENDSCROLL, self.OnScroll)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL, self.OnScroll)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_TOP, self.OnScroll)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_BOTTOM, self.OnScroll)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_LINEUP, self.OnScroll)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_LINEDOWN, self.OnScroll)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_PAGEUP, self.OnScroll)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_PAGEDOWN, self.OnScroll)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_THUMBTRACK, self.OnScroll)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_THUMBRELEASE, self.OnScroll)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_CHANGED, self.OnScroll)

        self.SetRange(minValue, maxValue)
        self.SetValue(value)

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)
        if (self.label_min):
            szr_lbl = wx.BoxSizer(wx.HORIZONTAL)
            szr_lbl.Add(self.label_min, 1, wx.EXPAND, 0)
            szr_lbl.Add(self.label_val, 1, wx.EXPAND, 0)
            szr_lbl.Add(self.label_max, 1, wx.EXPAND, 0)
            szr_main.Add(szr_lbl, 1, wx.EXPAND, 0)

        szr_main.Add(self.slider, 1, wx.EXPAND, 0)
        self.SetSizer(szr_main)

    def OnScroll(self, ev):
        v = ev.GetPosition() / self.resolution
        ev.SetPosition(v)
        if self.label_val: self.label_val.SetLabel( self.formater(v) )
        ev.Skip()

    def GetLineSize(self):
        return self.slider.GetLineSize() / self.resolution

    def GetMax(self):
        return self.slider.GetMax() / self.resolution

    def GetMin(self):
        return self.slider.GetMin() / self.resolution

    def GetPageSize(self):
        return self.slider.GetPageSize() / self.resolution

    def GetSelEnd(self):
        return self.slider.GetSelEnd() / self.resolution

    def GetSelStart(self):
        return self.slider.GetSelStart() / self.resolution

    def GetValue(self):
        return self.slider.GetValue() / self.resolution

    def SetLineSize(self, v):
        self.slider.SetLineSize(v*self.resolution)

    def SetMax(self, vmax):
        self.slider.SetMax(vmax*self.resolution)
        if self.label_max: self.label_max.SetLabel( self.formater(vmax) )

    def SetMin(self, vmin):
        self.slider.SetMin(vmin*self.resolution)
        if self.label_min: self.label_min.SetLabel( self.formater(vmin) )

    def SetPageSize(self, v):
        self.slider.SetPageSize(v*self.resolution)

    def SetRange(self, vmin, vmax):
        self.slider.SetRange(vmin*self.resolution, vmax*self.resolution)
        if self.label_min: self.label_min.SetLabel( self.formater(vmin) )
        if self.label_max: self.label_max.SetLabel( self.formater(vmax) )

    def SetValue(self, v):
        self.slider.SetValue(v*self.resolution)
        if self.label_val: self.label_val.SetLabel( self.formater(v) )


if __name__ == '__main__':
    def on_scroll(ev):
        print('in on_scroll', ev.GetPosition())

    def formater(v):
        return '#%f#' % v

    app = wx.App()

    frame = wx.Frame(None, -1)
    slider = FloatSlider(frame, style=wx.SL_LABELS, size=(300,150)) #, resolution=64325, formater=formater)

    slider.Bind(wx.EVT_COMMAND_SCROLL_ENDSCROLL, on_scroll)
#    slider.SetRange(-1.0, 1.23456)

    frame.Show()
    app.MainLoop()




