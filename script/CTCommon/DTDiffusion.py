#!/usr/bin/env python
# -*- coding: utf-8 -*-
# cython: linetrace=True
#************************************************************************
# --- Copyright (c) INRS 2016
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Diffusion along a path
"""

import numpy as np
import math

import logging
LOGGER = logging.getLogger("INRS.H2D2.Tools.Data.Diffusion")

def Gauss2D_getSigma(v, x):
    """
    Get Sigma so that gauss(x) = v
    """
    s = x*x / (2 * math.log(v))
    return math.sqrt(-s)

class Gauss2D:
    """
    2D anisotropic Gauss distribution with:
        amplitude (a)
        standard dev in x (sx)
        standard dev in y (sy)
    """
    def __init__(self, a=1.0, sx=1.0, sy=1.0):
        self.a  = a
        self.sx2 = 2*sx*sx
        self.sy2 = 2*sy*sy

    def val(self, x, y):
        """
        Value at point x, y
        """
        ex = math.exp(-(x*x)/self.sx2)
        ey = math.exp(-(y*y)/self.sy2)
        c = self.a
        return c * ex * ey

    def solvex(self, v, y=0.0):
        ey = math.exp(-(y*y)/self.sy2)
        x = v / (ey*self.a)
        x = math.log(x) * self.sx2
        return math.sqrt(-x)

    def solvey(self, v, x=0.0):
        ex = math.exp(-(x*x)/self.sx2)
        y = v / (ex*self.a)
        y = math.log(y) * self.sy2
        return math.sqrt(-y)

    def ddx(self, x, y):
        sxsx = 0.5*self.sx2
        ex = math.exp(-(x*x)/self.sx2)
        ey = math.exp(-(y*y)/self.sy2)
        f = x / sxsx
        c = self.a
        return - c * f * ex * ey

    def ddy(self, x, y):
        sysy = 0.5*self.sy2
        ex = math.exp(-(x*x)/self.sx2)
        ey = math.exp(-(y*y)/self.sy2)
        f = y / sysy
        c = self.a
        return - c * f * ex * ey

    def d2dx2(self, x, y):
        sxsx = 0.5*self.sx2
        ex = math.exp(-(x*x)/self.sx2)
        ey = math.exp(-(y*y)/self.sy2)
        c = self.a
        f = (x*x - sxsx) / (sxsx**2)
        return c * f * ex * ey

    def d2dy2(self, x, y):
        sysy = 0.5*self.sy2
        ex = math.exp(-(x*x)/self.sx2)
        ey = math.exp(-(y*y)/self.sy2)
        c = self.a
        f = (y*y - sysy) / (sysy**2)
        return c * f * ex * ey

    def draw(self):
        import matplotlib.pyplot as plt
        X  = np.linspace(-2, 2, 100)
        Y  = [ self.val(x, 0) for x in X]
        Yp = [ self.ddx(x, 0) for x in X]
        plt.plot(X,Y)
        plt.plot(X,Yp)
        plt.draw()
        plt.show()

    def intgNum(self):
        X = np.linspace(-15.0, 15.0, 1000)
        Y = np.linspace(-15.0, 15.0, 1000)
        sum = 0.0
        for x in X:
            for y in Y:
                sum += self.val(x, y)
        sum = sum * (X[1]-X[0]) * (Y[1]-Y[0])
        return sum

    def intg(self):
        return math.pi * self.a * math.sqrt(self.sx2 * self.sy2)

def ODEIntegrator_euler(f, x0, t0, dt, fargs):
    x = x0 + dt * f(x0, t0, fargs)
    return x

def ODEIntegrator_rk4(f, x0, t0, dt, fargs):
    t, x = t0, x0
    k1 = dt * f(x, t, fargs)
    k2 = dt * f(x + 0.5 * k1, t + 0.5 * dt, fargs)
    k3 = dt * f(x + 0.5 * k2, t + 0.5 * dt, fargs)
    k4 = dt * f(x + k3, t + dt, fargs)
    x = x0 + (k1 + 2.0 * ( k2 + k3 ) + k4) / 6.0
    return x

class DTDiffusion:
    """
    Abstract class to compute diffusion along a path
    """

    def __init__(self):
        self.diffItems = []

    def addItemDiffusion(self, t, x, y, a, w, h, g):
        self.diffItems.append( (t, x, y, a, w, h, g) )

    def xeqOnePath(self, path):
        raise NotImplementedError

    def xeq(self, path):
        """
        Return a list of diffusion items. Each item is described as:
        e = [t, x, y, amp, width, height, angle]
        where:
            t is the time
            x, y is the position
            amp is the amplitude at (x,y)
            rmax, rmin, angle is the diffusion ellipse
        """
        self.diffItems = []
        self.xeqOnePath(path)
        return self.diffItems

class DTDiffusion2DH2D2(DTDiffusion):
    idxs = np.array((0, 1, 2, 3, 4, 7, 10, 11))
    def __init__(self, diff_m = 1.0e-9, coef_v=1.0, coef_h=1.0, coef_l=1.0):
        """
        2D Diffusion
        Coefficients have the same meaning as in H2D2
        """
        self.diff_m = diff_m
        self.coef_v = coef_v
        self.coef_h = coef_h
        self.coef_l = coef_l
        super(DTDiffusion2DH2D2, self).__init__()

    # ---  Diffusivities trans & long
    def getDiffusivities(self, H, Dv, Dh):
        diff_c = self.diff_m
        diff_v = self.coef_v*(0.15*H*Dv)
        diff_h = self.coef_h*Dh
        Dtt = diff_c + (diff_v + diff_h)
        Dll = diff_c + (diff_v + diff_h)*self.coef_l
        #print((H, Dv, Dh), (diff_c, diff_v, diff_h), (Dtt, Dll))
        return Dtt, Dll

    def xtrValuesFromPath(self, p):
        t, x, y, u, v, H, Dv, Dh = p[ DTDiffusion2DH2D2.idxs ]
        H = max(1.0e-3, H)
        Dtt, Dll = self.getDiffusivities(H, Dv, Dh)
        return np.array( (t, x, y, u, v, H, Dtt, Dll) )

    def xtrDeltasFromPath(self, p):
        t, x, y, u, v, H, Dv, Dh = p[ DTDiffusion2DH2D2.idxs ]
        Dtt, Dll = self.getDiffusivities(H, Dv, Dh)
        return np.array( (t, x, y, u, v, H, Dtt, Dll) )


class DTDiffusion2DH2D2_Surface(DTDiffusion2DH2D2):
    """
    Gaussian diffusion.
    The initial distribution has an amplitude of 1.0 and passes through the point (rini, dilmin).
    Ellipses represent the area with value above dilmin.
    """
    def __init__(self, diff_m = 1.0e-9, coef_v=1.0, coef_h=1.0, coef_l=1.0, dilmin=1.0e-4, rini=1.0):
        super(DTDiffusion2DH2D2_Surface, self).__init__(diff_m, coef_v, coef_h, coef_l)
        self.dilmin = dilmin
        self.rini   = rini

    def xeqOnePath(self, path):
        p = path[0]
        t0, x0, y0, u0, v0, H0, Dtt0, Dll0 = self.xtrValuesFromPath(p)
        a0  = 1.0
        sl0 = Gauss2D_getSigma(self.dilmin, self.rini)
        st0 = Gauss2D_getSigma(self.dilmin, self.rini)
        gauss = Gauss2D(a0, sl0, st0)
        rl0 = gauss.solvex(self.dilmin) if self.dilmin else 2 * sl0   # rayon_long
        rt0 = gauss.solvey(self.dilmin) if self.dilmin else 2 * st0   # rayon trans
        self.addItemDiffusion(t0, x0, y0, a0, rl0, rt0, math.atan2(v0, u0))
        LOGGER.debug('DTDiffusion2DH2D2_Surface: %9.3e (%9.3f, %9.3f) (%9.3f, %9.3f)' % (a0, sl0, st0, rl0, rt0))
        for p0, p1 in zip(path[0:], path[1:]):
            t0, x0, y0, u0, v0, H0, Dtt0, Dll0 = self.xtrValuesFromPath(p0)
            t1, x1, y1, u1, v1, H1, Dtt1, Dll1 = self.xtrValuesFromPath(p1)

            # ---  New standard deviation
            dt = t1-t0
            sl1 = math.sqrt(2.0*Dll1*dt + sl0*sl0)
            st1 = math.sqrt(2.0*Dtt1*dt + st0*st0)
            a1  = (a0*sl0*st0) / (sl1*st1)         # Amplitude
            if a1 < self.dilmin: break

            # ---  Add a new ellipse
            gauss = Gauss2D(a1, sl1, st1)
            rl1 = gauss.solvex(self.dilmin) if self.dilmin else 2 * sl1   # rayon long
            rt1 = gauss.solvey(self.dilmin) if self.dilmin else 2 * st1   # rayon trans
            self.addItemDiffusion(t1, x1, y1, a1, rl1, rt1, math.atan2(v1, u1))

            a0, sl0, st0 = a1, sl1, st1
            LOGGER.debug('                           %9.3e (%9.3f, %9.3f) (%9.3f, %9.3f)' % (a1, sl1, st1, rl1, rt1))
        #dt = (p1[0]-path[0][0])
        #dtdone = dt / (path[-1][0]-path[0][0])
        #LOGGER.info('DTDiffusion2DH2D2_Surface: %9.1f %9.3e %6.2f%%' % (dt, a1, dtdone*100))

class DTDiffusion2DH2D2_DepthAverage(DTDiffusion2DH2D2):
    """
    Depth average diffusion
    """
    def __init__(self, diff_m = 1.0e-9, coef_v=1.0, coef_h=1.0, coef_l=1.0, dilmin=1.0e-4, rini=1.0):
        super(DTDiffusion2DH2D2_DepthAverage, self).__init__(diff_m, coef_v, coef_h, coef_l)
        self.dilmin = dilmin
        self.rini   = rini

    def f(self, a, t, fargs):
        dl, p, dpds, sl, st = fargs
        # ---  Les prof et diffusivités au point de calcul
        dHdl = self.xtrDeltasFromPath(dpds)[5]
        H, Dt, Dl = self.xtrValuesFromPath(p)[ (5,6,7), ]
        # ---  Les dérivées
        gauss = Gauss2D(a, sl, st)
        #dcdl   = gauss.ddx  (0.0, 0.0) # == 0
        #dcdt   = gauss.ddy  (0.0, 0.0) # == 0
        d2cdl2 = gauss.d2dx2(0.0, 0.0)
        d2cdt2 = gauss.d2dy2(0.0, 0.0)
        # ---  La diff totale
        difl = H*Dl*d2cdl2  # + Dl*dcdl*dHdl + H*dcdl*dDldl
        dift = H*Dt*d2cdt2
        diff = difl + dift
        #print('d2cdl2: ', difl, dift)
        # ---  Variation
        rhs = diff / (H+dHdl*dl)
        #print(H, H+dHdl*dl, da)
        return rhs

    def xeqOnePath(self, path):
        #p = path[0]
        t0, x0, y0, u0, v0, H0, Dtt0, Dll0 = self.xtrValuesFromPath(path[ 0])
        t1, x1, y1, u1, v1, H1, Dtt1, Dll1 = self.xtrValuesFromPath(path[-1])
        a  = 1.0
        sl = Gauss2D_getSigma(self.dilmin, self.rini)
        st = Gauss2D_getSigma(self.dilmin, self.rini)
        gauss = Gauss2D(a, sl, st)
        rl = gauss.solvex(self.dilmin) if self.dilmin else 2 * sl   # rayon_long
        rt = gauss.solvey(self.dilmin) if self.dilmin else 2 * st   # rayon trans
        self.addItemDiffusion(t0, x0, y0, a, rl, rt, math.atan2(v0, u0))
        LOGGER.debug('DTDiffusion2DH2D2_DepthAvg: Start   %9.3f %9.3f %i' % (t0, t1, len(path)))
        LOGGER.debug('DTDiffusion2DH2D2_DepthAvg: Add pnt %9.3f %9.3e (%9.3f %9.3f) (%9.3f %9.3f)' % (t0, a, sl, st, rl, rt))

        # ---  Loop on path points
        t_ori, sl_ori, st_ori = t0, sl, st
        for p0, p1 in zip(path[0:], path[1:]):
            t0, x0, y0, u0, v0, H0, Dtt0, Dll0 = self.xtrValuesFromPath(p0)
            t1, x1, y1, u1, v1, H1, Dtt1, Dll1 = self.xtrValuesFromPath(p1)

            dp10 = p1-p0
            dt10 = t1-t0
            #ds10 = math.sqrt((x1-x0)**2 + (y1-y0)**2)   # distance parcourue

            dsdt = 1.0 / dt10

            s = 0.0                 # [0, 1]
            t = t0
            dt = dt10 / 100         # pas de temps initial
            nstbl = 0
            LOGGER.debug('DTDiffusion2DH2D2_DepthAvg: New seg %9.3f %9.3f %9.3e' % (t0, t1, dt))
            while t < t1:
                dt = min(dt, t1-t)
                ds = dt * dsdt

                p = p0 + s*dp10     # pt de calcul
                # ---  f modifie a, mais pas st-sl. Il est plus stable d'utiliser
                # ---  Euler que rk4 qui a des pas intermédiaires
                a1 = ODEIntegrator_euler(self.f, a, t, dt, (ds, p, dp10, sl, st))
                da = a1 - a
                #print('%9.3f: %9.3e %9.3e %9.3e %9.3e' % (t, dt, a, a1, da))

                # ---  Simple convergence controller
                if abs(da) > 5.0e-4 or da > 0.0 or a1 < 0.0:
                    dt = dt / 2.0
                    nstbl = 0
                    #print('Step rejected: %.6e %.6e %.6e' % (dt, a1, da))
                    continue

                # ---  Step values
                dH = ds*self.xtrDeltasFromPath(dp10)[5]
                Hold, Dt, Dl = self.xtrValuesFromPath(p)[ [5,6,7] ]
                Hnew = max(1.0e-3, Hold+dH)
                # ---  New sigma-long & lat respecting volume
                try:
                    st = st * math.sqrt((Hold*a) / (Hnew*a1))
                    sl = sl * math.sqrt((Hold*a) / (Hnew*a1))
                except ValueError:
                    print(Hold, Hnew, a, a1)
                    raise
                # ---  Correct aspect-ratio to reproduce DTDiffusion2DH2D2_Surface ratio
                try:
                    sl_srf = math.sqrt(2.0*Dl*(t+dt-t_ori) + sl_ori*sl_ori)
                    st_srf = math.sqrt(2.0*Dt*(t+dt-t_ori) + st_ori*st_ori)
                except:
                    print(t, dt, t_ori, t+dt-t_ori, Dl, sl_ori)
                    print(Dl*(t+dt-t_ori), 2.0*Dl*(t+dt-t_ori) + sl_ori*sl_ori)
                    print(Dtt0, Dll0)
                    print(Dtt1, Dll1)
                    print(s, dp10)
                    raise
                ratio = math.sqrt((sl_srf/st_srf) * (st/sl))
                st = st / ratio
                sl = sl * ratio
                #print(Hnew, st, sl, sl/st, ratio)
                # ---  Update
                a = a1
                t += dt
                s += ds
                # ---  Simple convergence controller book-keeping
                nstbl += 1
                if da > -1.0e-4 and da < 0.0 and nstbl > 3:
                    dt = dt * math.sqrt(2.0)
                    #print('Step increase: %9.3e %9.6e' % (dt, da))
                    nstbl = 0
                # ---  Exit condition
                if a < self.dilmin: break
                #if da > 0.0:
                #    gauss = Gauss2D(a, sl, st)
                #    print('Amplitude: %.6e %.6e %.6e' % (t, a, Hnew*gauss.intg()), da)
                #raise

            # ---  Exit condition
            if a < self.dilmin: break

            # ---  Add a new ellipse
            gauss = Gauss2D(a, sl, st)
            rl = gauss.solvex(self.dilmin) if self.dilmin else 2 * sl   # rayon_long
            rt = gauss.solvey(self.dilmin) if self.dilmin else 2 * st   # rayon trans
            self.addItemDiffusion(t1, x1, y1, a, rl, rt, math.atan2(v1, u1))

            LOGGER.debug('DTDiffusion2DH2D2_DepthAvg: Add pnt %9.3f %9.3e (%9.3f %9.3f) (%9.3f %9.3f)' % (t1, a, sl, st, rl, rt))

if __name__ == "__main__":
    def main():
        #         t    x    y    u    v    -    -    H    -    -    u*  nu_p
        p0 = [   0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        p1 = [   1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        p2 = [   2.0, 2.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        p3 = [   3.0, 3.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        p4 = [   4.0, 4.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        p5 = [ 3600.0, 5.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]

        kwargs = {}
        kwargs['diff_m'] = 1.0e-9
        kwargs['coef_h'] = 1.0e-1
        kwargs['coef_l'] = 10.0
        kwargs['dilmin'] = 1.0e-6
        kwargs['rini']   = 100.0
        pts = [p0, p1, p2, p3, p4, p5]
        algo = DTDiffusion2DH2D2_Surface(**kwargs)
        ellps = algo.xeq( np.array(pts) )
        #print ellps
        algo = DTDiffusion2DH2D2_DepthAverage(**kwargs)
        ellps = algo.xeq( np.array(pts) )
        #print ellps

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    main()
            