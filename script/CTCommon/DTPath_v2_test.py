#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Data path algo
One makes the assumption that data is LINEARLY interpolated in time
"""

import os
#import sys
#selfDir = os.path.dirname(os.path.abspath(__file__))
#supPath = os.path.normpath(os.path.join(selfDir, '..'))
#if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import logging
import numpy as np

LOGGER = logging.getLogger("INRS.H2D2.Tools.Data.Path")

from . import DTPath_v2 as DTPath
from . import FEMesh
from . import FEMeshIO
from . import DTData
from . import DTDataIO
from . import DTField

def main():
    d = 'E:/Projets_simulation/LacErie/1x'
    #srs_proj = IPRaster.IPSpatialReference()
    #srs_grid = IPRaster.IPSpatialReference()
    #srs_proj.ImportFromFileAndGuess(os.path.join(d, 'erie_nad83.prj'))
    #srs_grid.ImportFromFileAndGuess(os.path.join(d, 'erie_utm.prj'))
    #grid2proj = IPRaster.IPCoordinateTransformation(srs_grid, srs_proj)
    #proj2grid = IPRaster.IPCoordinateTransformation(srs_proj, srs_grid)

    fpath = [os.path.join(d, 'erie_t6l.cor'), os.path.join(d, 'erie_t6l.ele')]
    #grid2proj = None
    m = FEMesh.FEMesh(fpath, withLocalizer=True)
    #m = FEMesh.FEMesh(fpath, projection=grid2proj, withLocalizer=True)

    d = 'E:/Projets_simulation/LacErie/1x/rdps/cw6'
    f = os.path.join(d, 'simul000.pst.sim')
    d = DTData.DTData(f)

    c = DTField.DT2DFiniteElementField(m, d)
    p = DTPath.DTAlgoPathT3()
    tini  = 1472688600.0
    dtsim = 10*3600.0
    dtdsp = 60.0
    dtclc = 15.0
    #X = (-83.131,)
    #Y = (41.988,)
    X = ( 323360.229,)
    Y = (4653642.919,)
    LOGGER.info('main: (tmin, tmax) (%.3f, %.3f)', tini, tini+dtsim)
    #print(proj2grid.TransformPoints(zip(X, Y)))
    path = p.xeq(X, Y, c, [], [tini], dtsim, dtdsp, dtclc)
    print(path)

#formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
formatter = logging.Formatter('%(asctime)s - %(message)s')
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)
LOGGER.addHandler(streamHandler)
LOGGER.setLevel(logging.DEBUG)

#import cProfile
#cProfile.run('main()')
main()
