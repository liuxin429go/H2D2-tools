set CC=%INRS_BLD%\compile_cython

pushd external
call %CC% pyqtree.pxd
call %CC% pyQT.pxd
popd

call %CC% CTEXception.pxd

call %CC% DTReducOperation.pxd
call %CC% DTDataBloc.pxd
call %CC% DTData.pxd
call %CC% DTDataIO.pxd

call %CC% FEMesh.pxd
call %CC% FEMeshIO.pxd

call %CC% DTField.pxd
::call %CC% DTPath.pxd
call %CC% DTPath_v2.pxd
call %CC% DTDiffusion.pxd
