# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from . import CTUtil

import matplotlib
import colorsys
import copy

ccv = rgb = matplotlib.colors.ColorConverter()

class AlgoStats:
    def __init__(self):
        self.wmin =  1.0e+99
        self.wmax = -1.0e+99
        self.smin =  1.0e+99
        self.smax = -1.0e+99
        self.imin =  1.0e+99
        self.imax = -1.0e+99
        self.npas = 0
        self.nseq = 0
        self.nnod = 0
        self.nalg = 0
        self.nitr = 0
        self.cnvg = False

    def __str__(self):
        return str([self.npas, self.nseq, self.nnod, self.nalg, self.nitr])

    def __add__(self, o):
        self.wmin = min(self.wmin, o.wmin)
        self.wmax = max(self.wmax, o.wmax)
        self.smin = min(self.smin, o.smin)
        self.smax = max(self.smax, o.smax)
        self.imin = min(self.imin, o.imin)
        self.imax = max(self.imax, o.imax)
        self.npas += o.npas
        self.nseq += o.nseq
        self.nnod += o.nnod
        self.nalg += o.nalg
        self.nitr += o.nitr
        self.cnvg &= o.cnvg
        return self

    def as_text(self):
        lines = [
            'Wall time:',
            '  total: %s' % CTUtil.seconds_to_iso(self.wmax),
            '  per step: %.2f' % (float(self.wmax)/self.npas),
            '  per iter: %.2f' % (float(self.wmax)/self.nitr),
            'Simulation time:',
            '  from: %i(s)' % self.smin,
            '  to:   %i(s)' % self.smax,
            'Time steps: %i' % self.npas,
            '  from: %i' % self.imin,
            '  to:   %i' % self.imax,
            'Sequences:',
            '  total: %i' % self.nseq,
            '  per step: %.2f' % (float(self.nseq)/self.npas),
            'Nodes:',
            '  total: %i' % self.nnod,
            '  per sqnc: %.2f' % (float(self.nnod)/self.nseq),
            '  per step: %.2f' % (float(self.nnod)/self.npas),
            'Algorithms:',
            '  total: %i' % self.nalg,
            '  per node: %.2f' % (float(self.nalg)/self.nnod),
            '  per sqnc: %.2f' % (float(self.nalg)/self.nseq),
            '  per step: %.2f' % (float(self.nalg)/self.npas),
            'Iterations:',
            '  total: %i' % self.nitr,
            '  per algo: %.2f' % (float(self.nitr)/self.nalg),
            '  per node: %.2f' % (float(self.nitr)/self.nnod),
            '  per sqnc: %.2f' % (float(self.nitr)/self.nseq),
            '  per step: %.2f' % (float(self.nitr)/self.npas),
            ]
        return '\n'.join( lines )

class AlgoBBox:
    def __init__(self, xmin=1.0e99, xmax=-1.0e99, ymin=1.0e99, ymax=-1.0e99):
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax

    def filter(self, x, y):
        self.xmin = min(self.xmin, x)
        self.xmax = max(self.xmax, x)
        self.ymin = min(self.ymin, y)
        self.ymax = max(self.ymax, y)

    def is_inside(self, x, y = None):
        if (x < self.xmin): return False
        if (x > self.xmax): return False
        if (y and y < self.ymin): return False
        if (y and y > self.ymax): return False
        return True

class AlgoIterator:
    def __init__(self, ctnr):
        self.stck = []
        self.stck.append ( [ctnr, 0] )

    def __iter__(self):
        return self

    def __next__(self):
        # ---  End of container --> pop recursively
        done = False
        while (not done):
            try:
                algo, indx = self.stck[-1]
                if (indx >= len(algo.entries)):
                    self.stck.pop()
                else:
                    done = True
            except:
                raise StopIteration

        # ---  Get current value
        algo, indx = self.stck[-1]
        cur = algo.entries[indx]
        self.stck[-1][1] += 1

        # ---  Try to descend --> push
        algo, indx = self.stck[-1]
        try:
            branch = algo.entries[indx-1].entries
            self.stck.append( [algo.entries[indx-1], 0] )
        except AttributeError:      # no .entries -->  no branch
            pass

        return cur

class AlgoContainer:
    def __init__(self, parent = None):
        self.typename= self.__class__.__name__
        self.parent  = parent
        self.value   = None
        self.entries = []
        self.bbox    = AlgoBBox()
        self.stts    = AlgoStats()

    def append(self, entry, value, Algo):
        if (value != self.value):
            self.entries.append( Algo(self) )
            self.value = value
        self.entries[-1].append(entry)
        self.bbox.filter(entry.x, entry.cria)

    def count(self):
        self.init_count()
        for e in self.entries:
            e.count()
            self.stts += e.stts

    def filter_bbox(self):
        self.bbox = AlgoBBox()
        for e in self.entries:
            e.filter_bbox()
            self.bbox.filter(e.bbox.xmin, e.bbox.ymin)
            self.bbox.filter(e.bbox.xmax, e.bbox.ymax)

    def get_rangex(self):
        return self.bbox.xmin, self.bbox.xmax

    def get_rangey(self):
        return self.bbox.ymin, self.bbox.ymax

    def get_stats(self):
        return self.stts.as_text()

    def resourcex(self, attrib):
        for e in self.entries:
            e.resourcex(attrib)

    def slice(self, cb, xmin = -1.0e99, xmax=1.0e99):
        if (self.bbox.xmin > xmax): return
        if (self.bbox.xmax < xmin): return
        for e in self.entries:
            e.slice(cb, xmin, xmax)

    def filter_on_entries(self, cb, fltr):
        for e in self.entries:
            e.filter_on_entries(cb, fltr)

    def filter_on_container(self, cb, fltr):
        if (fltr(self)):
            cb(self)
        else:
            for e in self.entries:
                e.filter_on_container(cb, fltr)

    def __iter__(self):
        return AlgoIterator(self)

class AlgoConvergence(AlgoContainer):
    def __init__(self, parent = None):
        super(AlgoConvergence, self).__init__(parent)
        self.sourcex = 'wtime'

    def init_count(self):
        self.stts = AlgoStats()

    def append(self, entry):
        if (len(self.entries) == 0):
            AlgoTimeStep.reset_counter()
            Entry.reset_counter()
        entry.set_x(self.sourcex)
        AlgoContainer.append(self, entry, entry.stime, AlgoTimeStep)

    def resourcex(self, attrib):
        self.sourcex = attrib
        AlgoContainer.resourcex(self, self.sourcex)
#        for e in self.entries:
#            e.resourcex(self.sourcex)
        self.filter_bbox()

    def set_sourcex(self, attrib):
        self.sourcex = attrib


class AlgoTimeStep(AlgoContainer):
    counter = 0

    @staticmethod
    def reset_counter():
        AlgoTimeStep.counter = 0

    def __init__(self, parent = None):
        super(AlgoTimeStep, self).__init__(parent)
        AlgoTimeStep.counter += 1
        self.ipas = AlgoTimeStep.counter

    def init_count(self):
        self.stts = AlgoStats()
        self.stts.npas = 1

    def append(self, entry):
        AlgoContainer.append(self, entry, entry.iseq, AlgoSequence)

class AlgoSequence(AlgoContainer):
    def __init__(self, parent = None):
        super(AlgoSequence, self).__init__(parent)
        self.nitr = 0

    def append(self, entry):
        self.nitr += 1
        entry.itsq = self.nitr
        AlgoContainer.append(self, entry, entry.inod, AlgoNode)

    def init_count(self):
        self.stts = AlgoStats()
        self.stts.nseq = 1

class AlgoNode(AlgoContainer):
    def __init__(self, parent = None):
        super(AlgoNode, self).__init__(parent)

    def init_count(self):
        self.stts = AlgoStats()
        self.stts.nnod = 1

    def count(self):
        AlgoContainer.count(self)
        self.stts.cnvg = self.entries[-1].stts.cnvg

    def append(self, entry):
        AlgoContainer.append(self, entry, entry.algo, AlgoAlgo)

class AlgoAlgo(AlgoContainer):
    def __init__(self, parent = None):
        super(AlgoAlgo, self).__init__(parent)

    def append(self, entry):
        if (not self.value):
            self.value = entry.algo
        entry.ipas = self.parent.parent.parent.ipas
        self.entries.append( copy.deepcopy(entry) )
        self.bbox.filter(entry.x, entry.cria)

    def count(self):
        self.stts = AlgoStats()
        self.stts.nalg = 1
        self.stts.nitr = len(self.entries)
        if (len(self.entries) > 0):
            self.stts.xmin = self.entries[ 0].x
            self.stts.xmax = self.entries[-1].x
            self.stts.wmin = self.entries[ 0].wtime
            self.stts.wmax = self.entries[-1].wtime
            self.stts.smin = self.entries[ 0].stime
            self.stts.smax = self.entries[-1].stime
            self.stts.imin = self.entries[ 0].ipas
            self.stts.imax = self.entries[-1].ipas
            self.stts.cnvg = (self.entries[-1].cria < 1.0)

    def filter_bbox(self):
        self.bbox = AlgoBBox()
        for e in self.entries:
            self.bbox.filter(e.x, e.cria)

    def filter_on_entries(self, cb, fltr):
        for e in self.entries:
            if fltr(e): cb(e)

    def filter_on_container(self, cb, fltr):
        if (fltr(self)):
            cb(self)

    def resourcex(self, attrib):
        for e in self.entries:
            e.set_x(attrib)

    def slice(self, cb, xmin=-1.0e99, xmax=1.0e99):
        if (self.bbox.xmin > xmax): return
        if (self.bbox.xmax < xmin): return
        for e in self.entries:
            if (e.x >= xmin and e.x <= xmax): cb(e)

class Entry:
    counter = 0

    @staticmethod
    def reset_counter():
        Entry.counter = 0

    def __init__(self):
        Entry.counter += 1
        self.index = Entry.counter
        self.x     = None
        self.wtime = 0.0
        self.stime = 0
        self.atime = 0
        self.ipas  = 0
        self.iseq  = 0
        self.inod  = 0
        self.algo  = ''
        self.iter  = 1
        self.itsq  = None
        self.cria  = 0.0

    def read(self, file):
        line = file.readline()
        try:
            if (line):
                toks = line.split()
                self.wtime = CTUtil.parse_to_seconds(toks[0])
                self.stime = int(toks[1])
                self.atime = int(toks[2])
                self.iseq  = int(toks[3])
                self.inod  = int(toks[4])
                self.algo  = toks[5]
                self.iter  = int(toks[6])
                self.cria  = float(toks[7])
        except Exception:
            line = ''
        return line

    def as_text(self):
        lines = [
            'Wall time:',
            '   time: %.2f(s)' % self.wtime,
            '   time: %s' % CTUtil.seconds_to_iso(self.wtime),
            'Simulation:',
            '   step: %i' % self.ipas,
            '   time: %i(s)' % self.stime,
            '   time: %s' % CTUtil.seconds_to_iso(self.stime),
            'Algorithm:',
            '   time: %i(s)' % self.atime,
            '   time: %s' % CTUtil.seconds_to_iso(self.atime),
            'Sequence index: %i' % self.iseq,
            'Node id: %i' % self.inod,
            'Algorithm: %s' % self.algo,
            'Iteration: %i / %i' % (self.iter, self.itsq),
            'Criterion: %.5e' % self.cria,
            ]
        return '\n'.join(lines)

    def __str__(self):
        return '%f %f' % (self.wtime, self.cria)

    def get_marker(self):
        if (self.algo == 'nwtn'):
            mrk = '^'
        elif (self.algo == 'pcrd'):
            mrk = 'v'
        else:
            mrk = '.'
        if (self.iter == 1):
            clr = 'cyan'
        else:
            clr = 'magenta'
        return (mrk, clr)

    def get_line(self):
        ind = (self.inod % 1000) - 1
        clr = CTUtil.get_clr(ind)
        r,g,b = ccv.to_rgb(clr)
        h,s,v = colorsys.rgb_to_hsv(r,g,b)
        v = CTUtil.clr_lum[self.algo]
        rgb = colorsys.hsv_to_rgb(h,s,v)
        stl = CTUtil.lin_stl[self.algo]
        return (stl, rgb)

    def set_x(self, attrib):
        self.x = self.__dict__[attrib]

class Filter:
    def __init__(self, expr):
        self.ast = compile(expr, '<string>', 'eval')

    def filter(self, var):
        return eval(self.ast, {}, var.__dict__)

if __name__ == '__main__':
    file = open(r'tracer.log', 'r')
    convhist = AlgoConvergence()

    entry = Entry()
    while (entry.read(file)):
        convhist.append(entry)
    file.close()

    convhist.filter_bbox()
    convhist.count()

    def fltr_cb(e):
        print(e)

    fltr = Filter('typename == "AlgoTimeStep"')
    convhist.filter_on_container(fltr_cb, fltr.filter)
