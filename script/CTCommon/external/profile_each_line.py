# -*- coding: utf-8 -*-
# https://gist.github.com/kylegibson/6583590
#
# https://stackoverflow.com/questions/3927628/how-can-i-profile-python-code-line-by-line

from decorator import decorator
from line_profiler import LineProfiler

@decorator
def profile_each_line(func, *args, **kwargs):
    profiler = LineProfiler()
    profiled_func = profiler(func)
    try:
        profiled_func(*args, **kwargs)
    finally:
        profiler.print_stats()