# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx
from wx.lib.mixins.listctrl import TextEditMixin

"""
The class should inherit from CheckListCtrlMixin but some methods
are private, preventing inheritance. In fact part of the code is
duplication of CheckListCtrlMixin
"""
class CTCheckListCtrlEdit(wx.ListCtrl, TextEditMixin):
    bmp_siz = (16,16)

    def __init__(self, *args, **kwds):
        col_name = kwds.pop("col_name", 'Expression')

        kwds["style"] = wx.LC_REPORT | wx.SUNKEN_BORDER | wx.LC_EDIT_LABELS

        wx.ListCtrl.__init__(self, *args, **kwds)
        TextEditMixin.__init__(self)

        # ---  Create image list of 2 bitmaps
        self.__imagelist_ = wx.ImageList(*self.bmp_siz)
        check_image   = self.__CreateBitmap(wx.CONTROL_CHECKED)
        uncheck_image = self.__CreateBitmap(0)
        self.uncheck_image = self.__imagelist_.Add(uncheck_image)
        self.check_image   = self.__imagelist_.Add(check_image)
        self.SetImageList(self.__imagelist_, wx.IMAGE_LIST_SMALL)

        self.__set_properties(col_name)
        self.__do_layout()

        self.Bind(wx.EVT_LIST_BEGIN_LABEL_EDIT, self.OnLabelEditBegin)
        self.Bind(wx.EVT_SIZE , self.OnResize)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnItemActivated)
        self.editor.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.Bind(wx.EVT_CHAR, self.OnCharEvent)

    def __set_properties(self, col_name):
        self.InsertColumn(0, '')
        self.InsertColumn(1, col_name)

    def __do_layout(self):
        col0_w = self.bmp_siz[0]+12
        col1_w = self.GetClientSize().GetWidth() - col0_w
        self.SetColumnWidth(0, col0_w)
        self.SetColumnWidth(1, col1_w)

    def __CreateBitmap(self, flag=0):
        """Create a bitmap of the platforms native checkbox. The flag
        is used to determine the checkboxes state (see wx.CONTROL_*)
        """
        w, h = self.bmp_siz
        bmp = wx.EmptyBitmap(*self.bmp_siz)
        dc = wx.MemoryDC(bmp)
        dc.Clear()
        wx.RendererNative.Get().DrawCheckBox(self, dc, (0, 0, w, h), flag)
        dc.SelectObject(wx.NullBitmap)
        return bmp

    def InsertItem(self, row, checked = False, text = '', editable=True):
        img_idx = 1 if checked else 0
        item = self.InsertImageItem (row, img_idx)
        self.SetItem(item, 1, text)
        self.SetItemData  (item, editable)

    def AppendItem(self, checked = False, text = '', editable=True):
        img_idx = 1 if checked else 0
        item = self.InsertImageItem (self.GetItemCount(), img_idx)
        self.SetItem(item, 1, text)
        self.SetItemData  (item, editable)

    def DeleteAllItems(self):
        for i in range(self.GetItemCount()):
            self.DeleteItem(0)

    def OnItemActivated(self, evt):
        self.ToggleItem(evt.m_itemIndex)

    def OnCharEvent(self, event):
        keycode  = event.GetKeyCode()
        ctrlDown = event.CmdDown()
        altDown  = event.AltDown()
        shiftDown= event.ShiftDown()

        if shiftDown: return
        if altDown: return

        n = self.GetItemCount()
        if (ctrlDown and keycode in [1]):     # Ctrl-A
            for i in range(n):
                self.CheckItem(i, True)
        elif (ctrlDown and keycode in [24]):  # Ctrl-X
            for i in range(n):
                self.CheckItem(i, False)
            i = self.GetFirstSelected()
            while i != -1:
                self.CheckItem(i, True)
                i = self.GetNextSelected(i)
        elif (ctrlDown and keycode in [26]):  # Ctrl-Z
            for i in range(n):
                self.CheckItem(i, False)
        else:
            event.Skip()

    def OnKeyDown(self, event):
        if (event.GetModifiers() == wx.MOD_CMD):
            c = event.GetKeyCode() - 48
            if c in range(10):
                self.editor.AppendText('{%i}' % c)
        else:
            event.Skip()

    def OnLabelEditBegin(self, event):
        row, col = event.m_itemIndex, event.m_col
        item = self.GetItem(row)
        if (col == 0):
            img_idx = item.GetImage()
            flag_check = (img_idx == 0)
            self.CheckItem(row, flag_check)
            event.Veto()
        else:
            isEditable = item.GetData() != 0
            if isEditable:
                event.Skip()
            else:
                event.Veto()

    def OnResize(self, event):
        col0_w = self.bmp_siz[0]+12
        col1_w = self.GetClientSize().GetWidth() - col0_w
        if (col1_w != self.GetColumnWidth(1)):
            self.SetColumnWidth(0, col0_w)
            self.SetColumnWidth(1, col1_w)

    def GetChecked(self):
        return [i for i in range(self.GetItemCount()) if self.IsChecked(i)]

    def GetCheckedItemCount(self):
        n = 0
        for i in range(self.GetItemCount()):
            if self.IsChecked(i): n = n + 1
        return n

    def CheckItem(self, row, check = True):
        img_idx = self.GetItem(row).GetImage()
        if img_idx == 0 and check is True:
            self.SetItemImage(row, 1)
            self.OnCheckItem(row, True)
        elif img_idx == 1 and check is False:
            self.SetItemImage(row, 0)
            self.OnCheckItem(row, False)

    def OnCheckItem(self, index, flag):
        pass

    def IsChecked(self, row):
        return self.GetItem(row).GetImage() == 1

    def ToggleItem(self, row):
        self.CheckItem(row, not self.IsChecked(row))

# end of class DAFilterListCtrl


if __name__ == "__main__":
    class TestFrame(wx.Frame):
        def __init__(self, *args, **kwds):
            kwds["style"] = wx.DEFAULT_FRAME_STYLE
            super(TestFrame, self).__init__(*args, **kwds)
            self.panel_1 = CTCheckListCtrlEdit(self, -1)

            self.__set_properties()
            self.__do_layout()

        def __set_properties(self):
            self.SetTitle("frame_1")
            self.panel_1.AppendItem(True,  "Checked item 1", False)
            self.panel_1.AppendItem(False, "Unchecked item 2", False)
            self.panel_1.AppendItem(True,  "Checked item 2", True)
            self.panel_1.AppendItem()

        def __do_layout(self):
            sizer_1 = wx.BoxSizer(wx.VERTICAL)
            sizer_1.Add(self.panel_1, 1, wx.EXPAND, 0)
            self.SetSizer(sizer_1)
            sizer_1.Fit(self)
            self.Layout()

    app = wx.App(False)
    frame_1 = TestFrame(None, -1, "")
    app.SetTopWindow(frame_1)
    frame_1.Show()
    app.MainLoop()
