# ----------------------------------------------------------------------------
# String history management
# ----------------------------------------------------------------------------

import wx

# Adapted from flatmenu.py
# Without path manipulation and directory change
# Correction of bugs

def GetMRUEntryLabel(n, path):
    """
    Returns the string used for the MRU list items in the menu.

    :param integer `n`: the index of the file name in the MRU list;
    :param string `path`: the full path of the file name.

    :note: The index `n` is 0-based, as usual, but the strings start from 1.
    """

    # we need to quote '&' characters which are used for mnemonics
    pathInMenu = path.replace("&", "&&")
    return "&%d %s"%(n + 1, pathInMenu)

class StringHistory:
    """
    The :class:`StringHistory` encapsulates a user interface convenience, the list of most
    recently visited strings as shown on a menu (usually the String menu).

    :class:`StringHistory` can manage one or more string menus. More than one menu may be
    required in an MDI application, where the string history should appear on each MDI
    child menu as well as the MDI parent frame.
    """

    def __init__(self, maxStrings=9, idBase=wx.ID_FILE1):
        """
        Default class constructor.

        :param integer `maxStrings`: the maximum number of strings that should be stored and displayed;
        :param integer `idBase`: defaults to ``wx.ID_FILE1`` and represents the id given to the first
         history menu item.

        :note: Since menu items can't share the same ID you should change `idBase` to one of
         your own defined IDs when using more than one :class:`StringHistory` in your application.
        """

        # The ID of the first history menu item (Doesn't have to be wxID_FILE1)
        self._idBase = idBase

        # Last n strings
        self._stringHistory = []

        # Menus to maintain (may need several for an MDI app)
        self._stringMenus = []

        # Max strings to maintain
        self._stringMaxStrings = maxStrings


    def GetMaxStrings(self):
        """ Returns the maximum number of strings that can be stored. """

        return self._stringMaxStrings


    # Accessors
    def GetHistoryString(self, index):
        """
        Returns the string at this index (zero-based).

        :param integer `index`: the index at which the string is stored in the string list (zero-based).
        """

        return self._stringHistory[index]


    def GetCount(self):
        """ Returns the number of strings currently stored in the string history. """

        return len(self._stringHistory)


    def GetMenus(self):
        """
        Returns the list of menus that are managed by this string history object.

        :see: :meth:`~StringHistory.UseMenu`.
        """

        return self._stringMenus


    # Set/get base id
    def SetBaseId(self, baseId):
        """
        Sets the base identifier for the range used for appending items.

        :param integer `baseId`: the base identifier for the range used for appending items.
        """

        self._idBase = baseId


    def GetBaseId(self):
        """ Returns the base identifier for the range used for appending items. """

        return self._idBase


    def GetNoHistoryStrings(self):
        """ Returns the number of strings currently stored in the string history. """

        return self.GetCount()


    def AddStringToHistory(self, fnNew):
        """
        Adds a string to the string history list, if the object has a pointer to an
        appropriate string menu.

        :param string `fnNew`: the string name to add to the history list.
        """

        # check if we don't already have this string
        numStrings = len(self._stringHistory)
        try:
            index = self._stringHistory.index(fnNew)
            self.RemoveStringFromHistory(index)
            numStrings -= 1
        except:
            pass

        # if we already have a full history, delete the one at the end
        if numStrings == self._stringMaxStrings:
            self.RemoveStringFromHistory(numStrings-1)

        # add a new menu item to all string menus
        for menu in self._stringMenus:
            if numStrings == 0 and menu.GetMenuItemCount() > 0:
                menu.AppendSeparator()
            menu.Append(self._idBase + numStrings, fnNew)

        # insert the new string in the beginning of the string history
        self._stringHistory.insert(0, fnNew)
        numStrings += 1


    def RemoveStringFromHistory(self, index):
        """
        Removes the specified string from the history.

        :param integer `index`: the zero-based index indicating the string name position in
         the string list.
        """

        numStrings = len(self._stringHistory)
        if index >= numStrings:
            raise Exception("Invalid index in RemoveStringFromHistory: %d (only %d strings)"%(index, numStrings))

        # delete the element from the array
        self._stringHistory.pop(index)
        numStrings -= 1

        for menu in self._stringMenus:
            # shift stringnames up
            for j in range(numStrings):
                menu.SetLabel(self._idBase + j, GetMRUEntryLabel(j, self._stringHistory[j]))

            # delete the last menu item which is unused now
            lastItemId = self._idBase + numStrings
            if menu.FindItemById(lastItemId):
                menu.Delete(lastItemId)

            if not self._stringHistory:
                lastMenuItem = menu.GetMenuItems()[-1]
                if lastMenuItem.IsSeparator():
                    menu.Delete(lastMenuItem)

                #else: menu is empty somehow


    def UseMenu(self, menu):
        """
        Adds this menu to the list of those menus that are managed by this string history
        object.

        :param `menu`: an instance of :class:`FlatMenu`.

        :see: :meth:`~StringHistory.AddStringsToMenu` for initializing the menu with stringnames that are already
         in the history when this function is called, as this is not done automatically.
        """

        if menu not in self._stringMenus:
            self._stringMenus.append(menu)


    def RemoveMenu(self, menu):
        """
        Removes this menu from the list of those managed by this object.

        :param `menu`: an instance of :class:`FlatMenu`.
        """

        self._stringMenus.remove(menu)


    def Load(self, config):
        """
        Loads the string history from the given `config` object.

        :param `config`: an instance of :class:`Config`.

        :note: This function should be called explicitly by the application.

        :see: :meth:`~StringHistory.Save`.
        """

        self._stringHistory = []
        buffer = "string%d"
        count = 1

        while 1:
            historyString = config.Read(buffer%count)
            if not historyString or len(self._stringHistory) >= self._stringMaxStrings:
                break

            self._stringHistory.append(historyString)
            count += 1

        self.AddStringsToMenu()


    def Save(self, config):
        """
        Saves the string history to the given `config` object.

        :param `config`: an instance of :class:`Config`.

        :note: This function should be called explicitly by the application.

        :see: :meth:`~StringHistory.Load`.
        """

        buffer = "string%d"

        for index in range(self._stringMaxStrings):

            if index < len(self._stringHistory):
                config.Write(buffer%(index+1), self._stringHistory[index])
            else:
                config.Write(buffer%(index+1), "")


    def AddStringsToMenu(self, menu=None):
        """
        Appends the strings in the history list, to all menus managed by the string history object
        if `menu` is ``None``. Otherwise it calls the auxiliary method :meth:`~StringHistory.AddStringsToMenu2`.

        :param `menu`: if not ``None``, an instance of :class:`FlatMenu`.
        """

        if not self._stringHistory:
            return

        if menu is not None:
            self.AddStringsToMenu2(menu)
            return

        for menu in self._stringMenus:
            self.AddStringsToMenu2(menu)


    def AddStringsToMenu2(self, menu):
        """
        Appends the strings in the history list, to the given menu only.

        :param `menu`: an instance of :class:`FlatMenu`.
        """

        if not self._stringHistory:
            return

        if menu.GetMenuItemCount():
            menu.AppendSeparator()

        for index in range(len(self._stringHistory)):
            menu.Append(self._idBase + index, GetMRUEntryLabel(index, self._stringHistory[index]))
