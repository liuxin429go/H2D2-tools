#!/usr/bin/env python
# -*- coding: utf-8 -*-
## cython: profile=True
# cython: linetrace=True
#************************************************************************
# --- Copyright (c) INRS 2012-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Unstructured triangular finite element mesh.
"""

import collections.abc
import sys
import hashlib
if sys.version_info < (3, 0):
    import cPickle as pickle
else:
    import pickle
import gc
#import json     # ujson exist aussi
#import jsonpickle
import logging
import math
import multiprocessing as mp
import os
import platform
import time     # for timming

import numpy as np

from . import FEMeshIO
from . import QTQuadTree
#from .external.profile_each_line import profile_each_line

LOGGER = logging.getLogger("INRS.H2D2.Tools.Mesh")
NAN = float('NaN')

# for jsonpickle
# def jdefault(obj):
#     #   doit traiter pyqtree.Index et les sous-classes jusqu'à avoir des types de base
#     return obj.__dict__

# ---  Get the default cache directory
cacheDefaultDir = ''
if platform.system() == 'Windows':
    if not cacheDefaultDir:
        try:
            p = os.path.join(os.environ['LOCALAPPDATA'], 'H2D2')
            if os.path.isdir(p): cacheDefaultDir = p
        except KeyError:
            pass
    if not cacheDefaultDir:
        try:
            p = os.path.join(os.environ['APPDATA'], 'H2D2')
            if os.path.isdir(p): cacheDefaultDir = p
        except KeyError:
            pass
    if not cacheDefaultDir:
        cacheDefaultDir = os.path.join(os.environ['APPDATA'], 'H2D2')
elif platform.system() == 'Linux':
    try:
        p = os.path.join(os.environ['HOME'], '.H2D2')
        if os.path.isdir(p): cacheDefaultDir = p
    except KeyError:
        pass
    if not cacheDefaultDir:
        cacheDefaultDir = os.path.join(os.environ['HOME'], '.H2D2')
else:
    raise RuntimeError('Unsupported platform: %s' % platform.system())

if cacheDefaultDir:
    cacheDefaultDir = os.path.join(cacheDefaultDir, 'cache')
    if not os.path.isdir(cacheDefaultDir):
        try:
            os.makedirs(cacheDefaultDir)
        except:
            raise RuntimeError('Could not locate/create H2D2 cache directory: %s' % cacheDefaultDir)
else:
    raise RuntimeError('Could not locate/create H2D2 cache directory')

def clearCache():
    import glob
    if cacheDefaultDir:
        for spt in ('*.pkl', '*.json'):
            pth = os.path.join(cacheDefaultDir, spt)
            for p in glob.glob(pth):
                os.remove(p)

def genSearchGridForIX(args):
    """
    Private function: Pool worker for FEMeshRGLocalizer
    Get the connectivities of the elements of each grid square
    """
    meshLocalizer, ix = args
    mesh  = meshLocalizer.mesh
    rgrid = meshLocalizer.regGrid
    tbl = []
    for iy in range(meshLocalizer.gridSize[1]):
        tbl.append([])
        elems = rgrid.getElemsInSquare(mesh, ix, iy)
        if len(elems) > 0:
            tbl[iy] = elems
    return (ix, tbl)


class FENode:
    __slots__ = 'ig', 'il', 'x', 'y'
    def __init__(self, il, ig, x, y):
        #LOGGER.debug("Create node %i %f %f" % (ig, x, y))
        """
        L'index global est dans le maillage de base, celui qui est lu. Il
        est cohérent avec les valeurs nodales définies sur ce maillage de base.
        L'index local est l'indice dans le sous-maillage. Il est cohérent
        avec la liste des noeuds du maillage.
        """
        self.il = il    # index local
        self.ig = ig    # index global
        self.x  = x
        self.y  = y

    #def encode(self):
    #    if isinstance(self, FENode):
    #        return {'__FENode__': True, 'data': (self.ig, self.getCoordinates())}
    #    return obj

    def clone(self, il):
        """
        Copy the node, with a new local index
        """
        return FENode(il, self.ig, self.x, self.y)

    def getGlobalIndex(self):
        """
        Return the global index in the base mesh
        """
        return self.ig

    def getLocalIndex(self):
        """
        Return the local index in the sub-mesh
        """
        return self.il

    def getCoordinates(self):
        """
        Return the (x, y) coordinates of the node
        """
        return (self.x, self.y)

    def setGlobalIndex(self, ig):
        """
        Set the glbal index in the base mesh
        """
        self.ig = ig

    def setLocalIndex(self, il):
        """
        Set the local index in the sub-mesh
        """
        self.il = il

    def setCoordinates(self, x, y):
        """
        Set the (x, y) coordinates of the node
        """
        self.x  = x
        self.y  = y

    def distance(self, other):
        dx = other.x - self.x
        dy = other.y - self.y
        return math.sqrt(dx*dx + dy*dy)

    def asTuple(self):
        return (self.ig, self.x, self.y)

    def __str__(self):
        return 'Node %7i (%13.6e, %13.6e)' % (self.ig, self.x, self.y)

class FENodeDeleted:
    def __init__(self, node):
        self._ig = node.ig    # index global

    def __str__(self):
        return 'Node %7i has been deleted' % self._ig

class FESide:
    def __init__(self, il, ig, no1, no2):
        self.il = il        # side node local index
        self.ig = ig        # side node global index
        self.no1 = no1 if no1.ig < no2.ig else no2
        self.no2 = no1 if no2.ig < no1.ig else no2

    def __str__(self):
        return '(%s, %s)' % (self.no1, self.no2)

    def __eq__(self, other):
        if self.no1.ig != other.no1.ig: return False
        if self.no2.ig != other.no2.ig: return False
        return True

    def __hash__(self):
        return hash( (self.no1.ig, self.no2.ig) )

    def getMidsideCoord(self):
        x1, y1 = self.no1.getCoordinates()
        x2, y2 = self.no2.getCoordinates()
        return 0.5*(x1+x2), 0.5*(y1+y2)

class FEElementDeleted:
    def __init__(self, elem):
        self._ig = elem.ig    # index global

    def __str__(self):
        return 'Element %7i has been deleted' % self._ig

class FEElementL2:
    __slots__ = 'il', 'ig', 'n1', 'n2', 'y21', 'x21', 'jaco'
    def __init__(self, il, ig, n1, n2):
        #LOGGER.debug("Create element %i (%i, %i, %i)" % (ig, n1.ig, n2.ig, n3.ig))
        self.il = il
        self.ig = ig
        self.n1 = n1
        self.n2 = n2
        self.y21 = n2.y - n1.y
        self.x21 = n2.x - n1.x
        self.jaco = NAN

    def __str_private(self):
        c = self.getConnectivities()
        return 'Element L2 %7i: [%7i, %7i]' % (self.ig, c[0], c[1])

    def __str__(self):
        return self.__str_private()

    def centerEdges(self):
        """
        Set the position of the edge nodes to the middle
        of the edge.
        """
        pass

    def getBbox(self):
        return (min(self.n1.x, self.n2.x),
                min(self.n1.y, self.n2.y),
                max(self.n1.x, self.n2.x),
                max(self.n1.y, self.n2.y))

    def getGlobalIndex(self):
        """
        Return the global index in the base mesh
        """
        return self.ig

    def getLocalIndex(self):
        """
        Return the local index in the sub-mesh
        """
        return self.il

    def getNode(self, inod):
        if inod == 0: return self.n1
        if inod == 1: return self.n2
        raise IndexError

    def getNodes(self):
        return [self.n1, self.n2]

    def getConnectivities(self):
        return [self.n1.getLocalIndex(), self.n2.getLocalIndex()]

    def getVertexConnectivities(self):
        return [self.n1.getLocalIndex(), self.n2.getLocalIndex()]

    def getCenter(self):
        x1, y1 = self.n1.getCoordinates()
        x2, y2 = self.n2.getCoordinates()
        return (x1+x2)/2, (y1+y2)/2

    def setGlobalIndex(self, ig):
        """
        Set the glbal index in the base mesh
        """
        self.ig = ig

    def setLocalIndex(self, il):
        """
        Set the local index in the sub-mesh
        """
        self.il = il

    def transformeInverse(self, x, y):
        if abs(self.x21) > abs(self.y21):
            ksi = (x-self.n1.x) / self.x21
        else:
            ksi = (y-self.n1.y) / self.y21
        ksi = 2.0*ksi - 1.0
        return ksi

    def interpolateRef(self, ksi, champ):
        N1 = (1.0 - ksi) / 2
        N2 = (1.0 + ksi) / 2
        resultat = N1*champ[self.n1.ig] + N2*champ[self.n2.ig]
        return resultat

    def interpolate(self, x, y, champ):
        ksi = self.transformeInverse(x, y)
        N1 = (1.0 - ksi) / 2
        N2 = (1.0 + ksi) / 2
        resultat = N1*champ[self.n1.ig] + N2*champ[self.n2.ig]
        return resultat

    #def ddxRef(self, ksi, eta, champ):
    #    (vkx, vex), (vky, vey) = self.jacobian()
    #    v1 = champ[self.n1.ig]
    #    v2 = champ[self.n2.ig]
    #    v3 = champ[self.n3.ig]
    #    return vkx*(v2-v1) + vex*(v3-v1)

    #def ddx(self, x, y, champ):
    #    return self.ddxRef(x, y, champ)

    #def ddyRef(self, ksi, eta, champ):
    #    (vkx, vex), (vky, vey) = self.jacobian()
    #    v1 = champ[self.n1.ig]
    #    v2 = champ[self.n2.ig]
    #    v3 = champ[self.n3.ig]
    #    return vky*(v2-v1) + vey*(v3-v1)

    #def ddy(self, x, y, champ):
    #    return self.ddyRef(x, y, champ)

    def sideLength(self):
        a = FENode.distance(self.n1, self.n2)
        return a

    def Jacobian(self):
        """
        Transformation from real element to reference element
        Matrix in C ordering
        """
        return math.hypot(self.x21, self.x21) / 2.0

    def jacobian(self):
        """
        Inverse of Jacobian, multiplied by detJ: j = D * J^-1
        Transformation from reference element to real element
        Matrix in C ordering
        """
        return 1.0 / self.Jacobian()

    def detJ(self):
        return self.jaco

    def integrate(self, champ):
        c = self.detJ()
        return c*(champ[self.n1.ig]+champ[self.n2.ig])

class FEElementT3:
    __slots__ = 'il', 'ig', 'n1', 'n2', 'n3', 'bbox', 'y31', 'x31', 'y21', 'x21', 'jaco'
    def __init__(self, il, ig, n1, n2, n3):
        #LOGGER.debug("Create element %i (%i, %i, %i)" % (ig, n1.ig, n2.ig, n3.ig))
        self.il = il
        self.ig = ig
        self.n1 = n1
        self.n2 = n2
        self.n3 = n3
        self.bbox = ()
        self.y31  = NAN
        self.x31  = NAN
        self.y21  = NAN
        self.x21  = NAN
        self.jaco = NAN

    def __str_private(self):
        c = self.getConnectivities()
        return 'Element T3 %7i: [%7i, %7i, %7i]' % (self.ig, c[0], c[1], c[2])

    def __str__(self):
        return self.__str_private()

    #def encode(self):
    #    if isinstance(self, FEElementT3):
    #        c = self.getConnectivities()
    #        return {'__FEElementT3__': True, 'data': (self.ig, self.getConnectivities())}
    #    return obj

    def centerEdges(self):
        """
        Set the position of the edge nodes to the middle
        of the edge.
        """
        pass

    def getBbox(self):
        return (min(self.n1.x, self.n2.x, self.n3.x),
                min(self.n1.y, self.n2.y, self.n3.y),
                max(self.n1.x, self.n2.x, self.n3.x),
                max(self.n1.y, self.n2.y, self.n3.y))

    def getGlobalIndex(self):
        """
        Return the global index in the base mesh
        """
        return self.ig

    def getLocalIndex(self):
        """
        Return the local index in the sub-mesh
        """
        return self.il

    def getNode(self, inod):
        if inod == 0: return self.n1
        if inod == 1: return self.n2
        if inod == 2: return self.n3
        raise IndexError

    def getNodes(self):
        return [self.n1, self.n2, self.n3]

    def getConnectivities(self):
        return [self.n1.getLocalIndex(), self.n2.getLocalIndex(), self.n3.getLocalIndex()]

    def getT3XtrnConnectivities(self):
        return [self.n1.getLocalIndex(), self.n2.getLocalIndex(), self.n3.getLocalIndex()]

    def getT3SplitConnectivities(self):
        return [ (self.n1.getLocalIndex(), self.n2.getLocalIndex(), self.n3.getLocalIndex()) ]

    def getVertexConnectivities(self):
        return [self.n1.getLocalIndex(), self.n2.getLocalIndex(), self.n3.getLocalIndex()]

    def getEdgeConnectivities(self, iside):
        if iside == 0: return [self.n1.getLocalIndex(), self.n2.getLocalIndex()]
        if iside == 1: return [self.n2.getLocalIndex(), self.n3.getLocalIndex()]
        if iside == 2: return [self.n3.getLocalIndex(), self.n1.getLocalIndex()]
        raise IndexError

    def getEdges(self, inode):
        if inode == self.n1.getLocalIndex(): return [0, 2]
        if inode == self.n2.getLocalIndex(): return [0, 1]
        if inode == self.n3.getLocalIndex(): return [2, 1]
        raise IndexError

    def getCenter(self):
        x1, y1 = self.n1.getCoordinates()
        x2, y2 = self.n2.getCoordinates()
        x3, y3 = self.n3.getCoordinates()
        return (x1+x2+x3)/3, (y1+y2+y3)/3

    def setGlobalIndex(self, ig):
        """
        Set the global index in the base mesh
        """
        self.ig = ig

    def setLocalIndex(self, il):
        """
        Set the local index in the sub-mesh
        """
        self.il = il

    def transforme(self, ksi, eta):
        lmbd = 1.0 - ksi - eta
        x1, y1 = self.n1.getCoordinates()
        x2, y2 = self.n2.getCoordinates()
        x3, y3 = self.n3.getCoordinates()
        return lmbd*x1 + ksi*x2 + eta*x3, lmbd*y1 + ksi*y2 + eta*y3

    def transformeInverse(self, x, y):
        detj = self.detJ()  # here, in case metrics are not yet computed
        x01 = x - self.n1.x
        y01 = y - self.n1.y
        ksi = (self.y31 * x01 - self.x31 * y01) / detj
        eta = (self.x21 * y01 - self.y21 * x01) / detj
        return ksi, eta

    def intersecteBbox(self, bbox):
        """
        Retourne True si l'élément intersecte le bbox passé en param.
        En fait, on teste l'intersection des 2 bbox, ce qui peut provoquer
        des faux-positifs.
        """
        if not self.bbox: self.bbox = self.getBbox()
        # Fast global test
        if self.bbox[0] > bbox[2]: return False     # xmin > xmax
        if self.bbox[1] > bbox[3]: return False     # ymin > ymax
        if self.bbox[2] < bbox[0]: return False     # xmax < xmin
        if self.bbox[3] < bbox[1]: return False     # ymax < ymin
        intersecte = False
        # Side 1-2
        xsmin = min(self.n1.x, self.n2.x)
        xsmax = max(self.n1.x, self.n2.x)
        ysmin = min(self.n1.y, self.n2.y)
        ysmax = max(self.n1.y, self.n2.y)
        if xsmin < bbox[0] < xsmax: intersecte = True
        if xsmin < bbox[2] < xsmax: intersecte = True
        if ysmin < bbox[1] < ysmax: intersecte = True
        if ysmin < bbox[3] < ysmax: intersecte = True
        # Side 2-3
        xsmin = min(self.n2.x, self.n3.x)
        xsmax = max(self.n2.x, self.n3.x)
        ysmin = min(self.n2.y, self.n3.y)
        ysmax = max(self.n2.y, self.n3.y)
        if xsmin < bbox[0] < xsmax: intersecte = True
        if xsmin < bbox[2] < xsmax: intersecte = True
        if ysmin < bbox[1] < ysmax: intersecte = True
        if ysmin < bbox[3] < ysmax: intersecte = True
        # Side 3-1
        xsmin = min(self.n3.x, self.n1.x)
        xsmax = max(self.n3.x, self.n1.x)
        ysmin = min(self.n3.y, self.n1.y)
        ysmax = max(self.n3.y, self.n1.y)
        if xsmin < bbox[0] < xsmax: intersecte = True
        if xsmin < bbox[2] < xsmax: intersecte = True
        if ysmin < bbox[1] < ysmax: intersecte = True
        if ysmin < bbox[3] < ysmax: intersecte = True
        return intersecte

    def estDansBbox(self, x, y):
        if not self.bbox: self.bbox = self.getBbox()
        if x < self.bbox[0]: return False
        if y < self.bbox[1]: return False
        if x > self.bbox[2]: return False
        if y > self.bbox[3]: return False
        return True

    def estDedans(self, x, y):
        if not self.estDansBbox(x, y): return False
        ksi, eta = self.transformeInverse(x, y)
        lmbd = 1.0 - ksi - eta
        return not((ksi < 0.0) or (eta < 0.0) or (lmbd < 0.0))

    def distMinRef(self, x, y):
        ksi, eta = self.transformeInverse(x, y)
        lmbd = 1.0 - ksi - eta
        return min(ksi, eta, lmbd)

    def interpolateRef_ndarray_1(self, ksi, eta, champ):
        lmbd = 1.0 - ksi - eta
        resultat = lmbd*champ[self.n1.ig] + ksi*champ[self.n2.ig] + eta*champ[self.n3.ig]
        return resultat

    def interpolateRef_ndarray_2(self, ksi, eta, champ):
        lmbd = 1.0 - ksi - eta
        resultat = lmbd*champ[self.n1.ig] + ksi*champ[self.n2.ig] + eta*champ[self.n3.ig]
        return resultat

    def interpolateRef(self, ksi, eta, champ):
        lmbd = 1.0 - ksi - eta
        resultat = lmbd*champ[self.n1.ig] + ksi*champ[self.n2.ig] + eta*champ[self.n3.ig]
        return resultat

    def interpolate(self, x, y, champ):
        ksi, eta = self.transformeInverse(x, y)
        lmbd = 1.0 - ksi - eta
        resultat = lmbd*champ[self.n1.ig] + ksi*champ[self.n2.ig] + eta*champ[self.n3.ig]
        return resultat

    def ddxRef(self, ksi, eta, champ):
        (vkx, vex), _ = self.jacobian()
        v1 = champ[self.n1.ig]
        v2 = champ[self.n2.ig]
        v3 = champ[self.n3.ig]
        return vkx*(v2-v1) + vex*(v3-v1)

    def ddx(self, x, y, champ):
        return self.ddxRef(x, y, champ)

    def ddyRef(self, ksi, eta, champ):
        _, (vky, vey) = self.jacobian()
        v1 = champ[self.n1.ig]
        v2 = champ[self.n2.ig]
        v3 = champ[self.n3.ig]
        return vky*(v2-v1) + vey*(v3-v1)

    def ddy(self, x, y, champ):
        return self.ddyRef(x, y, champ)

    def sideLength(self):
        a = FENode.distance(self.n1, self.n2)
        b = FENode.distance(self.n2, self.n3)
        c = FENode.distance(self.n3, self.n1)
        return (a, b, c)

    def Jacobian(self):
        """
        Transformation from real element to reference element
        Matrix in C ordering
        """
        if math.isnan(self.jaco): self.detJ()
        return np.array([[self.x21, self.x31], [self.y21, self.y31]])

    def jacobian(self):
        """
        Inverse of Jacobian: j = J^-1
        Transformation from reference element to real element
        Matrix in C ordering
        """
        if math.isnan(self.jaco): self.detJ()
        return np.array([[self.y31, -self.x31], [-self.y21, self.x21]]) / self.jaco

    def detJ(self):
        if math.isnan(self.jaco):
            self.y31 = self.n3.y - self.n1.y
            self.x31 = self.n3.x - self.n1.x
            self.y21 = self.n2.y - self.n1.y
            self.x21 = self.n2.x - self.n1.x
            self.jaco = (self.x21 * self.y31) - (self.x31 * self.y21)
        return self.jaco

    def perimeter(self):
        a, b, c = self.sideLength()
        return 0.5*(a+b+c)

    def angles(self):
        a, b, c = self.sideLength()
        p = 0.5*(a+b+c)
        t1 = 2 * math.atan2(math.sqrt((p-b)*(p-c)), math.sqrt(p*(p-a)))
        t2 = 2 * math.atan2(math.sqrt((p-c)*(p-a)), math.sqrt(p*(p-b)))
        t3 = 2 * math.atan2(math.sqrt((p-a)*(p-b)), math.sqrt(p*(p-c)))
        return (t1, t2, t3)

    def inCircleRadius(self):
        a, b, c = self.sideLength()
        p = 0.5*(a+b+c)
        return math.sqrt((p-a)*(p-b)*(p-c) / p)

    def circumCircleRadius(self):
        a, b, c = self.sideLength()
        p = 0.5*(a+b+c)                         # perimeter
        s = math.sqrt(p*(p-a)*(p-b)*(p-c))      # area
        R = (a*b*c) / (4*s)
        return R

    def integrate(self, champ):
        c = self.detJ() / 6.0
        return c*(champ[self.n1.ig]+champ[self.n2.ig]+champ[self.n3.ig])

class FEElementT6L:
    __slots__ = 'il', 'ig', 'n1', 'n2', 'n3', 'n4', 'n5', 'n6', 't3s'
    def __init__(self, il, ig, n1, n2, n3, n4, n5, n6):
        #LOGGER.debug("Create element %i (%i, %i, %i)" % (ig, n1.ig, n3.ig, n5.ig))
        self.il = il
        self.ig = ig
        self.n1 = n1
        self.n2 = n2
        self.n3 = n3
        self.n4 = n4
        self.n5 = n5
        self.n6 = n6
        self.t3s = ()

    def __str_private(self):
        c = self.getConnectivities()
        return 'Element T6L %7i: [%7i, %7i, %7i, %7i, %7i, %7i]' % (self.ig, c[0], c[1], c[2], c[3], c[4], c[5])

    def __str__(self):
        return self.__str_private()

    def __getattr__(self, name):
        """
        Transfert all unknown calls to a T3 element
        """
        t3 = FEElementT3(self.il, self.ig, self.n1, self.n3, self.n5)
        return getattr(t3, name)

    def centerEdges(self):
        """
        Set the position of the edge nodes to the middle
        of the edge.
        """
        self.n2.x = 0.5*(self.n1.x + self.n3.x)
        self.n2.y = 0.5*(self.n1.y + self.n3.y)
        self.n4.x = 0.5*(self.n3.x + self.n5.x)
        self.n4.y = 0.5*(self.n3.y + self.n5.y)
        self.n6.x = 0.5*(self.n5.x + self.n1.x)
        self.n6.y = 0.5*(self.n5.y + self.n1.y)

    def getBbox(self):
        return (min(self.n1.x, self.n3.x, self.n5.x),
                min(self.n1.y, self.n3.y, self.n5.y),
                max(self.n1.x, self.n3.x, self.n5.x),
                max(self.n1.y, self.n3.y, self.n5.y))

    def getGlobalIndex(self):
        """
        Return the global index in the base mesh
        """
        return self.ig

    def getLocalIndex(self):
        """
        Return the local index in the sub-mesh
        """
        return self.il

    def getNode(self, inod):
        if inod == 0: return self.n1
        if inod == 1: return self.n2
        if inod == 2: return self.n3
        if inod == 3: return self.n4
        if inod == 4: return self.n5
        if inod == 5: return self.n6
        raise IndexError

    def getNodes(self):
        return [self.n1, self.n2, self.n3, self.n4, self.n5, self.n6]

    def getConnectivities(self):
        return [self.n1.getLocalIndex(), self.n2.getLocalIndex(), self.n3.getLocalIndex(),
                self.n4.getLocalIndex(), self.n5.getLocalIndex(), self.n6.getLocalIndex()]

    def getVertexConnectivities(self):
        return [self.n1.getLocalIndex(), self.n3.getLocalIndex(), self.n5.getLocalIndex()]

    def getT3XtrnConnectivities(self):
        return [self.n1.getLocalIndex(), self.n3.getLocalIndex(), self.n5.getLocalIndex()]

    def getT3SplitConnectivities(self):
        return [(self.n1.getLocalIndex(), self.n2.getLocalIndex(), self.n6.getLocalIndex()),
                (self.n2.getLocalIndex(), self.n3.getLocalIndex(), self.n4.getLocalIndex()),
                (self.n6.getLocalIndex(), self.n4.getLocalIndex(), self.n5.getLocalIndex()),
                (self.n4.getLocalIndex(), self.n6.getLocalIndex(), self.n2.getLocalIndex())]

    def getCenter(self):
        x1, y1 = self.n1.getCoordinates()
        x2, y2 = self.n3.getCoordinates()
        x3, y3 = self.n5.getCoordinates()
        return (x1+x2+x3)/3, (y1+y2+y3)/3

    def setGlobalIndex(self, ig):
        """
        Set the glbal index in the base mesh
        """
        self.ig = ig

    def setLocalIndex(self, il):
        """
        Set the local index in the sub-mesh
        """
        self.il = il

    def __getSubT3(self, ksi, eta):
        if not self.t3s:
            self.t3s = (FEElementT3(self.il, self.ig, self.n2, self.n3, self.n4),
                        FEElementT3(self.il, self.ig, self.n6, self.n4, self.n5),
                        FEElementT3(self.il, self.ig, self.n4, self.n6, self.n2),
                        FEElementT3(self.il, self.ig, self.n1, self.n2, self.n6))
        if ksi >= 0.5: return self.t3s[0], 2*(ksi-0.5), 2*eta
        if eta >= 0.5: return self.t3s[1], 2*ksi, 2*(eta-0.5)
        if ksi >= 0.5-eta: return self.t3s[2], 2*(0.5-ksi), 2*(0.5-eta)
        return self.t3s[3], 2*ksi, 2*eta

    def interpolateRef_ndarray_1(self, ksi, eta, champ):
        t3, k, e = self.__getSubT3(ksi, eta)
        return t3.interpolateRef_ndarray_1(k, e, champ)

    def interpolateRef_ndarray_2(self, ksi, eta, champ):
        t3, k, e = self.__getSubT3(ksi, eta)
        return t3.interpolateRef_ndarray_2(k, e, champ)

    def interpolateRef(self, ksi, eta, champ):
        t3, k, e = self.__getSubT3(ksi, eta)
        return t3.interpolateRef(k, e, champ)

    def interpolate(self, x, y, champ):
        ksi, eta = self.transformeInverse(x, y)
        return self.interpolateRef(ksi, eta, champ)

    def ddxRef(self, ksi, eta, champ):
        t3, k, e = self.__getSubT3(ksi, eta)
        return t3.ddxRef(k, e, champ)

    def ddx(self, x, y, champ):
        return self.ddxRef(x, y, champ)

    def ddyRef(self, ksi, eta, champ):
        t3, k, e = self.__getSubT3(ksi, eta)
        return t3.ddyRef(k, e, champ)

    def ddy(self, x, y, champ):
        return self.ddyRef(x, y, champ)

class FEElementQ4:
    __slots__ = 'il', 'ig', 'n1', 'n2', 'n3', 'n4'
    def __init__(self, il, ig, n1, n2, n3, n4):
        #LOGGER.debug("Create element %i (%i, %i, %i)" % (ig, n1.ig, n2.ig, n3.ig))
        self.il = il
        self.ig = ig
        self.n1 = n1
        self.n2 = n2
        self.n3 = n3
        self.n4 = n4

    def __str_private(self):
        c = self.getConnectivities()
        return 'Element Q4 %7i: [%7i, %7i, %7i, %7i]' % (self.ig, c[0], c[1], c[2], c[3])

    def __str__(self):
        return self.__str_private()

    def centerEdges(self):
        """
        Set the position of the edge nodes to the middle
        of the edge.
        """
        pass

    def getBbox(self):
        return (min(self.n1.x, self.n2.x, self.n3.x, self.n4.x),
                min(self.n1.y, self.n2.y, self.n3.y, self.n4.y),
                max(self.n1.x, self.n2.x, self.n3.x, self.n4.x),
                max(self.n1.y, self.n2.y, self.n3.y, self.n4.y))

    def getGlobalIndex(self):
        """
        Return the global index in the base mesh
        """
        return self.ig

    def getLocalIndex(self):
        """
        Return the local index in the sub-mesh
        """
        return self.il

    def getNode(self, inod):
        if inod == 0: return self.n1
        if inod == 1: return self.n2
        if inod == 2: return self.n3
        if inod == 3: return self.n4
        raise IndexError

    def getNodes(self):
        return [self.n1, self.n2, self.n3, self.n4]

    def getConnectivities(self):
        return [self.n1.getLocalIndex(), self.n2.getLocalIndex(), self.n3.getLocalIndex(), self.n4.getLocalIndex()]

    def getVertexConnectivities(self):
        return [self.n1.getLocalIndex(), self.n2.getLocalIndex(), self.n3.getLocalIndex(), self.n4.getLocalIndex()]

    def getT3XtrnConnectivities(self):
        return [(self.n1.getLocalIndex(), self.n2.getLocalIndex(), self.n4.getLocalIndex()),
                (self.n3.getLocalIndex(), self.n4.getLocalIndex(), self.n2.getLocalIndex())]

    def getT3SplitConnectivities(self):
        return [(self.n1.getLocalIndex(), self.n2.getLocalIndex(), self.n4.getLocalIndex()),
                (self.n3.getLocalIndex(), self.n4.getLocalIndex(), self.n2.getLocalIndex())]

    def getCenter(self):
        x1, y1 = self.n1.getCoordinates()
        x2, y2 = self.n2.getCoordinates()
        x3, y3 = self.n3.getCoordinates()
        x4, y4 = self.n4.getCoordinates()
        return (x1+x2+x3+x4)/4, (y1+y2+y3+y4)/4

    def setGlobalIndex(self, ig):
        """
        Set the global index in the base mesh
        """
        self.ig = ig

    def setLocalIndex(self, il):
        """
        Set the local index in the sub-mesh
        """
        self.il = il

class FEMesh:
    #@profile_each_line
    def __init__(self, files=None, isSubGrid=False, parent=None, withLocalizer=False):
        self.nodes  = []
        self.elems  = []
        self.sideConnec = None
        self.lclzr  = None
        self.parent = parent
        self.isSubGrid = isSubGrid
        self.genLclzr  = withLocalizer
        self.md5 = hashlib.md5()
        if files:
            reader = FEMeshIO.constructReaderFromFile(files)
            reader.readMesh(self)

    def centerEdges(self):
        """
        Set the position of the edge nodes to the middle
        of the edge.
        """
        for e in self.elems:
            e.centerEdges()

    def getParent(self):
        return self.parent

    def isSubMesh(self):
        return self.isSubGrid

    def getMD5(self):
        return self.md5.hexdigest()

    def getBbox(self, stretchfactor=0.0, margins=(0, 0)):
        X, Y = FEMesh.getCoordinates(self)
        xmin = np.min(X)
        xmax = np.max(X)
        ymin = np.min(Y)
        ymax = np.max(Y)
        dx = (xmax-xmin)*stretchfactor + margins[0]
        dy = (ymax-ymin)*stretchfactor + margins[1]
        return (xmin-dx, ymin-dy, xmax+dx, ymax+dy)

    def getNbNodes(self):
        return len(self.nodes)

    def getNbElements(self):
        return len(self.elems)

    def getElement(self, ielem):
        if ielem < 0: raise IndexError
        return self.elems[ielem]

    def getElementNeighbour(self, ielem, iside):
        if ielem < 0: raise IndexError
        if self.sideConnec is None: self.genSideConnec()
        return self.sideConnec[ielem][iside]

    def getElements(self):
        return self.elems

    def iterElements(self):
        for item in self.elems:
            yield item

    def getCoordinates(self):
        X = np.zeros(len(self.nodes))
        Y = np.zeros(len(self.nodes))
        i = 0
        for n in self.nodes:
            X[i] = n.x
            i += 1
        i = 0
        for n in self.nodes:
            Y[i] = n.y
            i += 1
        return (X, Y)

    def getConnectivities(self):
        return np.array([e.getConnectivities() for e in self.elems])

    def getT3XtrnConnectivities(self):
        r = np.array([e.getT3XtrnConnectivities() for e in self.elems])
        r = np.reshape(r, (-1,3))
        return r

    def getT3SplitConnectivities(self):
        r = np.array([e.getT3SplitConnectivities() for e in self.elems])
        r = np.reshape(r, (-1,3))
        return r

    def getNode(self, ind):
        return self.nodes[ind]

    def getNodes(self):
        return self.nodes

    def iterNodes(self):
        for item in self.nodes:
            yield item

    def getNodeNumbersGlobal(self):
        """
        """
        return [n.getGlobalIndex() for n in self.nodes]

    def getNodeNumbersLocal(self):
        """
        """
        return [n.getLocalIndex() for n in self.nodes]

    def getLocalizer(self):
        return self.lclzr

    def addNodes(self, X, Y):
        n = self.getNbNodes()
        nodes = [ FENode(n+i, n+i, x, y) for i, (x, y) in enumerate(zip(X,Y)) ]
        self.nodes.extend(nodes)
        self.dirty = True
        return nodes

    def deleteNode(self, nod):
        """
        """
        il = nod.getLocalIndex()
        self.nodes[il] = FENodeDeleted(nod)
        self.dirty = True

    def addElements(self, connec):
        if len(connec) == 0: return []
        n = self.getNbElements()
        if len(connec[0]) == 2:
            elems = [ FEElementL2 (n+ie, n+ie, self.nodes[kne[0]], self.nodes[kne[1]]) for ie, kne in enumerate(connec) ]
        elif len(connec[0]) == 3:
            elems = [ FEElementT3 (n+ie, n+ie, self.nodes[kne[0]], self.nodes[kne[1]], self.nodes[kne[2]]) for ie, kne in enumerate(connec) ]
        elif len(connec[0]) == 4:
            elems = [ FEElementQ4 (n+ie, n+ie, self.nodes[kne[0]], self.nodes[kne[1]], self.nodes[kne[2]], self.nodes[kne[3]]) for ie, kne in enumerate(connec) ]
        elif len(connec[0]) == 6:
            elems = [ FEElementT6L(n+ie, n+ie, self.nodes[kne[0]], self.nodes[kne[1]], self.nodes[kne[2]], self.nodes[kne[3]], self.nodes[kne[4]], self.nodes[kne[5]]) for ie, kne in enumerate(connec) ]
        else:
            raise ValueError
        self.elems.extend(elems)
        self.dirty = True
        return elems

    def deleteElement(self, ele):
        """
        """
        il = ele.getLocalIndex()
        self.elems[il] = FEElementDeleted(ele)
        self.dirty = True

    def setCoord(self, X, Y):
        self.nodes = [ FENode(i, i, x, y) for i, (x, y) in enumerate(zip(X,Y)) ]
        bb = FEMesh.getBbox(self)
        if not self.isSubGrid:
            LOGGER.info('Mesh: coordinates:')
            LOGGER.info('       ll: (%f, %f)', bb[0], bb[1])
            LOGGER.info('       ur: (%f, %f)', bb[2], bb[3])
            LOGGER.info('       wh: (%f, %f)', bb[2]-bb[0], bb[3]-bb[1])
        else:
            LOGGER.debug('SubMesh: coordinates:')
            LOGGER.debug('       ll: (%f, %f)', bb[0], bb[1])
            LOGGER.debug('       ur: (%f, %f)', bb[2], bb[3])
            LOGGER.debug('       wh: (%f, %f)', bb[2]-bb[0], bb[3]-bb[1])
        # ---  md5
        try:
            self.md5.update(X.view(np.uint8))
            self.md5.update(Y.view(np.uint8))
        except ValueError:
            self.md5.update(X.tobytes())
            self.md5.update(Y.tobytes())

    def setNodes(self, nodes):
        assert self.isSubGrid
        self.nodes = nodes

    #@profile_each_line
    def setConnec(self, connec):
        self.md5.update(connec.view(np.uint8))

        if len(connec[0]) == 2:
            elems = [ FEElementL2 (ie, ie, self.nodes[kne[0]], self.nodes[kne[1]]) for ie, kne in enumerate(connec) ]
        elif len(connec[0]) == 3:
            elems = [ FEElementT3 (ie, ie, self.nodes[kne[0]], self.nodes[kne[1]], self.nodes[kne[2]]) for ie, kne in enumerate(connec) ]
        elif len(connec[0]) == 4:
            elems = [ FEElementQ4 (ie, ie, self.nodes[kne[0]], self.nodes[kne[1]], self.nodes[kne[2]], self.nodes[kne[3]]) for ie, kne in enumerate(connec) ]
        elif len(connec[0]) == 6:
            elems = [ FEElementT6L(ie, ie, self.nodes[kne[0]], self.nodes[kne[1]], self.nodes[kne[2]], self.nodes[kne[3]], self.nodes[kne[4]], self.nodes[kne[5]]) for ie, kne in enumerate(connec) ]
        else:
            raise ValueError
        self.elems = elems

        if self.genLclzr:
            self.lclzr = FEMeshQTLocalizer(self)

    def setElements(self, elems):
        assert self.isSubGrid
        self.elems = elems

    def genSideConnec(self):
        """
        Génère les connectivité des côtés des éléments.
        Pour chaque élément, pour chaque côté (0, 1, 2)
        l'élément auquel il est lié.
        """
        def insertSide(skin, sConnec, ie, ic, n1, n2):
            if n1 < n2:
                nmin, nmax = n1, n2
            else:
                nmin, nmax = n2, n1
            if nmin in skin:
                skmin = skin[nmin]
                if nmax in skmin:
                    ie0, ic0 = skmin[nmax]
                    ie1, ic1 = ie, ic
                    sConnec[ie0, ic0] = ie1
                    sConnec[ie1, ic1] = ie0
                    del skmin[nmax]
                    if len(skmin) == 0: del skin[nmin]
                else:
                    skmin[nmax] = (ie, ic)
            else:
                skin[nmin] = {}
                skin[nmin][nmax] = (ie, ic)

        skin = {}
        sConnec = np.empty((len(self.elems),3), dtype=np.int)
        sConnec.fill(-1)
        for e in self.elems:
            in1, in2, in3 = e.getT3XtrnConnectivities()
            insertSide(skin, sConnec, e.il, 0, in1, in2)
            insertSide(skin, sConnec, e.il, 1, in2, in3)
            insertSide(skin, sConnec, e.il, 2, in3, in1)
        del skin
        self.sideConnec = sConnec

    def genSkin_insertInSkin(self, skin, n1, n2):
        if n1 < n2:
            nmin, nmax = n1, n2
        else:
            nmin, nmax = n2, n1
        if nmin in skin:
            skmin = skin[nmin]
            if nmax in skmin:
                del skmin[nmax]
                if len(skmin) == 0: del skin[nmin]
            else:
                skmin[nmax] = (n1, n2)
        else:
            skin[nmin] = {}
            skin[nmin][nmax] = (n1, n2)

    def genSkin_deleteFromSkin(self, skin, n1, n2):
        nmin = min(n1,n2)
        nmax = max(n1,n2)
        try:
            del skin[nmin][nmax]
        except KeyError:
            pass
        try:
            if len(skin[nmin]) == 0: del skin[nmin]
        except KeyError:
            pass

    def genSkin(self, doChain = True):
        def genPolygons(self, skin):
            pgs = []
            while len(skin) > 0:
                pg = []
                k0, d = next(iter(skin.items()))
                _, (n0, n2) = d.popitem()
                pg.append(n0)
                pg.append(n2)
                self.genSkin_deleteFromSkin(skin, n0, n2)
                self.genSkin_deleteFromSkin(skin,-n0,-n2)
                k0 = n2
                try:
                    while n2 != n0 and -n2 != n0:
                        try:
                            d = skin[k0]
                        except KeyError:
                            k0 = -k0
                            d = skin[k0]
                        k1, (n1, n2) = d.popitem()
                        self.genSkin_deleteFromSkin(skin, n1, n2)
                        self.genSkin_deleteFromSkin(skin,-n1,-n2)
                        pg.append(n2)
                        k0 = n2
                except KeyError:
                    LOGGER.error('Unclosed polygon: %s', [n0, n2])
                pgs.append([abs(i) for i in pg])
            return pgs

        skin = {}
        for e in self.elems:
            n1, n2, n3 = e.getT3XtrnConnectivities()
            self.genSkin_insertInSkin(skin, n1, n2)
            self.genSkin_insertInSkin(skin, n2, n3)
            self.genSkin_insertInSkin(skin, n3, n1)
            if doChain:
                self.genSkin_insertInSkin(skin,-n1,-n2)
                self.genSkin_insertInSkin(skin,-n2,-n3)
                self.genSkin_insertInSkin(skin,-n3,-n1)

        if doChain:
            pgs = genPolygons(self, skin)
            connec = [e for pg in pgs for e in zip(pg[:-1], pg[1:])]
        else:
            connec = [skin[k0][k1] for k0 in skin for k1 in skin[k0] if skin[k0][k1][0] >= 0 and skin[k0][k1][1] >= 0]

        # ---  Create compact sub-grid
        subGrid = self.genCompactGrid(connec)
        return subGrid

    def genT3Grid(self, withLocalizer=False, split=True):
        """
        From a T6L grid, generate a T3 Grid either by taking only
        the vertex nodes, or by splitting each T6L in 4 T3.
        The new grid is not compacted.
        """
        if split:
            connec = self.getT3SplitConnectivities()
        else:
            connec = self.getT3XtrnConnectivities()

        # ---  Create sub-grid  (Instantiate the same type as self)
        subGrid = self.__class__(isSubGrid=True, parent=self, withLocalizer=withLocalizer)
        subGrid.setNodes(self.nodes)
        subGrid.setConnec(connec)
        return subGrid

    def genT6LGrid(self, withLocalizer=False):
        """
        From a T3 grid, generate a T6L grid by adding mid-side nodes
        """
        # ---  Get sides list
        sides = {}
        iside = self.getNbNodes()
        for ele in self.iterElements():
            if isinstance(ele, FEElementDeleted): continue
            assert isinstance(ele, FEElementT3)
            for js in range(3):
                i1, i2 = ele.getEdgeConnectivities(js)
                side = FESide(iside, iside, self.getNode(i1), self.getNode(i2))
                if side not in sides:
                    sides[side] = side
                    iside += 1

        # ---  Add side connect to T3 to build T6L connectivities
        connec = []
        for ele in self.iterElements():
            ino1, ino3, ino5 = ele.getConnectivities()
            sd13 = FESide(-1, -1, self.getNode(ino1), self.getNode(ino3))    # dumy side
            ino2 = sides[sd13].ig                                            # real side
            sd35 = FESide(-1, -1, self.getNode(ino3), self.getNode(ino5))
            ino4 = sides[sd35].ig
            sd51 = FESide(-1, -1, self.getNode(ino5), self.getNode(ino1))
            ino6 = sides[sd51].ig
            elc = (ino1, ino2, ino3, ino4, ino5, ino6)
            connec.append(elc)
        connec = np.array(connec)

        # ---  Coord - add mid-sides
        srt_sides = sorted(sides, key=lambda s : s.ig)
        newX = []
        newY = []
        for side in srt_sides:
            x, y = side.getMidsideCoord()
            newX.append(x)
            newY.append(y)
        X, Y = self.getCoordinates()
        X = np.append(X, newX)
        Y = np.append(Y, newY)

        # ---  Renumber
        n = 0
        for kne in connec:
            n += len(kne)*len(kne)
        row = np.zeros(n, dtype=np.int32)
        col = np.zeros(n, dtype=np.int32)
        dta = np.zeros(n, dtype=np.int32)
        n = 0
        for kne in connec:
            for i1 in kne:
                for i2 in kne:
                    row[n] = i1
                    col[n] = i2
                    dta[n] = 1
                    n += 1
        import scipy as sp
        import scipy.sparse as sp_sparse
        n = X.shape[0]
        crs = sp_sparse.csr_matrix((dta, (row, col)), shape=(n, n))
        prm_dir = sp_sparse.csgraph.reverse_cuthill_mckee(crs, True)
        prm_inv = np.empty(prm_dir.size, dtype=np.int32)
        for i in np.arange(prm_dir.size):
            prm_inv[prm_dir[i]] = i

        # ---  Create grid
        subGrid = self.__class__(isSubGrid=False, parent=self, withLocalizer=withLocalizer)
        subGrid.setCoord(X[prm_dir], Y[prm_dir])
        subGrid.setConnec(prm_inv.take(connec))

        ## # ---  Check renum des noeuds
        ## if False:
        ##     print('Check node renum')
        ##     for i in range(self.getNbNodes()):
        ##         ino = i
        ##         inn = prm_inv[i]
        ##         xo, yo = self.getNode(ino).getCoordinates()
        ##         xn, yn = subGrid.getNode(inn).getCoordinates()
        ##         if np.hypot(xn-xo,yn-yo) > 1.0e-6:
        ##             print(nod)
        ##             print(subGrid.getNode(inn))
        ##             print(ino, inn)
        ##             print(X[ino], Y[ino])
        ##             print(X[inn], Y[inn])
        ##             raise
        ##
        ## # ---  Check renum des elements
        ## if False:
        ##     print('Check element renum')
        ##     for eleo, elen in zip(self.iterElements(), subGrid.iterElements()):
        ##         kneo = eleo.getConnectivities()
        ##         knen = elen.getT3XtrnConnectivities()
        ##         for ino, inn in zip(kneo, knen):
        ##             xo, yo = self.getNode(ino).getCoordinates()
        ##             xn, yn = subGrid.getNode(inn).getCoordinates()
        ##             if np.hypot(xn-xo,yn-yo) > 1.0e-6:
        ##                 print(eleo)
        ##                 print(elen)
        ##                 print(kneo, knen)
        ##                 print((xo,yo), (xn,yn), np.hypot(xn-xo,yn-yo))
        ##                 assert False

        return subGrid

    def genSubGrid(self, filters=None):
        """
        Generate the sub-mesh using filters like SubGridFilterOnNodes.
        """
        if not filters: return self
        X, Y = self.getCoordinates()

        # ---  Nodes to keep
        nbNodes = self.getNbNodes()
        if isinstance(filters, collections.abc.Sequence):
            f = filters[0](nbNodes, X, Y)
            for fltr in filters[1:]:
                f = np.logical_and(f, fltr(nbNodes, X, Y))
        else:
            f = filters(nbNodes, X, Y)

        # ---  Connectivities in parent numbering
        elems  = [ e for e in self.elems if np.logical_or.reduce( f[e.getConnectivities()] )]
        connec = [ e.getConnectivities() for e in elems ]

        # ---  Create compact sub-grid
        subGrid = self.genCompactGrid(connec)
        for eold, enew in zip(elems, subGrid.elems):
            enew.setGlobalIndex( eold.getGlobalIndex() )

        return subGrid

    def genCompactGrid(self, connec = None):
        if connec is None:
            connec = [ e.getConnectivities() for e in self.elems if not isinstance(e, FEElementDeleted) ]

        # ---  Connectivities as np.array
        connex = np.array(connec)

        # ---  Activ nodes base on connec
        nbNodes = self.getNbNodes()
        activ = np.zeros(nbNodes, np.bool)
        activ[connex] = True

        # ---  Nodes and elements in local numbering
        renum = np.full(nbNodes, -1, np.int)
        iglb = 0
        ilcl = 0
        for flag in activ:
            if flag:
                renum[iglb] = ilcl
                ilcl += 1
            iglb += 1
        nodes = []
        iglb = 0
        for ilcl in renum:
            if ilcl >= 0:
                n = self.getNode(iglb)
                nodes.append( n.clone(ilcl) )
            iglb += 1
        kngNew = renum[connex]

        # ---  Create sub-grid
        subGrid = self.__class__(isSubGrid=True, parent=self)    # Instantiate the same type as self
        subGrid.setNodes(nodes)
        subGrid.setConnec(kngNew)
        return subGrid

    def localizePointsBruteForce(self, X, Y):
        lclzr = [(xy[0], xy[1], None) for xy in zip(X,Y)]
        nfound = 0
        for ip in range(len(lclzr)):
            x,y,e = lclzr[ip]
            if e: continue
            for ele in self.elems:
                if ele.estDedans(x, y):
                    e = ele
                    break
            if e:
                lclzr[ip] = (x,y,e)
                LOGGER.debug('localizePoints: Found %s in %s', (x,y), e.il)
                nfound += 1
                if nfound == len(lclzr): break
        return [ e for x,y,e in lclzr ]

    def localizePoints(self, X, Y, relative_tolerance=1.0e-12):
        LOGGER.info('Mesh: localize points with background grid')
        assert self.lclzr
        eles  = self.lclzr.getElementsOfPoints(X, Y, relative_tolerance)
        return eles

    def integrate(self, v):
        s = 0.0
        for e in self.elems:
            s += e.integrate(v)
        return s

    def write(self, writer):
        writer.writeMesh(self)

class SubGridFilterOnNodes:
    def __init__(self, keep=None, skip=[]):
        self.keep = keep
        self.skip = skip
    def evaluate(self, nbNodes, X, Y):
        if self.keep is None:
            f = np.full(nbNodes, True)
        else:
            f = np.full(nbNodes, False)
            f[self.keep] = True
        if len(self.skip) > 0:
            f[self.skip] = False
        return f
    def __call__(self, nbNodes, X, Y):
        return self.evaluate(nbNodes, X, Y)

class SubGridFilterOnBBox:
    def __init__(self, keep=None, skip=None):
        self.keep = keep
        self.skip = skip
    def evaluate(self, nbNodes, X, Y):
        f = np.full(nbNodes, False)
        if self.keep:
            assert f is not None
            k = np.logical_and.reduce((self.keep[0] <= X, X <= self.keep[2], self.keep[1] <= Y, Y <= self.keep[3]))
            f[k] = True
        if self.skip:
            assert f is not None
            k = np.logical_and.reduce((self.skip[0] <= X, X <= self.skip[2], self.skip[1] <= Y, Y <= self.skip[3]))
            f[k] = False
        return f
    def __call__(self, nbNodes, X, Y):
        return self.evaluate(nbNodes, X, Y)


class FE1DRegularGrid:
    """
    gridSize is the number of points
    """
    def __init__(self, ends=(0,0,1,1), gridSize=20):
        self.nbNod  = -1
        self.nbEle  = -1
        self.nodes  = []
        self.elems  = []
        #self.lclzr = None
        #self.genLclzr = False
        
        self.setDim(ends, gridSize)
        X, Y = self.getCoordinates()
        self.setCoord (X, Y)
        connec = self.getConnectivities()
        self.setConnec(connec)

    def setDim(self, ends, gridSize):
        assert gridSize > 0
        self.gridSize = gridSize
        self.ends = ends

        xmin = min(ends[0], ends[2])
        ymin = min(ends[1], ends[3])
        xmax = max(ends[0], ends[2])
        ymax = max(ends[1], ends[3])

        self.bbox = (xmin, ymin, xmax, ymax)
        self.bbdx = (self.bbox[2] - self.bbox[0]) / (self.gridSize-1)
        self.bbdy = (self.bbox[3] - self.bbox[1]) / (self.gridSize-1)
        LOGGER.info('FE1DRegularGrid: bbox: %s', str(self.bbox))
        LOGGER.info('FE1DRegularGrid: size: %s', str(self.gridSize))

    def setCoord(self, X, Y):
        self.nodes = [None] * len(X)
        for i, (x, y) in enumerate(zip(X,Y)):
            self.nodes[i] = FENode(i, i, x, y)

    #def setNodes(self, nodes):
    #    assert self.isSubGrid
    #    self.nodes = nodes

    def setConnec(self, connec):
        self.elems = [None] * len(connec)
        if len(connec[0]) == 2:
            for ie, (no1, no2) in enumerate(connec):
                self.elems[ie] = FEElementL2(ie, ie, self.nodes[no1], self.nodes[no2])
        else:
            raise ValueError

    def getGridSize(self):
        return self.gridSize

    def getNbNodes(self):
        return len(self.nodes)

    def getNbElements(self):
        return len(self.elems)

    def getBbox(self, stretchfactor=0.0, margins=(0,0)):
        xmin = self.bbox[0]
        ymin = self.bbox[1]
        xmax = self.bbox[2]
        ymax = self.bbox[3]
        dx = (xmax-xmin)*stretchfactor + margins[0]
        dy = (ymax-ymin)*stretchfactor + margins[1]
        return (xmin-dx, ymin-dy, xmax+dx, ymax+dy)

    def getNode(self, ind):
        """
        Return the node at index ind
        """
        return self.nodes[ind]

    def getNodes(self):
        """
        Return all the nodes
        """
        return self.nodes

    def iterNodes(self):
        for item in self.nodes:
            yield item

    def getCoordinates(self):
        """
        Return the coordinates of all the nodes of the grid
        """
        X = np.linspace(self.ends[0], self.ends[2], self.gridSize)
        Y = np.linspace(self.ends[1], self.ends[3], self.gridSize)
        return (X, Y)

    def getElement(self, ielem):
        return self.elems[ielem]

    def getElements(self):
        return self.elems

    def iterElements(self):
        for item in self.elems:
            yield item

    def getConnectivities(self):
        connec = [ (ix+0, ix+1) for ix in range(self.gridSize-1) ]
        return np.array(connec)

    def genSkin(self, doChain = True):
        """
        Shall return a list of (x, y) points forming the skin
        """
        raise NotImplementedError

    def isSubMesh(self):
        return False

    def localizePoints(self, X, Y, gridSize=None):
        raise NotImplementedError

    def integrate(self, v):
        s = 0.0
        for ele in self.elements:
            s += ele.integrate(v)
            raise NotImplementedError('FE1DRegularGrid.integrate: invalid code')
        return s

    def asRunLength(self, scale_factor=1.0):
        s = [ 0.0 ]
        ds = math.sqrt(self.bbdx*self.bbdx + self.bbdy*self.bbdy) * scale_factor
        for _i in range(1,self.gridSize):
            s.append( s[-1]+ds )
        return s

class FE2DRegularGrid:
    """
    gridSize is the number of points in each direction
    """
    def __init__(self, bbox=(0,0,1,1), gridSize=(20,20), isSubGrid=False, parent=None):
        self.nbNod  = -1
        self.nbEle  = -1
        self.nodes  = []
        self.elems  = []
        self.lclzr = None
        self.genLclzr = False
        self.parent    = parent
        self.isSubGrid = isSubGrid

        if not self.isSubGrid:
            self.setDim(bbox, gridSize)
            X, Y = self.getCoordinates()
            self.setCoord (X, Y)
            connec = self.getConnectivities()
            self.setConnec(connec)

    def setDim(self, bbox, gridSize):
        assert gridSize[0] > 0
        assert gridSize[1] > 0
        self.gridSize = tuple(gridSize)
        self.bbox = tuple(bbox)
        self.bbdx = (self.bbox[2] - self.bbox[0]) / (self.gridSize[0]-1)
        self.bbdy = (self.bbox[3] - self.bbox[1]) / (self.gridSize[1]-1)
        if not self.isSubGrid:
            LOGGER.info('2DRegularGrid: bbox: %s', str(self.bbox))
            LOGGER.info('2DRegularGrid: size: %s', str(self.gridSize))
        else:
            LOGGER.info('2DRegularGrid.SubMesh: bbox: %s', str(self.bbox))
            LOGGER.info('2DRegularGrid.SubMesh: size: %s', str(self.gridSize))

    def setCoord(self, X, Y):
        self.nodes = [None] * len(X)
        for i, (x, y) in enumerate(zip(X,Y)):
            self.nodes[i] = FENode(i, i, x, y)

    def setNodes(self, nodes):
        assert self.isSubGrid
        self.nodes = nodes

    #@profile_each_line
    def setConnec(self, connec):
        #self.md5.update(connec.view(np.uint8))

        self.elems = [None] * len(connec)
        if len(connec[0]) == 2:
            for ie, (no1, no2) in enumerate(connec):
                self.elems[ie] = FEElementL2(ie, ie, self.nodes[no1], self.nodes[no2])
        elif len(connec[0]) == 3:
            for ie, (no1, no2, no3) in enumerate(connec):
                self.elems[ie] = FEElementT3(ie, ie, self.nodes[no1], self.nodes[no2], self.nodes[no3])
        elif len(connec[0]) == 4:
            for ie, (no1, no2, no3, no4) in enumerate(connec):
                self.elems[ie] = FEElementQ4(ie, ie, self.nodes[no1], self.nodes[no2], self.nodes[no3], self.nodes[no4])
        elif len(connec[0]) == 6:
            for ie, (no1, no2, no3, no4, no5, no6) in enumerate(connec):
                self.elems[ie] = FEElementT6L(ie, ie, self.nodes[no1], self.nodes[no2], self.nodes[no3], self.nodes[no4], self.nodes[no5], self.nodes[no6])
        else:
            raise ValueError

        #if self.genLclzr:
        #    self.lclzr = FEMeshQTLocalizer(self)

    def setElements(self, elems):
        assert self.isSubGrid
        self.elems = elems

    def getGridSize(self):
        return self.gridSize

    def getNbNodes(self):
        return self.gridSize[0] * self.gridSize[1]

    def getNbElements(self):
        return (self.gridSize[0]-1) * (self.gridSize[1]-1)

    def getBbox(self, stretchfactor=0.0, margins=(0, 0)):
        xmin = self.bbox[0]
        ymin = self.bbox[1]
        xmax = self.bbox[2]
        ymax = self.bbox[3]
        dx = (xmax-xmin)*stretchfactor + margins[0]
        dy = (ymax-ymin)*stretchfactor + margins[1]
        return (xmin-dx, ymin-dy, xmax+dx, ymax+dy)

    def getNodeIJ(self, ix, iy):
        """
        Return the node identified by the indexes (ix, iy)
        """
        assert(0 <= ix < self.gridSize[0])
        assert(0 <= iy < self.gridSize[1])
        ind = ix*self.gridSize[0] + iy
        return self.getNode(ind)

    def getNode(self, ind):
        """
        Return the node at index ind
        """
        return self.nodes[ind]

    def getNodes(self):
        """
        Return all the nodes
        """
        return self.nodes

    def getNodeNumbersGlobal(self):
        """
        """
        return [n.getGlobalIndex() for n in self.nodes]

    def getNodeNumbersLocal(self):
        """
        """
        return [n.getLocalIndex() for n in self.nodes]

    def iterNodes(self):
        for item in self.nodes:
            yield item

    def getCoordinates(self):
        """
        Return the coordinates of all the nodes of the grid
        """
        X, Y = self.getMeshGrid()
        return (X.ravel(), Y.ravel())

    def getMeshGrid(self):
        """
        Return the meshgrid (in np sense) of the nodes
        """
        x = np.linspace(self.bbox[0], self.bbox[2], self.gridSize[0])
        y = np.linspace(self.bbox[1], self.bbox[3], self.gridSize[1])
        return np.meshgrid(x, y, indexing='ij')

    def getElement(self, ielem):
        return self.elems[ielem]

    def getElements(self):
        return self.elems

    def iterElements(self):
        for item in self.elems:
            yield item

    def getConnectivities(self):
        if not self.isSubGrid:
            nx, ny = self.gridSize
            connec = []
            for ix in range(nx-1):
                for iy in range(ny-1):
                    connec.append( ((ix+0)*ny + (iy+0),
                                    (ix+1)*ny + (iy+0),
                                    (ix+1)*ny + (iy+1),
                                    (ix+0)*ny + (iy+1)) )
        else:
            connec = [e.getConnectivities() for e in self.elems]
        return np.array(connec)

    def getT3XtrnConnectivities(self):
        if not self.isSubGrid:
            nx, ny = self.gridSize
            connec = []
            for ix in range(nx-1):
                for iy in range(ny-1):
                    connec.append( ((ix+0)*ny + (iy+0), (ix+1)*ny + (iy+0), (ix+0)*ny + (iy+1)) )  # 1, 2, 4
                    connec.append( ((ix+1)*ny + (iy+1), (ix+0)*ny + (iy+1), (ix+1)*ny + (iy+0)) )  # 3, 2, 4
        else:
            connec = []
            for e in self.elems:
                cs = e.getT3XtrnConnectivities()
                if isinstance(cs[0], int):
                    connec.append(cs)
                else:
                    for c in cs:
                        connec.append(c)
        return np.array(connec)

    def getT3SplitConnectivities(self):
        return self.getT3XtrnConnectivities()

    def genSkin(self, doChain = True):
        """
        Shall return a list of (x, y) points forming the skin
        """
        skin = []
        nx, ny = self.gridSize
        iy = 0
        for ix in range(nx-1):
            no1 = ix*ny + iy
            no2 = no1 + ny
            skin.append( (no1, no2) )
        ix = nx-1
        for iy in range(ny-1):
            no1 = ix*ny + iy
            no2 = no1 + 1
            skin.append( (no1, no2) )
        iy = ny-1
        for ix in range(nx-1, 0, -1):
            no1 = ix*ny + iy
            no2 = no1 - ny
            skin.append( (no1, no2) )
        ix = 0
        for iy in range(ny-1, 0, -1):
            no1 = ix*ny + iy
            no2 = no1 - 1
            skin.append( (no1, no2) )

        subGrid = self.__class__(isSubGrid=True, parent=self)    # Instantiate the same type as self
        subGrid.setDim(self.bbox, self.gridSize)
        subGrid.setNodes(self.nodes)
        subGrid.setConnec(skin)
        return subGrid

    def genCompactGrid(self):
        return self

    def isSubMesh(self):
        return self.isSubGrid

    def localizePoints(self, X, Y, gridSize=None, relative_tolerance=1.0e-12):
        lclzr = FE2DRegularGridLocalizer(self.bbox, gridSize)
        elems = lclzr.getElementsOfPoints(X, Y, relative_tolerance)
        return elems

    def integrate(self, v):
        s = 0.0
        for ix0 in range(self.gridSize[0]-1):
            ix1 = ix0+1
            for iy0 in range(self.gridSize[1]-1):
                iy1 = iy0+1
                no1 = FENode( *self.getNodeIJ(ix0, iy0) )
                no2 = FENode( *self.getNodeIJ(ix1, iy0) )
                no3 = FENode( *self.getNodeIJ(ix1, iy1) )
                no4 = FENode( *self.getNodeIJ(ix0, iy1) )
                ele = FEElementT3(-1, -1, no1, no2, no4)
                s += ele.integrate(v)
                ele = FEElementT3(-1, -1, no3, no4, no2)
                s += ele.integrate(v)
                raise NotImplementedError('FE2DRegularGrid.integrate: invalid code')
        return s

class FE2DRegularGridLocalizer:
    """
    gridSize is the number of points in each direction
    """
    def __init__(self, bbox, gridSize=(20,20)):
        assert gridSize[0] > 0
        assert gridSize[1] > 0
        self.gridSize = tuple(gridSize)
        self.bbox = tuple(bbox)
        self.bbdx = (self.bbox[2] - self.bbox[0]) / (self.gridSize[0]-1)
        self.bbdy = (self.bbox[3] - self.bbox[1]) / (self.gridSize[1]-1)

    def getSquare(self, ix, iy):
        xmin = self.bbox[0] + ix*self.bbdx
        xmax = xmin + self.bbdx
        ymin = self.bbox[1] + iy*self.bbdy
        ymax = ymin + self.bbdy
        return (xmin, ymin, xmax, ymax)

    def getPointsInSquare(self, X, Y, ix, iy):
        xmin = self.bbox[0] + ix*self.bbdx
        xmax = xmin + self.bbdx
        ymin = self.bbox[1] + iy*self.bbdy
        ymax = ymin + self.bbdy
        LOGGER.debug('FE2DRegularGridLocalizer: search box (%d,%d)', ix, iy)
        LOGGER.debug('       ll: (%f, %f)', xmin, ymin)
        LOGGER.debug('       ur: (%f, %f)', xmax, ymax)
        pts = np.nonzero( np.logical_and.reduce( (xmin <= X, X <= xmax, ymin <= Y, Y <= ymax) ) )
        return pts

    def getElemsInSquare(self, mesh, ix, iy):
        xmin = self.bbox[0] + ix*self.bbdx
        xmax = xmin + self.bbdx
        ymin = self.bbox[1] + iy*self.bbdy
        ymax = ymin + self.bbdy
        bbox = (xmin, ymin, xmax, ymax)
        LOGGER.debug('FE2DRegularGridLocalizer: search box (%d,%d)', ix, iy)
        LOGGER.debug('       ll: (%f, %f)', xmin, ymin)
        LOGGER.debug('       ur: (%f, %f)', xmax, ymax)

        eles = []
        for e in mesh.getElements():
            if e.intersecteBbox(bbox):
                eles.append(e)
        return eles

    def getSquareOfPoints(self, X, Y):
        """
        For each point in X,Y, returns the corresponding square (ix, iy)
        """
        xmin = self.bbox[0]
        ymin = self.bbox[1]
        xmax = self.bbox[2]
        ymax = self.bbox[3]
        idxs = []
        for x, y in zip(X,Y):
            ix = int((x-xmin) / self.bbdx) if xmin <= x <= xmax else None
            iy = int((y-ymin) / self.bbdy) if ymin <= y <= ymax else None
            idxs.append( (ix,iy) )
        return idxs

    def getElementsOfPoints(self, X, Y, relative_tolerance=1.0e-12):
        """
        For each point in X,Y, returns the corresponding FEElementT3
        """
        eles = []
        idxs = self.getSquareOfPoints(X, Y)
        for ix0,iy0,x,y in zip(idxs, X, Y):
            raise 'Algo a reviser comme dans FEMeshRGLocalizer'
            ix1 = ix0+1
            iy1 = iy0+1
            no1 = FENode( *self.getNode(ix0, iy0) )
            no2 = FENode( *self.getNode(ix1, iy0) )
            no3 = FENode( *self.getNode(ix1, iy1) )
            no4 = FENode( *self.getNode(ix0, iy1) )
            ele = FEElementT3(-1, -1, no1, no2, no4)
            if ele.estDedans(x, y):
                eles.append(ele)
            else:
                ele = FEElementT3(-1, -1, no3, no4, no2)
                if ele.estDedans(x, y):
                    eles.append(ele)
                else:
                    eles.append(None)
        return eles

# Cython does not have class static attributes
# i.e. made them globals
FEMeshRGLocalizer_FE_MAX_MAILLE = 200
FEMeshRGLocalizer_FE_MAILLE_ELEMENT = 4
class FEMeshRGLocalizer:
    """
    Localize points in a finite element mesh using a background regular grid
    """
    def __init__(self, mesh, gridSize = None):
        self.mesh = mesh
        if gridSize:
            self.gridSize = gridSize
            bbox = FEMesh.getBbox(self.mesh, stretchfactor=0.01)
        else:
            bbox, self.gridSize = self.__dimGridSize(self.mesh)

        self.regGrid  = FE2DRegularGridLocalizer(bbox, self.gridSize)
        self.tbl   = None
        self.cache = None

        # ---  Pickle file
        md5 = self.mesh.getMD5()
        p = cacheDefaultDir
        f = '.'.join( (md5, 'rg', 'json') )
        #f = '.'.join( (md5, 'rg', 'pkl') )
        self.cache = os.path.join(p, f)

        # ---  Unpickle or generate and pickle
        if os.path.isfile(self.cache):
            LOGGER.info('FEMeshRGLocalizer: cached info found in %s', self.cache)
            #self.tbl = json.load(open(self.cache, 'rb'))
            with open(self.cache, 'rb') as f:
                gc.disable()    # disable garbage collector
                self.tbl = pickle.load(f)
                gc.enable()     # reenable garbage collector
        else:
            self.__genSearchGrid(4)
            LOGGER.info('FEMeshRGLocalizer: caching info to %s', self.cache)
            #json.dump(self.tbl, open(self.cache, 'wb'))
            with open(self.cache, 'wb') as f:
                pickle.dump(self.tbl, f)

    def __dimGridSize(self, mesh):
        """
        Algo taken for Modeleur
        """
        bbx = FEMesh.getBbox(mesh)
        w = bbx[2] - bbx[0]
        h = bbx[3] - bbx[1]
        ratio = w / h

        # --- Limite le nombre de mailles à EF_MAX_MAILLE**2.
        nbrElemTot = self.mesh.getNbElements()
        nombreMailles = nbrElemTot * FEMeshRGLocalizer_FE_MAILLE_ELEMENT
        nombreMailles = min(nombreMailles, FEMeshRGLocalizer_FE_MAX_MAILLE**2)

        mailleX = int(math.sqrt(nombreMailles * ratio)) + 1
        mailleY = int(math.sqrt(nombreMailles / ratio)) + 1

        # ---  Pas de la maille
        tableDelX = w / (float(mailleX) - 0.5)
        tableDelY = h / (float(mailleY) - 0.5)

        # --- Nombre de mailles dans chaque direction
        tableIMax = int(math.ceil(w / tableDelX))
        tableJMax = int(math.ceil(h / tableDelY))

        # --- Pour être bien certain de coincer tout le domaine, on s'assure
        # --- qu'il y a au moins une maille dans toutes les directions
        tableIMax  = max(1, tableIMax)
        tableJMax  = max(1, tableJMax)

        # --- Décale de 0.2 * dimension pour être sur d'englober tout le domaine.
        tableDelX *= 0.2
        tableDelY *= 0.2
        new_bbx = (bbx[0]-tableDelX, bbx[1]-tableDelY, bbx[2]+tableDelX, bbx[3]+tableDelY)

        return new_bbx, (tableIMax, tableJMax)

    def __genSearchGrid(self, nbProc = -1):
        """
        Mount the list of elements for each grid square
        """
        LOGGER.info('FEMeshRGLocalizer.__genSearchGrid: background grid %s', self.gridSize)
        if nbProc < 0: nbProc = mp.cpu_count()-1

        # ---  Populate the table
        self.tbl = []
        for ix in range(self.gridSize[0]):
            self.tbl.append( [] )

        # ---  Distribute work on worker pool
        if nbProc > 1:
            pool = mp.Pool(nbProc)
            LOGGER.info('   distribute work load on a pool of %i work processes', nbProc)
            args = [ (self, ix) for ix in range(self.gridSize[0]) ]
            res  = pool.map_async(genSearchGridForIX, args)
            for ix,tbl in res.get():
                self.tbl[ix] = tbl
            #    LOGGER.debug('   ix: %3i, gridSize: %s' % (ix, self.gridSize))
            #    #for iy in range(self.gridSize[1]):
            #    #    LOGGER.debug('   ix: %3i, iy: %3i, number of elements: %i' % (ix, iy, len(self.tbl[ix][iy])))
        # ---  Algo sequentiel
        else:
            for ix in range(self.gridSize[0]):
                _, tbl_ix = genSearchGridForIX( (self,ix) )
                self.tbl[ix] = tbl_ix

    def getElementsOfPoints(self, X, Y, relative_tolerance=1.0e-12):
        """
        For each point in X,Y, returns the corresponding FEElementT3
        """
        eles = []
        idxs = self.regGrid.getSquareOfPoints(X, Y)
        for (ix,iy),x,y in zip(idxs, X, Y):
            LOGGER.debug('FEMeshRGLocalizer.getElementsOfPoints: localize %s in %s', (x, y), self.regGrid.getSquare(ix, iy))
            ele = None
            emin = None
            dmin = 1.0e+99
            try:
                for e in self.tbl[ix][iy]:
                    if e.estDedans(x, y):
                        ele = e
                        break
                    else:
                        dref = e.distMinRef(x, y)
                        if dref < dmin:
                            dmin, emin = dref, e
                            LOGGER.debug('   min distance: %s for %s', dmin, e)
                if not ele and emin:
                    ele = emin
                    LOGGER.debug('Extrapolating with: %s for %s', dmin, ele)
            except Exception as exc:
                LOGGER.error('FEMeshQTLocalizer.getElementsOfPoints:')
                LOGGER.error('   Exception: %s', str(exc))
                LOGGER.error('   Localize %s in %s', (x, y), e)
                pass
            eles.append(ele)
        return eles

class FEQuadTree(QTQuadTree.QTQuadTree):
    def __init__(self, *args, **kwargs):
        super(FEQuadTree, self).__init__() # *args, **kwargs)

class FEMeshQTLocalizer:
    """
    Localize points in a finite element mesh using a background quad-tree
    """

    def __init__(self, mesh):
        LOGGER.info('FEMeshQTLocalizer:')
        self.mesh = mesh
        self.__genQuadTree()

    def __genQuadTree(self):
        bbox = FEMesh.getBbox(self.mesh, stretchfactor=0.01)
        self.qTree = FEQuadTree(bbox, max_items=50, max_depth=16)
        cachePath = ''

        # ---  Pickle file
        md5 = self.mesh.getMD5()
        p = cacheDefaultDir
        f = '.'.join( (md5, 'qt', 'pkl') )
        cachePath = os.path.join(p, f)

        # ---  Unpickle or generate and pickle
        start = time.time()
        loaded = False
        if os.path.isfile(cachePath):
            LOGGER.info('FEMeshQTLocalizer: cached info found in %s', cachePath)
            try:
                #with open(cachePath, 'rb') as f:
                #    js = f.read()
                #    self.qTree = jsonpickle.decode(js)
                # disable garbage collector
                with open(cachePath, 'rb') as ofs:
                    gc.disable()    # disable garbage collector
                    self.qTree = pickle.load(ofs)
                    gc.enable()     # reenable garbage collector
                loaded = True
            except:
                LOGGER.info('FEMeshQTLocalizer: invalid cache')
        if not loaded:
            self.__genSearchGrid()
            # NOTE ---------
            # The cython/C++ FEQuadTree class is not picklable, but is fast
            # So for the mean time, deactivate pickle.
            #
            # LOGGER.info('FEMeshQTLocalizer: caching info to %s', cachePath)
            # #with open(cachePath, 'wb') as f:
            # #    js = jsonpickle.encode(self.qTree)
            # #    f.write(js)
            # with open(cachePath, 'wb') as ofs:
            #     pickle.dump(self.qTree, ofs)
            # END NOTE ---------
        end = time.time()
        LOGGER.info('FEMeshQTLocalizer: structure generated in %s s', end-start)

    def __genSearchGrid(self):
        """
        Get the the elements of each grid square
        """
        LOGGER.info('FEMeshQTLocalizer.__genSearchGrid')

        nel = self.mesh.getNbElements()
        nbl = max(nel//100, 100)
        ctr = 0 if nel > 1000 else (nel+1)  # Don't show progress for less than 1000 elem
        for e in self.mesh.iterElements():
            if ctr % nbl == 0: print('\r{pc:6.2f}%'.format(pc=100*(float(ctr)/float(nel))), end=" ")
            ctr += 1
            self.qTree.insert(e.il, e.getBbox())
        if ctr == nel: 
            print('\r{pc:6.2f}%'.format(pc=100.0))

    def getElementsOfPoints(self, X, Y, relative_tolerance=1.0e-12):
        """
        For each point in X,Y, returns the corresponding FEElementT3
        """
        eles = []
        npt = len(X)
        nbl = max(int(npt/100), 100)
        ctr = 0 if npt > 1000 else (npt+1)  # Don't show progress for less than 1000 points
        for x, y in zip(X, Y):
            if ctr % nbl == 0: print('\r{pc:6.2f}%'.format(pc=100*(float(ctr)/float(npt))), end=" ")
            ctr += 1
            epsx = x * relative_tolerance
            epsy = y * relative_tolerance
            iels = self.qTree.intersect( (x-epsx, y-epsy, x+epsx, y+epsy) )
            LOGGER.debug('FEMeshQTLocalizer.getElementsOfPoints: localize %s in %s', (x, y), iels)
            ele  = None
            emin = None
            dmin = 1.0e+99
            try:
                if len(iels) == 1:
                    ele = self.mesh.getElement(iels.pop())
                else:
                    for ie in iels:
                        e = self.mesh.getElement(ie)
                        if e.estDedans(x, y):
                            ele = e
                            break
                        else:
                            dref = e.distMinRef(x, y)
                            if abs(dref) < abs(dmin):
                                dmin, emin = dref, e
                                LOGGER.debug('   min distance: %s for %s', dmin, ie)
                    if not ele and emin:
                        ele = emin
                        LOGGER.debug('Extrapolating with: %s for %s', dmin, ele)
            except Exception as e:
                LOGGER.error('FEMeshQTLocalizer.getElementsOfPoints:')
                LOGGER.error('   Exception: %s', str(e))
                LOGGER.error('   Localize %s in %s', (x, y), iels)
                pass
            eles.append(ele)
        if ctr == npt: 
            print('\r{pc:6.2f}%'.format(pc=100.0))
        return eles

class FEInterpolatedPoints:
    """
    Interpolate data on points. The source is a mesh, either FiniteElement or RegularGrid.
    Points can be read from file or specified explicitly with setCoord(...)
    """
    def __init__(self, mesh, files = None):
        """
        Construct the interpolator with mesh and a background grid.
        Points can be read from file.
        """
        self.nbPnt  = -1
        self.coorX  = None
        self.coorY  = None
        self.elems  = []
        self.ksi    = np.zeros(1)   # for cython 
        self.eta    = np.zeros(1)
        self.mesh   = mesh
        self.md5    = None

        if files:
            reader = FEMeshIO.constructReaderFromFile(files)
            reader.readMesh(self)

    def __setCoordRef(self):
        """
        For each point, get the coordinates on the ref element.
        """
        assert(len(self.elems) == len(self.coorX))
        assert(len(self.elems) == len(self.coorY))
        ksi = np.zeros(len(self.coorX))
        eta = np.zeros(len(self.coorX))
        for i,(x,y,ele) in enumerate(zip(self.coorX, self.coorY, self.elems)):
            if ele: ksi[i], eta[i] = ele.transformeInverse(x, y)
        self.ksi = ksi
        self.eta = eta
        
    def __setCoordSmall(self, X, Y):
        """
        Set the coordinates from the points. The points are
        then localized in the base mesh.
        """
        # LOGGER.debug('FEInterpolatedPoints.__setCoordSmall %d', len(X))
        self.coorX = X
        self.coorY = Y
        self.nbPnt = len(X)
        self.elems = self.mesh.localizePoints(self.coorX, self.coorY)
        self.__setCoordRef()

    def __setCoordBig(self, X, Y):
        """
        Set the coordinates from the points. The points are
        then localized in the base mesh.
        """
        # LOGGER.debug('FEInterpolatedPoints.__setCoordBig %d', len(X))
        md5 = hashlib.md5()
        try:
            md5.update(X.view(np.uint8))
            md5.update(Y.view(np.uint8))
        except ValueError:
            md5.update(X.tobytes())
            md5.update(Y.tobytes())
        if self.md5 and self.md5.hexdigest() == md5.hexdigest(): return

        gmd5 = self.mesh.getMD5()
        smd5 = md5.hexdigest()
        p = cacheDefaultDir
        f = '.'.join( (gmd5, smd5, 'ip', 'pkl') )
        cachePath = os.path.join(p, f)

        start = time.time()
        loaded = False
        if os.path.isfile(cachePath):
            LOGGER.info('FEInterpolatedPoints: cached info found in %s', cachePath)
            try:
                with open(cachePath, 'rb') as ofs:
                    gc.disable()    # disable garbage collector
                    eles = pickle.load(ofs)
                    ksi  = pickle.load(ofs)
                    eta  = pickle.load(ofs)
                    gc.enable()     # reenable garbage collector
                self.coorX = X
                self.coorY = Y
                self.nbPnt = len(X)
                self.elems = [ self.mesh.getElement(ie) if ie else None for ie in eles ]
                self.ksi = ksi
                self.eta = eta
                loaded = True
            except Exception as e:
                LOGGER.info('FEInterpolatedPoints: invalid cache')
        if not loaded:
            LOGGER.info('FEInterpolatedPoints: caching info to %s', cachePath)
            self.coorX = X
            self.coorY = Y
            self.nbPnt = len(X)
            self.elems = self.mesh.localizePoints(self.coorX, self.coorY)
            self.__setCoordRef()
            with open(cachePath, 'wb') as ofs:
                eles = [ e.il if e else None for e in self.elems ]
                pickle.dump(eles, ofs)
                pickle.dump(self.ksi, ofs)
                pickle.dump(self.eta, ofs)
        end = time.time()
        self.md5 = md5
        LOGGER.info('FEInterpolatedPoints: structure generated in %s s', end-start)

    def setCoord(self, X, Y):
        """
        Set the coordinates from the points. The points are
        then localized in the base mesh.
        """
        # LOGGER.debug('FEInterpolatedPoints.setCoord %d', len(X))
        if X is self.coorX and Y is self.coorY: return 
        if len(X) < 1000:
            self.__setCoordSmall(X, Y)
        else:
            self.__setCoordBig(X, Y)

    def setConnec(self, *args, **kwargs):
        """
        Connectivities are not used.
        """
        pass

    def getNodes(self):
        """
        Returns the nodes of the elements of the mesh containing points.
        """
        uniquer = {}
        for elem in self.elems:
            uniquer[elem.n1.i] = None
            uniquer[elem.n2.i] = None
            uniquer[elem.n3.i] = None
        nodes = list(uniquer.keys())
        nodes.sort()
        LOGGER.debug('FEInterpolatedPoints: number of nodes: %9i', len(nodes))
        return nodes

    def getNbPoints(self):
        """
        Number of points.
        """
        return self.nbPnt

    def __interpolate_ndarray_1(self, data):
        """
        Interpolate data on the points.
        Data is a 1D numpy.array of double
        Version specialized for cython
        """
        # LOGGER.trace('FEInterpolatedPoints.__interpolate_ndarray_1')
        shp0 = self.coorX.shape[0]
        res = np.full(shp0, NAN)
        for i0, ele0 in enumerate(self.elems):
            if ele0: break
        if isinstance(ele0, FEElementT3):
            for i, eleT3 in enumerate(self.elems[i0:], i0):
                if eleT3:
                    res[i] = eleT3.interpolateRef_ndarray_1(self.ksi[i], self.ksi[i], data)
        elif isinstance(ele0, FEElementT6L):
            for i, eleT6L in enumerate(self.elems[i0:], i0):
                if eleT6L:
                    res[i] = eleT6L.interpolateRef_ndarray_1(self.ksi[i], self.ksi[i], data)
        else:
            for i, ele in enumerate(self.elems[i0:], i0):
                if ele:
                    res[i] = ele.interpolateRef_ndarray_1(self.ksi[i], self.ksi[i], data)
        return res
        
    def __interpolate_ndarray_2(self, data):
        """
        Interpolate data on the points.
        Data is a 2D numpy.array of double
        Version specialized for cython
        """
        # LOGGER.trace('FEInterpolatedPoints.__interpolate_ndarray_2')
        shp1 = data.shape[1]
        shp0 = self.coorX.shape[0]
        res = np.full( (shp0, shp1), NAN )
        for i0, ele0 in enumerate(self.elems):
            if ele0: break
        if isinstance(ele0, FEElementT3):
            for i, eleT3 in enumerate(self.elems[i0:], i0):
                if eleT3:
                    res[i] = eleT3.interpolateRef_ndarray_2(self.ksi[i], self.ksi[i], data)
        elif isinstance(ele0, FEElementT6L):
            for i, eleT6L in enumerate(self.elems[i0:], i0):
                if eleT6L:
                    res[i] = eleT6L.interpolateRef_ndarray_2(self.ksi[i], self.ksi[i], data)
        else:
            for i, ele in enumerate(self.elems[i0:], i0):
                if ele:
                    res[i] = ele.interpolateRef_ndarray_2(self.ksi[i], self.ksi[i], data)
        return res
        
    def interpolate(self, data):
        """
        Interpolate data on the points.
        """
        # LOGGER.trace('FEInterpolatedPoints.interpolate')
        if data.ndim == 1:
            res = self.__interpolate_ndarray_1(data)
        else:
            res = self.__interpolate_ndarray_2(data)
        return res

class FEProbeOnPoints:
    """
    Interpolate data on points. The source is a finite element mesh.
    Specialized for changing (moving) points.
    """
    def __init__(self, mesh):
        """
        Construct the interpolator with mesh and a background grid.
        """
        self.mesh  = mesh
        self.llzr  = self.mesh.getLocalizer()
        self.elems = None

    def __setCoord(self, X, Y):
        """
        Set the coordinates from the points. The points are
        then localized in the base mesh.
        """
        elemValid = (self.elems is not None) and (len(X) == len(self.elems))
        try:
            if elemValid:
                for x,y,e in zip(X, Y, self.elems):
                    if not e.estDedans(x,y):
                        elemValid = False
                        break
        except:
            elemValid = False
        if not elemValid:
            self.elems = self.llzr.getElementsOfPoints(X, Y)

    def getMesh(self):
        return self.mesh

    def interpolate(self, X, Y, data):
        """
        Interpolate data on the points.
        """
        def __do_intrp(e, x, y, d):
            if e:
                return e.interpolate(x, y, d)
            try:
                return (NAN,)*len(d[0])
            except:
                return NAN
        self.__setCoord(X,Y)
        return [ __do_intrp(e, x, y, data) for x,y,e in zip(X, Y, self.elems) ]


def genRegGrid(bbox, gridSize):
    bbdx = (bbox[2] - bbox[0]) / gridSize[0]
    bbdy = (bbox[3] - bbox[1]) / gridSize[1]
    return None


if __name__ == "__main__":
    import cProfile

    def t_probe(p):
        m = p.getMesh()
        pts = [ (-0.935, -0.812) ]
        d = np.zeros(m.getNbNodes())

        #X, Y = m.getNodes()
        #for x, y in zip(X,Y):
        for x,y in pts:
            print(x, y, p.interpolate( (x,), (y,), d))

    def t_main():
        d = 'E:/h2d2-dev/H2D2-tools/script/DADataAnalyzer/DALayers/'
        d = 'E:/Projets/ECCC/OP/grids/geometry/'
        reader = FEMeshIO.FEMeshH2D2T6LasT3IO( [d+'test.cor', d+'test.ele'],)
        mT3 = FEMesh()
        reader.readMesh(mT3)

        mT3 = mT3.genCompactGrid()
        writer = FEMeshIO.FEMeshH2D2T3IO( [d+'test.t3.cor', d+'test.t3.ele'],)
        mT3.write(writer)

        mT6 = mT3.genT6LGrid()
        writer = FEMeshIO.FEMeshH2D2T6LIO( [d+'test.t6l.cor', d+'test.t6l.ele'],)
        mT6.write(writer)

        for ele in mT3.iterElements():
            if ele.detJ() < 0.0:
                raise RuntimeError('%s : detJ negatif: %.3f' % (ele, ele.detJ()))

        for ele in mT6.iterElements():
            if ele.detJ() < 0.0:
                raise RuntimeError('%s : detJ negatif: %.3f' % (ele, ele.detJ()))

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)
    #cProfile.run('t_main()', sort='tottime')
    t_main()
