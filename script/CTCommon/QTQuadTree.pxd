# -*- coding: utf-8 -*-
# distutils: language = c++
## cython: profile=True
## cython: linetrace=True
# cython: boundscheck(False)
# cython: wraparound(False)
# cython: initializedcheck(False)
# cython: nonecheck(False)
# cython: language_level(3)

from libcpp.vector cimport vector

cdef extern from "QTQuadTree_c.h":
    cdef cppclass QTTree:
        QTTree()
        QTTree(double[4])
        QTTree(double[4], long, long)
        
        void            insert     (long, double[4])
        void            remove     (long, double[4])
        vector[long]    intersect  (double[4])

        const double*   bbox       ()
        long            size       ()
        long            maxItemsEff()
        long            maxDepthEff()
        long            maxItemsLmt()
        long            maxDepthLmt()

cdef class QTQuadTree:
    cdef QTTree *head
