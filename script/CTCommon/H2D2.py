#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2003-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from .external import async_popen
import os
import subprocess

def is_List(obj):
    return isinstance(obj, (list, tuple))

class H2D2:
    def __init__(self, args = ['h2d2'], cwd=None):
        self.p = async_popen.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=cwd)
        o = async_popen.recv_some(self.p, t=5.0, e=0)
        if (o.strip()[-2:] != '>>'):
            try:
                self.p.kill()
            except:
                pass
            raise RuntimeError('Could not successfully start %s' % args[0])
        self.xeq_cmd("MTFrame__tag__ = '##__EOR__##'")

    def __del__(self):
        try:
            pid = self.p.pid
            self.p.kill()
            os.kill(pid, 0)
        except:
            pass

    def __clean_out(self, out):
        if (out == ''): return out
        lines = []
        for l in out.split(os.linesep):
            if l[0:3] == '>> ':
                l = l[3:]
            if (len(l) > 0):
                try:
                    lines.append( str(l, 'utf8') )
                except:
                    lines.append( str(l) )
        if (len(lines) == 0):
            return ''
        elif (len(lines) == 1):
            return lines[0]
        else:
            return '\n'.join(lines)

    def __xeq_one_cmd(self, cmd, send_and_forget=False):
        lines = []
        c = cmd.strip()
        print(('H2D2.__xeq_one_cmd: cmd = %s' % c))

        self.p.send('%s\n %s' % (c, 'print(MTFrame__tag__)\n') )
        if (send_and_forget): return ''

        while True:
            o = async_popen.recv_some(self.p, e=0)
            if (o != ''):
                l = self.__clean_out(o)
                if ('##__EOR__##' in l):
                    i = l.index('##__EOR__##')
                    l = l[:i]
                    if (len(l) > 0): lines.append(l)
                    break
                if (len(l) > 0): lines.append(l)
        if (len(lines) == 0):
            return ''
        elif (len(lines) == 1):
            return lines[0]
        else:
            return '\n'.join(lines)

    def xeq_cmd(self, cmd, send_and_forget=False):
        __cmd = cmd if is_List(cmd) else [cmd]
        lines = []
        for c in __cmd:
            l = self.__xeq_one_cmd(c, send_and_forget)
            if (l is not ''): lines.append(l)

        if (len(lines) == 0):
            return ''
        elif (len(lines) == 1):
            return lines[0]
        else:
            return '\n'.join(lines)

if __name__ == '__main__':
    h2d2 = H2D2()
    print(h2d2.xeq_cmd( ['help()'] ))
    print(h2d2.xeq_cmd( ['exit()'] ))

