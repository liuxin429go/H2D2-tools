# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import calendar
import datetime
from dateutil.parser import parse as dateparse
from pytimeparse     import parse as deltaparse

def get_clr(id):
    clr_tbl = ['#FFA500',   # 'orange',
               '#7FFF00',   # 'chartreuse', 7FFF00
               '#00FF7F',   # 'springgreen',
               '#00BFFF',   # 'deepskyblue',
               '#9400D3',   # 'darkviolet',
               '#FF1493',   # 'deeppink',
               '#FFFF00',   # 'yellow',
               '#008000',   # 'green',
               '#00FFFF',   # 'cyan',
               '#0000FF',   # 'blue',
               '#FF00FF',   # 'magenta',
               '#FF0000',   # 'red'
              ]
    return clr_tbl[id % len(clr_tbl)]

lin_stl  = { 'nwtn': 'solid',
             'pcrd': 'dashed',
             'gmrs': 'dotted' }

clr_lum  = { 'nwtn': 1.0,
             'pcrd': 0.3,
             'gmrs': 0.7 }

#----------------------------------------------------
#    Fonctions d'utilitaires de timedelta
#----------------------------------------------------
def seconds_to_delta(seconds, *args):
    """
    Retourne une durée en secondes sour forme d'un
    datetime.timedelta
    """
    return datetime.timedelta(seconds=seconds)

def seconds_to_iso(seconds, *args):
    """
    Retourne une durée en secondes au format 'dd:hh:mm:ss'
    """
    dt = datetime.timedelta(seconds=seconds)
    r = dt.total_seconds()
    d, r = divmod(r, 3600*24)
    h, r = divmod(r, 3600)
    m, r = divmod(r, 60)
    s = r
    return '%02i:%02i:%02i:%02i' % (d,h,m,s)

def parse_to_seconds(w, *args):
    """
    Retourne une durée sous forme d'une chaîne
    en secondes
    """
    return deltaparse(w)

def parse_to_timedelta(w, *args):
    """
    Retourne une durée sous forme d'une chaîne
    en datetime.timedelta
    """
    return datetime.timedelta(seconds=deltaparse(w))

#----------------------------------------------------
#    Fonctions d'utilitaires de date
#----------------------------------------------------
def epoch_to_iso(epoch, *args):
    """
    Retourne une epoch en secondes au format iso.
    """
    try:
        d = datetime.datetime.fromtimestamp(epoch)
        return d.isoformat(' ')
    except:
        return '%s' % epoch

def epoch_to_isoutc(epoch, *args):
    """
    Retourne une epoch UTC en secondes au format iso.
    """
    try:
        d = datetime.datetime.utcfromtimestamp(epoch)
        return d.isoformat(' ')
    except:
        return '%s' % epoch

def epoch_to_datetime(epoch):
    """
    Retourne une date UTC à partir d'une epoch
    """
    return datetime.datetime.utcfromtimestamp( int(epoch) )

def parse_to_epoch(date):   # parse_to_epoch
    """
    Retourne une epoch d'une date UTC
    http://stackoverflow.com/questions/255035/converting-datetime-to-posix-time/5872022#5872022
    """
    try:        # in case we have a datetime
        return calendar.timegm(date.timetuple())
    except AttributeError:
        d = dateparse(date)
        return calendar.timegm(d.timetuple())

def parse_to_datetime(date):
    """
    Retourne une epoch d'une date UTC
    http://stackoverflow.com/questions/255035/converting-datetime-to-posix-time/5872022#5872022
    """
    return dateparse(date)

#----------------------------------------------------
#    Fonctions d'utilitaires de fichiers
#----------------------------------------------------
import glob
import os
import sys
def find_files_in_syspath(pattern, modules=['.'], matchFunc=os.path.isfile):
    ret = []
    for dirname in sys.path:
        for module in modules:
            searchpath = os.path.join(dirname, module, pattern)
            for match in glob.glob(searchpath):
                if matchFunc(match):
                    ret.append(match)
    return ret

if __name__ == "__main__":
    s = 123456.789
    print('seconds_to_delta:   ', seconds_to_delta(s))
    print('seconds_to_iso:     ', seconds_to_iso(s))
    print('parse_to_seconds:   ', parse_to_seconds(seconds_to_iso(s)))
    print('parse_to_timedelta: ', parse_to_timedelta(seconds_to_iso(s)))
    print()

    d = datetime.datetime.now()
    print('epoch_to_iso:       ', epoch_to_iso(0))
    print('epoch_to_isoutc:    ', epoch_to_isoutc(0))
    print('parse_to_epoch:     ', parse_to_epoch("1969-12-31 19:00:00"))
    print('parse_to_datetime:  ', parse_to_datetime("1969-12-31 19:00:00"))
