#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import wx

class CTTextCtrlHandler(logging.Handler):
    """
    Handler for the logging sytem, to redirect the output to a wx control
    """
    def __init__(self, ctrl):
        super(CTTextCtrlHandler, self).__init__()
        self.ctrl = ctrl

    def emit(self, record):
        s = self.format(record) + '\n'
        wx.CallAfter(self.ctrl.WriteText, s)
