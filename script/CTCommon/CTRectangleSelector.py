#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import matplotlib.widgets as mwg

# RectangleSelector of matplotlib version 1.5.1 does not handle
# lines adequatly. Lines are always drawn from (xmin,ymin) to (xmax,ymax)
# instead of retaining the end points coordinates.
class CTRectangleSelector(mwg.RectangleSelector):
    def draw_shape(self, extents):
        if self.drawtype == 'box':
            mwg.RectangleSelector.draw_shape(self, extents)
        if self.drawtype == 'line':
            x0, x1, y0, y1 = extents
            self.to_draw.set_data([x0, x1], [y0, y1])

    def get_data(self):
        return self.to_draw.get_data()

    def clear(self):
        for artist in self.artists:
            artist.set_visible(False)
        self.update()

if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plt

    def cb(eclick, erelease):
        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        print("(%3.2f, %3.2f) --> (%3.2f, %3.2f)" % (x1, y1, x2, y2))

    def main():
        fig, current_ax = plt.subplots()                    # make a new plotingrange
        N = 100000                                       # If N is large one can see
        x = np.linspace(0.0, 10.0, N)                    # improvement by use blitting!

        plt.plot(x, +np.sin(.2*np.pi*x), lw=3.5, c='b', alpha=.7)  # plot something
        plt.plot(x, +np.cos(.2*np.pi*x), lw=3.5, c='r', alpha=.5)
        plt.plot(x, -np.sin(.2*np.pi*x), lw=3.5, c='g', alpha=.3)

        r = CTRectangleSelector(current_ax, onselect=cb, drawtype='box')
        r.set_active(True)
        plt.show()

    main()
