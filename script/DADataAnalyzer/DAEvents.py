# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx.lib.newevent

UpdateDisplayListEvent, EVT_UPDATE_DISPLAY_LIST = wx.lib.newevent.NewCommandEvent()
EVT_UPDATE_DISPLAY_LIST_ID = 1
