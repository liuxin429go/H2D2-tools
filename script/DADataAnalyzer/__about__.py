# -*- coding: utf-8 -*-

__name__ = 'Data Analyzer'
__version__ = '21.04dev'
__date__ = 'yyyy-mm-dd'
__author__ = 'Yves Secretan'
__author_email__ = 'yves.secretan@ete.inrs.ca'
__copyright__ = ['Copyright (c) 2014-2018 INRS', 'Copyright (c) 2019 Yves Secretan']
__website__ = 'http://www.gre-ehn.ete.inrs.ca/H2D2'
__license__ = ['GNU Lesser General Public License, Version 3.0',
'''
DADataAnalyzer is free software: you can redistribute it and/or modify it under the terms of 
the GNU Lesser General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>. 
''']
# __status__ = 'Development Status :: 5 - Production/Stable'

__libraries__ = [
    'python', 'wxpython', 'matplotlib', 'mpldatacursor', 'gdal', 'pyqtree', 'pyshp', 'numpy'
    ]

__credits__ = {
    '16x16 Free Application Icons' :
        {'Licence' : '<a href=http://creativecommons.org/licenses/by-sa/3.0/>Creative Commons Attribution-Share Alike 3.0</a>',
         'Site' : 'http://www.aha-soft.com',
        },
    'Original GIS icons theme' :
        {'Licence' : '<a href=http://creativecommons.org/licenses/by-sa/3.0/>Creative Commons Attribution-Share Alike 3.0</a>',
         'Text' : 'bitmaps/LICENSE.TXT',
         'Site' : 'http://robert.szczepanek.pl/'
        },
    }
