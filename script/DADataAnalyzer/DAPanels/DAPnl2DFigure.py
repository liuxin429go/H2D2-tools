# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2015-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging

from CTCommon.CTWindow import CTWindowMPLFigure

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Panel.2D")

class DAPnl2DFigure(CTWindowMPLFigure):
    def __init__(self, *args, **kwds):
        self.axes = kwds.pop('axes')
        super(DAPnl2DFigure, self).__init__(*args, **kwds)

        self.layers = []        # list of DALayer
        self.isChanged = False

    def __get_layer_idx(self, layer):
        for i, l in enumerate(self.layers):
            if l == layer: return i
        raise ValueError

    def get_axes(self):
        return self.axes

    def get_layer(self, idx):
        return self.layers[idx]

    def get_layers(self):
        return self.layers

    def add_layer(self, layer):
        self.layers.append(layer)
        self.isChanged = True

    def del_item(self, layer, item=None):
        if item and len(layer) > 1:
            layer.remove(item)
            self.isChanged = True
        else:
            if item: assert layer[0] is item
            self.del_layer(layer)

    def del_layer(self, layer):
        layer.detach()
        self.layers.remove(layer)
        self.isChanged = True

    def drag_layer(self, lyr_src, lyr_dst):
        idx_old = self.__get_layer_idx(lyr_src)
        idx_new = self.__get_layer_idx(lyr_dst)
        if   idx_new < idx_old:
            self.layers.insert(idx_new,   self.layers.pop(idx_old))
        elif idx_new > idx_old:
            self.layers.insert(idx_new-1, self.layers.pop(idx_old))
        self.isChanged = True

    def is_checked(self, layer, item=None):
        if item: return item.isVisible()
        return layer.isVisible()

    def check_item(self, flag, layer, item=None):
        LOGGER.trace('DAPnl2DFigure.check_item: %s %s', flag, layer)
        if item:
            if item.isVisible() != flag:
                item.setVisible(flag)
                self.isChanged = True
        else:
            if layer.isVisible() != flag:
                layer.setVisible(flag)
                self.isChanged = True

    def __detach_layers(self):
        for lyr in self.layers:
            lyr.detach()

    def __attach_layers(self):
        for lyr in self.layers:
            lyr.attach(self.axes)

    def on_redraw(self, *args, **kwargs):
        if self.isChanged:
            self.__detach_layers()
            self.__attach_layers()
            self.canvas.draw()
            self.isChanged = False
