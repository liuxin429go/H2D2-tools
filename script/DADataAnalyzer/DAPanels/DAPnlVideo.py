# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import enum
import logging
import os
import time
import wx
import MplayerCtrl as mpc
import wx.lib.buttons as buttons

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Panel.Times")

dirName = os.path.dirname(os.path.abspath(__file__))
bitmapDir = os.path.normpath(os.path.join(dirName, 'bitmaps'))

States = enum.Enum('States', ('off', 'stopped', 'started', 'paused'))

class DAPnlVideo(wx.Panel):
    zeroTime = time.strftime('%H:%M:%S', time.gmtime(0))

    def __init__(self, *args, **kwds):
        self.fig  = kwds.pop('fig',  None)
        self.axes = kwds.pop('axes', None)
        mplayer = kwds.pop('mplayer', 'mplayer.exe')

        super(DAPnlVideo, self).__init__(*args, **kwds)

        self.__build_buttons()
        self.playbackSlider = wx.Slider(self, size=wx.DefaultSize)

        self.mplayer = mpc.MplayerCtrl(self, -1, mplayer)

        # create track counter
        self.trackCounter = wx.StaticText(self, label="00:00:00")

        # set up playback timer
        self.playbackTimer = wx.Timer(self)

        # create sizers
        mainSizer    = wx.BoxSizer(wx.VERTICAL)
        controlSizer = wx.BoxSizer(wx.HORIZONTAL)
        sliderSizer  = wx.BoxSizer(wx.HORIZONTAL)

        # layout
        sliderSizer.Add(self.playbackSlider, 1, wx.ALL|wx.EXPAND, 5)
        sliderSizer.Add(self.trackCounter, 0, wx.ALL|wx.CENTER, 5)
        controlSizer.Add(self.btn_play,  0, wx.LEFT, 3)
        controlSizer.Add(self.btn_next,  0, wx.LEFT, 3)
        controlSizer.Add(self.btn_pause, 0, wx.LEFT, 3)
        controlSizer.Add(self.btn_stop,  0, wx.LEFT, 3)
        mainSizer.Add(self.mplayer, 1, wx.ALL|wx.EXPAND, 5)
        mainSizer.Add(sliderSizer,  0, wx.ALL|wx.EXPAND, 5)
        mainSizer.Add(controlSizer, 0, wx.ALL|wx.CENTER, 5)
        self.SetSizer(mainSizer)

        self.Bind(mpc.EVT_MEDIA_STARTED,   self.on_media_started)
        self.Bind(mpc.EVT_MEDIA_FINISHED,  self.on_media_finished)
        self.Bind(mpc.EVT_PROCESS_STARTED, self.on_process_started)
        self.Bind(mpc.EVT_PROCESS_STOPPED, self.on_process_stopped)
        self.Bind(mpc.EVT_STDERR, self.on_stderr)
#        self.Bind(mpc.EVT_STDOUT, self.on_stdout)

        self.Bind(wx.EVT_TIMER, self.on_update_playback)
        self.Bind(wx.EVT_SLIDER,self.on_update_slider)

        self.Layout()

        self.layers = []
        self.file = ''
        self.path = ''
        self.__set_state(States.off)

    #----------------------------------------------------------------------
    def __set_state(self, state):
        if state == States.off:
            self.btn_play.Enable(False)
            self.btn_pause.Enable(False)
            self.btn_stop.Enable(False)
        elif state == States.stopped:
            self.btn_play.Enable(True)
            self.btn_pause.Enable(False)
            self.btn_stop.Enable(False)
        elif state == States.started:
            self.btn_play.Enable(True)
            self.btn_pause.Enable(True)
            self.btn_stop.Enable(True)
        elif state == States.paused:
            self.btn_play.Enable(True)
            self.btn_pause.Enable(True)
            self.btn_stop.Enable(True)

    #----------------------------------------------------------------------
    def __do_pause(self, pause):
        """"""
        if pause:
            self.mplayer.Pause()
            self.playbackTimer.Stop()
            self.__set_state(States.paused)
        else:
            self.mplayer.Pause()
            self.playbackTimer.Start()
            self.__set_state(States.started)

    #----------------------------------------------------------------------
    def __build_one_btn(self, bitmap, handler, name):
        """"""
        img = wx.Bitmap(os.path.join(bitmapDir, bitmap))
        btn = buttons.GenBitmapButton(self, bitmap=img, name=name)
        btn.SetInitialSize()
        btn.Bind(wx.EVT_BUTTON, handler)
        return btn

    #----------------------------------------------------------------------
    def __build_buttons(self):
        """
        Builds the audio bar controls
        """
        self.btn_play  = self.__build_one_btn('player_play.png', self.on_btn_start, 'play')
        self.btn_next  = self.__build_one_btn('player_next.png', self.on_btn_next,  'next')
        self.btn_pause = self.__build_one_btn('player_pause.png',self.on_btn_pause, 'pause')
        self.btn_stop  = self.__build_one_btn('player_stop.png', self.on_btn_stop,  'stop')

    #----------------------------------------------------------------------
    def get_axes(self):
        return self.axes

    def get_figure(self):
        return self.fig

    def get_layer(self, idx):
        return self.layers[idx]

    def get_layers(self):
        return self.layers

    #----------------------------------------------------------------------
    def on_stderr(self, event):
        print('stderr: %s' % event.data)

    def on_stdout(self, event):
        print('stdout: %s' % event.data)

    #----------------------------------------------------------------------
    def on_media_started(self, event):
        print('Media started!')
        t_len = 0
        while t_len < self.mplayer.GetTimeLength(): # waiting loop
            t_len = self.mplayer.GetTimeLength()
        self.playbackSlider.SetRange(0, t_len)
        self.playbackSlider.SetValue(0)
        self.trackCounter.SetLabel(DAPnlVideo.zeroTime)
        self.__set_state(States.paused)

    #----------------------------------------------------------------------
    def on_media_finished(self, event):
        print('Media finished!')
        self.playbackTimer.Stop()
        self.__set_state(States.stopped)

    #----------------------------------------------------------------------
    def on_process_started(self, event):
        print('Process started!')

    #----------------------------------------------------------------------
    def on_process_stopped(self, event):
        print('Process stopped!')

    #----------------------------------------------------------------------
    def on_btn_start(self, event):
        """
        Start the process
        Load the file and begin playback
        """
        try:
            self.on_stop(None)
        except:
            pass
        print("starting %s" % self.path)
        self.mplayer.Start()
        self.mplayer.Loadfile(self.path)

    #----------------------------------------------------------------------
    def on_btn_next(self, event):
        """
        """
        self.mplayer.FrameStep()
        self.on_update_playback(None)

    #----------------------------------------------------------------------
    def on_btn_pause(self, event):
        """"""
        if self.playbackTimer.IsRunning():
            print("pausing...")
            self.__do_pause(False)
        else:
            print("unpausing...")
            self.__do_pause(True)

    #----------------------------------------------------------------------
    def on_btn_stop(self, event):
        """"""
        print("stopping...")
        self.mplayer.Stop()
        self.playbackTimer.Stop()
        self.playbackSlider.SetValue(0)
        self.trackCounter.SetLabel(DAPnlVideo.zeroTime)
        self.__set_state(States.stopped)

    #----------------------------------------------------------------------
    def on_update_playback(self, event):
        """
        Updates playback slider and track counter
        """
        try:
            offset = self.mplayer.GetTimePos()
        except:
            return
        mod_off = str(offset)[-1]
        if mod_off == '0':      # every second
            print("Update offset")
            offset = int(offset)
            self.playbackSlider.SetValue(offset)
            secsPlayed = time.strftime('%H:%M:%S', time.gmtime(offset))
            self.trackCounter.SetLabel(secsPlayed)

    #----------------------------------------------------------------------
    def on_update_slider(self, event):
        """
        """
        time = event.GetSelection()
        self.mplayer.Seek(time, 2)  # Absolute time

    #----------------------------------------------------------------------
    def set_file(self, path):
        """
        Add a Movie and start playing it
        """
        self.file = path
        self.path = '"%s"' % path.replace("\\", "/")
        self.on_btn_start(None)

    def show(self, b):
        pass


#----------------------------------------------------------------------
#----------------------------------------------------------------------
if __name__ == "__main__":
    class Frame(wx.Frame):
        def __init__(self, parent, id, title, mplayer):
            super(Frame, self).__init__(parent, id, title, size = (1000, 800))
            self.panel = DAPnlVideo(self, -1, mplayer = mplayer)
            self.Show()

    app = wx.App(False)
    frame = Frame(None, -1, 'MediaPlayer Using MplayerCtrl', 'mplayer.exe')
    frame.panel.set_file(r'E:/dev/H2D2/tools/script/DADataAnalyzer/output.avi')

    app.MainLoop()
