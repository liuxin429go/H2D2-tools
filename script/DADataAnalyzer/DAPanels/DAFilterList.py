# -*- coding: utf-8 -*-
# generated by wxGlade 0.6.3 on Fri Oct  11 08:24:32 2013
#************************************************************************
# --- Copyright (c) INRS 2013-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx
from CTCommon.CTCheckListCtrl_v2 import CTCheckListCtrl
from .DAFilter import DAValueFilter

class DAFilterList:
    def __init__(self, filters = None):
        self.filters = []
        if (filters): self.asgFilters()

    def __len__(self):
        return len(self.filters)

    def addFilter(self, filter):
        self.filters.append(filter)

    def asgFilters(self, filters):
        self.filters = filters

    def isValid(self, v):
        for f in self.filters:
            if not f.isValid(v): return False
        return True

    def __str__(self):
        s = ', '.join( [ '"%s"' % str(f) for f in self.filters] )
        return '[ %s ]' % s

class DAFilterListCtrl(CTCheckListCtrl):
    help_ = """\
Expression must be a valid Python logical expression,
where {n} is the placeholder for the nth value.
    """
    help = '%s\n%s' % (help_, CTCheckListCtrl.help)

    def __init__(self, *args, **kwds):
        kwds['auto_extend'] = kwds.pop('auto_extend', True)
        super(DAFilterListCtrl, self).__init__(*args, **kwds)

        self.SetToolTip(DAFilterListCtrl.help)

    def __buildFilterList(self):
        filters = DAFilterList()
        for i in range(self.GetItemCount()):
            if self.IsItemChecked(i):
                f = self.GetItem(i).GetText()
                if (f): filters.addFilter(DAValueFilter(f))
        return filters

    def FilterListOnIndex(self, lst):
        filters = self.__buildFilterList()
        if len(filters) == 0: return None
        ret = []
        for i, v in enumerate(lst):
            if filters.isValid(i): ret.append(i)
        return ret

    def FilterListOnValue(self, lst):
        filters = self.__buildFilterList()
        if len(filters) == 0: return None
        ret = []
        for i, v in enumerate(lst):
            if filters.isValid(v): ret.append(i)
        return ret


if __name__ == "__main__":
    class TestFrame(wx.Frame):
        def __init__(self, *args, **kwds):
            kwds["style"] = wx.DEFAULT_FRAME_STYLE
            super(TestFrame, self).__init__(*args, **kwds)
            self.panel_1 = DAFilterListCtrl(self, -1)
            self.panel_1.SetValidator(self.validator)

            self.__set_properties()
            self.__do_layout()

        def __set_properties(self):
            self.SetTitle("frame_1")

        def __do_layout(self):
            sizer_1 = wx.BoxSizer(wx.VERTICAL)
            sizer_1.Add(self.panel_1, 1, wx.EXPAND, 0)
            self.SetSizer(sizer_1)
            sizer_1.Fit(self)
            self.Layout()

        def validator(self, txt):
            try:
                f = DAValueFilter(txt)
                return True
            except:
                return False

    app = wx.App(False)
    frame_1 = TestFrame(None, -1, "")
    app.SetTopWindow(frame_1)
    frame_1.Show()
    app.MainLoop()
