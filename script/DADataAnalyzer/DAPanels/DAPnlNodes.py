# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath( os.path.join(selfDir, '..') )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import logging
import wx

from CTCommon import FEMesh
from DAFrameField      import DAFrameField
from DAConfig.DAConfig import DAConfig
from .DAFilter      import loadFilter
from .DAFilterList  import DAFilterListCtrl
from .DARegGridList import DARegGridList
from CTCommon.CTCheckListCtrl_v2 import CTCheckListCtrl

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Panel.Nodes")

class DAPnlNodes(wx.Panel):
    hlp = """\
The panel Grid is used to manage the activ grid/nodes.
It allows to select the source grid, to filter nodes
either on node number or on position. It allows also
to create/edit a regular grid.

Expression for node filtering must be a valid Python
logical expression, where:
    {0} is the placeholder for node number
    {1} is the placeholder for x coordinate
    {2} is the placeholder for y coordinate

Examples:
    To get nodes from 1 to 999: {0} < 1000
    To filter on bbox: {1} > 0 and {1} < 1 and {2} > 0 and {2} < 1
"""

    def __init__(self, *args, **kwds):
        self.cb_on_grid_change = kwds.pop('cb_on_grid_change', None)
        super(DAPnlNodes, self).__init__(*args, **kwds)

        self.sbx_grid       = wx.StaticBox (self, wx.ID_ANY, "grid")
        self.cmb_grid       = wx.ComboBox  (self, wx.ID_ANY, choices=['NIL'], style=wx.CB_DROPDOWN | wx.CB_READONLY)

        self.sbx_sizes      = wx.StaticBox (self, wx.ID_ANY, "size")
        self.stx_sizes_nnod = wx.StaticText(self, wx.ID_ANY, "nodes:")
        self.txt_sizes_nnod = wx.TextCtrl  (self, wx.ID_ANY, "0", style=wx.TE_READONLY)
        self.stx_sizes_nele = wx.StaticText(self, wx.ID_ANY, "elements:")
        self.txt_sizes_nele = wx.TextCtrl  (self, wx.ID_ANY, "0", style=wx.TE_READONLY)

        self.sbx_bbgr       = wx.StaticBox (self, wx.ID_ANY, "bbox grid")
        self.sbx_bbgr_min   = wx.StaticBox (self, wx.ID_ANY, "min")
        self.sbx_bbgr_max   = wx.StaticBox (self, wx.ID_ANY, "max")
        self.txt_bbgr_xmin  = wx.TextCtrl  (self, wx.ID_ANY, "-1", style=wx.TE_READONLY)
        self.txt_bbgr_ymin  = wx.TextCtrl  (self, wx.ID_ANY, "-1", style=wx.TE_READONLY)
        self.txt_bbgr_xmax  = wx.TextCtrl  (self, wx.ID_ANY, "1",  style=wx.TE_READONLY)
        self.txt_bbgr_ymax  = wx.TextCtrl  (self, wx.ID_ANY, "1",  style=wx.TE_READONLY)

        self.sbx_bbpj       = wx.StaticBox (self, wx.ID_ANY, "bbox project")
        self.sbx_bbpj_min   = wx.StaticBox (self, wx.ID_ANY, "min")
        self.sbx_bbpj_max   = wx.StaticBox (self, wx.ID_ANY, "max")
        self.txt_bbpj_xmin  = wx.TextCtrl  (self, wx.ID_ANY, "-1", style=wx.TE_READONLY)
        self.txt_bbpj_ymin  = wx.TextCtrl  (self, wx.ID_ANY, "-1", style=wx.TE_READONLY)
        self.txt_bbpj_xmax  = wx.TextCtrl  (self, wx.ID_ANY, "1",  style=wx.TE_READONLY)
        self.txt_bbpj_ymax  = wx.TextCtrl  (self, wx.ID_ANY, "1",  style=wx.TE_READONLY)

        self.lst_sizes_fid  = DAFilterListCtrl(self, wx.ID_ANY, title="Filter on nodes",  enable_edit=True, history='H2D2: DataAnalyzer: Filter grid on nodes')
        self.lst_sizes_fbb  = DAFilterListCtrl(self, wx.ID_ANY, title="Filter on coord.", enable_edit=True, history='H2D2: DataAnalyzer: Filter grid on coord')
        self.sbx_sizes_flt  = wx.StaticBox    (self, wx.ID_ANY, "User defined grid filter")
        self.cmb_sizes_flt  = wx.ComboBox     (self, wx.ID_ANY, choices=['NIL'], style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.lst_sizes_reg  = DARegGridList   (self, wx.ID_ANY, title="Regular Grid", history='H2D2: DataAnalyzer: Regular grid')

        self.__set_properties()
        self.__do_layout()
        self.configChange()

        self.grids= []
        self.grid = None
        self.data = None

        self.Bind(wx.EVT_COMBOBOX, self.onComboGrid, self.cmb_grid)
        #self.Bind(wx.EVT_HELP, self.onHelp)

    def __set_properties(self):
        self.SetMinSize((-1, -1))
        self.txt_bbgr_xmin.SetToolTip("Bounding box of the active grid in grid coordinates")
        self.txt_bbgr_xmax.SetToolTip("Bounding box of the active grid in grid coordinates")
        self.txt_bbgr_ymin.SetToolTip("Bounding box of the active grid in grid coordinates")
        self.txt_bbgr_ymax.SetToolTip("Bounding box of the active grid in grid coordinates")
        self.txt_bbpj_xmin.SetToolTip("Bounding box of the active grid in project coordinates")
        self.txt_bbpj_xmax.SetToolTip("Bounding box of the active grid in project coordinates")
        self.txt_bbpj_ymin.SetToolTip("Bounding box of the active grid in project coordinates")
        self.txt_bbpj_ymax.SetToolTip("Bounding box of the active grid in project coordinates")

        self.cmb_grid.SetSelection(0)
        self.cmb_grid.SetToolTip("Select the active grid")

        self.lst_sizes_fid.SetValidator(self.__expr_validator_index)
        self.lst_sizes_fbb.SetValidator(self.__expr_validator_coord)
        # self.lst_nodvl_fvl.SetValidator(self.__expr_validator_value)
        self.cmb_sizes_flt.SetSelection(0)
        self.cmb_sizes_flt.SetToolTip("Select a user defined filter. A filter is Python class inheriting from DAGridFilter")

        self.SetToolTip(DAPnlNodes.hlp)

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)

        szr_grid = wx.StaticBoxSizer(self.sbx_grid, wx.VERTICAL)
        szr_grid.Add(self.cmb_grid, 0, wx.EXPAND, 0)

        szr_sizes = wx.StaticBoxSizer(self.sbx_sizes, wx.VERTICAL)
        szr_sizes_top = wx.GridSizer(2, 2, 0, 0)
        szr_sizes_top.Add(self.stx_sizes_nnod, 0, 0, 0)
        szr_sizes_top.Add(self.txt_sizes_nnod, 0, wx.EXPAND, 0)
        szr_sizes_top.Add(self.stx_sizes_nele, 0, 0, 0)
        szr_sizes_top.Add(self.txt_sizes_nele, 0, wx.EXPAND, 0)
        szr_sizes.Add(szr_sizes_top, 0, wx.EXPAND, 0)

        szr_bbgr = wx.StaticBoxSizer(self.sbx_bbgr, wx.VERTICAL)
        szr_bbgr_min = wx.StaticBoxSizer(self.sbx_bbgr_min, wx.HORIZONTAL)
        szr_bbgr_min.Add(self.txt_bbgr_xmin, 1, wx.EXPAND, 0)
        szr_bbgr_min.Add(self.txt_bbgr_ymin, 1, wx.EXPAND, 0)
        szr_bbgr.Add(szr_bbgr_min, 0, wx.EXPAND, 0)
        szr_bbgr_max = wx.StaticBoxSizer(self.sbx_bbgr_max, wx.HORIZONTAL)
        szr_bbgr_max.Add(self.txt_bbgr_xmax, 1, wx.EXPAND, 0)
        szr_bbgr_max.Add(self.txt_bbgr_ymax, 1, wx.EXPAND, 0)
        szr_bbgr.Add(szr_bbgr_max, 0, wx.EXPAND, 0)

        szr_bbpj = wx.StaticBoxSizer(self.sbx_bbpj, wx.VERTICAL)
        szr_bbpj_min = wx.StaticBoxSizer(self.sbx_bbpj_min, wx.HORIZONTAL)
        szr_bbpj_min.Add(self.txt_bbpj_xmin, 1, wx.EXPAND, 0)
        szr_bbpj_min.Add(self.txt_bbpj_ymin, 1, wx.EXPAND, 0)
        szr_bbpj.Add(szr_bbpj_min, 0, wx.EXPAND, 0)
        szr_bbpj_max = wx.StaticBoxSizer(self.sbx_bbpj_max, wx.HORIZONTAL)
        szr_bbpj_max.Add(self.txt_bbpj_xmax, 1, wx.EXPAND, 0)
        szr_bbpj_max.Add(self.txt_bbpj_ymax, 1, wx.EXPAND, 0)
        szr_bbpj.Add(szr_bbpj_max, 0, wx.EXPAND, 0)

        szr_sizes_flt = wx.StaticBoxSizer(self.sbx_sizes_flt, wx.VERTICAL)
        szr_sizes_flt.Add(self.cmb_sizes_flt, 0, wx.EXPAND, 0)

        szr_main.Add(szr_grid,  0, wx.EXPAND, 0)
        szr_main.Add(szr_sizes, 0, wx.EXPAND, 0)
        szr_main.Add(szr_bbgr,  0, wx.EXPAND, 0)
        szr_main.Add(szr_bbpj,  0, wx.EXPAND, 0)
        szr_main.Add(self.lst_sizes_fid, 2, wx.EXPAND, 0)
        szr_main.Add(self.lst_sizes_fbb, 2, wx.EXPAND, 0)
        szr_main.Add(szr_sizes_flt,      0, wx.EXPAND, 0)
        szr_main.Add(self.lst_sizes_reg, 1, wx.EXPAND, 0)
        self.SetSizer(szr_main)
        self.Layout()

    def __expr_validator_index(self, expr):
        try:
            ret = eval( expr.format(1) )
            return isinstance(ret, bool)
        except:
            return False

    def __expr_validator_coord(self, expr):
        try:
            ret = eval( expr.format(1, 1.0, 1.0) )
            return isinstance(ret, bool)
        except:
            return False

    #def __expr_validator_value(self, expr):
    #    try:
    #        ret = eval( expr.format( (1.0) ) )
    #        return isinstance(ret, bool)
    #    except:
    #        return False

    def __fill_nodes(self):
        if self.grid:
            nbNod = self.grid.getNbNodes()
            nbEle = self.grid.getNbElements()
            self.txt_sizes_nnod.SetValue('%i' % nbNod)
            self.txt_sizes_nele.SetValue('%i' % nbEle)

            x1, y1, x2, y2 = FEMesh.FEMesh.getBbox(self.grid)
            self.txt_bbgr_xmin.SetValue('%s' % x1)
            self.txt_bbgr_ymin.SetValue('%s' % y1)
            self.txt_bbgr_xmax.SetValue('%s' % x2)
            self.txt_bbgr_ymax.SetValue('%s' % y2)
            if self.grid.hasProjection():
                x1, y1, x2, y2 = self.grid.getBbox()
                self.txt_bbpj_xmin.SetValue('%s' % x1)
                self.txt_bbpj_ymin.SetValue('%s' % y1)
                self.txt_bbpj_xmax.SetValue('%s' % x2)
                self.txt_bbpj_ymax.SetValue('%s' % y2)
            self.cmb_grid.SetToolTip('%s' % self.grid.getLongName())
        elif self.data:
            nbNod = self.data.getNbNodes()
            self.txt_sizes_nnod.SetValue('%s' % nbNod)
            self.txt_sizes_nele.SetValue('---')
            self.txt_bbgr_xmin.SetValue('---')
            self.txt_bbgr_ymin.SetValue('---')
            self.txt_bbgr_xmax.SetValue('---')
            self.txt_bbgr_ymax.SetValue('---')
        else:
            self.txt_sizes_nnod.SetValue('---')
            self.txt_sizes_nele.SetValue('---')
            self.txt_bbgr_xmin.SetValue('---')
            self.txt_bbgr_ymin.SetValue('---')
            self.txt_bbgr_xmax.SetValue('---')
            self.txt_bbgr_ymax.SetValue('---')

    def onComboGrid(self, event):
        idx = event.GetInt()
        # ---  Get grid, taking into account NIL
        if self.cmb_grid.GetCount() > len(self.grids):
            if idx == 0:
                self.grid = None
            else:
                self.grid = self.grids[idx-1]
        else:
            self.grid = self.grids[idx]
        self.data = None
        self.__fill_nodes()
        if self.cb_on_grid_change:
            self.cb_on_grid_change(self.grid)

    def onHelp(self, event):
        #hlp = wx.HelpProvider.Get().GetHelp(self)
        print('DAPnlNodes.on_evt_help', hlp)
        # event.Skip()

    def  asgGrids(self, grids):
        self.grids = grids
        self.cmb_grid.Clear()
        if self.data and not self.grid:
            self.cmb_grid.Append("NIL")
        for g in self.grids:
            self.cmb_grid.Append(g.getShortName(), g.getLongName())

        # gridChange = not self.grid                # change only on first
        gridChange = (self.grid != self.grids[-1])  # set to last grid
        if gridChange:
            self.grid = self.grids[-1]
        for i in range(self.cmb_grid.GetCount()):
            if self.cmb_grid.GetClientData(i) == self.grid.getLongName():
                self.cmb_grid.SetSelection(i)
                break
        if gridChange:
            if self.cb_on_grid_change:
                self.cb_on_grid_change(self.grid)
        self.__fill_nodes()

    def asgData(self, data):
        if data and self.grid: assert data.grid is self.grid
        self.data = data
        self.__fill_nodes()

    def asgCurrentAxes(self, ax):
        self.lst_sizes_reg.asgCurrentAxes(ax)

    def __getActivNodes(self):
        if self.grid:
            nodes = self.grid.getNodes()
            keep1 = self.lst_sizes_fid.FilterListOnValue(nodes)
            keep2 = self.lst_sizes_fbb.FilterListOnValue(nodes)
            #keep3 = self.lst_nodvl_fvl.FilterListOnValue(nodes)
            keep3 = None
        elif self.data:
            xy = (float("inf"), float("inf"))
            nodes = [ xy for i in range(self.data.getNbNodes()) ]
            keep1 = self.lst_sizes_fid.FilterListOnIndex(nodes)
            keep2 = None
            keep3 = None
        else:
            keep1 = None
            keep2 = None
            keep3 = None

        set = None
        if keep1 or keep2 or keep3: set = {}
        if keep1:
            for it in keep1: set[it] = None
        if keep2:
            for it in keep2: set[it] = None
        if keep3:
            for it in keep3: set[it] = None
        return sorted(set.keys()) if set is not None else None

    def getField(self):
        rgrid = self.lst_sizes_reg.getRegGrid()
        usrfl = self.cmb_sizes_flt.GetSelection()
        if isinstance(rgrid, FEMesh.FE1DRegularGrid):
            field = DAFrameField.as1DRegularGridField(rgrid, self.data)
        elif isinstance(rgrid, FEMesh.FE2DRegularGrid):
            field = DAFrameField.as2DRegularGridField(rgrid, self.data)
        elif usrfl > 0:
            gf = self.cmb_sizes_flt.GetClientData(usrfl)
            fltr = loadFilter(gf[1], gf[2])
            sgrid = fltr.filter(self.grid)
            if self.data:
                field = DAFrameField.as2DFiniteElementSubGridField(sgrid, self.data)
            else:
                field = DAFrameField.as2DFiniteElementSubGridField(sgrid)
        elif not rgrid:
            nodes = self.__getActivNodes()
            # print('DAPnlNodes.getField.nodes', len(nodes) if nodes else nodes)
            if nodes:
                fltr = FEMesh.SubGridFilterOnNodes(keep=nodes)
                sgrid = self.grid.genSubGrid(fltr)
                # print('DAPnlNodes.getField.sgrid', type(sgrid))
                if self.data:
                    field = DAFrameField.as2DFiniteElementSubGridField(sgrid, self.data)
                else:
                    field = DAFrameField.as2DFiniteElementSubGridField(sgrid)
            else:
                if self.data:
                    field = self.data
                else:
                    field = DAFrameField.as2DFiniteElementField(grid=self.grid)
        else:
            raise ValueError('Not a valid mesh type')
        return field

    def configChange(self):
        self.cmb_sizes_flt.Clear()
        cfg = DAConfig()
        self.cmb_sizes_flt.Append('NIL', None)
        for gf in cfg.readAllGridFilter():
            self.cmb_sizes_flt.Append(gf[0], gf)
        self.cmb_sizes_flt.SetSelection(0)
    
    
#==============================================================================
#
#==============================================================================
if __name__ == "__main__":
    class MyDialogBox(wx.Dialog):
        def __init__(self, *args, **kwargs):
            kwargs["style"] = wx.CAPTION|wx.CLOSE_BOX|wx.MINIMIZE_BOX|wx.MAXIMIZE_BOX|wx.SYSTEM_MENU|wx.RESIZE_BORDER|wx.CLIP_CHILDREN
            super(MyDialogBox, self).__init__(*args, **kwargs)
            self.pnl = DAPnlNodes(self)
            self.btn_cancel = wx.Button(self, wx.ID_CANCEL, "")
            self.btn_ok     = wx.Button(self, wx.ID_OK, "")

            szr_frm = wx.BoxSizer(wx.VERTICAL)
            szr_frm.Add(self.pnl, 1, wx.EXPAND)

            szr_btn = wx.BoxSizer(wx.HORIZONTAL)
            szr_btn.Add(self.btn_cancel,0, wx.EXPAND, 0)
            szr_btn.Add(self.btn_ok,    0, wx.EXPAND, 0)
            szr_frm.Add(szr_btn, 0, wx.EXPAND, 0)

            self.SetSizer(szr_frm)
            self.Layout()

    class MyApp(wx.App):
        def OnInit(self):
            dlg = MyDialogBox(None)
            if dlg.ShowModal() == wx.ID_OK:
                print('OK')
            return True

    app = MyApp(False)
    app.MainLoop()

