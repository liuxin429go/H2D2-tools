# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath( os.path.join(selfDir, '..') )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import logging

import numpy as np  # Used for validation

import wx
from wx.lib.combotreebox import ComboTreeBox as wx_ComboTreeBox

from . import DAFilterList
from . import DANodalValueList

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Panel.Values")

class DAPnlNodVal(wx.Panel):
    help = """\
The panel Values is used to manage the active nodal values.
It allows to select the data set and the active variables (columns).
It allows also to filter the variables on their value.

Expression must be a valid Python expression,
where {n} is the placeholder for the nth value.
np is shortcut for numpy.

Examples:
    To get the min value between col 0 and 1:
        np.minimum({0}, {1})
    To compute the norm:
        np.sqrt({0}*{0} + {1}*{1}) or
        np.hypot({0}, {1})
"""

    def __init__(self, *args, **kwds):
        self.cb_on_data_change = kwds.pop('cb_on_data_change', None)
        super(DAPnlNodVal, self).__init__(*args, **kwds)

        self.sbx_data = wx.StaticBox (self, wx.ID_ANY, "data set")
        self.cmb_data = wx_ComboTreeBox(self, style=wx.CB_DROPDOWN | wx.CB_READONLY )

        self.lst_nodvl = DANodalValueList.DANodalValueListCtrl(self, wx.ID_ANY, auto_extend=True, enable_edit=True)
        self.lst_nodvl_fvl = DAFilterList.DAFilterListCtrl(self, wx.ID_ANY, title="Filter on values", enable_edit=True)

        self.__set_properties()
        self.__do_layout()

        self.fields= []
        self.field = None

        self.Bind(wx.EVT_COMBOBOX, self.on_cmb_data, self.cmb_data)

    def __set_properties(self):
        self.lst_nodvl.SetValidator(self.__expr_validator_expr)
        self.lst_nodvl_fvl.SetValidator(self.__expr_validator_value)
        self.lst_nodvl_fvl.Enable(False)
        #self.cmb_data.SetSelection(0)
        self.cmb_data.SetToolTip("Set the active field")

        self.SetToolTip (DAPnlNodVal.help)
        self.lst_nodvl.SetToolTip (DAPnlNodVal.help)

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)

        szr_data = wx.StaticBoxSizer(self.sbx_data, wx.VERTICAL)
        szr_data.Add(self.cmb_data, 0, wx.EXPAND, 0)

        szr_main.Add(szr_data,           0, wx.EXPAND, 0)
        szr_main.Add(self.lst_nodvl,     1, wx.EXPAND, 0)
        szr_main.Add(self.lst_nodvl_fvl, 1, wx.EXPAND, 0)

        self.SetSizer(szr_main)
        self.Layout()

    def __expr_validator_expr(self, expr):
        class DummyGrids:
            def __getitem__(self, idxs):
                if not isinstance(idxs, int): raise RuntimeError('Expression validation (grids): Invalid grid index type')
                return DummyGrid()
            def __getattribute__(self, name):
                raise AttributeError("Expression validation (grids): grids has no attribute '%s'" % name)
        class DummyGrid:
            # def __getattr__(self, name):
            #     attr = getattr(self.field.getGrid(), name)
            def __getattribute__(self, name):
                raise AttributeError("Expression validation (grid): grid has no attribute '%s'" % name)
            @property
            def datas(self):
                return DummyFields()
            def integrate(self, *args, **kwargs):
                if len(args)   != 1: raise RuntimeError('Expression validation (grid): One and exactly one argument expected')
                if len(kwargs) != 0: raise RuntimeError('Expression validation (grid): No keyword args expected')
            # def ddx(self, *args, **kwargs):
            #     pass
            # def ddy(self, *args, **kwargs):
            #     pass
        class DummyFields:
            def __getattribute__(self, name):
                raise AttributeError("Expression validation (fields): fields has no attribute '%s'" % name)
            def __getitem__(self, idxs):
                if not isinstance(idxs, int): raise RuntimeError('Expression validation (fields): Invalid field index type')
                return DummyField()
        class DummyField:
            def __getattribute__(self, name):
                raise AttributeError("Expression validation (field): field has no attribute '%s'" % name)
            def __getitem__(self, idxs):
                if isinstance(idxs, tuple):
                    if len(idxs) == 1:
                        if not isinstance(idxs[0], slice): raise RuntimeError('Expression validation (field): A slice was expected')
                    elif len(idxs) == 2:
                        if not isinstance(idxs[0], slice): raise RuntimeError('Expression validation (field): A slice was expected')
                        if not isinstance(idxs[1], int): raise RuntimeError('Expression validation (field): Invalid value index')
                    else:
                        raise RuntimeError
                else:
                    if not isinstance(idxs, slice): raise RuntimeError('Expression validation (field): A slice was expected')
                return 0
            @property
            def values(self):
                return self
        try:
            n = self.lst_nodvl.GetItemCount()
            _xpr = expr.format(*list(range(n)))
            _lcl = {
                'grids' : DummyGrids(),
                'grid'  : DummyGrid(),
                'datas' : DummyFields(),
                'data'  : DummyField(),
                }
            _ret = eval(_xpr, None, _lcl)
            return True
        except AttributeError as e:
            type(e)(str(e) + '\nEvaluating "%s"' % expr)
            LOGGER.debug("%s", str(e))
            raise
        except Exception as e:
            type(e)(str(e) + '\nEvaluating "%s"' % expr)
            LOGGER.debug("%s", str(e))
            raise

    def __expr_validator_value(self, expr):
        try:
            ret = eval( expr.format( (1.0) ) )
            return isinstance(ret, bool)
        except:
            return False

    def __fill_data(self):
        for row in reversed(list(range(self.lst_nodvl.GetItemCount()))):
            editable = self.lst_nodvl.GetItem(row).GetData() != 0
            if not editable:
                self.lst_nodvl.DeleteItem(row)

        if self.field:
            for i in range( self.field.getNbVal() ):
                text = '%s' % self.field.names[i] if i < len(self.field.names) else '%s' % i
                self.lst_nodvl.InsertItem(i, text = text, checked = True, editable = False)
            self.lst_nodvl.Enable(True)

            self.cmb_data.SetToolTip('%s' % self.field.getLongName())
        else:
            self.lst_nodvl.Enable(False)

    def on_cmb_data(self, event):
        itm = self.cmb_data.GetSelection()
        try:
            idx = self.cmb_data.GetClientData(itm)
            self.field = self.fields[idx]
            self.__fill_data()
            if self.cb_on_data_change:
                self.cb_on_data_change(self.field)
        except:
            pass

    def asgGrid(self, grid):
        if self.field and grid is self.field.getGrid(): return

        field = None
        for f in self.fields:
            if f.getGrid() is grid:
                field = f
                break
        if field:
            self.field = self.fields[-1]
            item = self.cmb_data.FindString('%s' % self.field.getShortName())
            self.cmb_data.SetSelection(item)
            if self.cb_on_data_change:
                self.cb_on_data_change(self.field)
            self.cmb_data.Enable(True)
        else:
            self.field = None
            self.cmb_data.Enable(False)
            if self.cb_on_data_change:
                self.cb_on_data_change(self.field)
        self.__fill_data()

    def asgDatas(self, fields):
        self.fields = fields
        # ---  Fill the tree
        self.cmb_data.Clear()
        grids = set( [ f.getGrid() for f in fields] )
        for g in grids:
            gname = '%s' % g.getShortName() if g else 'None - grid not set'
            item = self.cmb_data.Append(gname)
            for i, f in enumerate(self.fields):
                if f.getGrid() is g:
                    jtem = self.cmb_data.Append('%s' % f.getShortName(), parent=item)
                    self.cmb_data.SetClientData(jtem, i)
        # ---  Select the field
        if self.field:
            idx = self.fields.index(self.field)
            item = self.cmb_data.FindClientData(idx)
            self.cmb_data.SetSelection(item)
            self.cmb_data.SetToolTip('%s' % self.field.getLongName())
        else:
            self.field = self.fields[-1]
            idx = len(self.fields) - 1
            item = self.cmb_data.FindClientData(idx)
            self.cmb_data.SetSelection(item)
            self.cmb_data.SetToolTip('%s' % self.field.getLongName())
            if self.cb_on_data_change:
                self.cb_on_data_change(self.field)
        self.cmb_data.Enable(True)
        self.__fill_data()

    def getActivNodVal(self):
        ops = self.lst_nodvl.BuildOpList()
        if self.field:
            grids = list( set( [f.getGrid() for f in self.fields] ) )
            grid  = self.field.getGrid()
            for op in ops:
                op.setLocals(grids=grids, grid=grid, datas=self.fields, data=self.field)
        return ops

    def getActivNodNames(self):
        return self.lst_nodvl.BuildNameList()


#==============================================================================
#
#==============================================================================
if __name__ == "__main__":
    class MyDialogBox(wx.Dialog):
        def __init__(self, *args, **kwargs):
            kwargs["style"] = wx.CAPTION|wx.CLOSE_BOX|wx.MINIMIZE_BOX|wx.MAXIMIZE_BOX|wx.SYSTEM_MENU|wx.RESIZE_BORDER|wx.CLIP_CHILDREN
            super(MyDialogBox, self).__init__(*args, **kwargs)
            self.pnl = DAPnlNodVal(self)
            self.btn_cancel = wx.Button(self, wx.ID_CANCEL, "")
            self.btn_ok     = wx.Button(self, wx.ID_OK, "")

            szr_frm = wx.BoxSizer(wx.VERTICAL)
            szr_frm.Add(self.pnl, 1, wx.EXPAND)

            szr_btn = wx.BoxSizer(wx.HORIZONTAL)
            szr_btn.Add(self.btn_cancel,0, wx.EXPAND, 0)
            szr_btn.Add(self.btn_ok,    0, wx.EXPAND, 0)
            szr_frm.Add(szr_btn, 0, wx.EXPAND, 0)

            self.SetSizer(szr_frm)
            self.Layout()

    class MyApp(wx.App):
        def OnInit(self):
            dlg = MyDialogBox(None)
            if dlg.ShowModal() == wx.ID_OK:
                print('OK')
            return True

    app = MyApp(False)
    app.MainLoop()
