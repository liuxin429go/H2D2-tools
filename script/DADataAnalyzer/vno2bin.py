#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************
"""
Transforms an ASCII file to binary
"""
import array
import glob
import optparse
import os
import struct
import sys

__package__ = 'vno2bin'
__version__ = '19.04'

class EOFError(Exception):
    def __init__(self):
        pass
    def __str__(self):
        return 'EOF reached'

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()
        
def read_header(fi):
    tks = fi.readline().split()
    if (len(tks) != 3): raise EOFError()
    return int(tks[0]), int(tks[1]), float(tks[2])

def write_header(fo, nrow, ncol, time):
    data = struct.pack('ddd', float(nrow), float(ncol), time)
    fo.write(data)

def read_record(fi):
    tks = fi.readline().split()
    return list(map(float, tks))

def write_record(fo, vals):
    v = array.array('d', vals)
    v.tofile(fo)

def xeq(fi, fo):
    done = False
    try:
        while (not done):
            nrow, ncol, time = read_header(fi)
            write_header(fo, nrow, ncol, time)
            #sys.stderr.write('Reading:  NROW=%7i,  NCOL=%7i, TIME=%17.4f\n' % (nrow, ncol, time))
            for ir in range(nrow):
                rec = read_record(fi)
                write_record(fo, rec)
    except EOFError:
        pass

def oneFile(finp, fout):
    with open(finp, 'r') as fi, open(fout, 'wb') as fo:
        xeq(fi, fo)

def main(opt_args = None):
    print('%s %s' % (__package__, __version__))

    # ---  Define options
    usage  = '%s [options]' % __package__
    parser = optparse.OptionParser(usage)
    parser.add_option("-i", "--fi", "--input", dest="inp", default=None,
                  help="ascii input file or glob pattern", metavar="input_path")
    parser.add_option("-o", "--fo", "--output", dest="out", default=None,
                  help="binary output file or output directory", metavar="output_path")

    # ---  Parse options
    if (not opt_args): opt_args = sys.argv[1:]
    (options, args) = parser.parse_args(opt_args)
    if (not options.inp):
        parser.print_help()
        return
    if (not options.out):
        parser.print_help()
        return

    if os.path.isfile(options.inp):
        if os.path.isfile(options.out):
            finp = options.inp
            fout = options.out
            oneFile(finp, fout)
        elif os.path.isdir(options.out):
            finp = options.inp
            fout = os.path.join(options.out, os.path.basename(finp))
            oneFile(finp, fout)
        else:
            finp = options.inp
            fout = options.out
            oneFile(finp, fout)
    else:
        if os.path.isdir(options.out):
            nfiles = 0
            for finp in glob.iglob(options.inp):
                nfiles += 1
            printProgressBar(0, nfiles, prefix = 'Progress:', suffix = 'Complete', length = 50)
            for ifile, finp in enumerate(glob.iglob(options.inp)):
                fout = os.path.join(options.out, os.path.basename(finp))
                oneFile(finp, fout)
                printProgressBar(ifile+1, nfiles, prefix = 'Progress:', suffix = 'Complete', length = 50)
        else:
            raise('Invalid arguments: "%s" must be an existing directory' % options.fout)
    
if __name__ == '__main__':
    main()
