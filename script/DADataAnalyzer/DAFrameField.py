# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import copy
import os

from CTCommon.DTData  import DTData
from CTCommon.DTField import DT2DFiniteElementField
from CTCommon.DTField import DT2DRegularGridField
from CTCommon.DTField import DT1DRegularGridField
from CTCommon.FEMesh  import FE2DRegularGrid
from CTCommon.FEMesh  import FE1DRegularGrid

from DAFrameGrid import DAFrameGrid

class DAFrameField:
    cntr = 0

    def __init__(self, field=None, grid=None):
        self.field = field
        self.grid  = grid

        self.data  = None
        self.file  = None
        self.names = []
        self.epoch = None

        self.idx = DAFrameField.cntr
        DAFrameField.cntr += 1

    def __copy__(self):
        """
        Shallow copy, while reseting the activ data
        """
        newone = type(self)()
        newone.field = None
        newone.grid  = self.grid
        newone.data  = None
        newone.file  = self.file
        newone.names = self.names
        newone.epoch = self.epoch
        newone.field = copy.copy(self.field)
        return newone

    @classmethod
    def as2DFiniteElementField(cls, grid=None, file=None, names=[], epoch=None):
        assert grid is None or isinstance(grid, DAFrameGrid)
        assert file is None or os.path.isfile(file)

        data  = DTData(fnam=file)
        field = DT2DFiniteElementField(grid, data=data)
        self = cls(field, grid)
        self.data  = data
        self.file  = file
        self.names = names
        self.epoch = epoch
        return self

    @classmethod
    def as2DFiniteElementSubGridField(cls, sgrid, field=None):
        assert sgrid is None or isinstance(sgrid, DAFrameGrid)
        assert field is None or isinstance(field, DAFrameField)

        rgdata  = field.data if field else None
        rgfield = DT2DFiniteElementField(sgrid, data=rgdata)
        self = cls(rgfield, sgrid)
        if field:
            self.file  = field.file
            self.names = field.names
            self.epoch = field.epoch
        return self

    @classmethod
    def as2DRegularGridField(cls, rgrid, field):
        assert rgrid is None or isinstance(rgrid, FE2DRegularGrid)
        assert field is None or isinstance(field, DAFrameField)

        rgfield = DT2DRegularGridField(rgrid, field)
        self = cls(rgfield, rgrid)
        try:
            self.file  = field.file
            self.names = field.names
            self.epoch = field.epoch
        except:
            pass
        return self

    @classmethod
    def as1DRegularGridField(cls, rgrid, field):
        assert rgrid is None or isinstance(rgrid, FE1DRegularGrid)
        assert field is None or isinstance(field, DAFrameField)

        rgfield = DT1DRegularGridField(rgrid, field)
        self = cls(rgfield, rgrid)
        try:
            self.file  = field.file
            self.names = field.names
            self.epoch = field.epoch
        except:
            pass
        return self

    def __str__(self):
        return self.getShortName()

    def getShortName(self):
        name = os.path.basename(self.file) if self.file else 'Field name not set'
        return '#%02i %s' % (self.idx, name)

    def getLongName(self):
        name = self.file if self.file else 'Field name not set'
        return '#%02i %s' % (self.idx, name)

    def getNames(self):
        return self.names

    def getName(self, i):
        return self.names[i] if i < len(self.names) else '%i' % i

    def __getattr__(self, name):
        """
        Transfert all unknown calls to the field or data attribute
        """
        try:
            return getattr(self.field, name)
        except AttributeError:
            if self.data:
                return getattr(self.data, name)
            else:
                raise
