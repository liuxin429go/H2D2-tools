# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import tempfile
import PIL

import wx
from wx.html import HtmlEasyPrinting #, HtmlWindow

tmpDir = None
if not tmpDir:
    try:
        tmpDir = os.environ['TEMP']
    except KeyError:
        pass
if not tmpDir:
    try:
        tmpDir = os.environ['TMP']
    except KeyError:
        pass
if not tmpDir:
    try:
        tmpDir = os.environ['TMPDIR']
    except KeyError:
        pass
if not tmpDir:
    raise RuntimeError('Environment variable TEMP or TMP must be defined')

class DAPrinterHtml:
    DPI = 300
    ctr = 0

    def __init__(self, parent):
        self.parent  = parent
        self.printer = HtmlEasyPrinting(name='DADataAnalyzer', parentWindow=self.parent)
        self.printer.GetPrintData().SetPaperId(wx.PAPER_LETTER)
        self.printer.GetPrintData().SetOrientation(wx.LANDSCAPE)
        self.printer.GetPrintData().SetQuality(DAPrinter.DPI)
        
    def __createTmpFileNames(self, suffixes):
        root = os.path.join(tmpDir, '~daprinter_%06d_%03d' % (os.getpid(), DAPrinter.ctr))
        tmps = [ '%s.%s' % (root, suffix) for suffix in suffixes ]
        DAPrinter.ctr += 1
        return tmps
        
    def __createHtmlPage(self, tmpHtml, tmpBmp, width, height):
        with PIL.Image.open(tmpBmp) as im:
            width, height = im.size
        html = '''
<html>
<body>
<center><img src="{fic}" width="{w}" height="{h}" border="2"></center>
</body>
</html>
'''
        with open(tmpHtml, 'w') as f:
            f.write(html.format(fic=tmpBmp, w=width, h=height))

    def setupPage(self):
        self.printer.PageSetup()

    def printPage(self, fig):
        tmpHtm, tmpBmp = self.__createTmpFileNames( ('htm', 'png') )

        try:
            w, h = fig.get_size_inches()
            dpi = int(fig.get_dpi())
            fig.savefig(tmpBmp, dpi=dpi)
            HTML_DPI= 96
            self.__createHtmlPage(tmpHtm, tmpBmp, w*HTML_DPI, h*HTML_DPI)
            #self.printer.PrintFile(tmpHtm)
            self.printer.PreviewFile(tmpHtm)
        finally:
            print('in finally')
            #if os.path.isfile(tmpHtm): os.remove(tmpHtm)
            #if os.path.isfile(tmpBmp): os.remove(tmpBmp)
            

            
class DAPrintout(wx.Printout):
    ctr = 0
    
    def __init__(self, fig):
        wx.Printout.__init__(self)
        self.fig = fig

    def HasPage(self, page):
        return page <= 1

    def GetPageInfo(self):
        return (1, 1, 1, 1)

    def __createTmpFileNames(self, suffixes):
        root = os.path.join(tmpDir, '~daprinter_%06d_%03d' % (os.getpid(), DAPrintout.ctr))
        tmps = [ '%s.%s' % (root, suffix) for suffix in suffixes ]
        DAPrintout.ctr += 1
        return tmps
        
    def __setup(self, dc, fig):
        print('DAPrintout.__setup')
        dpi = int(fig.get_dpi())
        w, h = fig.get_size_inches() * dpi
        maxX = w
        maxY = h

        # Let's have at least 50 device units margin
        marginX = 50
        marginY = 50

        # Add the margin to the graphic size
        maxX += (2 * marginX)
        maxY += (2 * marginY)

        # Get the size of the DC in pixels
        (wDC, hDC) = dc.GetSize()

        # Calculate a suitable scaling factor
        scaleX = float(wDC) / maxX
        scaleY = float(hDC) / maxY

        # Use x or y scaling factor, whichever fits on the DC
        actualScale = min(scaleX, scaleY)

        # Calculate the position on the DC for centering the graphic
        posX = (wDC - (w * actualScale)) / 2.0
        posY = (hDC - (h * actualScale)) / 2.0
        print('actualScale, posX, posY', actualScale, posX, posY)

        # Set the scale and origin
        dc.SetUserScale(actualScale, actualScale)
        dc.SetDeviceOrigin(int(posX), int(posY))

    def OnPrintPage(self, ipage):
        print('DAPrintout.OnPrintPage')
        dc = self.GetDC()
        self.__setup(dc, self.fig)
        
        tmpBmp, = self.__createTmpFileNames( ('png',) )
        try:
            self.fig.savefig(tmpBmp)
            bmp = wx.Bitmap(tmpBmp)
            dc.DrawBitmap(bmp, 10, 10)
        finally:
            if os.path.isfile(tmpBmp): os.remove(tmpBmp)
        
        return True

class DAPrinter():
    def __init__(self, parent):
        self.parent = parent

        self.printData = wx.PrintData()
        self.printData.SetPaperId    (wx.PAPER_LETTER)
        self.printData.SetOrientation(wx.LANDSCAPE)
        self.printData.SetPrintMode  (wx.PRINT_MODE_PRINTER)

    def setupPage(self):
        psdd = wx.PageSetupDialogData(self.printData)
        psdd.EnablePrinter(True)
        # psdd.CalculatePaperSizeFromId()
        dlg = wx.PageSetupDialog(self.parent, psdd)
        dlg.ShowModal()

        # this makes a copy of the wx.PrintData instead of just saving
        # a reference to the one inside the PrintDialogData that will
        # be destroyed when the dialog is destroyed
        self.printData = wx.PrintData( dlg.GetPageSetupData().GetPrintData() )

        dlg.Destroy()

    def printPreview(self, fig):
        data = wx.PrintDialogData(self.printData)
        printout  = DAPrintout(fig)
        printout2 = DAPrintout(fig)
        self.preview = wx.PrintPreview(printout, printout2, data)

        if not self.preview.IsOk():
            raise Exception("Houston, we have a problem...\n")

        pfrm = wx.PreviewFrame(self.preview, self.parent, "DADataAnalyzer print preview")

        pfrm.Initialize()
        pfrm.SetPosition(self.parent.GetPosition())
        pfrm.SetSize(self.parent.GetSize())
        pfrm.Show(True)

    def printPage(self, fig):
        pdd = wx.PrintDialogData(self.printData)
        pdd.SetToPage(1)
        printer = wx.Printer(pdd)
        printout = DAPrintout(fig)

        if not printer.Print(self.parent, printout, True):
            wx.MessageBox("There was a problem printing.\nPerhaps your current printer is not set correctly?", "Printing", wx.OK)
        else:
            self.printData = wx.PrintData( printer.GetPrintDialogData().GetPrintData() )
        printout.Destroy()

