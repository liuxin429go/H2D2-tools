# -*- coding: utf-8 -*-
# generated by wxGlade 0.7.0 on Sun Aug 23 11:59:25 2015
#************************************************************************
# --- Copyright (c) INRS 2015-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from datetime import datetime
import dateutil.parser
import six
import sys

import wx
import wx.lib.mixins.listctrl as listmix

from DAConfig import DAConfig

class DAListEdit(wx.ListCtrl,
                 listmix.ListCtrlAutoWidthMixin,
                 listmix.TextEditMixin):
    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)
        listmix.TextEditMixin.__init__(self)

class DAListNoEdit(wx.ListCtrl,
                   listmix.ListCtrlAutoWidthMixin):
    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)

class DADlgDataColumnPanel(wx.Panel):
    """
    Panel to display the data column labels.
    Can be editable
    """
    def __init__(self, *args, **kwds):
        self.editable  = kwds.pop("editable", True)
        self.staticBox = kwds.pop("staticBox", "")

        wx.Panel.__init__(self, *args, **kwds)
        ListClass = DAListEdit if self.editable else DAListNoEdit
        self.shsMain  = wx.SplitterWindow(self, wx.ID_ANY, style=wx.SP_3D|wx.SP_BORDER)
        self.lstName = ListClass(self.shsMain, wx.ID_ANY, style=wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.lstCols = ListClass(self.shsMain, wx.ID_ANY, style=wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.sbxLst = wx.StaticBox(self, wx.ID_ANY, self.staticBox) if self.staticBox else None

        self.__create_menus()
        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.onSelectItem, self.lstName)
        if self.editable:
            self.Bind(wx.EVT_LIST_COL_RIGHT_CLICK,  self.onPopup)
            self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.onPopup)
            self.Bind(wx.EVT_LIST_BEGIN_LABEL_EDIT, self.onBeginLabelEdit)
            self.Bind(wx.EVT_LIST_END_LABEL_EDIT,   self.onEndLabelEdit)
            self.Bind(wx.EVT_MENU, self.onMnuAdd, self.mnu_add)
            self.Bind(wx.EVT_MENU, self.onMnuDel, self.mnu_del)

        self.curSelId = -1

    def __create_menus(self):
        self.mnu = wx.Menu()
        self.mnu_add = wx.MenuItem(self.mnu, wx.ID_ANY, "Add", "Add an entry", wx.ITEM_NORMAL)
        self.mnu.Append(self.mnu_add)
        self.mnu_del = wx.MenuItem(self.mnu, wx.ID_ANY, "Delete", "Delete the selected entry", wx.ITEM_NORMAL)
        self.mnu.Append(self.mnu_del)

    def __set_properties(self):
        self.shsMain.SetMinimumPaneSize(100)
        self.shsMain.SetSashGravity(0.5)

    def __do_layout(self):
        self.shsMain.SplitHorizontally(self.lstName, self.lstCols)

        szrMain = wx.BoxSizer(wx.VERTICAL)
        if self.sbxLst:
            szrBox = wx.StaticBoxSizer(self.sbxLst,  wx.VERTICAL)
            szrBox.Add(self.shsMain,  1, wx.EXPAND, 0)
            szrMain.Add(szrBox, 1, wx.EXPAND, 0)
        else:
            szrMain.Add(self.shsMain, 1, wx.EXPAND, 0)

        self.SetSizer(szrMain)
        self.Layout()

    def onSelectItem(self, event):
        # ---  Init
        self.lstCols.DeleteAllItems()
        for _ic in range(self.lstCols.GetColumnCount()): 
            self.lstCols.DeleteColumn(0)
        self.lstCols.InsertColumn(0, "Column", width=40)
        self.lstCols.InsertColumn(1, "Name")
        self.lstCols.setResizeColumn(0)

        # ---  Fill columns
        jd = event.GetIndex()
        id = self.lstName.GetItemData(jd)
        for i, t in enumerate( self.data[id][1] ):
            self.lstCols.InsertItem(i, "%s" % i)
            self.lstCols.SetItem   (i, 1, "%s" % t)
        self.curSelId = id

    def onBeginLabelEdit(self, event):
        self.mnuEventId = event.GetId()
        if   self.mnuEventId == self.lstName.GetId():
            pass
        elif self.mnuEventId == self.lstCols.GetId():
            if event.GetColumn() == 0:
                event.Veto()
            else:
                event.Skip()

    def onEndLabelEdit(self, event):
        self.mnuEventId = event.GetId()
        if   self.mnuEventId == self.lstName.GetId():
            jd1 = self.lstName.GetFirstSelected()
            if jd1 >= 0:
                id1 = self.lstName.GetItemData (jd1)
                txt = event.GetText()
                self.data[id1][0] = txt
        elif self.mnuEventId == self.lstCols.GetId():
            jd1 = self.lstName.GetFirstSelected()
            if jd1 >= 0:
                id1 = self.lstName.GetItemData (jd1)
                # Don't know why, but selection in listEditType is not activated
                # for the first row until some other row has been selected.
                # Use event.Index instead.
                # id2 = self.lstCols.GetFirstSelected()
                id2 = event.Index
                txt = event.GetText()
                self.data[id1][1][id2] = txt

    def onMnuAdd(self, event):
        addType = False
        addKind = False
        if self.mnuEventId == self.lstName.GetId():
            addType = True
            addKind = True
        elif self.mnuEventId == self.lstCols.GetId():
            addType = False
            addKind = True

        if addType:
            name = '#%i - New type' % len(self.data)
            self.data.append( [name, []] )
            i = self.lstName.GetItemCount()
            self.lstName.InsertItem(i, name)
            self.lstName.SetItemData(i, len(self.data)-1)
            self.lstName.Select(i)
            self.lstName.EnsureVisible(i)
        if addKind:
            jd = self.lstName.GetFirstSelected()
            id = self.lstName.GetItemData(jd)
            i = len(self.data[id][1])
            t = '#%i-%i - New label' % (id, i)
            self.data[id][1].append(t)
            self.lstCols.InsertItem(i, "%s" % i)
            self.lstCols.SetItem   (i, 1, "%s" % t)
            self.lstCols.Select(i)
            self.lstCols.EnsureVisible(i)

    def onMnuDel(self, event):
        if self.mnuEventObject == self.lstName:
            jd = self.lstName.GetFirstSelected()
            id = self.lstName.GetItemData(jd)
            del (self.data[id])
            self.lstName.DeleteItem(id)
            self.lstName.Select(max(0, id-1))
        elif self.mnuEventObject == self.lstCols:
            sel = []
            i = self.lstCols.GetFirstSelected()
            if i != -1: sel.append(i)
            while i != -1:
                i = self.lstCols.GetNextSelected(i)
                if i != -1: sel.append(i)
            sel.sort(reverse=True)

            jd = self.lstName.GetFirstSelected()
            id = self.lstName.GetItemData(jd)
            for i in sel:
                del (self.data[id][1][i])
                self.lstCols.DeleteItem(i)

    def onPopup(self, event):
        self.mnuEventId = event.GetId()
        self.PopupMenu(self.mnu)

    def setData(self, data=None, ncol=-1):
        # ---  Get data from config
        self.data = data if data else DAConfig.DAConfig().readAllDataColumnLabels()

        # ---  Empty widget
        self.lstName.DeleteAllItems()
        for _ic in range(self.lstName.GetColumnCount()): 
            self.lstName.DeleteColumn(0)
        self.lstName.InsertColumn(0, "Kind")
        self.lstName.setResizeColumn(0)

        # ---  Fill widget with filter on col number
        j = 0
        for i, c in enumerate(self.data):
            nc = len(c[1])
            if ncol == -1 or ncol == nc:
                self.lstName.InsertItem (i, "%s" % c[0])
                self.lstName.SetItemData(j, i)
                j +=1 
        self.lstName.Select(0)

    def getSelection(self):
        """
        Returns the current selection

        Returns:
            tuple: (name, list of column labels)
        """
        return self.data[self.curSelId]

    def getData(self):
        """
        Returns all the items

        Returns:
            tuple: (name, list of column labels)
        """
        return self.data

class DADlgDataEpochPanel(wx.Panel):
    """
    Panel to display the data column labels.
    Can be editable
    """
    def __init__(self, *args, **kwds):
        self.editable  = kwds.pop("editable", True)
        self.staticBox = kwds.pop("staticBox", "")

        wx.Panel.__init__(self, *args, **kwds)
        ListClass = DAListEdit if self.editable else wx.ListCtrl
        self.lstEpoch = ListClass(self, wx.ID_ANY, style=wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.sbxLst = wx.StaticBox(self, wx.ID_ANY, self.staticBox) if self.staticBox else None

        self.__create_menus()
        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_SIZE, self.onResize)
        if self.editable:
            self.Bind(wx.EVT_LIST_COL_RIGHT_CLICK,  self.onPopup)
            self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.onPopup)
            self.Bind(wx.EVT_LIST_BEGIN_LABEL_EDIT, self.onBeginLabelEdit)
            self.Bind(wx.EVT_LIST_END_LABEL_EDIT,   self.onEndLabelEdit)
            self.Bind(wx.EVT_MENU, self.onMnuAdd, self.mnu_add)
            self.Bind(wx.EVT_MENU, self.onMnuDel, self.mnu_del)

    def __create_menus(self):
        self.mnu = wx.Menu()
        self.mnu_add = wx.MenuItem(self.mnu, wx.ID_ANY, "Add", "Add an entry", wx.ITEM_NORMAL)
        self.mnu.Append(self.mnu_add)
        self.mnu_del = wx.MenuItem(self.mnu, wx.ID_ANY, "Delete", "Delete the selected entry", wx.ITEM_NORMAL)
        self.mnu.Append(self.mnu_del)

    def __set_properties(self):
        self.lstEpoch.InsertColumn(0, 'Name', width=60)
        self.lstEpoch.InsertColumn(1, 'Epoch')

    def __do_layout(self):
        szrMain = wx.BoxSizer(wx.VERTICAL)
        if self.sbxLst:
            szrBox = wx.StaticBoxSizer(self.sbxLst,  wx.VERTICAL)
            szrBox.Add(self.lstEpoch,  1, wx.EXPAND, 0)
            szrMain.Add(szrBox, 1, wx.EXPAND, 0)
        else:
            szrMain.Add(self.lstEpoch, 1, wx.EXPAND, 0)
        self.SetSizer(szrMain)
        self.Layout()

    def onResize(self, event):
        w, _ = event.Size
        self.lstEpoch.SetColumnWidth(0, 0.4*w-10)   # 10 is heuristic;
        self.lstEpoch.SetColumnWidth(1, 0.6*w-10)   # Editor need room to work
        event.Skip()

    def onBeginLabelEdit(self, event):
        self.mnuEventId = event.GetId()
        if self.mnuEventId != self.lstEpoch.GetId():
            event.Veto()

    def onEndLabelEdit(self, event):
        row_id = event.GetIndex()
        col_id = event.GetColumn()
        txt = event.GetText()
        try:
            dt = dateutil.parser.isoparse(txt)
            txt = dt.isoformat()
            self.data[row_id][col_id] = txt
            self.lstEpoch.SetItem(row_id, col_id, txt)
        except ValueError as why:
            dlg = wx.MessageDialog(self, str(why), 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
            event.Veto()

    def onMnuAdd(self, event):
        i = len(self.data)
        n = '#%i - New epoch' % i
        e = datetime.isoformat( datetime.utcnow() )
        self.lstEpoch.InsertItem(i, "%s" % n)
        self.lstEpoch.SetItem   (i, 1, "%s" % e)
        self.lstEpoch.Select(i)
        self.lstEpoch.EnsureVisible(i)
        self.data.append( [n,e] )

    def onMnuDel(self, event):
        if self.lstEpoch.GetSelectedItemCount() == 1:
            i = self.lstEpoch.GetFirstSelected()
            del self.data[i]
            self.lstEpoch.DeleteItem(i)
        else:
            event.Veto()

    def onPopup(self, event):
        self.mnuEventId = event.GetId()
        self.PopupMenu(self.mnu)

    def setData(self, data=None):
        # ---  Get data
        self.data = data if data else DAConfig.DAConfig().readAllEpoch()

        # ---  Fill widget
        self.lstEpoch.DeleteAllItems()
        for item in self.data:
            self.lstEpoch.Append( (item[0], item[1]) )
        self.lstEpoch.Select(0)

    def getSelection(self):
        """
        Returns the current selection

        Returns:
            tuple: (name, list of column labels)
        """
        id = self.lstEpoch.GetFirstSelected()
        n, e = self.data[id]
        return n, dateutil.parser.isoparse(e)

    def getData(self):
        """
        Returns all the items

        Returns:
            tuple: (name, list of column labels)
        """
        return self.data

class DADlgDataColumns(wx.Dialog):
    """
    Dialog to edit the data column labels
    """
    def __init__(self, *args, **kwds):
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER

        super(DADlgDataColumns, self).__init__(*args, **kwds)
        self.pnl = DADlgDataColumnPanel(self, wx.ID_ANY, editable=True)
        self.btnCancel = wx.Button(self, wx.ID_CANCEL, "")
        self.btnOk     = wx.Button(self, wx.ID_OK, "")

        self.Bind(wx.EVT_BUTTON, self.onBtnCancel,  self.btnCancel)
        self.Bind(wx.EVT_BUTTON, self.onBtnOK,      self.btnOk)

        self.__set_properties()
        self.__do_layout()

    def __set_properties(self):
        self.btnOk.SetDefault()
        self.SetTitle("Data column labels")
        self.SetSize((400, 400))

    def __do_layout(self):
        szrBtn = wx.BoxSizer(wx.HORIZONTAL)
        szrBtn.AddStretchSpacer(prop=20)
        szrBtn.Add(self.btnOk,     1, 0, 0)
        szrBtn.Add(self.btnCancel, 1, 0, 0)
        szrMain = wx.BoxSizer(wx.VERTICAL)
        szrMain.Add(self.pnl, 1, wx.EXPAND, 0)
        szrMain.Add(szrBtn,   0, 0, 0)
        self.SetSizer(szrMain)
        self.Layout()

    def onBtnCancel(self, event):
       self.Destroy()

    def onBtnOK(self, event):
        event.Skip()

    def setData(self, data):
        self.pnl.setData(data)

    def getSelection(self):
        """
        Returns the current selection

        Returns:
            tuple: (name, list of column labels)
        """
        return self.pnl.getSelection()

    def getData(self):
        """
        Returns all the items

        Returns:
            tuple: (name, list of column labels)
        """
        return self.pnl.getData()

class DADlgDataUnits(wx.Dialog):
    """
    Dialog to choose both data column labels and epoch
    """
    def __init__(self, *args, **kwds):
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER

        super(DADlgDataUnits, self).__init__(*args, **kwds)

        self.shsMain  = wx.SplitterWindow(self, wx.ID_ANY, style=wx.SP_3D|wx.SP_BORDER)
        self.pnlCols  = DADlgDataColumnPanel(self.shsMain, wx.ID_ANY, editable=False, staticBox="Data column label")
        self.pnlEpoch = DADlgDataEpochPanel (self.shsMain, wx.ID_ANY, editable=False, staticBox="Epoch")

        self.btnCancel = wx.Button(self, wx.ID_CANCEL, "")
        self.btnOk     = wx.Button(self, wx.ID_OK, "")

        self.Bind(wx.EVT_BUTTON, self.onBtnCancel,  self.btnCancel)
        self.Bind(wx.EVT_BUTTON, self.onBtnOK,      self.btnOk)

        self.__set_properties()
        self.__do_layout()

    def __set_properties(self):
        self.btnOk.SetDefault()
        self.shsMain.SetMinimumPaneSize(100)
        self.shsMain.SetSashGravity(0.5)
        self.SetTitle("Data units")
        self.SetSize((400, 400))

    def __do_layout(self):
        szrBtn = wx.BoxSizer(wx.HORIZONTAL)
        szrBtn.AddStretchSpacer(prop=20)
        szrBtn.Add(self.btnOk,     1, 0, 0)
        szrBtn.Add(self.btnCancel, 1, 0, 0)
        szrMain = wx.BoxSizer(wx.VERTICAL)

        self.shsMain.SplitHorizontally(self.pnlCols, self.pnlEpoch, 200)
        szrMain.Add(self.shsMain, 1, wx.EXPAND)
        szrMain.Add(szrBtn,       0, wx.EXPAND)
        self.SetSizer(szrMain)
        self.Layout()

    def onBtnCancel(self, event):
       self.Destroy()

    def onBtnOK(self, event):
        event.Skip()

    def setData(self, dataColumn=None, dataEpoch=None, ncol=-1):
        self.setDataColumnLabels(ncol=ncol, data=dataColumn)
        self.setDataEpoch(data=dataEpoch)

    def setDataColumnLabels(self, data=None, ncol=-1):
        return self.pnlCols.setData(data=data, ncol=ncol)

    def getColumnLabels(self):
        """
        Returns the current selection

        Returns:
            tuple: (name, list of column labels)
        """
        return self.pnlCols.getSelection()

    def setDataEpoch(self, data=None):
        return self.pnlEpoch.setData(data=data)

    def getEpoch(self):
        """
        Returns the current selection

        Returns:
            tuple: (name, list of column labels)
        """
        return self.pnlEpoch.getSelection()


if __name__ == "__main__":
    class TestApp(wx.App):
        def OnInit(self):
            dlg = DADlgDataUnits(None, wx.ID_ANY, "")
            self.SetTopWindow(dlg)
            if dlg.ShowModal() == wx.ID_OK:
                print('OK')
                print(dlg.getColumnLabels())
                print(dlg.getEpoch())
            return 1

    app = TestApp(0)
    app.MainLoop()

