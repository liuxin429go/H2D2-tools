# -*- coding: utf-8 -*-
# -*- mode: python -*-
import os
import shutil
import sys
import glob
sys.setrecursionlimit(5000)

pkg_path = os.path.join(os.environ['INRS_DEV'], 'H2D2-tools', 'script', 'DADataAnalyzer')
sup_path = os.path.dirname(pkg_path)

hiddenimports=[
   'scipy._lib.messagestream',
   'scipy.special.cython_special',
   'scipy.spatial.transform._rotation_groups',
   'scipy.stats',
   #
   'CTCommon.external',
   'CTCommon.external.pyqtree',
   'CTCommon.QTQuadTree',
   'CTCommon.DTDataBloc',
   'CTCommon.DTPathError',
   'IPImageProcessor.KML',
   ]

datas = [
    ('../IPImageProcessor/KML/images/*.png',        'bitmaps/IPImageProcessor/KML'),
    ('../DADataAnalyzer/bitmaps/*.png',             'bitmaps/DADataAnalyzer'),
    ('../DADataAnalyzer/bitmaps/cmaps/*.png',       'bitmaps/DADataAnalyzer'),
    ('../DADataAnalyzer/bitmaps/16x16-free-application-icons/16x16/*.png', 'bitmaps/DADataAnalyzer'),
    ('../DADataAnalyzer/Filters/*.py',              'Filters'),
    ('../DADataAnalyzer/DAHelp/assets/css/*.css',   'assets/DADataAnalyzer/DAHelp/css'),
    ('../DADataAnalyzer/DAHelp/assets/js/*.js',     'assets/DADataAnalyzer/DAHelp/js'),
    ('../DADataAnalyzer/lgpl*.txt',                 '.'),
    ('../DADataAnalyzer/*.htm*',                    '.'),
    ]
    
binaries = [
    (shutil.which('gdalsrsinfo'),                   '.'),
    ]

# ---  Patch for gdal/proj
# Les fichiers de proj ne sont pas tirés par osgeo/gdal et 
# pyproj n'apparait pas comme hidden import.
if hasattr(sys, 'real_prefix'):  # check if in a virtual environment
    root_path = sys.real_prefix
else:
    root_path = sys.prefix
tgt_gdal_data = os.path.join('Library', 'share', 'proj')
src_gdal_data = os.path.join(root_path, 'Library', 'share', 'proj')
datas.append( (src_gdal_data, tgt_gdal_data) )

# ---  Patch for matplotlib > 3.3.0 with pyinstaller 3.6
# https://github.com/matplotlib/matplotlib/issues/17962
# https://stackoverflow.com/questions/63163027/how-to-use-pyinstaller-with-matplotlib-in-use
from PyInstaller.utils.hooks import exec_statement
mpl_version = exec_statement(
    "import matplotlib; print(matplotlib.__version__)")
if mpl_version.split('.') > ['3', '3', '0']:
    mpl_data_dir = exec_statement(
        "import matplotlib; print(matplotlib._get_data_path())")
    mpl_data = (mpl_data_dir, 'matplotlib/mpl-data')
    datas.append(mpl_data)

block_cipher = None

a = Analysis(['DADataAnalyzer.py'],
             pathex=[pkg_path, sup_path],
             binaries=binaries,
             datas=datas,
             hiddenimports= hiddenimports,
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='DADataAnalyzer',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='DADataAnalyzer')
