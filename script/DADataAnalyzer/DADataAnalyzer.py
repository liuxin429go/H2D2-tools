#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013-2017
# --- Institut National de la Recherche Scientifique (INRS)
# --- Copyright (c) 2018 Yves Secretan
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import argparse
import sys
import wx

if __name__ == "__main__":
    import os
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import DAFrame

RPDB2_IMPORTED = False
try:
    import rpdb2
    RPDB2_IMPORTED = True
except ImportError:
    pass

LST_EVENTS = {
    wx.EVT_LIST_BEGIN_DRAG.typeId       : 'EVT_LIST_BEGIN_DRAG',
    wx.EVT_LIST_BEGIN_RDRAG.typeId      : 'EVT_LIST_BEGIN_RDRAG',
    wx.EVT_LIST_BEGIN_LABEL_EDIT.typeId : 'EVT_LIST_BEGIN_LABEL_EDIT',
    wx.EVT_LIST_END_LABEL_EDIT.typeId   : 'EVT_LIST_END_LABEL_EDIT',
    wx.EVT_LIST_DELETE_ITEM.typeId      : 'EVT_LIST_DELETE_ITEM',
    wx.EVT_LIST_DELETE_ALL_ITEMS.typeId : 'EVT_LIST_DELETE_ALL_ITEMS',
    wx.EVT_LIST_ITEM_SELECTED.typeId    : 'EVT_LIST_ITEM_SELECTED',
    wx.EVT_LIST_ITEM_DESELECTED.typeId  : 'EVT_LIST_ITEM_DESELECTED',
    wx.EVT_LIST_ITEM_ACTIVATED.typeId   : 'EVT_LIST_ITEM_ACTIVATED',
    wx.EVT_LIST_ITEM_FOCUSED.typeId     : 'EVT_LIST_ITEM_FOCUSED',
    wx.EVT_LIST_ITEM_MIDDLE_CLICK.typeId : 'EVT_LIST_ITEM_MIDDLE_CLICK',
    wx.EVT_LIST_ITEM_RIGHT_CLICK.typeId : 'EVT_LIST_ITEM_RIGHT_CLICK',
    wx.EVT_LIST_KEY_DOWN.typeId         : 'EVT_LIST_KEY_DOWN',
    wx.EVT_LIST_INSERT_ITEM.typeId      : 'EVT_LIST_INSERT_ITEM',
    wx.EVT_LIST_COL_CLICK.typeId        : 'EVT_LIST_COL_CLICK',
    wx.EVT_LIST_COL_RIGHT_CLICK.typeId  : 'EVT_LIST_COL_RIGHT_CLICK',
    wx.EVT_LIST_COL_BEGIN_DRAG.typeId   : 'EVT_LIST_COL_BEGIN_DRAG',
    wx.EVT_LIST_COL_DRAGGING.typeId     : 'EVT_LIST_COL_DRAGGING',
    wx.EVT_LIST_COL_END_DRAG.typeId     : 'EVT_LIST_COL_END_DRAG',
    wx.EVT_LIST_CACHE_HINT.typeId       : 'EVT_LIST_CACHE_HINT',
}

class DADataAnalyzerApp(wx.App):
    """
    DADataAnalyzer application
    The class is very thin, but permits to debug the events
    fired by the windows.
    """
    def __init__(self, *args, **kwargs):
        super(DADataAnalyzerApp, self).__init__(*args, **kwargs)

    # def FilterEvent(self, event):
    #     if isinstance(event, (wx._core.ListEvent)):
    #         itp = event.GetEventType()
    #         try:
    #             print((LST_EVENTS[itp]))
    #         except:
    #             print (itp)
    #     event.Skip()
    #     return -1

if __name__ == "__main__":
    def main(optArgs=None):
        # ---  Parse les options
        usage  = '%s [options]' % __package__
        parser = argparse.ArgumentParser(usage)
        parser.add_argument("-d", "--debug",
                            dest="dbg",
                            default=False,
                            action='store_true',
                            help="debug the run")

        # --- Parse les arguments de la ligne de commande
        if not optArgs:
            optArgs = sys.argv[1:]
        (options, unknowns) = parser.parse_known_args(optArgs)

        # --- Debug si demandé
        if options.dbg:
            if not RPDB2_IMPORTED:
                raise 'rpdb2 must be installed to debug'
            rpdb2.start_embedded_debugger("inrs_iehss", fAllowUnencrypted=True)

        # --- Lance l'application
        app = DADataAnalyzerApp(False)

        #provider = wx.SimpleHelpProvider()
        #wx.HelpProvider.Set(provider)

        frame = DAFrame.DAFrame(None, -1, "")
        app.SetTopWindow(frame)
        frame.Show()

        app.MainLoop()

    main()
