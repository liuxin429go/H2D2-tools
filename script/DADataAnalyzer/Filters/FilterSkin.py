# -*- coding: utf-8 -*-

from DAPanels.DAFilter import DAGridFilter
import numpy as np

class FilterSkin(DAGridFilter):
    def filter(self, srcMesh):
        return srcMesh.genSkin()
