# -*- coding: utf-8 -*-

from DAPanels.DAFilter import DAGridFilter
import numpy as np

class FilterElementsWithTwoNodesOnBoundary(DAGridFilter):
    def __init__(self):
        self.srcMesh = None
        self.skinNodes = None
        
    def filterElem(self, ele):
        iel = ele.getLocalIndex()
        nod = ele.getNodes()
        for iside in (0, 1, 2):
            if self.srcMesh.getElementNeighbour(iel, iside) < 0: 
                continue
            n1, n2 = ele.getEdgeConnectivities(iside)
            if n1 in self.skinNodes and n2 in self.skinNodes:
                return True
        return False
    
    def filter(self, srcMesh):
        if srcMesh.isSubMesh():
            raise RuntimeError("Invalid mesh type: mesh can not be a submesh")
        
        self.srcMesh = srcMesh
        skin = srcMesh.genSkin(doChain=False)
        self.skinNodes = set( skin.getNodeNumbersGlobal() )

        elems  = [ e for e in srcMesh.iterElements() if self.filterElem(e) ]
        connec = [ e.getConnectivities() for e in elems ]
        if connec:
            return srcMesh.genCompactGrid(connec)
        else:
            raise RuntimeError("Filter returned an empty grid")