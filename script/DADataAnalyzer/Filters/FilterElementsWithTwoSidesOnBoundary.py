# -*- coding: utf-8 -*-

from DAPanels.DAFilter import DAGridFilter
import numpy as np

class FilterElementsWithTwoSidesOnBoundary(DAGridFilter):
    def filter(self, srcMesh):
        elems = []
        for ele in srcMesh.iterElements():
            iel = ele.getLocalIndex()
            ie0 = srcMesh.getElementNeighbour(iel, 0)
            ie1 = srcMesh.getElementNeighbour(iel, 1)
            ie2 = srcMesh.getElementNeighbour(iel, 2)
            if (ie0 < 0 and ie1 < 0) or (ie0 < 0 and ie2 < 0) or (ie1 < 0 and ie2 < 0):
                elems.append(ele)
        connec = [ e.getConnectivities() for e in elems ]
        if connec:
            return srcMesh.genCompactGrid(connec)
        else:
            raise RuntimeError("Filter returned an empty grid")
