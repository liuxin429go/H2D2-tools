# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import numpy as np

from CTCommon.FEMesh import FEMesh

class DAFrameGrid(FEMesh):
    cntr = 0
    
    def __init__(self, files=None, grid2proj=None, proj2grid=None, parent=None, **kwargs):
        assert files is None or all([os.path.isfile(f) for f in files])
        super(DAFrameGrid, self).__init__(files=files, parent=parent, **kwargs)
        self.files = files
        self.grid2proj = grid2proj if grid2proj else parent.grid2proj if parent else None
        self.proj2grid = proj2grid if proj2grid else parent.proj2grid if parent else None
        self.bboxProj  = None
        self.idx = DAFrameGrid.cntr
        DAFrameGrid.cntr += 1

    def __xtrBbox(self):
        """
        Bounding box for projected coord, without stretching & margins
        """
        if self.isSubGrid:     # For sub-grids
            raise 'DAFrameGrid.getBbox not validated for subgrids'
        else:
            X, Y = self.getCoordinates()
            xmin = np.min(X)
            xmax = np.max(X)
            ymin = np.min(Y)
            ymax = np.max(Y)
        return (xmin, ymin, xmax, ymax)

    def __str__(self):
        return self.getShortName()

    def getShortName(self):
        if self.files:
            if len(self.files) == 1:
                name = os.path.basename(self.files[0])
            else:
                name = ' '.join( (os.path.basename(self.files[0]), '...') )
        else:
            name = 'Grid name not set'
        return '#%02i %s' % (self.idx, name)

    def getLongName(self):
        name = '; '.join(self.files) if self.files else 'Grid name not set'
        return '#%02i %s' % (self.idx, name)

    def getCoordinates(self):
        """
        Specialize getCoordinates to return projected coordinates.
        Raw coord can be retrieved by a call to the parent method
        FEMesh.getCoordinates
        """
        X, Y = FEMesh.getCoordinates(self)
        if self.grid2proj:
            r = self.grid2proj.TransformPoints(list(zip(X, Y)))
            X, Y, Z = list(zip(*r))
            X, Y = np.array(X), np.array(Y)
        return X, Y

    def getBbox(self, stretchfactor=0, margins=(0, 0)):
        """
        Specialize getBbox to the bounding box for projected coordinates.
        BBox for raw coord can be retrieved by a call to the parent method
        FEMesh.getBbox
        """
        if not self.bboxProj:
            self.bboxProj = self.__xtrBbox()
        xmin, ymin, xmax, ymax = self.bboxProj
        dx = (xmax-xmin)*stretchfactor + margins[0]
        dy = (ymax-ymin)*stretchfactor + margins[1]
        return (xmin-dx, ymin-dy, xmax+dx, ymax+dy)

    def getFilenames(self):
        return self.files

    def getProjectionFromProjToGrid(self):
        return self.proj2grid

    def getProjectionFromGridToProj(self):
        return self.grid2proj

    def hasProjection(self):
        return self.grid2proj is not None

    def projectToGrid(self, X, Y):
        """
        Projet X, Y coord form project to grid coordinates
        """
        if self.proj2grid:
            r = self.proj2grid.TransformPoints(list(zip(X, Y)))
            X, Y, Z = list(zip(*r))
            X, Y = np.array(X), np.array(Y)
        return X, Y

    def projectToProj(self, X, Y):
        """
        Projet X, Y coord form grid coordinates to project
        """
        if self.grid2proj:
            r = self.grid2proj.TransformPoints(list(zip(X, Y)))
            X, Y, Z = list(zip(*r))
            X, Y = np.array(X), np.array(Y)
        return X, Y

