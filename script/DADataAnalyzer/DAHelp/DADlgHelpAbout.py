#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import collections.abc
import io
import logging
import os
import sys
import six

import wx
import wx.html2
import webbrowser

import __about__

if getattr(sys, 'frozen', False):
    ROOTDIR  = os.path.join(sys._MEIPASS)
    ASSTSDIR = os.path.join(sys._MEIPASS, 'assets', 'DADataAnalyzer', 'DAHelp')
else:
    ROOTDIR  = os.path.join(os.path.dirname(__file__), '..')
    ASSTSDIR = os.path.join(os.path.dirname(__file__), 'assets')
DA_ROOT = os.path.normpath(ROOTDIR).replace('\\', '/')
CSSFILE = os.path.join(ASSTSDIR, 'css', 'dahelp.css').replace('\\', '/')
JSFILE  = os.path.join(ASSTSDIR, 'js',  'tablesort.js').replace('\\', '/')

LOGGER = logging.getLogger('INRS.H2D2.Tools.DataAnalyzer.Help.About')

"""
https://github.com/wxWidgets/Phoenix/issues/1256
On Windows, wx.WebView is configured with IE7. One has to change
Reg setting to IE11.
"""
def setReg(name, regPath, value):
    """
    Set Windows registry regpath to value
    """
    try:
        winreg.CreateKey(winreg.HKEY_CURRENT_USER, regPath)
        regKey = winreg.OpenKey(winreg.HKEY_CURRENT_USER, regPath, 0, winreg.KEY_WRITE)
        winreg.SetValueEx(regKey, name, 0, winreg.REG_DWORD, value)
        winreg.CloseKey(regKey)
        return True
    except WindowsError:
        return False

def getReg(name, regPath):
    """
    Get Windows registry regpath value
    """
    try:
        regKey = winreg.OpenKey(winreg.HKEY_CURRENT_USER, regPath, 0, winreg.KEY_READ)
        value, _regType = winreg.QueryValueEx(regKey, name)
        winreg.CloseKey(regKey)
        return value
    except WindowsError:
        return None

if wx.Platform == '__WXMSW__':
    import winreg
    regPath = r'Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION'
    exe = os.path.basename(sys.executable)
    setReg(exe, regPath, 11001)
    LOGGER.debug('Registry:')
    LOGGER.debug('   Key: %s', regPath)
    LOGGER.debug('   Val: %s', getReg(exe, regPath))

def isIterable(arg):
    """
    Return True if arg is iterable but not a string
    https://stackoverflow.com/questions/1055360/how-to-tell-a-variable-is-iterable-but-not-a-string
    """
    return isinstance(arg, collections.abc.Iterable) and not isinstance(arg, six.string_types)

class About:
    """
    About info
    """
    @staticmethod
    def asHtml():
        """
        Return class info as html page
        """
        def __addOneRow(oss, k, v):
            oss.write('   <tr>\n')
            oss.write('      <td>%s</td>\n' % k)
            if isIterable(v):
                oss.write('      <td>\n')
                __addAsTable(oss, v)
                oss.write('      </td>\n')
            else:
                oss.write('      <td>%s</td>\n' % v)
            oss.write('   </tr>\n')
        def __addAsTable(oss, v):
            oss.write('<table>\n')
            oss.write('<tbody>\n')
            for item in v:
                oss.write('   <tr style="border: none"><td style="padding: 0px">%s</td></tr>\n' % item)
            oss.write('</tbody>\n')
            oss.write('</table>\n')

        oss = io.StringIO()

        oss.write('<!DOCTYPE html>\n')
        oss.write('<html>\n')
        oss.write('<title>About</title>\n')
        oss.write('<meta charset="utf-8">\n')
        oss.write('<link rel="stylesheet" href="%s">\n' % CSSFILE)
        oss.write('<body class="w3-container">\n')

        oss.write('<h1><center>%s</center></h1>' % 'H2D2 Data Analyzer')
        oss.write('<table class="w3-table" style="font-size:12px">\n')
        oss.write('<tbody>\n')
        __addOneRow(oss, 'Version', __about__.__version__)
        __addOneRow(oss, 'Frozen on', '')
        __addOneRow(oss, 'Copyright', __about__.__copyright__)
        __addOneRow(oss, 'License', __about__.__license__[0])
        __addOneRow(oss, '', __about__.__license__[1])
        __addOneRow(oss, '', '')
        __addOneRow(oss, 'Description', 'H2D2 Data Analyzer ...')
        __addOneRow(oss, 'Developers', __about__.__author__)
        #__addOneRow(oss, 'git', __about__.__git__)
        oss.write('</tbody>\n')
        oss.write('</table>\n')

        oss.write('</body>\n')
        oss.write('</html>\n')
        return oss.getvalue()

class Licence:
    """
    Licence info
    """
    @staticmethod
    def asHtml():
        """
        Return class info as html page
        """
        LIC_FILE = os.path.normpath( os.path.join(DA_ROOT, 'lgpl-3.0.txt') )
        oss = io.StringIO()

        oss.write('<!DOCTYPE html>\n')
        oss.write('<html>\n')
        oss.write('<title>Licence</title>\n')
        oss.write('<meta charset="utf-8">\n')
        oss.write('<body class="w3-container">\n')
        oss.write('<pre>\n')

        with open(LIC_FILE, encoding='utf-8') as ifs:
            for line in ifs:
                oss.write(line)
        oss.write('</pre>\n')
        oss.write('</body>\n')
        oss.write('</html>\n')
        return oss.getvalue()

class Credits:
    @staticmethod
    def asHtml():
        """
        Return class info as html page
        """
        oss = io.StringIO()

        oss.write('<!DOCTYPE html>\n')
        oss.write('<html>\n')
        oss.write('<title>System</title>\n')
        oss.write('<meta charset="utf-8">\n')
        oss.write('<link rel="stylesheet" href="%s">\n' % CSSFILE)
        oss.write('<body class="w3-container">\n')
        oss.write('<h3>Credits</h3>\n')

        for title, entries in __about__.__credits__.items():
            oss.write('<h4 style="font-size:15px">%s</h4>\n' % title)
            oss.write('<ul style="list-style: none;"><li>')
            oss.write('<table class="w3-table" style="font-size:12px">\n')
            oss.write('<tbody>\n')
            for entry, value in entries.items():
                oss.write('   <tr>\n')
                oss.write('      <td>%s</td>\n' % entry)
                oss.write('      <td>%s</td>\n' % value)
                oss.write('   </tr>\n')
            oss.write('</tbody>\n')
            oss.write('</table>\n')
            oss.write('</li></ul>')
            oss.write('<p>\n')

        oss.write('</body>\n')
        oss.write('</html>\n')
        return oss.getvalue()

class Modules:
    """
    Python modules info
    """
    @staticmethod
    def getModulesInfo():
        """
        https://stackoverflow.com/questions/38267791/how-to-i-list-imported-modules-with-their-version/38268153#38268153
        """
        mdls = {}
        for name, module in sys.modules.items():
            skip = False
            skip |= '.' in name             # skip submodules
            skip |= name.startswith('_')    # skip system modules
            skip |= __name__ in name        # skip self

            if not skip:
                pth, vrs = '', ''
                if hasattr(module, '__file__'):    pth = module.__file__
                if hasattr(module, '__version__'): vrs = module.__version__
                if not pth: pth = ''        # provision against None
                if not vrs: vrs = ''        # provision against None
                mdls[name] = [pth, vrs]
        return mdls

    @staticmethod
    def asHtml():
        """
        Return class info as html page
        """
        oss = io.StringIO()

        oss.write('<!DOCTYPE html>\n')
        oss.write('<html>\n')
        oss.write('<title>DADataAnalyzer</title>\n')
        oss.write('<meta charset="utf-8">\n')
        oss.write('<link rel="stylesheet" href="%s">\n' % CSSFILE)
        oss.write('<script type="text/javascript" src="%s"></script>\n' % JSFILE)
        oss.write('<body class="w3-container">\n')
        oss.write('<h3>Modules</h3>\n')

        oss.write('<table class="w3-table-alt" style="font-size:12px" id="idTbl" >\n')
        oss.write('<thead>\n')
        oss.write('   <tr style="cursor:pointer">\n')
        oss.write('      <th onclick="sortTable(\'idTbl\', 0)">Name</th>\n')
        oss.write('      <th onclick="sortTable(\'idTbl\', 1)">Version</th>\n')
        oss.write('      <th onclick="sortTable(\'idTbl\', 2)">Path</th>\n')
        oss.write('   </tr>\n')
        oss.write('</thead>\n')
        oss.write('<tbody>\n')

        mdlInfo = Modules.getModulesInfo()
        for key in sorted(mdlInfo):
            val = mdlInfo[key]
            oss.write('   <tr class="item">\n')
            oss.write('      <td>%s</td>\n' % key)
            oss.write('      <td>%s</td>\n' % val[1])
            oss.write('      <td>%s</td>\n' % val[0])
            oss.write('   </tr>\n')

        oss.write('</tbody>\n')
        oss.write('</table>\n')
        oss.write('</body>\n')
        oss.write('</html>\n')
        # lines = oss.getvalue()
        # print('----------------------')
        # for l in lines.split('\n')[:20]:
        #     print(l)
        # print('----------------------')
        return oss.getvalue()

    @staticmethod
    def asHtml_tablecss():
        """
        Return class info as html page
        """
        oss = io.StringIO()

        oss.write('<!DOCTYPE html>\n')
        oss.write('<html>\n')
        oss.write('<title>DADataAnalyzer</title>\n')
        oss.write('<meta charset="utf-8">\n')
        oss.write('<link rel="stylesheet" href="%s">\n' % CSSFILE)
        oss.write('<script type="text/javascript" src="%s"></script>\n' % JSFILE)
        oss.write('<body class="w3-container>\n')
        oss.write('<h3>Modules</h3>\n')

        oss.write('<table class="table100" id="idTbl">\n')
        oss.write('<thead>\n')
        oss.write('   <tr style="cursor:pointer">\n')
        oss.write('      <th class="cell100 column0" onclick="sortTable(\'idTbl\', 0)">Name</th>\n')
        oss.write('      <th class="cell100 column1" onclick="sortTable(\'idTbl\', 1)">Version</th>\n')
        oss.write('      <th class="cell100 column2" onclick="sortTable(\'idTbl\', 2)">Path</th>\n')
        oss.write('   </tr>\n')
        oss.write('</thead>\n')
        oss.write('<tbody>\n')

        mdlInfo = Modules.getModulesInfo()
        for key in sorted(mdlInfo):
            val = mdlInfo[key]
            oss.write('   <tr>\n')
            oss.write('      <td class="cell100 column0">%s</td>\n' % key)
            oss.write('      <td class="cell100 column1">%s</td>\n' % val[1])
            oss.write('      <td class="cell100 column2">%s</td>\n' % val[0])
            oss.write('   </tr>\n')

        oss.write('</tbody>\n')
        oss.write('</table>\n')
        oss.write('</body>\n')
        oss.write('</html>\n')
        # lines = oss.getvalue()
        # print('----------------------')
        # for l in lines.split('\n')[:20]:
        #     print(l)
        # print('----------------------')
        return oss.getvalue()

class System:
    """
    System info
    """
    CONDA_KEYS_ALL = ['build', 'build_number', 'channel', 'constrains', 'depends', 'extracted_package_dir', 'features', 'files', 'fn', 'license', 'link', 'md5', 'name', 'package_tarball_full_path', 'paths_data', 'requested_spec', 'sha256', 'size', 'subdir', 'timestamp', 'track_features', 'url', 'version']
    CONDA_KEYS = ['name', 'version', 'build', 'build_number', 'fn', 'md5', 'sha256']

    @staticmethod
    def getPythonInfo():
        """
        Retrive python info
        """
        import glob, json

        info = {}
        info['python version']=sys.version
        info['sys.prefix']=sys.prefix

        isConda = os.path.exists(os.path.join(sys.prefix, 'conda-meta'))
        if not isConda: return info

        for conda in ['conda', 'anaconda']:
            ptrn = os.path.join(sys.prefix, 'conda-meta', '%s-*.json' % conda)
            for fpath in glob.glob(ptrn):
                vrs = os.path.basename(fpath).split('-')[1]
                vrs = vrs.split('.')
                if len(vrs) <= 1: continue
                with open(fpath) as jsonFile:
                    data = json.load(jsonFile)
                    for key in System.CONDA_KEYS:
                        info['conda: %s' % key] = data[key]
        return info

    @staticmethod
    def getSystemInfo():
        """
        Retrive system info
        """
        import platform, socket, psutil
        info={}
        info['platform']=platform.system()
        info['platform-release']=platform.release()
        info['platform-version']=platform.version()
        info['architecture']=platform.machine()
        info['hostname']=socket.gethostname()
        info['processor']=platform.processor()
        info['ram']=str(round(psutil.virtual_memory().total / (1024.0 **3)))+' GB'
        return info

    @staticmethod
    def asHtml():
        """
        Return class info as html page
        """
        oss = io.StringIO()

        oss.write('<!DOCTYPE html>\n')
        oss.write('<html>\n')
        oss.write('<title>System</title>\n')
        oss.write('<meta charset="utf-8">\n')
        oss.write('<link rel="stylesheet" href="%s">\n' % CSSFILE)
        oss.write('<body class="w3-container">\n')

        oss.write('<h3>System</h3>\n')
        oss.write('<table class="w3-table-alt" style="font-size:12px">\n')
        oss.write('<thead>\n')
        oss.write('   <tr>\n')
        oss.write('      <th>Item</th>\n')
        oss.write('      <th>Value</th>\n')
        oss.write('   </tr>\n')
        oss.write('</thead>\n')
        oss.write('<tbody>\n')
        info = System.getSystemInfo()
        for key, val in info.items():
            oss.write('   <tr class="item">\n')
            oss.write('      <td>%s</td>\n' % key)
            oss.write('      <td>%s</td>\n' % val)
            oss.write('   </tr>\n')
        oss.write('</tbody>\n')
        oss.write('</table>\n')

        oss.write('<h3>Python</h3>\n')
        oss.write('<table class="w3-table-alt" style="font-size:12px">\n')
        oss.write('<thead>\n')
        oss.write('   <tr>\n')
        oss.write('      <th>Item</th>\n')
        oss.write('      <th>Value</th>\n')
        oss.write('   </tr>\n')
        oss.write('</thead>\n')
        oss.write('<tbody>\n')
        info = System.getPythonInfo()
        for key, val in info.items():
            oss.write('   <tr class="item">\n')
            oss.write('      <td>%s</td>\n' % key)
            oss.write('      <td>%s</td>\n' % val)
            oss.write('   </tr>\n')
        oss.write('</tbody>\n')
        oss.write('</table>\n')

        oss.write('</tbody>\n')
        oss.write('</table>\n')
        oss.write('</body>\n')
        oss.write('</html>\n')
        return oss.getvalue()

class HtmlWindow(wx.Panel):
    def __init__(self, *args, **kwds):
        page = kwds.pop('page', '')
        url  = kwds.pop('url', '')
        file = kwds.pop('file', '')
        kwds['style'] = kwds.get('style', 0) | wx.TAB_TRAVERSAL
        super(HtmlWindow, self).__init__(*args, **kwds)

        self.browser = wx.html2.WebView.New(self)
        self.__set_properties(page, url, file)
        self.__do_layout()

        self.browser.Bind(wx.html2.EVT_WEBVIEW_NAVIGATING, self.onNav)
        self.browser.Bind(wx.html2.EVT_WEBVIEW_NEWWINDOW,  self.onNewWindow)
        self.lastUrl = ''

    def __set_properties(self, page, url, file):
        if page:
            self.browser.SetPage(page, '')
        if url:
            self.browser.LoadURL(url)
        if file:
            self.browser.SetPage(open(file, encoding='utf-8').read(), '')

    def __do_layout(self):
        szr = wx.BoxSizer(wx.VERTICAL)
        szr.Add(self.browser, 1, wx.EXPAND, 0)
        self.SetSizer(szr)

    def onNav(self, event):
        """
        https://stackoverflow.com/questions/29508872/how-to-use-webbrowser-to-open-the-link-when-it-is-clicked-in-wxpython-frame
        """
        LOGGER.trace('onNav called')
        url = event.GetURL().strip()
        url = url.replace('about:/DA_ROOT/', 'file:///'+DA_ROOT+'/')
        if url != self.lastUrl:
            webbrowser.open(url)
            self.lastUrl = url
        event.Veto()

    def onNewWindow(self, event):
        LOGGER.trace('onNewWindow called')
        url = event.GetURL()
        url = url.replace('about:/DA_ROOT/', 'file:///'+DA_ROOT+'/')
        if url != self.lastUrl:
            webbrowser.open(url)
            self.lastUrl = url
        event.Veto()

class DADlgHelpAbout(wx.Dialog):
    def __init__(self, *args, **kwds):
        title = kwds.pop('title', __about__.__name__)
        kwds['style'] = kwds.get('style', 0) | wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER
        super(DADlgHelpAbout, self).__init__(*args, **kwds)

        self.nbkMain    = wx.Notebook(self, wx.ID_ANY)
        self.tabAbout   = HtmlWindow(self.nbkMain, wx.ID_ANY, page=About.asHtml())
        self.tabLicences= HtmlWindow(self.nbkMain, wx.ID_ANY, page=Licence.asHtml())
        self.tabCredits = HtmlWindow(self.nbkMain, wx.ID_ANY, page=Credits.asHtml())
        self.tabModules = HtmlWindow(self.nbkMain, wx.ID_ANY, page=Modules.asHtml())
        self.tabSystem  = HtmlWindow(self.nbkMain, wx.ID_ANY, page=System.asHtml())
        self.btnOK = wx.Button(self, wx.ID_OK, '')

        self.__set_properties(title)
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON, self.onBtnOK, self.btnOK)

    def __set_properties(self, title):
        """
        Private
        """
        self.SetTitle(title)
        self.SetSize((600, 400))

    def __do_layout(self):
        """
        Private
        """
        szrMain = wx.BoxSizer(wx.VERTICAL)
        self.nbkMain.AddPage(self.tabAbout,    'About')
        self.nbkMain.AddPage(self.tabLicences, 'Licence')
        self.nbkMain.AddPage(self.tabCredits,  'Credits')
        self.nbkMain.AddPage(self.tabModules,  'Python modules')
        self.nbkMain.AddPage(self.tabSystem,   'System')
        szrMain.Add(self.nbkMain, 1, wx.EXPAND, 0)
        szrMain.Add(self.btnOK,   0, wx.ALIGN_RIGHT, 0)
        self.SetSizer(szrMain)
        self.Layout()

    def onBtnOK(self, event):
        """
        Exit
        """
        event.Skip()

if __name__ == '__main__':
    LOGGER.trace = LOGGER.debug
    class MyApp(wx.App):
        def OnInit(self):
            dlg = DADlgHelpAbout(None, wx.ID_ANY, '')
            self.SetTopWindow(dlg)
            dlg.ShowModal()
            dlg.Destroy()
            return True

    app = MyApp(0)
    app.MainLoop()
