// Adapted from
// https://webdesires.co.uk/knowledge-base/sort-table-by-column-using-javascript-text-sort-number-sort/

var asc = 0;

function sort_table(table, col)
{
   if (asc == 2) {asc = -1;} else {asc = 2;}
   var rows = table.tBodies[0].rows;
   var rlen = rows.length-1;
   var arr = [];
   var i, j, cells, clen;
   // Fill the array with values from the table
   for (i = 0; i < rlen; i++)
   {
      cells = rows[i].cells;
      clen = cells.length;
      arr[i] = [];
      for (j = 0; j < clen; j++)
      { 
         arr[i][j] = cells[j].innerHTML; 
      }
   }
   // Sort the array by the specified column number (col) and order (asc)
   arr.sort(function(a, b)
   {
      var retval = 0;
      var col1 = a[col].toLowerCase();
      var col2 = b[col].toLowerCase();
      if (col1 != col2)
      {
         retval = (col1 > col2) ? asc : -1*asc;
      }
      return retval;      
   });
   for (var rowidx=0;rowidx<rlen;rowidx++)
   {
      for(var colidx=0;colidx<arr[rowidx].length;colidx++)
      { 
         table.tBodies[0].rows[rowidx].cells[colidx].innerHTML=arr[rowidx][colidx]; 
      }
   }
   
   // Remove sort orders
   var hdrs = table.rows[0].cells;
   var nhdr = hdrs.length;
   var hdr;
   for (var i = 0; i < nhdr; i++)
   {
      hdr = hdrs[i];
      hdr.innerHTML = hdr.innerHTML.split('<span')[0];
   }
   // Set sort orders for col
   var hdr = hdrs[col];
   if (asc == -1) {
      hdr.innerHTML = hdr.innerHTML + '<span class="sortorder">▼</span>';
      } else {
      hdr.innerHTML = hdr.innerHTML + '<span class="sortorder">▲</span>';
   }
}

function sortTable(id, col) {
   sort_table(document.getElementById(id), col);
}
