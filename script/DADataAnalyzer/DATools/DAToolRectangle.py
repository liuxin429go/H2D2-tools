#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath( os.path.join(selfDir, '..') )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import wx

from CTCommon.CTRectangleSelector import CTRectangleSelector

class DAToolRectangle:
    def __init__(self, window, axes, cb):
        self.parent = window
        self.cb = cb
        self.RS = CTRectangleSelector(axes,
                                      self.onSelect,
                                      drawtype='box',
                                      button=[1],           # don't use middle/right button
                                      interactive=True,
                                      #minspanx=5,
                                      #minspany=5,
                                      #spancoords='pixels'
                                      )
        self.RS.set_active(False)

        self.mnu = wx.Menu()
        self.mnu_bbox = wx.MenuItem(self.mnu, wx.ID_ANY, "bbox to clipboard",   "Copy to clipboard as a bounding box (xmin, ymin, xmax, ymax)", wx.ITEM_NORMAL)
        self.mnu_fltr = wx.MenuItem(self.mnu, wx.ID_ANY, "filter to clipboard", "Copy to clipboard as a coordinate filter", wx.ITEM_NORMAL)
        #self.mnu_wkt = wx.MenuItem(self.mnu, wx.ID_ANY, "WKT to clipboard", "Copy to clipboard as a WKT string", wx.ITEM_NORMAL)
        self.mnu.Append(self.mnu_bbox)
        self.mnu.Append(self.mnu_fltr)

        self.parent.Bind(wx.EVT_CONTEXT_MENU, self.onPopupMenu)
        self.parent.Bind(wx.EVT_MENU, self.onMenuBbox, self.mnu_bbox)
        self.parent.Bind(wx.EVT_MENU, self.onMenuFltr, self.mnu_fltr)

        self.bbox = None

    @staticmethod
    def delete(self):
        try:
            self.RS.set_active(False)
            self.RS.clear()
            del(self.RS)
            self.RS = None
            self.parent.Unbind(wx.EVT_CONTEXT_MENU)
        except RuntimeError:
            pass

    def __setBBox(self, eclick, erelease):
        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        self.bbox = (min(x1, x2), min(y1, y2), max(x1, x2), max(y1, y2))

    def onPopupMenu(self, event):
        self.parent.PopupMenu(self.mnu)

    def onMenuBbox(self, event):
        x1, y1, x2, y2 = self.bbox
        clipdata = wx.TextDataObject()
        clipdata.SetText('(%s, %s, %s, %s)' % (x1, y1, x2, y2))
        wx.TheClipboard.Open()
        wx.TheClipboard.SetData(clipdata)
        wx.TheClipboard.Close()

    def onMenuFltr(self, event):
        x1, y1, x2, y2 = self.bbox
        clipdata = wx.TextDataObject()
        clipdata.SetText('{1} >= %s and {2} >= %s and {1} <= %s and {2} <= %s' % (x1, y1, x2, y2))
        wx.TheClipboard.Open()
        wx.TheClipboard.SetData(clipdata)
        wx.TheClipboard.Close()

    def onSelect(self, eclick, erelease):
        self.__setBBox(eclick, erelease)
        x1, y1, x2, y2 = self.bbox
        self.cb( (x2-x1)*(y2-y1) )

    def activate(self, state = True):
        self.RS.set_active(state)

if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plt

    def cb(l):
        print(l)

    def main(parent):
        fig, current_ax = plt.subplots()                 # make a new plotingrange
        N = 100000                                       # If N is large one can see
        x = np.linspace(0.0, 10.0, N)                    # improvement by use blitting!

        plt.plot(x, +np.sin(.2*np.pi*x), lw=3.5, c='b', alpha=.7)  # plot something
        plt.plot(x, +np.cos(.2*np.pi*x), lw=3.5, c='r', alpha=.5)
        plt.plot(x, -np.sin(.2*np.pi*x), lw=3.5, c='g', alpha=.3)

        r = DAToolRectangle(parent, current_ax, cb)
        r.activate(True)
        plt.show()

    app = wx.App(0)
    frame = wx.Frame(None, -1, "")
    app.SetTopWindow(frame)
    frame.Show()
    main(frame)
    app.MainLoop()
