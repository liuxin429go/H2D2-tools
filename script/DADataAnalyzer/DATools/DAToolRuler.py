#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import math

from pubsub import pub
import wx

from CTCommon.CTRectangleSelector import CTRectangleSelector

class DAToolRuler:
    def __init__(self, window, axes, cb):
        self.parent = window
        self.cb = cb
        self.RS = CTRectangleSelector(axes,
                                      self.__get_length,
                                      drawtype='line',
                                      button=[1],     # don't use middle/right button
                                      interactive=True,
                                      #minspanx=5,
                                      #minspany=5,
                                      #spancoords='pixels'
                                      )
        self.RS.set_active(False)

        self.mnu = wx.Menu()
        self.mnu_bbxtpl = wx.MenuItem(self.mnu, wx.ID_ANY, "endpoints as (x0,y0), (x1,y1)",   "Copy endpoint location to clipboard as tuples", wx.ITEM_NORMAL)
        self.mnu_bbxlst = wx.MenuItem(self.mnu, wx.ID_ANY, "endpoints as [(x0,y0), (x1,y1)]", "Copy endpoint location to clipboard as list of tuples.", wx.ITEM_NORMAL)
        #self.mnu_bbxwkt = wx.MenuItem(self.mnu, wx.ID_ANY, "Line as WKT",  "Line to clipboard as WKT", wx.ITEM_NORMAL)
        self.mnu_gentpl = wx.MenuItem(self.mnu, wx.ID_ANY, "point generator as (x,y)",   "Write tuple of tuples generator to clipboard. The required number of points shall be edited.", wx.ITEM_NORMAL)
        self.mnu_genlst = wx.MenuItem(self.mnu, wx.ID_ANY, "point generator as [(x,y)]", "Write list of tuples generator to clipboard. The required number of points shall be edited.", wx.ITEM_NORMAL)
        #self.mnu_genwkt = wx.MenuItem(self.mnu, wx.ID_ANY, "Pick as WKT",  "Copy x, y location to clipboard as WKT", wx.ITEM_NORMAL)

        self.mnu.Append(self.mnu_bbxtpl)
        self.mnu.Append(self.mnu_bbxlst)
        #self.mnu.Append(self.mnu_bbxwkt)
        self.mnu.AppendSeparator()
        self.mnu.Append(self.mnu_gentpl)
        self.mnu.Append(self.mnu_genlst)
        #self.mnu.Append(self.mnu_genwkt)

        self.parent.Bind(wx.EVT_CONTEXT_MENU, self.onPopupMenu)
        self.parent.Bind(wx.EVT_MENU, self.onMenuBbxTpl, self.mnu_bbxtpl)
        self.parent.Bind(wx.EVT_MENU, self.onMenuBbxLst, self.mnu_bbxlst)
        #self.parent.Bind(wx.EVT_MENU, self.onMenuBbxWKT, self.mnu_bbxwkt)
        self.parent.Bind(wx.EVT_MENU, self.onMenuGenTpl, self.mnu_gentpl)
        self.parent.Bind(wx.EVT_MENU, self.onMenuGenLst, self.mnu_genlst)
        #self.parent.Bind(wx.EVT_MENU, self.onMenuGenWKT, self.mnu_genwkt)
        
        self.parent.Bind(wx.EVT_MENU_OPEN,      self.onMenuOpen)
        self.parent.Bind(wx.EVT_MENU_CLOSE,     self.onMenuClose)
        self.parent.Bind(wx.EVT_MENU_HIGHLIGHT, self.onMenuHighlight)

    def onMenuOpen(self, event):
        pub.sendMessage('statusbar.pushhelp')

    def onMenuClose(self, event):
        pub.sendMessage('statusbar.popreset')

    def onMenuHighlight(self, event):
        try:
            mnu = event.GetMenu()
            id = event.GetMenuId()
            hlp = mnu.GetHelpString(id)
            pub.sendMessage('statusbar.sethelp', message=hlp)
        except:
            pass

    @staticmethod
    def delete(self):
        try:
            self.RS.set_active(False)
            self.RS.clear()
            del(self.RS)
            self.RS = None
            self.parent.Unbind(wx.EVT_CONTEXT_MENU)
        except RuntimeError:
            pass

    def __get_length(self, eclick, erelease):
        # eclick and erelease do not preserve order.
        # but it has no impact on length
        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        self.cb( math.hypot(x2-x1, y2-y1) )

    def activate(self, state = True):
        self.RS.set_active(state)

    def __copyToClipboard(self, txt):
        clipdata = wx.TextDataObject(txt)
        wx.TheClipboard.Open()
        wx.TheClipboard.SetData(clipdata)
        wx.TheClipboard.Close()

    def onPopupMenu(self, event):
        self.parent.PopupMenu(self.mnu)

    def onMenuBbxTpl(self, event):
        try:
            (x0, x1), (y0, y1) = self.RS.get_data()
            fmt = '({x0:f}, {y0:f}), ({x1:f}, {y1:f})'
            txt = fmt.format(x0=x0, y0=y0, x1=x1, y1=y1)
            self.__copyToClipboard('%s' % txt)
        except:
            pub.sendMessage('statusbar.flashmessage', message='No line')

    def onMenuBbxLst(self, event):
        try:
            (x0, x1), (y0, y1) = self.RS.get_data()
            fmt = '[ ({x0:f}, {y0:f}), ({x1:f}, {y1:f}) ]'
            txt = fmt.format(x0=x0, y0=y0, x1=x1, y1=y1)
            self.__copyToClipboard('%s' % txt)
        except:
            pub.sendMessage('statusbar.flashmessage', message='No line')

    def onMenuGenTpl(self, event):
        try:
            (x0, x1), (y0, y1) = self.RS.get_data()
            fmt = '((x, y) for x, y in np.linspace(({x1:f}, {y1:f}), ({x2:f}, {y2:f}), Number_Of_Points ))'
            txt = fmt.format(x0=x0, y0=y0, x1=x1, y1=y1)
            self.__copyToClipboard('%s' % txt)
        except:
            pub.sendMessage('statusbar.flashmessage', message='No line')

    def onMenuGenLst(self, event):
        try:
            (x0, x1), (y0, y1) = self.RS.get_data()
            fmt = '[(x, y) for x, y in np.linspace(({x0:f}, {y0:f}), ({x1:f}, {y1:f}), Number_Of_Points )]'
            txt = fmt.format(x0=x0, y0=y0, x1=x1, y1=y1)
            self.__copyToClipboard('%s' % txt)
        except:
            pub.sendMessage('statusbar.flashmessage', message='No line')

if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plt

    def cb(l):
        print(l)

    def main(parent=None):
        fig, current_ax = plt.subplots()                 # make a new plotingrange
        N = 100000                                       # If N is large one can see
        x = np.linspace(0.0, 10.0, N)                    # improvement by use blitting!

        plt.plot(x, +np.sin(.2*np.pi*x), lw=3.5, c='b', alpha=.7)  # plot something
        plt.plot(x, +np.cos(.2*np.pi*x), lw=3.5, c='r', alpha=.5)
        plt.plot(x, -np.sin(.2*np.pi*x), lw=3.5, c='g', alpha=.3)

        r = DAToolRuler(parent, current_ax, cb)
        r.activate(True)
        plt.show()

    app = wx.App(0)
    frame = wx.Frame(None, -1, "")
    app.SetTopWindow(frame)
    frame.Show()
    main(frame)
    app.MainLoop()
