#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath( os.path.join(selfDir, '..') )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

from pubsub import pub
import wx

class DAToolPoint:
    def __init__(self, window, axes, cb):
        self.parent = window
        self.cb = cb

        self.canvas = axes.figure.canvas
        self.cidBtn = self.canvas.mpl_connect('button_release_event', self.onSelect)
        self.cidPrs = self.canvas.mpl_connect('key_press_event',      self.onKeyPress)
        self.cidRls = self.canvas.mpl_connect('key_release_event',    self.onKeyRelease)
        lines = axes.plot([], [], linestyle='' , marker='1', color="red") # , markersize=3)
        assert len(lines) == 1
        self.points = lines[0]

        self.mnu = wx.Menu()
        self.mnu_tpl = wx.MenuItem(self.mnu, wx.ID_ANY, "Pick (x,y)",   "Copy x,y location to clipboard as tuple", wx.ITEM_NORMAL)
        self.mnu_lst = wx.MenuItem(self.mnu, wx.ID_ANY, "Pick [(x,y)]", "Copy x,y location to clipboard as list of tuple", wx.ITEM_NORMAL)
        self.mnu_wkt = wx.MenuItem(self.mnu, wx.ID_ANY, "Pick as WKT",  "Copy x, y location to clipboard as WKT", wx.ITEM_NORMAL)
        self.mnu.Append(self.mnu_tpl)
        self.mnu.Append(self.mnu_lst)
        #self.mnu.Append(self.mnu_wkt)

        self.parent.Bind(wx.EVT_CONTEXT_MENU, self.onPopupMenu)
        self.parent.Bind(wx.EVT_MENU, self.onMenuTpl, self.mnu_tpl)
        self.parent.Bind(wx.EVT_MENU, self.onMenuLst, self.mnu_lst)
        #self.parent.Bind(wx.EVT_MENU, self.onMenuWKT, self.mnu_wkt)

        self.xs = []
        self.ys = []
        self.keyCtrlOn = False

    @staticmethod
    def delete(self):
        try:
            self.points.remove()
            self.canvas.draw_idle()
            self.canvas.mpl_disconnect(self.cidBtn)
            self.canvas.mpl_disconnect(self.cidPrs)
            self.canvas.mpl_disconnect(self.cidRls)
            self.parent.Unbind(wx.EVT_CONTEXT_MENU)
        except RuntimeError:
            pass

    def __copyToClipboard(self, txt):
        clipdata = wx.TextDataObject(txt)
        wx.TheClipboard.Open()
        wx.TheClipboard.SetData(clipdata)
        wx.TheClipboard.Close()

    def onPopupMenu(self, event):
        self.parent.PopupMenu(self.mnu)

    def onMenuTpl(self, event):
        if len(self.xs) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of points')
            return
        fmt = '({x:f}, {y:f})'
        lst = [ fmt.format(x=x, y=y) for x, y in zip(self.xs, self.ys) ]
        txt = ', '.join(lst)
        self.__copyToClipboard('%s' % txt)

    def onMenuLst(self, event):
        if len(self.xs) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of points')
            return
        fmt = '({x:f}, {y:f})'
        lst = [ fmt.format(x=x, y=y) for x, y in zip(self.xs, self.ys) ]
        txt = ', '.join(lst)
        self.__copyToClipboard('[%s]' % txt)

    def onMenuWKT(self, event):
        if len(self.xs) == 0:
            pub.sendMessage('statusbar.flashmessage', message='Empty list of points')
            return
        fmt = '({x:s} {y:s})'
        if len(self.xs) == 1:
            txt = fmt.format(x=self.xs[0], y=self.ys[0])
            self.__copyToClipboard('POINT(%s)' % txt)
        else:
            lst = [ fmt.format(x=x, y=y) for x, y in zip(self.xs, self.ys) ]
            txt = ' '.join(lst)
            self.__copyToClipboard('MULTIPOINT(%s)' % txt)

    def onSelect(self, event):
        if event.button != 1: return    # Move only on left button
        x, y = event.xdata, event.ydata
        if self.keyCtrlOn:
            self.xs.append(x)
            self.ys.append(y)
        else:
            self.xs = [x]
            self.ys = [y]
        self.points.set_xdata(self.xs)
        self.points.set_ydata(self.ys)
        self.canvas.draw_idle()
        self.cb(x, y)

    def onKeyPress(self, event):
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if event.key == 'ctrl+control':
            self.keyCtrlOn = True

    def onKeyRelease(self, event):
        self.keyCtrlOn = False

    def activate(self, state = True):
        pass

if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plt

    def cb(x, y):
        print(x, y)

    def main(parent=None):
        fig, current_ax = plt.subplots()                 # make a new plotingrange
        N = 100000                                       # If N is large one can see
        x = np.linspace(0.0, 10.0, N)                    # improvement by use blitting!

        plt.plot(x, +np.sin(.2*np.pi*x), lw=3.5, c='b', alpha=.7)  # plot something
        plt.plot(x, +np.cos(.2*np.pi*x), lw=3.5, c='r', alpha=.5)
        plt.plot(x, -np.sin(.2*np.pi*x), lw=3.5, c='g', alpha=.3)

        r = DAToolPoint(parent, current_ax, cb)
        r.activate(True)
        plt.show()

    app = wx.App(0)
    frame = wx.Frame(None, -1, "")
    app.SetTopWindow(frame)
    frame.Show()
    main(frame)
    app.MainLoop()
