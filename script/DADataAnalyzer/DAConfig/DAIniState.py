# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import wx

def nospace(name):
    r = name
    r = r.strip()
    r = r.replace(' ', '_')
    r = r.replace('/', '|')
    return r

def join(*args):
    r = os.path.normpath( os.path.join(*args) )
    return r.replace('\\', '/')

class DAIniState:
    CONFIG_VNDR  = 'H2D2'
    CONFIG_APP   = 'Data Analyzer'
    CONFIG_CONF  = ': '.join( (CONFIG_VNDR, CONFIG_APP, 'Initial state') )

    def __init__(self):
        self.config = wx.Config(appName      = DAIniState.CONFIG_APP,
                                vendorName   = DAIniState.CONFIG_VNDR,
                                localFilename= DAIniState.CONFIG_CONF,
                                style=wx.CONFIG_USE_LOCAL_FILE)

    def __readSubGroups(self, gpath):
        oPath = self.config.GetPath()
        self.config.SetPath(gpath)

        groups = []
        more, value, index = self.config.GetFirstGroup()
        while more:
            groups.append(value)
            more, value, index = self.config.GetNextGroup(index)

        self.config.SetPath(oPath)
        return groups

    def addPath(self, name, path):
        oPath = self.config.GetPath()
        nPath = join('/', 'paths', nospace(name))
        self.config.SetPath(nPath)
        self.config.Write(join(nPath, 'path'), path)
        self.config.Flush()
        self.config.SetPath(oPath)

    def setPath(self, name, path):
        oPath = self.config.GetPath()
        nPath = join('/', 'paths', nospace(name))
        self.config.DeleteEntry(nPath)
        self.config.Flush()
        self.config.SetPath(oPath)
        return self.addPath(name, path)

    def getPath(self, name):
        gPath = join('/', 'paths')
        names = self.__readSubGroups(gPath)
        if name in names:
            return self.config.Read(join(gPath, name, 'path'))
        return ''


if __name__ == "__main__":
    def main():
        cfg = DAIniState()
        cfg.config.DeleteAll()
        cfg.addPath('gridDir', 'abcde')
        cfg.addPath('dataDir', 'zxcvb')

        print (cfg.getPath('gridDir'))
        cfg.setPath('gridDir', 'abcde2')
        print (cfg.getPath('gridDir'))

    main()
