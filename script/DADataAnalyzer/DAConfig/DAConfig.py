# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import json
import os
import wx

DCL_BASE = [
    ("H2D2 St-Venant: solution ", ['qx', 'qy', 'h']),
    ("H2D2 St-Venant: residuum",  ['qx', 'qy', 'h']),
    ("H2D2 St-Venant: CFL-Pe-Fr", ['CFL','Pe', 'Fr']),
    ("H2D2 St-Venant: simulation post-processing", ['u', 'v', 'h', '|u|', 'H', '|q|', 'Fr', 'u*', 'nu_p', 'nu_n']),
    ("H2D2 St-Venant: post-processing for advection-diffusion", ['u', 'v', 'h', 'Dv', 'Dh']),
    ("H2D2 St-Venant: components", ['cnv_x', 'cnv_y', 'grv_x', 'grv_y', 'man_x', 'man_y', 'nu_p_x', 'nu_p_y', 'nu_n_x', 'nu_n_y', 'wnd_x', 'wnd_y', 'cor_x', 'cor_y']),
    ("H2D2 Advection-Diffusion: solution", ['c']),
    ("H2D2 Advection-Diffusion: residuum", ['c']),
    ("H2D2 Advection-Diffusion: diffusivities", ['akl', 'akt', 'dxx', 'dxy', 'dyy']),
    ("H2D2 Advection-Diffusion: clf: simulation post-processing", ['clf', 'dgd']),
    ("H2D2 Advection-Diffusion: tmp: simulation post-processing", ['T', 'Hs', 'Ha', 'Hbr', 'He', 'Hc', 'Hi', 'Hbd', 'Hp', 'sumH']),
    ]

EPOCH_BASE = [
    ("Unix", "1970-01-01T00:00:00Z"),
]

def nospace(name):
    r = name
    r = r.strip()
    r = r.replace(' ', '_')
    r = r.replace('/', '|')
    return r

def join(*args):
    r = os.path.normpath( os.path.join(*args) )
    return r.replace('\\', '/')

class DAConfig:
    CONFIG_VNDR  = 'H2D2'
    CONFIG_APP   = 'Data Analyzer'
    CONFIG_CONF  = ': '.join( (CONFIG_VNDR, CONFIG_APP, 'Global configuration') )

    def __init__(self):
        self.config = wx.Config(appName      = DAConfig.CONFIG_APP,
                                vendorName   = DAConfig.CONFIG_VNDR,
                                localFilename= DAConfig.CONFIG_CONF,
                                style=wx.CONFIG_USE_LOCAL_FILE)
        self.__set_default()

    def __set_default(self):
        items = self.readAllDataColumnLabels()
        if not items:
            self.asgDataColumnLabels(DCL_BASE)
        items = self.readAllEpoch()
        if not items:
            self.asgEpoch(EPOCH_BASE)

    def __readSubGroups(self, gpath):
        oPath = self.config.GetPath()
        self.config.SetPath(gpath)

        groups = []
        more, value, index = self.config.GetFirstGroup()
        while more:
            groups.append(value)
            more, value, index = self.config.GetNextGroup(index)

        self.config.SetPath(oPath)
        return groups

    def addSRS(self, name, descr, urn, wkt):
        oPath = self.config.GetPath()
        path = join('/', 'predefined_srs', nospace(name))
        self.config.SetPath(path)
        self.config.Write(join(path, 'name'), name)
        self.config.Write(join(path, 'description'), descr)
        self.config.Write(join(path, 'urn'), urn)
        self.config.Write(join(path, 'wkt'), wkt)
        self.config.Flush()
        self.config.SetPath(oPath)

    def asgSRS(self, items):
        oPath = self.config.GetPath()
        path = join('/', 'predefined_srs')
        self.config.DeleteGroup(path)
        self.config.Flush()
        for item in items:
            self.addSRS(*item)
        self.config.SetPath(oPath)

    def readOneSRS(self, name):
        gPath  = join('/', 'predefined_srs')
        groups = self.__readSubGroups(gPath)
        for sgrp in groups:
            n = self.config.Read   (join(gPath, sgrp, 'name'))
            if n == name:
                d = self.config.Read(join(gPath, sgrp, 'description'))
                u = self.config.Read(join(gPath, sgrp, 'urn'))
                w = self.config.Read(join(gPath, sgrp, 'cwkt'))
                return [n, d, u, w]
        raise KeyError('SRS name not found in config: %s' % name)

    def readAllSRS(self):
        items = []
        gPath  = join('/', 'predefined_srs')
        groups = self.__readSubGroups(gPath)
        for sgrp in groups:
            n = self.config.Read(join(gPath, sgrp, 'name'))
            d = self.config.Read(join(gPath, sgrp, 'description'))
            u = self.config.Read(join(gPath, sgrp, 'urn'))
            w = self.config.Read(join(gPath, sgrp, 'wkt'))
            items.append( [n, d, u, w] )
        return items

    def addGridFilter(self, fname, fpath, fclass):
        oPath = self.config.GetPath()
        path = join('/', 'grid_filter', nospace(fname))
        self.config.SetPath(path)
        self.config.Write(join(path, 'name'), fname)
        self.config.Write(join(path, 'path'), fpath)
        self.config.Write(join(path, 'class'), fclass)
        self.config.Flush()
        self.config.SetPath(oPath)

    def asgGridFilter(self, items):
        oPath = self.config.GetPath()
        path = join('/', 'grid_filter')
        self.config.DeleteGroup(path)
        self.config.Flush()
        for item in items:
            self.addGridFilter(*item)
        self.config.SetPath(oPath)

    def readOneGridFilter(self, name):
        gPath  = join('/', 'grid_filter')
        groups = self.__readSubGroups(gPath)
        for sgrp in groups:
            n = self.config.Read(join(gPath, sgrp, 'name'))
            if n == name:
                p = self.config.Read(join(gPath, sgrp, 'path'))
                c = self.config.Read(join(gPath, sgrp, 'class'))
                return [n, p, c]
        raise KeyError('Grid filter name not found in config: %s' % name)

    def readAllGridFilter(self):
        items = []
        gPath  = join('/', 'grid_filter')
        groups = self.__readSubGroups(gPath)
        for sgrp in groups:
            n = self.config.Read(join(gPath, sgrp, 'name'))
            p = self.config.Read(join(gPath, sgrp, 'path'))
            c = self.config.Read(join(gPath, sgrp, 'class'))
            items.append( [n, p, c] )
        return items

    def addDataColumnLabels(self, name, labels):
        oPath = self.config.GetPath()
        path = join('/', 'data_column_labels', nospace(name))
        jl = json.dumps( list(labels) )
        self.config.SetPath(path)
        self.config.Write(join(path, 'name'), name)
        self.config.Write(join(path, 'labels'), jl)
        self.config.Flush()
        self.config.SetPath(oPath)

    def asgDataColumnLabels(self, items):
        oPath = self.config.GetPath()
        path = join('/', 'data_column_labels')
        self.config.DeleteGroup(path)
        self.config.Flush()
        for item in items:
            self.addDataColumnLabels(*item)
        self.config.SetPath(oPath)

    def readOneDataColumnLabels(self, name):
        gPath  = join('/', 'data_column_labels')
        groups = self.__readSubGroups(gPath)
        for sgrp in groups:
            n = self.config.Read(join(gPath, sgrp, 'name'))
            if n == name:
                jl = self.config.Read(join(gPath, sgrp, 'labels'))
                l = json.load(jl)
                return [n, l]
        raise KeyError('Data column label name not found in config: %s' % name)

    def readAllDataColumnLabels(self):
        items = []
        gPath  = join('/', 'data_column_labels')
        groups = self.__readSubGroups(gPath)
        for sgrp in groups:
            n = self.config.Read(join(gPath, sgrp, 'name'))
            jl = self.config.Read(join(gPath, sgrp, 'labels'))
            l = json.loads(jl)
            items.append( [n, l] )
        return items

    def addEpoch(self, name, epoch):
        print('addEpoch: %s' % epoch)
        oPath = self.config.GetPath()
        path = join('/', 'epoch', nospace(name))
        self.config.SetPath(path)
        self.config.Write(join(path, 'name'), name)
        self.config.Write(join(path, 'epoch'), epoch)
        self.config.Flush()
        self.config.SetPath(oPath)

    def asgEpoch(self, items):
        oPath = self.config.GetPath()
        path = join('/', 'epoch')
        self.config.DeleteGroup(path)
        self.config.Flush()
        for item in items:
            self.addEpoch(*item)
        self.config.SetPath(oPath)

    def readOneEpoch(self, name):
        gPath  = join('/', 'epoch')
        groups = self.__readSubGroups(gPath)
        for sgrp in groups:
            n = self.config.Read(join(gPath, sgrp, 'name'))
            if n == name:
                e = self.config.Read(join(gPath, sgrp, 'epoch'))
                return [n, e]
        raise KeyError('Epoch name not found in config: %s' % name)

    def readAllEpoch(self):
        items = []
        gPath  = join('/', 'epoch')
        groups = self.__readSubGroups(gPath)
        for sgrp in groups:
            n = self.config.Read(join(gPath, sgrp, 'name'))
            e = self.config.Read(join(gPath, sgrp, 'epoch'))
            items.append( [n, e] )
        return items

if __name__ == "__main__":
    def main():
        cfg = DAConfig()
        cfg.config.DeleteAll()
        cfg.addSRS('WGS 84', 'WGS84 spatial reference system (EPSG:4326)', 'EPSG:4326', '')
        cfg.addSRS('NAD83(CSRS) / UTM zone 18N', 'NAD83(CSRS) / UTM zone 18N (EPSG:2959)', 'EPSG:2959', '')

        nam = "Spot elements with 2 edges on boundary"
        pth = r'E:\bld-1804\H2D2-tools\script\DADataAnalyzer\FilterElementsWithTwoSidesOnBoundary.py'
        cls = 'FilterElementsWithTwoSidesOnBoundary'
        cfg.addGridFilter(nam, pth, cls)
        
        nam = "Wind"
        lbl = ['wx', 'wy']
        cfg.addDataColumnLabels(nam, lbl)
        
        srs = cfg.readAllSRS()
        print (srs)
        flt = cfg.readAllGridFilter()
        print (flt)
        lbl = cfg.readAllDataColumnLabels()
        print (lbl)

    main()
