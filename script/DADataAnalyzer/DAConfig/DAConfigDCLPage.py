# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx
import wx.lib.mixins.listctrl as listmix

from DADlgDataUnits import DADlgDataColumns

# DCL stands for Data Column Label

class DAConfigDCLListView(wx.ListView, listmix.ListCtrlAutoWidthMixin):
    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListView.__init__(self, parent, ID, pos, size, style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)
        
class DAConfigDCLPage(wx.Panel):
    def __init__(self, *args, **kwds):
        wx.Panel.__init__(self, *args, **kwds)

        lstSyle = wx.LC_HRULES | wx.LC_NO_HEADER | wx.LC_REPORT | wx.LC_VRULES 
        self.lstDCL = DAConfigDCLListView(self, wx.ID_ANY, style=lstSyle)
        self.btnEdit = wx.Button  (self, wx.ID_EDIT,    "")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON,            self.onbtnEdit,  self.btnEdit)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED,self.onLstSelect,self.lstDCL)
        
        self.data = []
        
    def __set_properties(self):
        self.lstDCL.InsertColumn(0, 'Name')
        self.lstDCL.InsertColumn(1, 'Labels')
        self.lstDCL.setResizeColumn(0)

    def __do_layout(self):
        szrMain = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Data column label"),  wx.VERTICAL)
        szrBtm  = wx.BoxSizer(wx.HORIZONTAL)
        szrBtm.Add((0, 0), 1, wx.EXPAND, 5 )
        szrBtm.Add(self.btnEdit, 0, 0, 0)
        szrMain.Add(self.lstDCL, 1, wx.EXPAND, 0)
        szrMain.Add(szrBtm,      0, wx.EXPAND, 0)
        self.SetSizer(szrMain)
        self.Layout()

    def setData(self, data):
        self.data = data
        self.lstDCL.DeleteAllItems()
        for item in self.data:
            self.lstDCL.Append( (item[0],item[1]) )

    def getData(self):
        return self.data
    
    def onLstSelect(self, event):
        idx = self.lstDCL.GetFirstSelected()
        if idx == wx.NOT_FOUND: return

        n, l = self.data[idx]
        s = 'Name: %s\nLabels: %s' % (n, l)
        event.GetEventObject().SetToolTip(s)

    def onbtnEdit(self, event):
        errMsg = None
        dlg = DADlgDataColumns(self)
        dlg.setData(self.data)
        if dlg.ShowModal() == wx.ID_OK:
            dcls = dlg.getData()
            self.setData(dcls)
        dlg.Destroy()

        if errMsg:
            dlg = wx.MessageDialog(self, errMsg, 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
        
