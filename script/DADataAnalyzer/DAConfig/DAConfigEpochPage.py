# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from DADlgDataUnits import DADlgDataEpochPanel

class DAConfigEpochPage(DADlgDataEpochPanel):
    def __init__(self, *args, **kwds):
        kwds['editable'] = True
        super(DAConfigEpochPage, self).__init__(*args, **kwds)
