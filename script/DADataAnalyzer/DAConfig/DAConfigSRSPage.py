# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx
import wx.lib.mixins.listctrl as listmix

from DADlgCoordinateSystem import DADlgCoordinateSystem

class DAConfigSRSListView(wx.ListView, listmix.ListCtrlAutoWidthMixin):
    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListView.__init__(self, parent, ID, pos, size, style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)
        
class DAConfigSRSPage(wx.Panel):
    def __init__(self, *args, **kwds):
        wx.Panel.__init__(self, *args, **kwds)

        lstSyle = wx.LC_HRULES | wx.LC_NO_HEADER | wx.LC_REPORT | wx.LC_VRULES 
        self.lstSRS = DAConfigSRSListView(self, wx.ID_ANY, style=lstSyle)
        self.btnAdd = wx.Button  (self, wx.ID_ADD,    "")
        self.btnDel = wx.Button  (self, wx.ID_DELETE, "")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON,            self.onBtnAdd,   self.btnAdd)
        self.Bind(wx.EVT_BUTTON,            self.onBtnDelete,self.btnDel)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED,self.onLstSelect,self.lstSRS)
        
        self.data = []
        
    def __set_properties(self):
        self.lstSRS.InsertColumn(0, 'SRS')
        self.lstSRS.InsertColumn(1, 'EPSG')
        self.lstSRS.setResizeColumn(0)

    def __do_layout(self):
        szrMain = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Spatial Coordinate System"),  wx.VERTICAL)
        szrBtm  = wx.BoxSizer(wx.HORIZONTAL)
        szrBtm.Add((0, 0), 1, wx.EXPAND, 5 )
        szrBtm.Add(self.btnAdd, 0, 0, 0)
        szrBtm.Add(self.btnDel, 0, 0, 0)
        szrMain.Add(self.lstSRS, 1, wx.EXPAND, 0)
        szrMain.Add(szrBtm,      0, wx.EXPAND, 0)
        self.SetSizer(szrMain)
        self.Layout()

    def setData(self, data):
        self.data = data
        self.lstSRS.DeleteAllItems()
        for item in self.data:
            self.lstSRS.Append( (item[0],item[2]) )

    def getData(self):
        return self.data
    
    def onLstSelect(self, event):
        idx = self.lstSRS.GetFirstSelected()
        if idx == wx.NOT_FOUND: return

        s = 'Name: %s\nDescription: %s\n%s\nWKT: %s' % (self.data[idx])
        event.GetEventObject().SetToolTip(s)

    def onBtnAdd(self, event):
        errMsg = None
        dlg = DADlgCoordinateSystem(self)
        if dlg.ShowModal() == wx.ID_OK:
            srs = dlg.get_srs()
            name = srs.GetName()
            if self.lstSRS.FindItem(-1, name) != wx.NOT_FOUND:
                errMsg = 'Name "%s" is not unique' % name
            if not errMsg:
                try:
                    urn = '%s:%s' % (srs.GetAuthorityName(None), srs.GetAuthorityCode(None))
                    wkt = ''
                    desc = '%s (%s)' %(name, urn)
                except:
                    urn  = ""
                    wkt  = srs.ExportToWkt()
                    desc = '%s (WKT:%s...)' %(name, wkt[:20])
                self.data.append( (name, desc, urn, wkt) )
                self.lstSRS.Append( (name, urn) )
        dlg.Destroy()

        if errMsg:
            dlg = wx.MessageDialog(self, errMsg, 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
        
    def onBtnDelete(self, event):
        errMsg = ''
        idx = self.lstSRS.GetFirstSelected()
        if idx == wx.NOT_FOUND:
            errMsg = 'Nothing selected'
        if not errMsg:   
            nam = self.lstSRS.GetItemText(idx)
            dlg = wx.MessageDialog(self, 'Are you sure? \n', 'Delete %s' % nam, wx.YES_NO)
            if (dlg.ShowModal() == wx.ID_YES):
                self.lstSRS.DeleteItem(idx)
                del self.data[idx]
            else:
                event.Skip()

        if errMsg:
            dlg = wx.MessageDialog(self, errMsg, 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
