#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2021
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging

from IPImageProcessor.LandXML.LandXMLWriter import LandXMLWriter

from .DAExportBase   import DAExportBase
from CTCommon.DTData import DTDataOp, DTDataOpCol

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Export.LandXML")

class DAExportLXML(DAExportBase):
    def __init__(self):
        self.writer = None
        self.errMsgs = []

    def writeLineInfo (self, item, *args, **kwargs):
        assert NotImplementedError
    def writePointInfo(self, item, *args, **kwargs):
        assert NotImplementedError
    def writePolyInfo (self, item, *args, **kwargs):
        assert NotImplementedError
    def writeEllipsesAsPolyCollection(self, item, *args, **kwargs):
        assert NotImplementedError
    def writeGeometryAsLineCollection(self, item, *args, **kwargs):
        assert NotImplementedError
    def writeIsoLineAsLineCollection (self, item, *args, **kwargs):
        assert NotImplementedError
    def writeIsoValueAsPolyCollection(self, item, *args, **kwargs):
        assert NotImplementedError
    def writeQuiverAsPolyCollection  (self, item, *args, **kwargs):
        assert NotImplementedError
    def writeNodesAsPointCollection  (self, item, *args, **kwargs):
        assert NotImplementedError
    def writePathAsLineCollection    (self, item, *args, **kwargs):
        assert NotImplementedError
    def writeTextAsPointCollection   (self, item, *args, **kwargs):
        assert NotImplementedError

    def export(self, fname, meta, field, data, cols, *args, **kwargs):
        title = kwargs.pop('title', '')
        self.errMsgs = []

        # ---  Open the core files
        if not self.errMsgs:
            self.writer = LandXMLWriter(meta, fname)

        # ---  Write the headers
        if not self.errMsgs:
            self.writer.writeHeader()
        
        # ---  Write the layers
        if data.ndim == 1:
            col = cols[0]
            name = ''
            if isinstance(col, DTDataOpCol):
                name = field.names[col.col]
            elif isinstance(col, DTDataOp):
                name = col.src
            self.writer.writeOneTIN(field.getGrid(), data, name=name)
        else:
            assert len(cols) == data.shape[-1]
            for icol, col in enumerate(cols):
                name = ''
                if isinstance(col, DTDataOpCol):
                    name = field.names[col.col]
                elif isinstance(col, DTDataOp):
                    name = col.src
                self.writer.writeOneTIN(field.getGrid(), data[:,icol], name=name)

        # ---  Write the footers
        if not self.errMsgs:
            self.writer.writeFooter()

        # ---  Close the core files
        if self.writer: del self.writer
        self.writer = None
        
        # ---  Write the srs
        # if not self.errMsgs:
        #     with open(fname+'.prj', 'wt') as ofs:
        #         ofs.write(meta['srs_wkt'])

        # ---  Error message
        errMsg = '\n'.join(self.errMsgs)
        return errMsg

if __name__ == '__main__':
    pass
