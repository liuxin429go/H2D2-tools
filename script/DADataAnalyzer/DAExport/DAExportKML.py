#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging

from IPImageProcessor.KML.KMLWriter import KMLWriter

from .DAExportBase import DAExportBase
from DALayers      import DALayer  # for assertions

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Export.KML")
#LOGGER = mp.get_logger()

class DAExportKML(DAExportBase):
    def __init__(self):
        self.writer = None

    def writeLineInfo (self, item, *args, **kwargs):
        self.writer.writeLineColors(item.colors)
    def writePointInfo(self, item, *args, **kwargs):
        self.writer.writePointColors(item.colors)
    def writePolyInfo (self, item, *args, **kwargs):
        self.writer.writePolyColors(item.colors)

    def writeEllipsesAsPolyCollection(self, item, *args, **kwargs):
        self.writer.writeEllipsesAsPolyCollection(item.poly, item.levels, item.colors, *args, **kwargs)
    def writeGeometryAsLineCollection(self, item, *args, **kwargs):
        self.writer.writeGeometryAsLineCollection(item.poly, item.levels, item.colors, *args, **kwargs)
    def writeIsoLineAsLineCollection (self, item, *args, **kwargs):
        self.writer.writeIsoLineAsLineCollection(item.poly, item.levels, item.colors, *args, **kwargs)
    def writeIsoValueAsPolyCollection(self, item, *args, **kwargs):
        self.writer.writeIsoValueAsLineCollection(item.poly, item.levels, item.colors, *args, **kwargs)
    def writeQuiverAsPolyCollection  (self, item, *args, **kwargs):
        self.writer.writeQuiverAsPolyCollection(item.poly, item.levels, item.colors, *args, **kwargs)
    def writeNodesAsPointCollection  (self, item, *args, **kwargs):
        self.writer.writeNodesAsPointCollection(item.poly, item.levels, item.colors, *args, **kwargs)
    def writePathAsLineCollection    (self, item, *args, **kwargs):
        self.writer.writePathAsLineCollection(item.poly, item.levels, item.colors, *args, **kwargs)
    def writeTextAsPointCollection   (self, item, *args, **kwargs):
        self.writer.writeTextAsPointCollection(item.poly, item.levels, item.colors, *args, **kwargs)
        
    def exportOneLayer(self, layer, *args, **kwargs):
        assert isinstance(layer, DALayer.DALayer)
        if not layer.visible: return
        
        items = []
        for item in layer:
            assert isinstance(item, DALayer.DALayerItemCS)
            if not item.visible: continue
            xprtItem = self.exportOneItem(layer, item)
            if xprtItem: items.append(xprtItem)

        for item in items:
            item.styler(item, *args, **kwargs)

        self.writer.openElem('Folder')
        self.writer.writeTag('name', layer.title)
        self.writer.writeTag('visibility', 1)
        self.writer.writeTag('open', 0)

        for item in items:
            kwargs['title'] = item.title
            item.writer(item, *args, **kwargs)

        self.writer.closeElem()  # Folder
        
    def export(self, fname, meta, layers, *args, **kwargs):
        title = kwargs.pop('title', '')

        self.writer = KMLWriter(meta, fname)
        self.writer.writeHeader()
        self.writer.writeDocumentSchema()
        self.writer.writeDocumentStyle()

        for layer in layers:
            self.exportOneLayer(layer, *args, **kwargs)
            
        self.writer.writeFooter()
        self.writer.close()
        return None

if __name__ == '__main__':
    pass
