#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging

from IPImageProcessor.SHP.SHPWriter import SHPWriter
from IPImageProcessor.SHP.SHPWriter import SLDWriter
from IPImageProcessor.SHP.SHPWriter import isoType
from IPImageProcessor.IPPlotter     import IPPlotType

from .DAExportBase import DAExportBase
from DALayers      import DALayer  # for assertions

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Export.SHP")
#LOGGER = mp.get_logger()

class DAExportSHP(DAExportBase):
    def __init__(self):
        self.wSLD = None
        self.wSHP = None
        self.errMsgs = []

    def writeLineInfo (self, item, *args, **kwargs):
        self.wSLD.writeLineColors(item.colors) #, item.levels)
    def writePointInfo(self, item, *args, **kwargs):
        assert NotImplementedError
    def writePolyInfo (self, item, *args, **kwargs):
        self.wSLD.writePolyColors(item.colors) #, item.levels)

    def writeEllipsesAsPolyCollection(self, item, *args, **kwargs):
        self.errMsgs.append('Export not yet implemented for %s' % str(item))
        
    def writeGeometryAsLineCollection(self, item, *args, **kwargs):
        self.wSHP.writeGeometryAsLineCollection(item.poly, item.levels, item.colors, *args, **kwargs)
    def writeIsoLineAsLineCollection (self, item, *args, **kwargs):
        self.wSHP.writeIsoLineAsLineCollection(item.poly, item.levels, item.colors, *args, **kwargs)
    def writeIsoValueAsPolyCollection(self, item, *args, **kwargs):
        self.wSHP.writeIsoValueAsPolyCollection(item.poly, item.levels, item.colors, *args, **kwargs)
    def writeQuiverAsPolyCollection  (self, item, *args, **kwargs):
        self.wSHP.writeQuiverAsPolyCollection(item.poly, item.levels, item.colors, *args, **kwargs)

    def writeNodesAsPointCollection  (self, item, *args, **kwargs):
        self.errMsgs.append('Export not yet implemented for %s' % str(item))

    def writePathAsLineCollection    (self, item, *args, **kwargs):
        self.wSHP.writePathAsLineCollection(item.poly, item.levels, item.colors, *args, **kwargs)

    def writeTextAsPointCollection   (self, item, *args, **kwargs):
        self.errMsgs.append('Export not yet implemented for %s' % str(item))

    def exportOneLayer(self, layer, *args, **kwargs):
        assert isinstance(layer, DALayer.DALayer)
        if not layer.visible: return
        
        items = []
        for item in layer:
            assert isinstance(item, DALayer.DALayerItemCS)
            if not item.visible: continue
            xprtItem = self.exportOneItem(layer, item)
            if xprtItem: items.append(xprtItem)

        self.wSLD.openLayer(layer.title)
        for item in items:
            item.writer(item, *args, **kwargs)
            item.styler(item, *args, **kwargs)
        self.wSLD.closeLayer()
    
    def export(self, fname, meta, layers, *args, **kwargs):
        title = kwargs.pop('title', '')
        self.errMsgs = []

        # ---  Controls
        active_layers = [ l for l in layers if l.visible ]
        if len(active_layers) <= 0:
            self.errMsgs.append("No visible layers")
        elif len(active_layers) > 1:
            ltype = type(active_layers[0])
            if not all( [ isinstance(l, ltype) for l in active_layers[1:] ] ):
                self.errMsgs.append("Shapefile are limited to layers of same kind")

        # ---  Open the core files
        if not self.errMsgs:
            self.wSHP = SHPWriter(meta, fname)
            self.wSLD = SLDWriter(meta, fname)

        # ---  Write the headers
        if not self.errMsgs:
            self.wSHP.writeHeader()
            self.wSLD.writeHeader()
        
        # ---  Write the layers
        for layer in layers:
            if not self.errMsgs:
                self.exportOneLayer(layer, *args, **kwargs)

        # ---  Write the footers
        if not self.errMsgs:
            self.wSHP.writeFooter()
            self.wSLD.writeFooter()

        # ---  Close the core files
        if self.wSHP: del self.wSHP
        if self.wSLD: del self.wSLD
        self.wSHP = None
        self.wSLD = None
        
        # ---  Write the srs
        if not self.errMsgs:
            with open(fname+'.prj', 'wt') as ofs:
                ofs.write(meta['srs_wkt'])

        # ---  Error message
        errMsg = '\n'.join(self.errMsgs)
        return errMsg

if __name__ == '__main__':
    pass
