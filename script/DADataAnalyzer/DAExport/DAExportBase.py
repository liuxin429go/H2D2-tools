#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging

from IPImageProcessor.IPPlotter   import IPPlotType
from IPImageProcessor.MPL.MPLUtil import MPLUtil

from DALayers import DALayer

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Export")
#LOGGER = mp.get_logger()

class DAExportItem:
    def __init__(self, kind, title, poly, levels, colors, styleFnc, writeFnc):
        self.kind   = kind
        self.title  = title
        self.poly   = poly
        self.levels = levels
        self.colors = colors
        self.styler = styleFnc
        self.writer = writeFnc

    def __str__(self):
        return 'Export item: Title: %s; Kind: %s' % (self.title, str(self.kind))
        
class DAExportBase:
    def __init__(self):
        pass

    def writeLineInfo (self, *args, **kwargs):
        assert NotImplementedError
    def writePointInfo(self, *args, **kwargs):
        assert NotImplementedError
    def writePolyInfo (self, *args, **kwargs):
        assert NotImplementedError

    def writeEllipsesAsPolyCollection(self, *args, **kwargs):
        assert NotImplementedError
    def writeGeometryAsLineCollection(self, *args, **kwargs):
        assert NotImplementedError
    def writeIsoLineAsLineCollection (self, *args, **kwargs):
        assert NotImplementedError
    def writeIsoValueAsPolyCollection(self, *args, **kwargs):
        assert NotImplementedError
    def writeQuiverAsPolyCollection  (self, *args, **kwargs):
        assert NotImplementedError
    def writeNodesAsPointCollection  (self, *args, **kwargs):
        assert NotImplementedError
    def writePathAsLineCollection    (self, *args, **kwargs):
        assert NotImplementedError
    def writeTextAsPointCollection   (self, *args, **kwargs):
        assert NotImplementedError

    def exportOneItem(self, layer, item):
        assert isinstance(item, DALayer.DALayerItemCS)

        xprt = None
        kind = item.kind
        CS   = item.cs    # contourSet
        if   kind is IPPlotType.CONTOUR_FILL:
            title= item.title if item.title else layer.title if layer.title else 'Isovalue'
            poly = MPLUtil.getPolygonsFromContourSet(CS)
            lvls = MPLUtil.getLevelsFromContourSet(CS)
            clrs = MPLUtil.getColorsFromContourSet(CS)
            xprt = DAExportItem(kind, title, poly, lvls, clrs, self.writePolyInfo, self.writeIsoValueAsPolyCollection)
        elif kind is IPPlotType.CONTOUR_LINE:
            title= item.title if item.title else layer.title if layer.title else 'Isoline'
            poly = MPLUtil.getPolylinesFromContourSet(CS)
            lvls = MPLUtil.getLevelsFromContourSet(CS)
            clrs = MPLUtil.getColorsFromContourSet(CS)
            xprt = DAExportItem(kind, title, poly, lvls, clrs, self.writeLineInfo, self.writeIsoLineAsLineCollection)
        elif kind is IPPlotType.QUIVER:
            title= item.title if item.title else layer.title if layer.title else 'Quiver'
            poly = MPLUtil.getQuiverFromContourSet(CS)
            lvls = None
            clrs = MPLUtil.getColorsFromContourSet(CS)[0]
            xprt = DAExportItem(kind, title, poly, lvls, clrs, self.writePolyInfo, self.writeQuiverAsPolyCollection)
        elif kind is IPPlotType.MESH:
            title= item.title if item.title else layer.title if layer.title else 'Geometry'
            poly = MPLUtil.getLines2DFromContourSet(CS)
            lvls = None
            clrs = MPLUtil.getColorsFromContourSet(CS)
            xprt = DAExportItem(kind, title, poly, lvls, clrs, self.writeLineInfo, self.writeGeometryAsLineCollection)
        elif kind is IPPlotType.SUB_MESH:
            title= item.title if item.title else layer.title if layer.title else 'Geometry'
            poly = MPLUtil.getLines2DFromContourSet(CS)
            lvls = None
            clrs = MPLUtil.getColorsFromContourSet(CS)
            xprt = DAExportItem(kind, title, poly, lvls, clrs, self.writeLineInfo, self.writeGeometryAsLineCollection)
        elif kind is IPPlotType.NODES:
            title= item.title if item.title else layer.title if layer.title else 'Nodes'
            poly = MPLUtil.getLines2DFromContourSet(CS)
            lvls = None
            clrs = MPLUtil.getColorsFromContourSet(CS)
            xprt = DAExportItem(kind, title, poly, lvls, clrs, self.writePointInfo, self.writeNodesAsPointCollection)
        elif kind is IPPlotType.PARTICLE_PATH:
            title= item.title if item.title else layer.title if layer.title else 'Particle Path'
            poly = MPLUtil.getLinesFromContourSet([CS])
            lvls = None
            clrs = MPLUtil.getColorsFromContourSet([CS])
            xprt = DAExportItem(kind, title, poly, lvls, clrs, self.writeLineInfo, self.writePathAsLineCollection)
        elif kind is IPPlotType.ELLIPSE:
            title= item.title if item.title else layer.title if layer.title else 'Ellipse'
            poly = MPLUtil.getEllipsesFromContourSet(CS)
            lvls = None
            clrs = MPLUtil.getColorsFromContourSet(CS)
            if len(clrs) == 1: clrs = clrs[0]*len(poly)
            xprt = DAExportItem(kind, title, poly, lvls, clrs, self.writePolyInfo, self.writeEllipsesAsPolyCollection)
        elif kind is IPPlotType.GRAPHE:
            pass
        elif kind is IPPlotType.TEXT:
            title= item.title if item.title else layer.title if layer.title else 'Annotation'
            poly = MPLUtil.getAnnotationsFromContourSet(CS)
            lvls = None
            #clrs = MPLUtil.getColorsFromContourSet(CS)
            #if len(clrs) == 1: clrs = clrs[0]*len(poly)
            clrs = ['ffffffff'] * len(poly)
            xprt = DAExportItem(kind, title, poly, lvls, clrs, self.writePointInfo, self.writeTextAsPointCollection)
        elif kind is IPPlotType.IMAGE:
            pass

        return xprt

    def export(self, fname, meta, layers, *args, **kwargs):
        assert NotImplementedError

if __name__ == '__main__':
    pass
