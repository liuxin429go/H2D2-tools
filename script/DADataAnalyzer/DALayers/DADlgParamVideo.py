#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013-2017
# --- Institut National de la Recherche Scientifique (INRS)
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx
import collections.abc

class DADlgParamVideo(wx.Dialog):
    def __init__(self, *args, **kwds):
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        super(DADlgParamVideo, self).__init__(*args, **kwds)

        self.szr_top_staticbox  = wx.StaticBox(self, -1, "Name")
        self.szr_val_staticbox  = wx.StaticBox(self, -1, "Values")
        self.szr_opts_staticbox = wx.StaticBox(self, -1, "Options")

        bmpUndo = wx.ArtProvider.GetBitmap(wx.ART_UNDO, wx.ART_OTHER) #, (16, 16))

        self.stx_title    = wx.StaticText  (self, -1, "Layer name")
        self.txt_title    = wx.TextCtrl    (self, -1, "")
        self.stx_valmin   = wx.StaticText  (self, -1, "min ")
        self.txt_valmin   = wx.TextCtrl    (self, -1, "")
        self.btn_valmin   = wx.BitmapButton(self, -1, bmpUndo, size=(16,16))
        self.stx_valmax   = wx.StaticText  (self, -1, "max ")
        self.txt_valmax   = wx.TextCtrl    (self, -1, "")
        self.btn_valmax   = wx.BitmapButton(self, -1, bmpUndo, size=(16,16))
        self.stx_valnbr   = wx.StaticText  (self, -1, "Number of values ")
        self.spn_valnbr   = wx.SpinCtrl    (self, -1, "21", min=1, max=100, style=wx.TE_PROCESS_TAB)
        self.btn_valnbr   = wx.BitmapButton(self, -1, bmpUndo, size=(16,16))
        self.stx_tstep    = wx.StaticText  (self, -1, "Time step ")
        self.txt_tstep    = wx.TextCtrl    (self, -1, "")
        self.btn_tstep    = wx.BitmapButton(self, -1, bmpUndo, size=(16,16))
        self.chk_isol     = wx.CheckBox    (self, -1, "IsoLine")
        self.chk_log      = wx.CheckBox    (self, -1, "Log scale")
        self.chk_opentop  = wx.CheckBox    (self, -1, "Top open ended")
        self.chk_openbtm  = wx.CheckBox    (self, -1, "Bottom open ended")
        self.chk_colorbar = wx.CheckBox    (self, -1, "Hide color bar")
        self.btn_ok       = wx.Button      (self, wx.ID_OK, "")
        self.btn_cancel   = wx.Button      (self, wx.ID_CANCEL, "")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON, self.on_btn_valmin, self.btn_valmin)
        self.Bind(wx.EVT_BUTTON, self.on_btn_valmax, self.btn_valmax)
        self.Bind(wx.EVT_BUTTON, self.on_btn_valnbr, self.btn_valnbr)
        self.Bind(wx.EVT_BUTTON, self.on_btn_tstep,  self.btn_tstep)
        self.Bind(wx.EVT_BUTTON, self.on_btn_ok,     self.btn_ok)
        self.Bind(wx.EVT_BUTTON, self.on_btn_cancel, self.btn_cancel)

        self.cb = None

    def __set_properties(self):
        self.SetTitle("Isovalue parameters")
        self.txt_title.SetToolTip   ("Name of the layer that will appear in the display list")
        self.txt_valmin.SetToolTip  ("Minimum value for the range of iso to draw")
        self.txt_valmax.SetToolTip  ("Maximum value for the range of iso to draw")
        self.chk_isol.SetToolTip    ("Draw lines instead of filling the contour")
        self.chk_log.SetToolTip     ("Values drawn on log scale")
        self.chk_opentop.SetToolTip ("Values at top are open ended")
        self.chk_openbtm.SetToolTip ("Values at bottom are open ended")
        self.chk_colorbar.SetToolTip("Hide the color bar")
        self.btn_ok.SetToolTip      ("Apply the changes")
        self.btn_cancel.SetToolTip  ("Cancel changes and close the dialog box")

        self.btn_ok.SetDefault()

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)
        szr_btn  = wx.BoxSizer(wx.HORIZONTAL)
        szr_data = wx.BoxSizer(wx.VERTICAL)
        szr_top  = wx.StaticBoxSizer(self.szr_top_staticbox, wx.HORIZONTAL)
        szr_bttm = wx.BoxSizer(wx.HORIZONTAL)
        szr_val  = wx.StaticBoxSizer(self.szr_val_staticbox, wx.VERTICAL)
        szr_opts = wx.StaticBoxSizer(self.szr_opts_staticbox, wx.VERTICAL)

        szr_opts_placeholder = wx.BoxSizer(wx.HORIZONTAL)

        szr_top.Add(self.stx_title, 1, wx.EXPAND, 0)
        szr_top.Add(self.txt_title, 3, wx.EXPAND, 0)
        szr_data.Add(szr_top, 1, wx.EXPAND, 0)

        szr_valnbr = wx.BoxSizer(wx.HORIZONTAL)
        szr_valmax = wx.BoxSizer(wx.HORIZONTAL)
        szr_valmin = wx.BoxSizer(wx.HORIZONTAL)
        szr_tstep  = wx.BoxSizer(wx.HORIZONTAL)
        szr_valmin.Add(self.stx_valmin, 4, wx.ALIGN_BOTTOM, 0)
        szr_valmin.Add(self.txt_valmin, 6, wx.EXPAND, 0)
        szr_valmin.Add(self.btn_valmin, 1, wx.EXPAND, 0)
        szr_val.Add(szr_valmin, 1, wx.EXPAND, 0)
        szr_valmax.Add(self.stx_valmax, 4, wx.ALIGN_BOTTOM, 0)
        szr_valmax.Add(self.txt_valmax, 6, wx.EXPAND, 0)
        szr_valmax.Add(self.btn_valmax, 1, wx.EXPAND, 0)
        szr_val.Add(szr_valmax, 1, wx.EXPAND, 0)
        szr_valnbr.Add(self.stx_valnbr, 4, wx.ALIGN_BOTTOM, 0)
        szr_valnbr.Add(self.spn_valnbr, 6, wx.EXPAND, 0)
        szr_valnbr.Add(self.btn_valnbr, 1, wx.EXPAND, 0)
        szr_val.Add(szr_valnbr, 1, wx.EXPAND, 0)
        szr_tstep.Add(self.stx_tstep, 4, wx.ALIGN_BOTTOM, 0)
        szr_tstep.Add(self.txt_tstep, 6, wx.EXPAND, 0)
        szr_tstep.Add(self.btn_tstep, 1, wx.EXPAND, 0)
        szr_val.Add(szr_tstep, 1, wx.EXPAND, 0)

        szr_opts.Add(self.chk_isol, 0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_log,  0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_opentop, 0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_openbtm, 0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_colorbar, 0, wx.EXPAND, 0)
        szr_opts.Add(szr_opts_placeholder, 1, wx.EXPAND, 0)

        szr_bttm.Add(szr_val,  2, wx.EXPAND, 0)
        szr_bttm.Add(szr_opts, 1, wx.EXPAND, 0)

        szr_data.Add(szr_bttm, 2, wx.EXPAND, 0)

        szr_main.Add(szr_data, 0, 0, 0)
        szr_btn.AddStretchSpacer(prop=4)
        szr_btn.Add(self.btn_ok, 0, wx.EXPAND, 0)
        szr_btn.Add(self.btn_cancel, 0, wx.EXPAND, 0)
        szr_main.Add(szr_btn, 1, wx.EXPAND, 0)
        self.SetSizer(szr_main)
        szr_main.Fit(self)
        self.Layout()

    def get_title(self):
        return self.txt_title.GetValue()

    def set_title(self, t):
        self.txt_title.SetValue(t)

    title = property(get_title, set_title)

    def get_time_step(self):
        tstp = self.txt_tstep.GetValue()
        return float(tstp)

    def set_time_step(self, tstp=1.0, tstp0=1.0):
        self.tstp0 = tstp0
        self.txt_tstep.SetValue('%s' % tstp)

    time_step = property(get_time_step, set_time_step)

    def get_values(self):
        min = eval(self.txt_valmin.GetValue())
        max = eval(self.txt_valmax.GetValue())
        nvl = self.spn_valnbr.GetValue()
        return (min, max, nvl)

    def set_values(self, min=0.0, max=1.0, nval=11, min0=0.0, max0=1.0, nval0=11):
        self.min0 = min0
        self.max0 = max0
        self.nvl0 = nval0
        self.txt_valmin.SetValue('%s' % min)
        self.txt_valmax.SetValue('%s' % max)
        self.spn_valnbr.SetValue(nval)

    values = property(get_values, set_values)

    def get_checks(self):
        b1 = self.chk_isol.IsChecked()
        b2 = self.chk_log.IsChecked()
        b3 = self.chk_opentop.IsChecked()
        b4 = self.chk_openbtm.IsChecked()
        b5 = self.chk_colorbar.IsChecked()
        return (b1, b2, b3, b4, b5)

    def set_checks(self, iso_line = False, log_scale = False, open_top = False, open_bottom = False, hide_bar = False):
        self.chk_isol.SetValue(iso_line)
        self.chk_log.SetValue(log_scale)
        self.chk_opentop.SetValue(open_top)
        self.chk_openbtm.SetValue(open_bottom)
        self.chk_colorbar.SetValue(hide_bar)

    options = property(get_checks, set_checks)

    def set_callback(self, cb):
        if not isinstance(cb, collections.abc.Callable): raise TypeError('Callable expected')
        self.cb = cb

    def on_btn_valmin(self, event):
        self.txt_valmin.SetValue('%s' % self.min0)
        
    def on_btn_valmax(self, event):
        self.txt_valmax.SetValue('%s' % self.max0)
        
    def on_btn_valnbr(self, event):
        self.spn_valnbr.SetValue(self.nvl0)
        
    def on_btn_tstep(self, event):
        self.txt_tstep.SetValue('%s' % self.tstp0)
        
    def on_btn_ok(self, event):
        if self.cb:
            self.cb(self)
        else:
            event.Skip()

    def on_btn_cancel(self, event):
        self.Destroy()


if __name__ == "__main__":
    def cb(dlg):
        print(dlg.title)
        print(dlg.values)
        print(dlg.options)

    app = wx.App(False)
    dlg = DADlgParamVideo(None, -1, "")
    dlg.set_values(min=-1.0, max=100.0, nval=21)
    dlg.set_time_step(2.0)
    dlg.set_checks(False, False, True, False, False)
    dlg.set_callback(cb)
    app.SetTopWindow(dlg)
    dlg.Show()
    app.MainLoop()
