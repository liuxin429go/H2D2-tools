#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import collections.abc

import wx
import wx.lib.colourselect as  wxcsel

import DADlgColorSpan

class DADlgParamVec(wx.Dialog):
    def __init__(self, *args, **kwds):
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        super(DADlgParamVec, self).__init__(*args, **kwds)
        
        self.szr_top_staticbox  = wx.StaticBox(self, wx.ID_ANY, "Name")
        self.szr_val_staticbox  = wx.StaticBox(self, wx.ID_ANY, "Values")
        self.szr_opts_staticbox = wx.StaticBox(self, wx.ID_ANY, "Options")
        
        bmpUndo = wx.ArtProvider.GetBitmap(wx.ART_UNDO, wx.ART_OTHER) #, (16, 16))

        self.stx_title    = wx.StaticText  (self, wx.ID_ANY, "Layer name ")
        self.txt_title    = wx.TextCtrl    (self, wx.ID_ANY, "")
        self.stx_color    = wx.StaticText  (self, wx.ID_ANY, "Color ")
        self.csl_color    = wxcsel.ColourSelect(self, wx.ID_ANY, " "*20)
        self.csl_cmap     = wx.Button      (self, wx.ID_ANY, "")
        self.btn_color    = wx.BitmapButton(self, wx.ID_ANY, bmpUndo, size=(16,16))
        self.stx_valmin   = wx.StaticText  (self, wx.ID_ANY, "min ")
        self.txt_valmin   = wx.TextCtrl    (self, wx.ID_ANY, "")
        self.btn_valmin   = wx.BitmapButton(self, wx.ID_ANY, bmpUndo, size=(16,16))
        self.stx_valmax   = wx.StaticText  (self, wx.ID_ANY, "max ")
        self.txt_valmax   = wx.TextCtrl    (self, wx.ID_ANY, "")
        self.btn_valmax   = wx.BitmapButton(self, wx.ID_ANY, bmpUndo, size=(16,16))
        
        self.chk_log      = wx.CheckBox    (self, wx.ID_ANY, "Log scale ")
        self.chk_opentop  = wx.CheckBox    (self, wx.ID_ANY, "Top open ended ")
        self.chk_openbtm  = wx.CheckBox    (self, wx.ID_ANY, "Bottom open ended ")
        self.chk_color    = wx.CheckBox    (self, wx.ID_ANY, "Fixed color ")
        self.chk_colorbar = wx.CheckBox    (self, wx.ID_ANY, "Hide color bar ")
        self.btn_ok       = wx.Button      (self, wx.ID_OK, "")
        self.btn_cancel   = wx.Button      (self, wx.ID_CANCEL, "")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON,  self.on_csl_cmap,   self.csl_cmap)
        self.Bind(wx.EVT_BUTTON,  self.on_btn_color,  self.btn_color)
        self.Bind(wx.EVT_BUTTON,  self.on_btn_valmin, self.btn_valmin)
        self.Bind(wx.EVT_BUTTON,  self.on_btn_valmax, self.btn_valmax)
        self.Bind(wx.EVT_CHECKBOX,self.on_chk_color,  self.chk_color)
        self.Bind(wx.EVT_BUTTON,  self.on_btn_ok,     self.btn_ok)
        self.Bind(wx.EVT_BUTTON,  self.on_btn_cancel, self.btn_cancel)

        self.cb = None
        self.on_chk_color(None)

    def __set_properties(self):
        self.SetTitle("Quiver parameters")
        self.txt_title.SetToolTip   ("Name of the layer that will appear in the display list")
        self.txt_valmin.SetToolTip  ("Value for the minimum color")
        self.txt_valmax.SetToolTip  ("Value for the maximum color")
        self.chk_log.SetToolTip     ("Values drawn on log scale")
        self.chk_opentop.SetToolTip ("Values at top are open ended")
        self.chk_openbtm.SetToolTip ("Values at bottom are open ended")
        self.chk_colorbar.SetToolTip("Hide the color bar")
        self.btn_ok.SetToolTip      ("Apply the changes")
        self.btn_cancel.SetToolTip  ("Cancel changes and close the dialog box")

        self.chk_color.SetValue(True)
        self.csl_color.SetValue('#000000')

        self.btn_ok.SetDefault()

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)
        szr_btn  = wx.StdDialogButtonSizer()
        szr_data = wx.BoxSizer(wx.VERTICAL)
        szr_top  = wx.StaticBoxSizer(self.szr_top_staticbox, wx.HORIZONTAL)
        szr_bttm = wx.BoxSizer(wx.HORIZONTAL)
        szr_val  = wx.StaticBoxSizer(self.szr_val_staticbox, wx.VERTICAL)
        szr_opts = wx.StaticBoxSizer(self.szr_opts_staticbox, wx.VERTICAL)

        szr_opts_placeholder = wx.BoxSizer(wx.HORIZONTAL)

        szr_top.Add(self.stx_title, 0, wx.ALIGN_BOTTOM, 0)
        szr_top.Add(self.txt_title, 1, wx.EXPAND, 0)

        szr_color  = wx.BoxSizer(wx.HORIZONTAL)
        szr_valmin = wx.BoxSizer(wx.HORIZONTAL)
        szr_valmax = wx.BoxSizer(wx.HORIZONTAL)
        szr_color.Add(self.stx_color, 2, wx.ALIGN_BOTTOM, 0)
        szr_color.Add(self.csl_color, 6, wx.EXPAND, 0)
        szr_color.Add(self.csl_cmap,  6, wx.EXPAND, 0)
        szr_color.Add(self.btn_color, 1, wx.EXPAND, 0)
        szr_val.Add(szr_color, 0, wx.EXPAND, 0)
        szr_valmin.Add(self.stx_valmin, 2, wx.ALIGN_BOTTOM, 0)
        szr_valmin.Add(self.txt_valmin, 6, wx.EXPAND, 0)
        szr_valmin.Add(self.btn_valmin, 1, wx.EXPAND, 0)
        szr_val.Add(szr_valmin, 0, wx.EXPAND, 0)
        szr_valmax.Add(self.stx_valmax, 2, wx.ALIGN_BOTTOM, 0)
        szr_valmax.Add(self.txt_valmax, 6, wx.EXPAND, 0)
        szr_valmax.Add(self.btn_valmax, 1, wx.EXPAND, 0)
        szr_val.Add(szr_valmax, 0, wx.EXPAND, 0)
        szr_val.Add(szr_opts_placeholder, 1, wx.EXPAND, 0)

        szr_opts.Add(self.chk_log,     0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_opentop, 0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_openbtm, 0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_color,   0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_colorbar,0, wx.EXPAND, 0)
        #szr_opts.Add(szr_opts_placeholder, 1, wx.EXPAND, 0)

        szr_bttm.Add(szr_val,  2, wx.EXPAND, 0)
        szr_bttm.Add(szr_opts, 1, wx.EXPAND, 0)

        szr_data.Add(szr_top,  0, wx.EXPAND, 0)
        szr_data.Add(szr_bttm, 1, wx.EXPAND, 0)

        szr_btn.AddButton(self.btn_ok)
        szr_btn.AddButton(self.btn_cancel)
        szr_btn.Realize()

        szr_main.Add(szr_data, 0, wx.EXPAND, 0)
        szr_main.Add(szr_btn,  0, wx.ALIGN_RIGHT | wx.ALL, 4)

        self.SetSizer(szr_main)
        self.Fit()
        self.szr_color = szr_color      # szr needed to switch buttons
        # ---  Increase size just a bit
        w, h = self.GetSize()
        self.SetSize( (w+10, h))

    def __setCmapButton(self):
        # ---  Get size
        bmp = self.csl_cmap.GetBitmap()
        if self.csl_cmap.GetBitmap().GetSize():
            w, h = bmp.GetSize()
        else:
            w1 = self.csl_color.GetClientSize()[0]
            w2 = self.csl_cmap.GetClientSize()[0]
            w, h = max(w1, w2), 15
        bmp = DADlgColorSpan.getCmapBmp(self.cmap, (w, h))
        self.csl_cmap.SetBitmap(bmp)

    def get_title(self):
        return self.txt_title.GetValue()

    def set_title(self, t):
        self.txt_title.SetValue(t)

    title = property(get_title, set_title)

    def get_values(self):
        min = eval(self.txt_valmin.GetValue())
        max = eval(self.txt_valmax.GetValue())
        clr = self.csl_color.GetValue().GetAsString(flags=wx.C2S_HTML_SYNTAX)
        cmap = self.cmap
        cmin = self.cmin
        cmax = self.cmax
        return (min, max, clr, cmap, cmin, cmax)

    def set_values(self, min =0.0, max =1.0, clr ='#000000', cmap ='jet', cmin =0.0, cmax =1.0, 
                         min0=0.0, max0=1.0, clr0='#000000', cmap0='jet', cmin0=0.0, cmax0=1.0):
        self.min0  = min0
        self.max0  = max0
        self.clr0  = clr0
        self.cmap0 = cmap0
        self.cmin0 = cmin0
        self.cmax0 = cmax0

        self.cmap = cmap
        self.cmin = cmin
        self.cmax = cmax

        self.txt_valmin.SetValue('%s' % min)
        self.txt_valmax.SetValue('%s' % max)
        self.csl_color.SetValue (clr)
        self.__setCmapButton()

    values = property(get_values, set_values)

    def get_checks(self):
        b1 = self.chk_log.IsChecked()
        b2 = self.chk_opentop.IsChecked()
        b3 = self.chk_openbtm.IsChecked()
        b4 = self.chk_color.IsChecked()
        b5 = self.chk_colorbar.IsChecked()
        return (b1, b2, b3, b4, b5)

    def set_checks(self, log_scale=False, open_top=False, open_bottom=False, fixed_color=False, hide_bar=False):
        self.chk_log.SetValue(log_scale)
        self.chk_opentop.SetValue(open_top)
        self.chk_openbtm.SetValue(open_bottom)
        self.chk_color.SetValue  (fixed_color)
        if fixed_color:
            self.chk_colorbar.SetValue(True)
        else:
            self.chk_colorbar.SetValue(hide_bar)

        # CheckBox.SetValue does not cause a wxEVT_CHECKBOX event to get emitted.
        # Do it explicitely
        self.on_chk_color(None) 

    options = property(get_checks, set_checks)

    def set_callback(self, cb):
        if not isinstance(cb, collections.abc.Callable): raise TypeError('Callable expected')
        self.cb = cb

    def on_btn_color(self, event):
        self.csl_color.SetValue(self.clr0)
        
    def on_btn_valmin(self, event):
        self.txt_valmin.SetValue('%s' % self.min0)
        
    def on_btn_valmax(self, event):
        self.txt_valmax.SetValue('%s' % self.max0)
        
    def on_csl_cmap(self, event):
        dlg = DADlgColorSpan.DADlgColorSpan(self)
        if dlg.ShowModal() == wx.ID_OK:
            self.cmap, self.cmin, self.cmax = dlg.getColorSpan()
            self.__setCmapButton()
        dlg.Destroy()
        
    def on_chk_color(self, event):
        enable = self.chk_color.GetValue()
        self.csl_color.Enable(enable)
        self.csl_color.Show  (enable)
        self.csl_cmap.Enable (not enable)
        self.csl_cmap.Show   (not enable)
        self.chk_colorbar.Enable(not enable)
        self.szr_color.Layout()

    def on_btn_ok(self, event):
        if self.cb:
            self.cb(self)
        else:
            event.Skip()

    def on_btn_cancel(self, event):
        self.Destroy()


if __name__ == "__main__":
    def cb(dlg):
        print(dlg.title)
        print(dlg.values)
        print(dlg.options)

    app = wx.App(0)
    dlg = DADlgParamVec(None, -1, "")
    dlg.set_values(min=-1.0, max=100.0, clr='#8080FF')
    dlg.set_checks(False, False, True, True, False)
    dlg.set_callback(cb)
    app.SetTopWindow(dlg)
    dlg.Show()
    app.MainLoop()
