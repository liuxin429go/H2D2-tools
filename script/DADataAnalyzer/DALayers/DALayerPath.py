# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath( os.path.join(selfDir, '..', '..') )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)
    supPath = os.path.normpath( os.environ['INRS_LXT'] )
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import logging
import sys
import threading
import time
import traceback
if sys.version_info < (3, 0):
    import cPickle as pickle
else:
    import _pickle as pickle

import wx
import wx.lib.newevent
import numpy as np
from scipy import interpolate

import DAFrameGrid       # for assertion in ctr
import DAEvents
from . import DALayer
from . import DADlgParamPath
from CTCommon import CTUtil
#from CTCommon import DTPath
from CTCommon import DTPath_v2 as DTPath
from CTCommon import DTDiffusion
from IPImageProcessor import IPRaster
import mplcursors

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Layer.StreamLines")

# https://wiki.wxpython.org/LongRunningTasks
ThreadResultEvent, EVT_THREAD_RESULT = wx.lib.newevent.NewCommandEvent()
EVT_THREAD_RESULT_ID   = 1

class DADataCursor:
    """
    Encapsulate a mplcursors.Cursor.
    The mplcursors.Cursor will be constructed on activate() and
    removed on deactivate()
    """
    def __init__(self, CS, formatter=None, xytext=(5,5)):
        self.CS     = CS
        self.fmtr   = formatter
        self.xytext = xytext
        self.cursor = None
        self.activate()

    def __del__(self):
        try:
            self.deactivate()
        except RuntimeError:        # canvas might be already dead
            pass

    def activate(self):
        if not self.cursor:
            self.cursor = mplcursors.Cursor([self.CS], hover=False)
        if self.fmtr:
            self.cursor.connect("add", self.fmtr)

    def deactivate(self):
        if self.cursor:
            self.cursor.remove()
            self.cursor = None

class DAPathItem(DALayer.DALayerItemCS):
    def __init__(self, *args, dc=None, **kwargs):
        super(DAPathItem, self).__init__(*args, **kwargs)
        self.dc  = dc
        self.fig = self.cs.get_figure()

    def setVisible(self, v):
        """
        Overload setVisible from parent to add dataCursors

        Args:
            v (bool): visible
        """
        super(DAPathItem, self).setVisible(v)
        self.cs.set_figure(self.fig)
        if self.dc:
            self.dc.activate() if v else self.dc.deactivate()

class DALayerPathlines(wx.Frame, DALayer.DALayer):
    count = 0
    ELLPS_COLOR = "#4682B4"
    ELLPS_ALPHA = 0.4

    # Il faut absolument hériter de Frame pour être notifié des événements
    def __init__(self, page, field, times, cols, names):
        if not isinstance(field.grid, (DAFrameGrid.DAFrameGrid,)):
            raise RuntimeError('Invalid grid type. Only finite element grids are supported.')

        wx.Frame.__init__(self, None, wx.ID_ANY)
        DALayer.DALayer.__init__(self)
        self.pane  = page
        self.field = field
        self.times = times
        self.cols  = cols
        self.names = names

        # Set up event handler for worker thread results
        self.Bind(EVT_THREAD_RESULT, self.onThreadResult)
        self.Bind(wx.EVT_TIMER,      self.onTimer)

        self.fig   = self.pane.get_figure()
        self.ax    = self.pane.get_axes()
        self.dlgPrm= None
        self.dlgBar= None
        self.thread= None
        self.timer = None

        DALayerPathlines.count += 1
        tmin, tmax = (times[0], times[-1])
        self.title = 'Pathlines #%03i: %s @ [%s, %s]' % (DALayerPathlines.count, names[0], tmin, tmax)
        self.tstart, self.ninj, self.dtinj = (tmin, 1, 3600.0)
        self.dtsim, self.dtdsp, self.dtclc = (tmax-tmin, 120.0, 15.0)
        self.kind,  self.dtell, self.rinit, self.mindl = (DADlgParamPath.DIFF_TYPE.DEPTH_AVERAGE, 30*60.0, 10.0, 1.e-6)
        self.diffm, self.wvert, self.whorz, self.wlong = 1.0e-9, 1.0, 1.0, 1.0
        self.chk_cursor, self.chk_line, self.chk_ells = True, True, False
        self.X, self.Y = [-1], [-1]

        self.diffs  = None
        self.paths  = []
        self.prgrs  = None
        self.threadErrMsg = ''

        self.nval = 100
        self.log_scale = False

    def onTimer(self):
        LOGGER.debug('DALayerPathlines.onTimer')
        start = time.time()
        if not self.dlgBar and self.thread and self.thread.isAlive():
            style = wx.PD_CAN_ABORT | wx.PD_ESTIMATED_TIME | wx.PD_REMAINING_TIME | wx.PD_AUTO_HIDE
            self.dlgBar = wx.ProgressDialog("Computing the streamlines", "Be patient, can be long", style=style)

        keepGoing = True
        if self.dlgBar:
            pths, (ixy, nxy), (ipt, npt), (tc, dt), isCanceled = self.prgrs
            if ixy > 0 and ipt > 0:
                dt = max(dt, 1.0e-99)   # Provision against Zero Division
                msg = "Point %i/%i; Injection %i/%i" % (ixy, nxy, ipt, npt)
                pct = int(100.0*tc/dt)
                (keepGoing, skip) = self.dlgBar.Update(pct, msg)

        if not keepGoing and self.thread and self.thread.isAlive():
            LOGGER.debug('ProgressThread: Notify worker to stop')
            self.prgrs[4] = True

        doContinue = keepGoing and self.thread and self.thread.isAlive() and self.prgrs[0] is None
        if not doContinue:
            LOGGER.debug('ProgressThread: posting event')
            if self.timer:
                self.timer.Stop()
                del self.timer
                self.timer = None
            evt = ThreadResultEvent(EVT_THREAD_RESULT_ID, isCanceled=not keepGoing)
            wx.PostEvent(self, evt)

        if not doContinue and self.dlgBar:
            self.dlgBar.Hide()
            self.dlgBar.Destroy()
            self.dlgBar = None

        if self.timer:
            end = time.time()
            millis = int((end - start) * 1000 + 0.5)
            sleep = max(500 - millis, 1)
            self.timer.Restart(sleep)

    def onThreadResult(self, event):
        LOGGER.debug('DALayerPathlines.onThreadResult isCanceled=%s', event.isCanceled)
        errMsg = []
        pths, (ixy, nxy), (ipt, npt), (tc, dt), isCanceled = self.prgrs
        if isCanceled:
            errMsg.append("Paths computation was canceled")
        if nxy < 0 or ixy != nxy:
            errMsg.append("Only %i points out if %i where computed" % (ixy, nxy))
        if npt < 0 or ipt != npt:
            errMsg.append("Only %i paths out if %i where computed" % (ipt, npt))
        if not pths:
            errMsg.append("No paths where computed")
        else:
            if len(pths) != (nxy*npt):
                errMsg.append("Only %i paths out if %i where computed" % (len(pths), nxy*npt))
        if self.threadErrMsg:
            errMsg.append(self.threadErrMsg)
        if errMsg:
            errMsg = '\n'.join(errMsg)
            dlg = wx.MessageDialog(self, errMsg, 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()

        if pths:
            self.paths = self.prgrs[0]
            self.genIpFig()
            evt = DAEvents.UpdateDisplayListEvent(DAEvents.EVT_UPDATE_DISPLAY_LIST_ID)
            wx.PostEvent(self.pane, evt)

        self.thread = None

    def dataCursorPathFormatter(self, selection):
        txt = selection.annotation.get_text()
        try:
            idx = selection.target.index
            
            lbl = txt.split('\n')[0]
            ip = eval(lbl.split(':')[-1])   # path  index
            jj = idx[0]                     # point index

            pth = self.paths[ip]
            t0  = pth[ 0][0]
            tx  = pth[jj][0]
            cx  = pth[jj][3]
            try:
                sth = 't0={t:s}'.format(t=CTUtil.epoch_to_isoutc(t0))
                sdh = 'dt={d:s}'.format(d=CTUtil.seconds_to_iso (tx-t0))
                stf = 't0={t:,.3f}'.format(t=t0)
                sdf = 'dt={d:,.3f}'.format(d=tx-t0)
                s = (stf, sth, sdf, sdh)
                selection.annotation.set_text('\n'.join(s))
            except:
                stf = 't0={t:,.3f}'.format(t=t0)
                sdf = 'dt={d:,.3f}'.format(d=tx-t0)
                #x = 'x={x:,.3f}'.format(x=kwargs['x'])
                #y = 'y={y:,.3f}'.format(y=kwargs['y'])
                s = (stf, sdf)
                selection.annotation.set_text('\n'.join(s))
        except Exception as e:
            errMsg = '%s\n%s' % (str(e), traceback.format_exc())
            LOGGER.error(errMsg)

    def dataCursorEllpsFormatter(self, selection):
        try:
            txt = selection.annotation.get_text()
            idx = selection.target.index

            lbl = txt.split('\n')[0]
            ip = eval(lbl.split(':')[-1])   # path  index
            jj = idx[0]

            pth = self.paths[ip]
            t0  = pth[ 0][0]
            tx  = pth[jj][0]
            cx  = pth[jj][3]
            d = self.diffs[jj]
            st0 = 't0={t:s}'.format(t=CTUtil.epoch_to_isoutc(t0))
            sta = 'ta={t:s}'.format(t=CTUtil.epoch_to_isoutc(tx))
            sdh = 'dt={d:s}'.format(d=CTUtil.seconds_to_iso(tx-t0))
            scm = 'c.max={c:,.3e}'.format(c=cx)
            s = (st0, sta, sdh, scm)
            selection.annotation.set_text('\n'.join(s))
        except Exception as e:
            errMsg = '%s\n%s' % (str(e), traceback.format_exc())
            LOGGER.error(errMsg)

    @staticmethod
    def interpPath(p, dt):
        path = np.array( [tuple(pnt) for pnt in p] )
        path_t = path[:,0]
        path_d = path[:,1:]
        tn = np.arange(path_t[0], path_t[-1], dt)
        f = interpolate.interp1d(path_t, path_d, assume_sorted=True, axis=0)
        dn = f(tn)
        tn = tn.reshape( (-1,1) )
        return np.concatenate( (tn, dn), axis=1 )

    def runPathCalculation(self):
        """
        Thread method to calculate the paths
        Results will be in self.prgrs
        """
        LOGGER.debug('WorkerThread: start')
        self.prgrs = [None, (-1,-1), (-1,-1), (-1.0,-1.0), False]
        try:
            algo = DTPath.DTAlgoPathT3()
            X, Y = self.field.getGrid().projectToGrid(self.X, self.Y)
            tinj = [ (self.tstart + it*self.dtinj) for it in range(self.ninj) ]
            algo.xeq(X, Y, self.field, self.cols, tinj, self.dtsim, self.dtdsp, self.dtclc, self.prgrs)
        except Exception as e:
            self.threadErrMsg = str(e)
        LOGGER.debug('WorkerThread: done')

    def genPath(self):
        # ---  Compute (or read) paths
        if not self.Y and os.path.isfile(self.X):
            wx.BeginBusyCursor()
            with open(self.X, 'rb') as fi: self.paths = pickle.load(fi)
            wx.EndBusyCursor()
            self.genIpFig()
        else:
            self.thread = threading.Thread(target=self.runPathCalculation)
            # the log system looks kind of locked by the thread.
            # the calls here seem to unlock the log system
            LOGGER.debug('DALayerPathlines.genPath: thread created: %s' % self.thread.name)
            self.thread.start()
            self.timer = wx.CallLater(500, self.onTimer)

    def genIpFig(self):
        tinj = [ (self.tstart + it*self.dtinj) for it in range(self.ninj) ]
        tend = tinj[-1] + self.dtsim

        # ---  Set diffusion algorithm
        AlgoDiff = None
        if self.kind == DADlgParamPath.DIFF_TYPE.DEPTH_AVERAGE:
            AlgoDiff = DTDiffusion.DTDiffusion2DH2D2_DepthAverage
        elif self.kind == DADlgParamPath.DIFF_TYPE.SURFACE:
            AlgoDiff = DTDiffusion.DTDiffusion2DH2D2_Surface

        # ---  Loop on the paths
        kwPoly = {}
        kwPoly.update( IPRaster.IPAxes.genKwNorm(self.tstart, tend) )
        kwPoly['linewidth'] = 1.0
        kwEllp = {}
        kwEllp['color'] = self.ELLPS_COLOR
        kwEllp['alpha'] = self.ELLPS_ALPHA
        self.diffs  = None      # Diffusion data
        self.items  = []
        items_pnt   = []
        xy0 = None
        for ip, pth in enumerate(self.paths):
            t, xy = pth[0].t(), pth[0].xy()      # First point
            #print(t, xy)

            # ---  Draw the path
            if self.chk_line and len(pth) > 0:
                kwPoly['label'] = 'path_id: %d' % len(self.items)
                txy = np.array([(p.t(), p.x(), p.y()) for p in pth])
                T, X, Y = txy[:,0], txy[:,1], txy[:,2]
                X, Y = self.field.getGrid().projectToProj(X, Y)
                cs = self.ax.drawOnePath(T, X, Y, **kwPoly)
                dc = None
                if self.chk_cursor: 
                    dc = DADataCursor(cs, formatter=self.dataCursorPathFormatter, xytext=(5,5))
                items_pnt.append( DAPathItem(cs, IPRaster.IPPlotType.PARTICLE_PATH, 'Path %02d at %.3f' % (ip, t), dc=dc) )

            # ---  Draw the ellipses
            if self.chk_ells:
                kwEllp['label'] = 'path_id: %d' % len(self.items)
                pell = DALayerPathlines.interpPath(pth, self.dtell)
                algo = AlgoDiff(self.diffm, self.wvert, self.whorz, self.wlong, self.mindl, self.rinit)
                self.diffs = algo.xeq(pell)
                X = np.array([ d[1] for d in self.diffs ])
                Y = np.array([ d[2] for d in self.diffs ])
                W = np.array([ d[4] for d in self.diffs ])
                H = np.array([ d[5] for d in self.diffs ])
                A = np.array([ d[6] for d in self.diffs ])
                W, H = self.field.getGrid().projectToProj(W+X, H+Y)
                X, Y = self.field.getGrid().projectToProj(X, Y)
                W = W - X
                H = H - Y
                ellps = [ [x, y, w, h, a] for x, y, w, h, a in zip(X, Y, W, H, A) ]
                cs = self.ax.drawEllipses(ellps, **kwEllp)
                dc = None
                if self.chk_cursor: 
                    dc = DADataCursor(cs, formatter=self.dataCursorEllpsFormatter, xytext=(5,5))
                items_pnt.append( DAPathItem(cs, IPRaster.IPPlotType.ELLIPSE, 'Ellipses %02d at %.3f' % (ip, t), dc=dc) )

            if xy != xy0:
                if items_pnt: self.items.extend( items_pnt )
                items_pnt = []
                xy0 = xy

        if items_pnt: self.items.extend( items_pnt )
        self.pane.add_layer(self)
        self.pane.redraw()

    def __xeqDraw(self):
        if self.items:
            self.pane.del_layer(self)
        self.genPath()

    def draw(self):
        if not self.dlgPrm:
            self.dlgPrm = DADlgParamPath.DADlgParamPathline(self.pane)
            self.dlgPrm.set_title (self.title)
            self.dlgPrm.set_values_inj (self.tstart, self.ninj, self.dtinj)
            self.dlgPrm.set_values_sim (self.dtsim, self.dtdsp, self.dtclc)
            self.dlgPrm.set_values_diff(self.kind, self.dtell, self.rinit, self.mindl, self.diffm, self.wvert, self.whorz, self.wlong)
            self.dlgPrm.set_checks(self.chk_cursor, self.chk_line, self.chk_ells)
            self.dlgPrm.set_positions(self.X, self.Y)

        if self.dlgPrm.ShowModal() == wx.ID_OK:
            self.title = self.dlgPrm.get_title()
            self.tstart, self.ninj, self.dtinj = self.dlgPrm.get_values_inj()
            self.dtsim, self.dtdsp, self.dtclc = self.dlgPrm.get_values_sim()
            self.kind, self.dtell, self.rinit, self.mindl, self.diffm, self.wvert, self.whorz, self.wlong = self.dlgPrm.get_values_diff()
            self.chk_cursor, self.chk_line, self.chk_ells, dummy_1 = self.dlgPrm.get_checks()
            self.X, self.Y = self.dlgPrm.get_positions()

            self.__xeqDraw()

        #self.dlgPrm.EndModal(wx.ID_OK)

    def __str__(self):
        return self.title

if __name__ == "__main__":
    class DummyPane:
        def get_axes(self):
            return None
        def get_figure(self):
            return None

    def main():
        from CTCommon.FEMesh  import FEMesh
        from CTCommon.DTData  import DTData
        from CTCommon.DTField import DT2DFiniteElementField
        from DAFrameGrid  import DAFrameGrid
        from DAFrameField import DAFrameField
        p = r'E:\bld-1804\bug\day5_7h_oscillations\output'
        f = os.path.join(p, '_a.cor'), os.path.join(p, '_a.ele')
        mesh = DAFrameGrid(files=f, withLocalizer=True)

        f = os.path.join(p, '_h2d2.pst.sim')
        #data  = DTData(f)
        #field = DT2DFiniteElementField(mesh, data)
        field = DAFrameField.as2DFiniteElementField(grid=mesh, file=f)

        l = DALayerPathlines(DummyPane(), field, [0.0, 1.0], [], ['dummy'])
        l.tstart, l.ninj, l.dtinj =  1245082200.0, 1, 900.0
        l.dtsim, l.dtdsp, l.dtclc = 24*3600.0, 120, 15
        l.dtell, l.wvert, l.whorz, l.wlong = 1800.0, 1.0, 1.0, 10.0
        l.chk_cursor, l.chk_line, l.chk_ells, dummy_1 = False, False, False, False
        XY = [(314449.24054516136, 5070243.175497941)]
        l.X, l.Y = [x for x,y in XY], [y for x,y in XY]
        #l.genIpFig()
        l.genPath()

    formatter = logging.Formatter('%(asctime)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    app = wx.App(False)
    main()
    LOGGER.info('Start main loop')
    app.MainLoop()
