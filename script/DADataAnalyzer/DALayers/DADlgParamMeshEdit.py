#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2015-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx
import wx.lib.colourselect as  wxcsel
import collections.abc

markers = {
'point'         : ".",
'pixel'         : ",",
'circle'        : "o",
'triangle_down' : "v",
'triangle_up'   : "^",
'triangle_left' : "<",
'triangle_right': ">",
'tri_down'      : "1",
'tri_up'        : "2",
'tri_left'      : "3",
'tri_right'     : "4",
'octagon'       : "8",
'square'        : "s",
'pentagon'      : "p",
'star'          : "*",
'hexagon1'      : "h",
'hexagon2'      : "H",
'plus'          : "+",
'x'             : "x",
'diamond'       : "D",
'thin_diamond'  : "d",
'vline'         : "|",
'hline'         : "_",
'tickleft'      : 'TICKLEFT',
'tickright'     : 'TICKRIGHT',
'tickup'        : 'TICKUP',
'tickdown'      : 'TICKDOWN',
'caretleft'     : 'CARETLEFT',
'caretright'    : 'CARETRIGHT',
'caretup'       : 'CARETUP',
'caretdown'     : 'CARETDOWN',
'nothing'       : None,
}

class DADlgParamMeshEdit(wx.Dialog):
    def __init__(self, *args, **kwds):
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        super(DADlgParamMeshEdit, self).__init__(*args, **kwds)

        self.szr_top_staticbox  = wx.StaticBox(self, -1, "Name")
        self.szr_val_staticbox  = wx.StaticBox(self, -1, "Values")
        self.stx_title = wx.StaticText(self, -1, "Layer name")
        self.txt_title = wx.TextCtrl  (self, -1, "")
        self.stx_ndmrk = wx.StaticText(self, -1, "Node marker")
        self.stx_elclr = wx.StaticText(self, -1, "Elem color")
        self.stx_ndclr = wx.StaticText(self, -1, "Node color")

        self.btn_ndmrk = wx.ComboBox(self, choices=list(markers.keys()))
        self.btn_elclr = wxcsel.ColourSelect(self, -1, " "*20)
        self.btn_ndclr = wxcsel.ColourSelect(self, -1, " "*20)

        self.btn_ok     = wx.Button(self, wx.ID_OK, "")
        self.btn_cancel = wx.Button(self, wx.ID_CANCEL, "")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wxcsel.EVT_COLOURSELECT, self.on_btn_color)
        self.Bind(wx.EVT_BUTTON, self.on_btn_ok,  self.btn_ok)
        self.Bind(wx.EVT_BUTTON, self.on_btn_cancel, self.btn_cancel)

        self.cb = None

    def __set_properties(self):
        self.SetTitle("Mesh parameters")
        self.txt_title.SetToolTip ("Name of the layer")
        self.btn_ok.SetToolTip    ("Apply the changes")
        self.btn_cancel.SetToolTip("Cancel changes and close the dialog box")

        self.btn_ok.SetDefault()

        self.btn_ndmrk.SetValue('point')
        self.btn_elclr.SetValue('#00FF00')
        self.btn_ndclr.SetValue('#00FFFF')

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)
        szr_btn  = wx.BoxSizer(wx.HORIZONTAL)
        szr_data = wx.BoxSizer(wx.VERTICAL)
        szr_top  = wx.StaticBoxSizer(self.szr_top_staticbox, wx.HORIZONTAL)
        szr_bttm = wx.BoxSizer(wx.HORIZONTAL)
        szr_val  = wx.StaticBoxSizer(self.szr_val_staticbox, wx.VERTICAL)

        szr_top.Add(self.stx_title, 1, wx.EXPAND, 0)
        szr_top.Add(self.txt_title, 3, wx.EXPAND, 0)
        szr_data.Add(szr_top, 1, wx.EXPAND, 0)

        szr_ndmrk = wx.BoxSizer(wx.HORIZONTAL)
        szr_ndmrk.Add(self.stx_ndmrk, 3, wx.ALIGN_BOTTOM, 0)
        szr_ndmrk.Add(self.btn_ndmrk, 2, wx.EXPAND, 0)
        szr_val.Add(szr_ndmrk, 1, wx.EXPAND, 0)
        szr_elclr = wx.BoxSizer(wx.HORIZONTAL)
        szr_elclr.Add(self.stx_elclr, 3, wx.ALIGN_BOTTOM, 0)
        szr_elclr.Add(self.btn_elclr, 2, wx.EXPAND, 0)
        szr_val.Add(szr_elclr, 1, wx.EXPAND, 0)
        szr_ndclr = wx.BoxSizer(wx.HORIZONTAL)
        szr_ndclr.Add(self.stx_ndclr, 3, wx.ALIGN_BOTTOM, 0)
        szr_ndclr.Add(self.btn_ndclr, 2, wx.EXPAND, 0)
        szr_val.Add(szr_ndclr, 1, wx.EXPAND, 0)

        szr_bttm.Add(szr_val,  2, wx.EXPAND, 0)
        szr_data.Add(szr_bttm, 2, wx.EXPAND, 0)

        szr_main.Add(szr_data, 0, 0, 0)
        szr_btn.AddStretchSpacer(prop=4)
        szr_btn.Add(self.btn_ok, 0, wx.EXPAND, 0)
        szr_btn.Add(self.btn_cancel, 0, wx.EXPAND, 0)
        szr_main.Add(szr_btn, 1, wx.EXPAND, 0)

        self.SetSizer(szr_main)
        szr_main.Fit(self)
        self.Layout()

    def on_btn_color(self, event):
        clr = event.GetValue().GetAsString(flags=wx.C2S_HTML_SYNTAX)

    def get_title(self):
        return self.txt_title.GetValue()

    def set_title(self, t):
        self.txt_title.SetValue(t)

    title = property(get_title, set_title)

    def get_marker(self):
        return self.btn_ndmrk.GetValue()

    def set_marker(self, t):
        _v = markers[t]  # check the value exist
        self.btn_ndmrk.SetValue(t)

    marker = property(get_marker, set_marker)

    def get_element_color(self):
        clr = self.btn_elclr.GetValue().GetAsString(flags=wx.C2S_HTML_SYNTAX)
        return clr

    def set_element_color(self, c):
        self.btn_elclr.SetValue(c)

    element_color = property(get_element_color, set_element_color)

    def get_node_color(self):
        clr = self.btn_ndclr.GetValue().GetAsString(flags=wx.C2S_HTML_SYNTAX)
        return clr

    def set_node_color(self, c):
        self.btn_ndclr.SetValue(c)

    node_color = property(get_node_color, set_node_color)

    def set_callback(self, cb):
        if not isinstance(cb, collections.abc.Callable): raise TypeError('Callable expected')
        self.cb = cb

    def on_btn_ok(self, event):
        if self.cb:
            self.cb(self)
        else:
            event.Skip()

    def on_btn_cancel(self, event):
        self.Destroy()


if __name__ == "__main__":
    def cb(dlg):
        print((dlg.title))
        print((dlg.values))
        print((dlg.options))

    app = wx.App(False)
    dlg = DADlgParamMeshEdit(None, -1, "")
    dlg.set_callback(cb)
    app.SetTopWindow(dlg)
    dlg.Show()
    app.MainLoop()
