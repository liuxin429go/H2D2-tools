# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import matplotlib

class DALayerItemBase:
    def __init__(self, title=''):
        self.title  = title
        self.visible= True

    def isVisible(self):
        return self.visible
        
    def setVisible(self, v):
        self.visible = v

    def attach(self):
        raise NotImplementedError

    def detach(self):
        raise NotImplementedError

class DALayerItemCS(DALayerItemBase):
    def __init__(self, cs, kind, title=''):
        super(DALayerItemCS, self).__init__(title)
        self.kind = kind
        self.cs   = cs

    @staticmethod
    def __attach_one(cs, ax):
        if isinstance(cs, matplotlib.collections.Collection):
            ax.add_collection(cs)
        elif isinstance(cs, matplotlib.artist.Artist):
            ax.add_artist(cs)
        else:
            raise ValueError('Unhandled type: %s' % type(cs))

    def __attach(self, cs, ax):
        if isinstance(cs, (list,tuple)):
            for c in cs: 
                self.__attach(c, ax)
        elif hasattr(cs, 'collections'):
            for c in cs.collections: 
                self.__attach_one(c, ax)
        else:
            self.__attach_one(cs, ax)

    def attach(self, ax):
        if not self.visible: return
        self.__attach(self.cs, ax)

    def __detach(self, cs):
        try:
            if isinstance(cs, (list,tuple)):
                for c in cs:
                    self.__detach(c)
            elif hasattr(cs, 'collections'):
                for c in cs.collections:
                    c.remove()
            else:
                cs.remove()
        except:
            pass

    def detach(self):
        self.__detach(self.cs)

class DALayerItemCB(DALayerItemBase):
    def __init__(self, cb, title=''):
        super(DALayerItemCB, self).__init__(title)
        self.cb = cb
        
    def attach(self, ax):
        if self.visible:
            self.cb.hide(False)

    def detach(self):
        self.cb.hide(True)

class DALayer:
    count = 0
    
    def __init__(self, title=''):
        self.title  = title if title else 'Layer #%3i' % DALayer.count
        self.visible= True
        self.items  = []
        self.cbid   = -1        # arg to IPAxes.getInsetAxes

        DALayer.count += 1

    def __iter__(self):
        return self.items.__iter__()

    def __getitem__(self, i):
        return self.items[i]

    def __len__(self):
        return len(self.items)

    def append(self, item):
        return self.items.append(item)
        
    def remove(self, item):
        assert(item in self.items)
        item.detach()
        return self.items.remove(item)
    
    def hasNavigator(self):
        return False
    
    def isVisible(self):
        return self.visible

    def setVisible(self, v):
        for item in self.items: 
            item.setVisible(v)
        self.visible = v
        
    def getDrawableItems(self):
        return [ item for item in self.items if item.visible ]

    def attach(self, ax):
        for item in self.items:
            item.attach(ax)

    def detach(self):
        # ---  Detach first the colorbar(s)
        for item in reversed(self.items):
            if isinstance(item, DALayerItemCB):
                item.detach()
        # ---  Then the rest
        for item in reversed(self.items):
            if not isinstance(item, DALayerItemCB):
                item.detach()
