# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import wx

from . import DALayer
from . import DADlgParamIso
from . import DANavigator
from IPImageProcessor import IPRaster
from IPImageProcessor.IPColorBar import IPDraggableColorBarWithFrame

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Layer.Iso")

class DALayerIso(DALayer.DALayer):
    count = 0

    def __init__(self, page, field, names):
        super(DALayerIso, self).__init__()
        self.pane  = page
        self.field = field
        self.names = names

        self.fig   = self.pane.get_figure()
        self.ax    = self.pane.get_axes()
        self.nav   = None

        DALayerIso.count += 1
        self.title = 'Iso #%03i: %s @ %s' % (DALayerIso.count, names[0], str(field))
        self.vmin, self.vmax, self.nval = (self.field.getDataMin(), self.field.getDataMax(), 21)
        self.cmap, self.cmin, self.cmax = ('jet', 0.0, 1.0)
        self.iso_line, self.log_scale, self.open_top, self.open_btm, self.hide_bar = (False, False, False, False, True)

        self.min0,  self.max0,  self.nvl0  = self.vmin, self.vmax, self.nval
        self.cmap0, self.cmin0, self.cmax0 = self.cmap, self.cmin, self.cmax

    def genIpFig(self):
        kwargs = {}
        kwargs.update( IPRaster.IPAxes.genKwLimits(self.vmin, self.vmax, self.nval, self.log_scale) )
        kwargs.update( IPRaster.IPAxes.genKwCmap  (self.cmap, self.cmin, self.cmax) )
        
        self.items = []
        if self.iso_line:
            cs = self.ax.drawContourLine(self.field.getGrid(), self.field.getDataActual(), **kwargs)
            self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.CONTOUR_LINE, 'Isoline') )
        else:
            kwargs.update( IPRaster.IPAxes.genKwExtend(self.open_top, self.open_btm) )
            cs = self.ax.drawContourFill(self.field.getGrid(), self.field.getDataActual(), **kwargs)
            self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.CONTOUR_FILL, 'Isovalue') )
        if not self.hide_bar:
            cb = IPDraggableColorBarWithFrame(cs, self.ax, 
                                              fraction=0.05, 
                                              ticks=kwargs['levels'], 
                                              format='%.3e', 
                                              label=self.title, 
                                              keyAction=False)
            self.items.append( DALayer.DALayerItemCB(cb, 'Colorbar') )
        self.pane.add_layer(self)

    def __xeqDraw(self):
        if self.items:
            self.pane.del_layer(self)
        self.genIpFig()
        self.pane.redraw()

    def draw(self):
        dlg = DADlgParamIso.DADlgParamIso(self.pane)
        dlg.set_title (self.title)
        dlg.set_values(self.vmin, self.vmax, self.nval, self.cmap,  self.cmin,  self.cmax,
                       self.min0, self.max0, self.nvl0, self.cmap0, self.cmin0, self.cmax0)
        dlg.set_checks(self.iso_line, self.log_scale, self.open_top, self.open_btm, self.hide_bar)

        if dlg.ShowModal() == wx.ID_OK:
            self.title = dlg.title
            self.vmin, self.vmax, self.nval, self.cmap, self.cmin, self.cmax = dlg.values
            self.iso_line, self.log_scale, self.open_top, self.open_btm, self.hide_bar = dlg.options
            self.__xeqDraw()

        dlg.Destroy()

    def onNavigatorChange(self, x, y, v):
        self.pane.doCenter(x, y)

    def onNavigatorClose(self):
        self.nav = None

    def hasNavigator(self):
        return True
        
    def navigate(self):
        if not self.nav:
            self.nav = DANavigator.DANavigator(self.pane, on_change=self.onNavigatorChange, on_close=self.onNavigatorClose)
            data = self.field.getDataActual()
            grid = self.field.getGrid()
            self.nav.setData(grid, data)

    def __str__(self):
        return self.title

if __name__ == "__main__":
    dummy = DALayerIso(None, 1.0, None, [0, 1])
