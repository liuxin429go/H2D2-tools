# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx

from . import DALayer
from IPImageProcessor import IPRaster

class DALayerImage(DALayer.DALayer):
    count = 0

    def __init__(self, page, image = None):
        super(DALayerImage, self).__init__()
        self.pane = page
        self.img  = image

        self.fig  = self.pane.get_figure()
        self.ax   = self.pane.get_axes()

        DALayerImage.count += 1
        self.title = 'Image #%03i' % DALayerImage.count

    def genIpFig(self):
        cs = self.ax.drawImage(self.img)
        self.items = [ DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.IMAGE, 'Image') ]
        self.pane.add_layer(self)

    def __xeqDraw(self):
        if self.items:
            self.pane.del_layer(self)
        self.genIpFig()
        self.pane.redraw()

    def draw(self):
        ## dlg = DADlgParamMesh.DADlgParamMesh(self.pane)
        ## dlg.set_title (self.title)
        ## dlg.set_checks(self.drwSkin, self.drwElem, self.drwNode, self.drwElemNumber, self.drwNodeNumber)
        ## dlg.set_marker(self.marker)
        ## dlg.set_element_color(self.elem_color)
        ## dlg.set_node_color   (self.node_color)
        ## dlg.set_element_number_color(self.elnb_color)
        ## dlg.set_node_number_color   (self.nonb_color)
        ## 
        ## errMsg = ''
        ## if dlg.ShowModal() == wx.ID_OK:
        ##     self.title = dlg.title
        ##     self.drwSkin, self.drwElem, self.drwNode, self.drwElemNumber, self.drwNodeNumber = dlg.options
        ##     self.marker = dlg.marker
        ##     self.elem_color = dlg.element_color
        ##     self.node_color = dlg.node_color
        ##     self.elnb_color = dlg.element_number_color
        ##     self.nonb_color = dlg.node_number_color
        ##     try:
        ##         self.__xeqDraw()
        ##     except Exception as e:
        ##         import traceback
        ##         errMsg = '\n'.join( ('Drawing the skin of a skin/L2 mesh?', str(e), traceback.format_exc()) )
        ## 
        ## dlg.Destroy()
        ## if errMsg:
        ##     dlg = wx.MessageDialog(self.pane, errMsg, 'Error', wx.OK | wx.ICON_ERROR)
        ##     dlg.ShowModal()
        ##     dlg.Destroy()
        self.__xeqDraw()

    def __str__(self):
        return self.title

if __name__ == "__main__":
    dummy = DALayerImage(None, 1.0, None)
