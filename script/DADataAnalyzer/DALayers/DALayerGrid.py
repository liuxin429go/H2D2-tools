# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx

from CTCommon.FEMesh import FE1DRegularGrid, FE2DRegularGrid
from . import DALayer
from . import DADlgParamMesh
from IPImageProcessor import IPRaster

RegGrids = (FE1DRegularGrid, FE2DRegularGrid)

class DALayerGrid(DALayer.DALayer):
    count = 0

    def __init__(self, page, field):
        super(DALayerGrid, self).__init__()
        self.pane = page
        self.grid = field.getGrid()

        self.fig  = self.pane.get_figure()
        self.ax   = self.pane.get_axes()

        DALayerGrid.count += 1
        if isinstance(self.grid, RegGrids):
            self.title = 'Regular Grid #%03i' % DALayerGrid.count
        else:
            self.title = 'Grid #%03i @ %s' % (DALayerGrid.count, str(field.grid))
        self.drwSkin = True
        self.drwElem = True
        self.drwNode = False
        self.drwNodeNumber = False
        self.drwElemNumber = False
        self.marker = 'pixel'
        self.elem_color = '#8080FF'
        self.node_color = '#00DFDF'
        self.elnb_color = '#000000'
        self.nonb_color = '#000000'

    def genIpFig(self):
        if self.drwSkin:
            m = self.grid.genSkin(doChain = False)
        else:
            m = self.grid
            
        self.items = []
        if m.isSubMesh():
            if self.drwElem:
                cs = self.ax.drawMesh(m, color = self.elem_color)
                self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.SUB_MESH, 'Sub-grid') )
            if self.drwNode:
                cs = self.ax.drawNodesMesh(m, marker = DADlgParamMesh.markers[self.marker], color = self.node_color)
                self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.NODES, 'Sub-grid: nodes') )
            if self.drwNodeNumber:
                cs = self.ax.drawNodeNumbersMesh(m, color = self.nonb_color)
                self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.TEXT, 'Sub-grid: node numbers') )
            if self.drwElemNumber:
                cs = self.ax.drawElemNumbersMesh(m, color = self.elnb_color)
                self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.TEXT, 'Sub-grid: element numbers') )
        else:
            if self.drwElem:
                cs = self.ax.drawMesh(m, color = self.elem_color)
                self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.MESH, 'Grid') )
            if self.drwNode:
                cs = self.ax.drawNodesMesh(m, marker = DADlgParamMesh.markers[self.marker], color = self.node_color)
                self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.NODES, 'Nodes') )
            if self.drwNodeNumber:
                cs = self.ax.drawNodeNumbersMesh(m, color = self.nonb_color)
                self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.TEXT, 'Node numbers') )
            if self.drwElemNumber:
                cs = self.ax.drawElemNumbersMesh(m, color = self.elnb_color)
                self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.TEXT, 'Element numbers') )
        self.pane.add_layer(self)

    def __xeqDraw(self):
        if self.items:
            self.pane.del_layer(self)
        self.genIpFig()
        self.pane.redraw()

    def draw(self):
        dlg = DADlgParamMesh.DADlgParamMesh(self.pane)
        dlg.set_title (self.title)
        dlg.set_checks(self.drwSkin, self.drwElem, self.drwNode, self.drwElemNumber, self.drwNodeNumber)
        dlg.set_marker(self.marker)
        dlg.set_element_color(self.elem_color)
        dlg.set_node_color   (self.node_color)
        dlg.set_element_number_color(self.elnb_color)
        dlg.set_node_number_color   (self.nonb_color)

        errMsg = ''
        if dlg.ShowModal() == wx.ID_OK:
            self.title = dlg.title
            self.drwSkin, self.drwElem, self.drwNode, self.drwElemNumber, self.drwNodeNumber = dlg.options
            self.marker = dlg.marker
            self.elem_color = dlg.element_color
            self.node_color = dlg.node_color
            self.elnb_color = dlg.element_number_color
            self.nonb_color = dlg.node_number_color
            try:
                self.__xeqDraw()
            except Exception as e:
                import traceback
                errMsg = '\n'.join( ('Drawing the skin of a skin/L2 mesh?', str(e), traceback.format_exc()) )

        dlg.Destroy()
        if errMsg:
            dlg = wx.MessageDialog(self.pane, errMsg, 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()

    def __str__(self):
        return self.title

if __name__ == "__main__":
    dummy = DALayerGrid(None, 1.0, None)
