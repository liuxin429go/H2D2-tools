# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import logging
import wx

from . import DALayer
from . import DADlgParamGridQuality
from . import DANavigator
from IPImageProcessor import IPRaster
from IPImageProcessor.IPColorBar import IPDraggableColorBarWithFrame
from CTCommon import FEMeshMetric
try:
    from CTCommon.FEMeshMetricKind    import ELEMENT_QUALITY_METRICS as EQM
except ImportError:
    from CTCommon.FEMeshMetricKind_pp import ELEMENT_QUALITY_METRICS as EQM

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Layer.GridQuality")

class DALayerGridQuality(DALayer.DALayer):
    count = 0

    def __init__(self, page, field):
        super(DALayerGridQuality, self).__init__()
        self.pane  = page
        self.field = field
        self.grid  = field.getGrid()
        self.data  = None

        self.fig   = self.pane.get_figure()
        self.ax    = self.pane.get_axes()
        self.nav   = None

        DALayerGridQuality.count += 1
        self.title = 'Grid Quality #%03i' % DALayerGridQuality.count
        self.crtrn = EQM.aspectRatio
        self.vmin, self.vmax, self.nval = (0.0, 1.0, 21)
        self.cmap, self.cmin, self.cmax = ('jet', 0.0, 1.0)
        self.iso_line, self.log_scale, self.open_top, self.open_btm, self.hide_bar = (False, False, False, False, True)

        self.min0, self.max0, self.nvl0 = self.vmin, self.vmax, self.nval
        self.cmap0, self.cmin0, self.cmax0 = self.cmap, self.cmin, self.cmax

    def genIpFig(self):
        kwargs = {}
        kwargs.update( IPRaster.IPAxes.genKwLimits(self.vmin, self.vmax, self.nval, self.log_scale) )
        kwargs.update( IPRaster.IPAxes.genKwCmap  (self.cmap, self.cmin, self.cmax) )

        wx.BeginBusyCursor()
        try:
            datas = FEMeshMetric.getMeshMetrics(self.grid, [self.crtrn])
            self.data = datas[0]
        except Exception as e:
            print(e)
            pass
        finally:
            wx.EndBusyCursor()
        
        self.items = []
        if self.iso_line:
            cs = self.ax.drawContourLine(self.grid, self.data, **kwargs)
            self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.CONTOUR_LINE, 'Isoline') )
        else:
            kwargs.update( IPRaster.IPAxes.genKwExtend(self.open_top, self.open_btm) )
            cs = self.ax.drawContourFill(self.grid, self.data, **kwargs)
            self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.CONTOUR_FILL, 'Isovalue') )
        if not self.hide_bar:
            cb = IPDraggableColorBarWithFrame(cs, self.ax, 
                                              fraction=0.05, 
                                              ticks=kwargs['levels'], 
                                              format='%.3e', 
                                              label=self.title, 
                                              keyAction=False)
            self.items.append( DALayer.DALayerItemCB(cb, 'Colorbar') )
        self.pane.add_layer(self)

    def __xeqDraw(self):
        if self.items:
            self.pane.del_layer(self)
        self.genIpFig()
        self.pane.redraw()

    def draw(self):
        dlg = DADlgParamGridQuality.DADlgParamGridQuality(self.pane)
        dlg.set_title (self.title)
        dlg.set_values(self.crtrn,
                       self.vmin, self.vmax, self.nval, self.cmap,  self.cmin,  self.cmax,
                       self.min0, self.max0, self.nvl0, self.cmap0, self.cmin0, self.cmax0)
        dlg.set_checks(self.iso_line, self.log_scale, self.open_top, self.open_btm, self.hide_bar)

        if dlg.ShowModal() == wx.ID_OK:
            self.title = dlg.title
            self.crtrn, self.vmin, self.vmax, self.nval, self.cmap, self.cmin, self.cmax = dlg.values
            self.iso_line, self.log_scale, self.open_top, self.open_btm, self.hide_bar = dlg.options
            self.__xeqDraw()

        dlg.Destroy()

    def onNavigatorChange(self, x, y, v):
        self.pane.doCenter(x, y)

    def onNavigatorClose(self):
        self.nav = None

    def hasNavigator(self):
        return True
        
    def navigate(self):
        if not self.nav:
            self.nav = DANavigator.DANavigator(self.pane, on_change=self.onNavigatorChange, on_close=self.onNavigatorClose)
            data = self.data
            grid = self.grid
            self.nav.setData(grid, data)

    def __str__(self):
        return self.title

if __name__ == "__main__":
    dummy = DALayerGridQuality(None, 1.0, None)
