# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2018
# --- Institut National de la Recherche Scientifique (INRS)
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import math

import numpy as np
from pubsub import pub
from matplotlib.lines         import Line2D
from matplotlib.backend_bases import MouseButton
import wx

if __name__ == '__main__':
    import os
    import sys
    selfDir = os.path.dirname (os.path.abspath(__file__))
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)
    supPath = os.path.normpath(os.path.join(supPath, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

from .DALayer            import DALayer, DALayerItemCS
from .DADlgParamMeshEdit import DADlgParamMeshEdit, markers
from DAFrameGrid         import DAFrameGrid
from IPImageProcessor    import IPRaster
from CTCommon.FEMeshIO   import FEMeshH2D2T6LasT3IO
from CTCommon            import FEMesh

from .external           import tripy

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Layer.GridEdit")

class DACSEdgeEntry:
    def __init__(self, ele=None, isd=-1):
        self.ele = ele
        self.isd = isd

    def __str__(self):
        return '%s on %s' % (self.ele, self.isd)

    def __getitem__(self, i):
        if i == 0: return self.ele
        if i == 1: return self.isd
        raise IndexError

    def __eq__(self, other):
        return self.ele == other.ele and self.isd == other.isd

    def hasEle(self, ele):
        return self.ele == ele

    def hasSide(self, isd):
        return self.isd == isd

    def isOK(self):
        if self.ele and self.isd >= 0: return True
        if self.ele is None and self.isd == -1: return True
        return False

    def isEmpty(self):
        return self.ele is None

    def setValues(self, ele=None, isd=-1):
        self.ele = ele
        self.isd = isd

class DACSEdge:
    def __init__(self, ics=-1, ele0=None, isd0=-1, ele1=None, isd1=-1):
        self.ics     = ics
        self.entries = [ DACSEdgeEntry(ele0, isd0), DACSEdgeEntry(ele1, isd1) ]

    def __str__(self):
        return '\n'.join(('csedge: %d' % self.ics,
                          '   %s' % str(self.entries[0]),
                          '   %s' % str(self.entries[1])))

    def __iter__(self):
        validEntries = [ it for it in self.entries if not it.isEmpty() ]
        for it in validEntries:
            yield it

    def __getitem__(self, i):
        return self.entries[i]

    def append(self, ele, isd):
        assert self.entries[1].isEmpty()
        if self.entries[0].isEmpty(): return self.entries[0].setValues(ele, isd)
        if self.entries[1].isEmpty(): return self.entries[1].setValues(ele, isd)
        raise ValueError

    def reset(self):
        self.ics = -1
        self.entries[0].setValues()
        self.entries[1].setValues()

    def replace(self, eleOld, isdOld, eleNew, isdNew):
        if self.entries[0].hasEle(eleOld): return self.entries[0].setValues(eleNew, isdNew)
        if self.entries[1].hasEle(eleOld): return self.entries[1].setValues(eleNew, isdNew)
        raise ValueError

    def hasEle(self, ele):
        if self.entries[0].hasEle(ele): return True
        if self.entries[1].hasEle(ele): return True
        return False

    def isOK(self):
        if len(self.entries) != 2: return False
        i0OK = self.entries[0].isOK()
        i1OK = self.entries[1].isOK()
        return (not i1OK) or i0OK

    def isEmpty(self):
        return self.entries[0].isEmpty() and self.entries[1].isEmpty()

    def getICS(self):
        return self.ics

    def getOtherElement(self, ele):
        if self.entries[0].hasEle(ele): return self.entries[1][0]
        if self.entries[1].hasEle(ele): return self.entries[0][0]
        raise ValueError

    def getSize(self):
        if not self.entries[1].isEmpty(): return 2
        if not self.entries[0].isEmpty(): return 1
        return 0

class DALayerGridEditor(DALayer):
    DEBUG_VALIDATE = True
    DEBUG_PRINT    = True

    TOLERANCE_FLAT = 5.0    # in deg.
    count = 0

    def __init__(self, page, field):
        super(DALayerGridEditor, self).__init__()
        self.pane = page
        self.prnt = field.getGrid()
        self.grid = self.__cloneGrid(self.prnt)

        self.fig       = self.pane.get_figure()
        self.ax        = self.pane.get_axes()
        self.canvas    = self.fig.canvas
        self.toolbar   = self.pane.get_toolbar()
        self.oldCursor = None

        self.__create_menu()
        #self.pane.Bind(wx.EVT_MENU, self.onMnuNodeMove,self.mnuNodeMove)
        self.pane.Bind(wx.EVT_MENU, self.onMnuNodeCenter,self.mnuNodeCenter)
        self.pane.Bind(wx.EVT_MENU, self.onMnuNodeDelete,self.mnuNodeDelete)
        self.pane.Bind(wx.EVT_MENU, self.onMnuEdgeSwap,  self.mnuEdgeSwap)
        self.pane.Bind(wx.EVT_MENU, self.onMnuEdgeSplit, self.mnuEdgeSplit)
        # Menu are special beast. The event look-up is different.
        # It goes from menu to top-frame
        frame = wx.GetTopLevelParent(self.pane)
        frame.Bind(wx.EVT_MENU_CLOSE, self.onMnuNodeClose)
        frame.Bind(wx.EVT_MENU_CLOSE, self.onMnuNodeClose)

        DALayerGridEditor.count += 1
        self.title = 'Grid editor #%03i' % DALayerGridEditor.count
        self.drwElem = True
        self.marker = 'circle'
        self.elem_color = '#8080FF'
        self.node_color = '#00DFDF'

        self.locked = False
        self.ptIdx = -1
        self.ptXY  = None
        self.edIdx = -1
        self.edXY  = None
        self.overlay = Line2D([], [], linestyle='solid', marker='s', color='r')
        self.csEdg = None
        self.csVtx = None
        self.dirty = False      # True if nodes/elem added/removed
        self.modif = False      # Grid modified

    def __create_menu(self):
        self.mnuNode = wx.Menu()
        #self.mnuNodeMove   = wx.MenuItem(self.mnuNode, wx.ID_ANY, "Move",   "Move the node with the mouse", wx.ITEM_NORMAL)
        self.mnuNodeCenter = wx.MenuItem(self.mnuNode, wx.ID_ANY, "Center", "Center the node with the surrounding nodes", wx.ITEM_NORMAL)
        self.mnuNodeDelete = wx.MenuItem(self.mnuNode, wx.ID_ANY, "Delete", "Delete the node, coarsening the grid", wx.ITEM_NORMAL)
        #self.mnuNode.Append(self.mnuNodeMove)
        self.mnuNode.Append(self.mnuNodeCenter)
        self.mnuNode.Append(self.mnuNodeDelete)

        self.mnuEdge = wx.Menu()
        self.mnuEdgeSwap  = wx.MenuItem(self.mnuEdge, wx.ID_ANY, "Swap",   "Swap the diagonal", wx.ITEM_NORMAL)
        self.mnuEdgeSplit = wx.MenuItem(self.mnuEdge, wx.ID_ANY, "Refine", "Refine the edge, adding a node", wx.ITEM_NORMAL)
        self.mnuEdge.Append(self.mnuEdgeSwap)
        self.mnuEdge.Append(self.mnuEdgeSplit)

    def __del__(self):
        try:
            canvas = self.canvas
            canvas.mpl_disconnect('motion_notify_event')
            canvas.mpl_disconnect('button_press_event')
            canvas.mpl_disconnect('button_release_event')
            canvas.mpl_disconnect('pick_event')
            canvas.mpl_disconnect('key_press_event')
            canvas.mpl_disconnect('key_release_event')
        except:
            pass

    def __str__(self):
        return self.title

    def __cloneGrid(self, grid):
        files     = grid.getFilenames()
        grid2proj = grid.getProjectionFromGridToProj()
        proj2grid = grid.getProjectionFromProjToGrid()

        clone = DAFrameGrid(files=None, grid2proj=grid2proj, proj2grid=proj2grid)

        reader = FEMeshH2D2T6LasT3IO(files)
        reader.readMesh(clone)
        fltr = FEMesh.SubGridFilterOnBBox(keep=clone.getBbox())
        return clone.genSubGrid(fltr)

    def __connectCS2Grid(self):
        """
        Build self.edges_cs2ef:
        For each CS edge id, contains a list of 2 items: tuple (elem, side number) or None
        Build self.edges_ef2cs:
        For each FE element, contains a list of 3 cs id
        """
        XE, YE = self.csEdg.get_data()          # [x00, x01, d0, x10, x11, d1, x20, x21, d2 ...]
        XV, YV = self.csVtx.get_data()          # [x0, x1, x2, ...]
        dtxy = [('x', XE.dtype), ('y', YE.dtype)] # numpy dtype

        # ---  Edge data as [ [[x0, y0] [x1,y1]] ... ]
        XE = np.reshape(XE, (-1,3))
        YE = np.reshape(YE, (-1,3))
        XYE0 = np.stack((XE[:,0], YE[:,0]), axis=-1)
        XYE1 = np.stack((XE[:,1], YE[:,1]), axis=-1)
        XYE  = np.stack((XYE0, XYE1), axis=1)

        # ---  Vertex data as [ (x1, y1) (x2, y2) ... ]
        XYV = np.stack((XV, YV), axis=-1)   # As a 2D array
        XYV = np.ravel(XYV)                 # Flatten
        XYVv = XYV.view(dtype=dtxy)         # View

        # ---  Sorting index
        JV = np.argsort(XYVv, order=('x', 'y'), axis=-1)
        # print(XYVv[JV])

        # ---  Get edge connectivities
        dtij = [('i0', np.int64), ('i1', np.int64)]
        EC = np.zeros(XYE.shape[0], dtype=dtij)
        for ie, xye in enumerate(XYE):
            xyv = xye.view(dtype=dtxy)
            i0, i1 = np.searchsorted(XYVv, xyv, sorter=JV)
            i0, i1 = JV[i0], JV[i1]
            EC[ie] = (min(i0, i1), max(i0, i1))
        IV = np.argsort(EC, order=('i0', 'i1'), axis=-1)

        # ---  Make connection with grid
        self.edges_cs2ef = [ DACSEdge(i) for i in range(XYE.shape[0]) ]
        ec = np.zeros(1, dtype=dtij)
        for ele in self.grid.iterElements():
            for iside in range(3):
                no0, no1 = ele.getEdgeConnectivities(iside)
                ec[0] = (min(no0, no1), max(no0, no1))
                i0 = np.searchsorted(EC, ec, sorter=IV)[0]
                self.edges_cs2ef[IV[i0]].append(ele, iside)

        # ---  Make reverse connection
        self.edges_ef2cs = [[-1,-1,-1] for i in range(self.grid.getNbElements()) ]
        for ics, csEdge in enumerate(self.edges_cs2ef):
            for ele, isd in csEdge:
                ie = ele.getLocalIndex()
                self.edges_ef2cs[ie][isd] = ics

        self.__debug_validate()

    def __debug_print(self):
        if not DALayerGridEditor.DEBUG_PRINT: return
        ncs = len(self.edges_cs2ef)
        nel = len(self.edges_ef2cs)
        print('')
        print('--------------')
        print('edges_cs2ef: len=%d' % ncs)
        for ics, items in enumerate(self.edges_cs2ef):
            if items:
                print('  ', ics, ' '.join([ '(%s, %d)' % (str(i),s) for i,s in items ]))
            else:
                print('  ', ics, 'Deleted entry')

        print('edges_ef2cs: len=%d' % nel)
        for iel, items in enumerate(self.edges_ef2cs):
            if items:
                print('  ', iel, items)
            else:
                print('  ', iel, 'Deleted entry')
        print('--------------')

    def __debug_validate(self):
        self.__debug_print()
        if not DALayerGridEditor.DEBUG_VALIDATE: return
        inError = 0
        ncs = len(self.edges_cs2ef)
        nel = len(self.edges_ef2cs)
        # ---  Loop on edge ics
        print('Validate: check connection cs2ef with ef2cs')
        for ics, csEdge in enumerate(self.edges_cs2ef):
            icsOK = True
            # ---  Check for edge type
            if icsOK and not isinstance(csEdge, DACSEdge):
                print('Assertion error: cs2ef edge type should be a DACSEdge got %s' % type(csEdge))
                icsOK = False
            if icsOK and ics != csEdge.isEmpty(): continue
            if icsOK and ics != csEdge.getICS():
                print('Assertion error: cs2ef index %d does not fit with edge ics %d' % (ics, csEdge.getICS()))
                icsOK = False
            if icsOK and not csEdge.isOK():
                print('Assertion error: invalid csEdge')
                print("   ", csEdge)
                icsOK = False
            # ---  Loop over entries
            for ele, isid in csEdge:
                # ---  Check if element exist
                if icsOK and isinstance(ele, FEMesh.FEElementDeleted):
                    print('Assertion error: element deleted %s' % (ele))
                    icsOK = False
                if icsOK:
                    iel = ele.getLocalIndex()
                    if iel < 0 or iel >= nel:
                        print('Assertion error: iel not in [0, %d[ : got %d' % (nel, iel))
                        icsOK = False
                # --- Check if ef2cs item is OK
                if icsOK and not isinstance(self.edges_ef2cs[iel], (list, type(None))):
                    print('Assertion error: ef2cs item should be in (list, None) got %s' % self.edges_ef2cs[iel])
                    icsOK = False
                if icsOK and self.edges_ef2cs[iel] is None: continue
                if icsOK and len(self.edges_ef2cs[iel]) != 3:
                    print('Assertion error: ef2cs item list len should be 3, got %d' % len(self.edges_ef2cs[iel]))
                    icsOK = False
                # --- Check if element in ef2cs is connected to ics
                if icsOK and self.edges_ef2cs[iel][isid] != ics:
                    print('Assertion error: ics not found')
                    print(ics, ' '.join([ '(%s, %d)' % (str(i),s) for i,s in csEdge ]))
                    print('   ', iel, self.edges_ef2cs[iel])
                    icsOK = False
            if not icsOK:
                inError += 1

        # ---  Loop on edge ics
        print('Validate: check connection ef2cs with cs2ef')
        for iel, items in enumerate(self.edges_ef2cs):
            eleOK = True
            ele = self.grid.getElement(iel)
            if eleOK and items and isinstance(ele, FEMesh.FEElementDeleted):
                print('Assertion error: element is deleted but ef2cs entry is %s' % items)
                eleOK = False
            if eleOK and items is None: continue
            if eleOK and len(self.edges_ef2cs[iel]) != 3:
                print('Assertion error: ef2cs item list len should be 3, got %d' % len(self.edges_ef2cs[iel]))
                eleOK = False
            # ---  Check connection to cs2ef
            for _, ics in enumerate(items):
                if eleOK and ics < 0 or ics > ncs:
                    print('Assertion error: ics outof bound %d for %s' % (ics, ele))
                    eleOK = False
                csEdge = self.edges_cs2ef[ics]
                # ---  Check for list size
                if eleOK and not isinstance(csEdge, DACSEdge):
                    print('Assertion error: cs2ef entry should be a DACSEdge got %s' % type(csEdge))
                    eleOK = False
                if eleOK and not csEdge.isOK():
                    print('Assertion error: invalid csEdge')
                    print("   ", csEdge)
                    icsOK = False
                if eleOK and csEdge.isEmpty():
                    print('Assertion error: csEdge is empty')
                    print("   ", csEdge)
                    icsOK = False
                if eleOK and not csEdge.hasEle(ele):
                    print('Assertion error: ele not found in cs2ef')
                    print('   ', ele, items)
                    print('   ', str(csEdge))
                    eleOK = False
            if not eleOK:
                inError += 1

        # ---  Loop on elements
        for iel, ele in enumerate(self.grid.iterElements()):
            eleOK = True

            if isinstance(ele, FEMesh.FEElementDeleted):
                if eleOK and self.edges_ef2cs[iel] is not None:
                    print('Assertion error: ef2cs of deleted element shall be None: %s' % ele)
                    eleOK = False
                continue

            items = self.edges_ef2cs[iel]
            if eleOK and not isinstance(items, list):
                print('Assertion error: ef2cs entry shall be a list: %s %s' % (ele, items))
                eleOK = False
            if eleOK and len(items) != 3:
                print('Assertion error: len(items): expected in 3, got %d' % len(items))
                print('   ', ele, items)
                eleOK = False
            for isd, ics in enumerate(items):
                if eleOK and ics < 0 or ics > ncs:
                    print('Assertion error: ics value ou of bound %d for ele %s' % (ics, ele))
                    eleOK = False
                if eleOK and not self.edges_cs2ef[ics].hasEle(ele):
                    print(ics, ' '.join([ '(%s, %d)' % (str(i),s) for i,s in items ]))
                    print('   ', iel, self.edges_ef2cs[iel])
            if not eleOK:
                inError += 1

        assert inError == 0

    def __getStarXY(self, ptId):
        """
        Return the star, other nodes of the elements
        having ptId as vertex:
            X,Y: coordinates of the nodes
        """
        XV, YV = self.csVtx.get_data()
        XE, YE = self.csEdg.get_data()
        x0, y0 = XV[ptId], YV[ptId]
        IX = (XE == x0)
        IY = (YE == y0)
        I = np.where( np.all((IX, IY), axis=0) )[0]
        IV = [i+1 if i%3 == 0 else i-1 for i in I]
        return XE[IV], YE[IV]

    def __getStarIdx(self, ptId):
        """
        Return the star, other nodes of the elements
        having ptId as vertex:
            IV:   indices of the vertices
            IE:   indices of the edges
        """
        XV, YV = self.csVtx.get_data()
        XE, YE = self.csEdg.get_data()
        x0, y0 = XV[ptId], YV[ptId]
        IX = (XE == x0)
        IY = (YE == y0)
        I = np.where( np.all((IX, IY), axis=0) )[0]
        # les indices sont faux
        # Il faut recherche les coor dans XV
        IV = None # [i+1 if i%3 == 0 else i-1 for i in I]
        IE = I // 3
        return IV, IE

    def __genIpFig(self):
        kwargs = {}
        kwargs['marker'] = markers[self.marker]
        kwargs['markerfacecolor']  = self.node_color
        kwargs['color']  = self.elem_color
        kwargs['picker'] = True
        kwargs['pickradius'] = 5
        self.csEdg, self.csVtx = self.ax.drawMesh(self.grid, **kwargs)
        self.items = []
        self.items.append( DALayerItemCS(self.csEdg, IPRaster.IPPlotType.MESH,  'Edges') )
        self.items.append( DALayerItemCS(self.csVtx, IPRaster.IPPlotType.NODES, 'Nodes') )
        self.pane.add_layer(self)

    def __xeqDraw(self):
        if self.items:
            self.pane.del_layer(self)
        self.__genIpFig()

        canvas = self.canvas
        canvas.mpl_connect('motion_notify_event', self.onMouseMove)
        canvas.mpl_connect('button_press_event',  self.onMouseClick)
        canvas.mpl_connect('button_release_event',self.onMouseUnclick)
        canvas.mpl_connect('scroll_event',        self.onMouseScroll)
        canvas.mpl_connect('pick_event',          self.onPick)
        canvas.mpl_connect('key_press_event',     self.onKeyPress)
        canvas.mpl_connect('key_release_event',   self.onKeyRelease)

        self.pane.redraw()

    def draw(self):
        dlg = DADlgParamMeshEdit(self.pane)
        dlg.set_title (self.title)
        dlg.set_marker(self.marker)
        dlg.set_element_color(self.elem_color)
        dlg.set_node_color   (self.node_color)

        errMsg = ''
        if True: # dlg.ShowModal() == wx.ID_OK:
            self.title  = dlg.title
            self.marker = dlg.marker
            self.elem_color = dlg.element_color
            self.node_color = dlg.node_color
            try:
                self.__xeqDraw()
                self.__connectCS2Grid()
            except Exception as e:
                import traceback
                errMsg = '\n'.join( ('Drawing the skin of a skin/L2 mesh?', str(e), traceback.format_exc()) )

        dlg.Destroy()
        if errMsg:
            dlg = wx.MessageDialog(self.pane, errMsg, 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()

    def isModified(self):
        return self.modif

    def commit(self):
        if not self.modif: return
        if self.dirty:
            errMsg = '\n'.join( [
                "Edited grid is no more compatible with original grid.",
                "Node or element have been added/removed.",
                "",
                "Use save instead of commit."])
            dlg = wx.MessageDialog(self.pane, errMsg, 'Warning', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
        else:
            X, Y = self.csVtx.get_data()
            for i, (x, y) in enumerate(zip(X, Y)):
                nodL = self.grid.getNode(i)
                nodG = self.prnt.getNode(nodL.getGlobalIndex())
                nodG.setCoordinates(x,y)
            self.prnt.centerEdges()
            self.modif = False

    def getSourceGrid(self):
        return self.prnt

    def getGrid(self):
        if self.dirty:
            wx.BeginBusyCursor()
            try:
                t3Grid  = self.grid.genCompactGrid()
                t6lGrid = t3Grid.genT6LGrid()
                return t6lGrid
            finally:
                wx.EndBusyCursor()
        else:
            self.commit()
            return self.prnt

    def onMouseClickLeft(self, event):
        """
        Draw the overlay
        """
        # print('DALayerGridEditor.onMouseClickLeft', event)
        if not self.locked: return
        if self.ptIdx < 0: return
        try:
            XS, YS = self.__getStarXY(self.ptIdx)
            x,  y  = self.ptXY
            X = [ (x, xs) for xs in XS ]
            Y = [ (y, ys) for ys in YS ]
            X = np.asarray(X, dtype=np.float64)
            Y = np.asarray(Y, dtype=np.float64)
            X = np.insert(X, 2, np.nan, axis=1)
            Y = np.insert(Y, 2, np.nan, axis=1)
            X = np.ravel(X)
            Y = np.ravel(Y)
            self.overlay.set_data(X, Y)
            self.ax.add_artist(self.overlay)
            event.canvas.draw()
        except Exception as e:
            print(str(e))
            self.locked = False

    def onMouseClickRight(self, event):
        """
        Draw the overlay
        Display pop-up menu
        """
        # print('DALayerGridEditor.onMouseClickRight', event)
        # print('DALayerGridEditor.onMouseClickRight', event.xdata, event.ydata)
        try:
            if self.ptIdx != -1:
                x, y = self.ptXY
                self.overlay.set_data([x], [y])
                self.ax.add_artist(self.overlay)
                event.canvas.draw()
                self.locked = False
                self.pane.PopupMenu(self.mnuNode)
            elif self.edIdx != -1:
                xy1, xy2 = self.edXY
                X = [ (xy1[0], xy2[0]) ]
                Y = [ (xy1[1], xy2[1]) ]
                X = np.asarray(X, dtype=np.float64)
                Y = np.asarray(Y, dtype=np.float64)
                X = np.insert(X, 2, np.nan, axis=1)
                Y = np.insert(Y, 2, np.nan, axis=1)
                X = np.ravel(X)
                Y = np.ravel(Y)
                self.overlay.set_data(X, Y)
                self.ax.add_artist(self.overlay)
                event.canvas.draw()
                self.locked = False
                self.pane.PopupMenu(self.mnuEdge)
        except Exception as e:
            print(str(e))
            pass

    def onMouseClick(self, event):
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if event.xdata  is None: return
        if event.button == MouseButton.LEFT:
            return self.onMouseClickLeft(event)
        if event.button == MouseButton.RIGHT:
            return self.onMouseClickRight(event)

    def onMouseUnclickLeft(self, event):
        if not self.locked: return
        # print('DALayerGridEditor.onMouseUnclickLeft', event.xdata, event.ydata)
        try:
            self.overlay.set_data([], [])
            self.overlay.remove()
        except ValueError:
            pass

        X, Y = self.csVtx.get_data()
        x0 = X[self.ptIdx]
        y0 = Y[self.ptIdx]
        X[self.ptIdx] = event.xdata
        Y[self.ptIdx] = event.ydata
        self.csVtx.set_data(X, Y)

        X, Y = self.csEdg.get_data()
        IX = (X == x0)
        IY = (Y == y0)
        I = np.where( np.all((IX, IY), axis=0) )
        X[I] = event.xdata
        Y[I] = event.ydata
        self.csEdg.set_data(X, Y)
        #action = Action(ptsMove= [(self.ptIdx, event.xdata, event.ydata),
        #                edgMove= [ (I, event.xdata, event.ydata) for i in I])

        event.canvas.draw()
        self.ptIdx = -1
        self.locked = False
        self.modif  = True

    def onMouseUnclick(self, event):
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if event.xdata  is None: return
        if event.button == MouseButton.LEFT:
            return self.onMouseUnclickLeft(event)
        else:
            pass

    def onMouseMove(self, event):
        """
        Update star
        """
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if event.xdata  is None: return
        if not self.locked: return
        # print('DALayerGridEditor.onMouseMove', event.xdata, event.ydata)
        x, y = event.xdata, event.ydata
        X, Y = self.overlay.get_data()
        X[0::3] = x
        Y[0::3] = y
        self.overlay.set_data(X, Y)
        event.canvas.draw()

    def onMouseScroll(self, event):
        """
        Reset the pick event generated by scroll
        """
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        self.ptIdx = -1
        self.edIdx = -1
        self.locked = False

    def onKeyPress(self, event):
        """
        Le changement de curseur est OK, mais la souris reste
        prise par zoom/pan
        """
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if event.key == 'ctrl+control' and not self.oldCursor:
            print('DALayerGridEditor.onKeyPress', event.key)
            self.oldCursor = event.canvas.GetCursor()
            newCursor = wx.Cursor(wx.CURSOR_ARROW)
            event.canvas.SetCursor(newCursor)
            self.toolbar.saveState()
            self.toolbar.resetState()

    def onKeyRelease(self, event):
        if event.canvas is not self.canvas: return
        if event.inaxes is None: return
        if event.key == 'control':
            print('DALayerGridEditor.onKeyRelease', event.key)
            if self.oldCursor:
                event.canvas.SetCursor(self.oldCursor)
                self.oldCursor = None
                self.toolbar.restoreState()

    def onMnuEdgeClose(self, event):
        """
        A menu was used, so deactivate move
        """
        # ---  Erase overlay
        try:
            self.overlay.set_data([], [])
            self.overlay.remove()
            self.canvas.draw()
        except ValueError:
            pass
        # ---  Reset locked state
        self.locked = False

    def onMnuEdgeSplit1(self, event):
        print('In onMnuEdgeSplit1: %s', event)
        assert self.edges_cs2ef[self.edIdx].getSize() == 1
        self.__debug_validate()

        try:
            self.overlay.set_data([], [])
            self.overlay.remove()
        except ValueError:
            pass

        edIdx = self.edIdx
        assert self.edges_cs2ef[edIdx][1].isEmpty()

        # ---  Get splitted element
        ele0, isd0 = self.edges_cs2ef[edIdx][0]
        iel0 = ele0.getLocalIndex()

        # ---  Get surrounding nodes
        isd1 = (isd0+1) % 3
        isd2 = (isd0+2) % 3
        ino0, ino1 = ele0.getEdgeConnectivities(isd0)
        ino1, ino2 = ele0.getEdgeConnectivities(isd1)

        # --- Get all ics
        #ics0 = edIdx
        ics1 = self.edges_ef2cs[iel0][isd1]
        ics2 = self.edges_ef2cs[iel0][isd2]

        # ---  Get center position
        XV, YV = self.csVtx.get_data()
        xy0 = XV[ino0], YV[ino0]
        xy1 = XV[ino1], YV[ino1]
        xy2 = XV[ino2], YV[ino2]
        xc = 0.5*(xy0[0] + xy1[0])
        yc = 0.5*(xy0[1] + xy1[1])

        # ---  Modify FE grid
        nodes = self.grid.addNodes( (xc,), (yc,) )
        inoc = nodes[0].getLocalIndex()
        elems = self.grid.addElements([(inoc, ino1, ino2),
                                       (inoc, ino2, ino0)])
        self.grid.deleteElement(ele0)

        # ---  Update edges structure
        ncs = len(self.edges_cs2ef)
        self.edges_cs2ef.append( DACSEdge(ncs+0, elems[0], 0) )
        self.edges_cs2ef.append( DACSEdge(ncs+1, elems[0], 2, elems[1], 0) )
        self.edges_cs2ef.append( DACSEdge(ncs+2, elems[1], 2) )
        self.edges_cs2ef[ics1].replace(ele0, isd1, elems[0], 1)
        self.edges_cs2ef[ics2].replace(ele0, isd2, elems[1], 1)
        self.edges_cs2ef[edIdx].reset()
        #
        self.edges_ef2cs.append( [ncs+0, ics1, ncs+1] )
        self.edges_ef2cs.append( [ncs+1, ics2, ncs+2] )
        self.edges_ef2cs[iel0] = None

        # ---  Modify vertex data
        XV, YV = self.csVtx.get_data()
        XV = np.append(XV, xc)
        YV = np.append(YV, yc)

        # ---  Modify edge data
        XE, YE = self.csEdg.get_data()
        XE = np.reshape(XE, (-1,3))
        YE = np.reshape(YE, (-1,3))
        XE[edIdx,:] = np.nan
        YE[edIdx,:] = np.nan
        XE = np.append(XE, [(xc, xy1[0], np.nan), (xc, xy2[0], np.nan), (xc, xy0[0], np.nan)] )
        YE = np.append(YE, [(yc, xy1[1], np.nan), (yc, xy2[1], np.nan), (yc, xy0[1], np.nan)] )
        XE = np.ravel(XE)
        YE = np.ravel(YE)

        # ---  Assign back vertex and edges
        self.csVtx.set_data((XV, YV))
        self.csEdg.set_data((XE, YE))

        self.canvas.draw()
        self.ptIdx = -1
        self.edIdx = -1
        self.locked = False
        self.modif  = True
        self.dirty  = True
        self.__debug_validate()

    def onMnuEdgeSplit2(self, event):
        print('In onMnuEdgeSplit2: %s', event)
        assert self.edges_cs2ef[self.edIdx].getSize() == 2
        self.__debug_validate()

        try:
            self.overlay.set_data([], [])
            self.overlay.remove()
        except ValueError:
            pass

        edIdx = self.edIdx

        # ---  Get cavity nodes
        ele0, isd0 = self.edges_cs2ef[edIdx][0]
        ele1, isd1 = self.edges_cs2ef[edIdx][1]
        iel0 = ele0.getLocalIndex()
        iel1 = ele1.getLocalIndex()
        ino0, ino2 = ele0.getEdgeConnectivities(isd0)
        ino2, ino3 = ele0.getEdgeConnectivities((isd0+1) % 3)
        ino_, ino1 = ele1.getEdgeConnectivities((isd1+1) % 3)
        if ino0 != ino_:
            print('ino != ino_')
            print(isd0, ele0)
            print(isd1, ele1)
        assert ino0 == ino_

        # ---  Get cavity ics
        ics1 = self.edges_ef2cs[iel1][(isd1+1) % 3]  # ino0, ino1
        ics2 = self.edges_ef2cs[iel1][(isd1+2) % 3]  # ino1, ino2
        ics3 = self.edges_ef2cs[iel0][(isd0+1) % 3]  # ino2, ino3
        ics4 = self.edges_ef2cs[iel0][(isd0+2) % 3]  # ino3, ino0

        # ---  Get center position
        XV, YV = self.csVtx.get_data()
        xy0 = XV[ino0], YV[ino0]
        xy1 = XV[ino1], YV[ino1]
        xy2 = XV[ino2], YV[ino2]
        xy3 = XV[ino3], YV[ino3]
        #xc = 0.25*(xy0[0] + xy1[0] + xy2[0] + xy3[0])
        #yc = 0.25*(xy0[1] + xy1[1] + xy2[1] + xy3[1])
        xc = 0.5*(xy0[0] + xy2[0])
        yc = 0.5*(xy0[1] + xy2[1])

        # ---  Modify FE grid
        nodes = self.grid.addNodes( (xc,), (yc,) )
        inoc = nodes[0].getLocalIndex()
        elems = self.grid.addElements([(inoc, ino0, ino1),
                                       (inoc, ino1, ino2),
                                       (inoc, ino2, ino3),
                                       (inoc, ino3, ino0)])
        self.grid.deleteElement(ele0)
        self.grid.deleteElement(ele1)

        # ---  Update edges structure
        ncs = len(self.edges_cs2ef)
        self.edges_cs2ef.append( DACSEdge(ncs+0, elems[0], 0, elems[3], 2) )
        self.edges_cs2ef.append( DACSEdge(ncs+1, elems[1], 0, elems[0], 2) )
        self.edges_cs2ef.append( DACSEdge(ncs+2, elems[2], 0, elems[1], 2) )
        self.edges_cs2ef.append( DACSEdge(ncs+3, elems[3], 0, elems[2], 2) )
        self.edges_cs2ef[ics1].replace(ele1, -1, elems[0], 1)
        self.edges_cs2ef[ics2].replace(ele1, -1, elems[1], 1)
        self.edges_cs2ef[ics3].replace(ele0, -1, elems[2], 1)
        self.edges_cs2ef[ics4].replace(ele0, -1, elems[3], 1)
        self.edges_cs2ef[edIdx].reset()
        #
        self.edges_ef2cs.append( [ncs+0, ics1, ncs+1] )
        self.edges_ef2cs.append( [ncs+1, ics2, ncs+2] )
        self.edges_ef2cs.append( [ncs+2, ics3, ncs+3] )
        self.edges_ef2cs.append( [ncs+3, ics4, ncs+0] )
        self.edges_ef2cs[iel0] = None
        self.edges_ef2cs[iel1] = None

        # ---  Modify vertex data
        XV, YV = self.csVtx.get_data()
        XV = np.append(XV, xc)
        YV = np.append(YV, yc)

        # ---  Modify edge data
        XE, YE = self.csEdg.get_data()          # [x00, x01, d0, x10, x11, d1, x20, x21, d2 ...]
        XE = np.reshape(XE, (-1,3))
        YE = np.reshape(YE, (-1,3))
        XE[edIdx,:] = np.nan
        XE[edIdx,:] = np.nan
        XE = np.append(XE, [(xc, xy0[0], np.nan), (xc, xy1[0], np.nan), (xc, xy2[0], np.nan), (xc, xy3[0], np.nan)] )
        YE = np.append(YE, [(yc, xy0[1], np.nan), (yc, xy1[1], np.nan), (yc, xy2[1], np.nan), (yc, xy3[1], np.nan)] )
        XE = np.ravel(XE)
        YE = np.ravel(YE)

        # ---  Assign back vertex and edges
        self.csVtx.set_data((XV, YV))
        self.csEdg.set_data((XE, YE))

        self.canvas.draw()
        self.ptIdx = -1
        self.edIdx = -1
        self.locked = False
        self.modif  = True
        self.dirty  = True
        self.__debug_validate()

    def onMnuEdgeSplit(self, event):
        print('In onMnuEdgeSplit: %s', event)
        edIdx = self.edIdx
        if   self.edges_cs2ef[edIdx].getSize() == 1:
            return self.onMnuEdgeSplit1(event)
        elif self.edges_cs2ef[edIdx].getSize() == 2:
            return self.onMnuEdgeSplit2(event)
        else:
            errMsg = '\n'.join(('Internal structure corrupted.',
                                'Edge detected with %d neighbours.' %  len(self.edges_cs2ef[edIdx])))
            raise RuntimeError(errMsg)

    def __getEdgeLength(self, ino0, ino1):
        x0, y0 = self.grid.getNode(ino0).getCoordinates()
        x1, y1 = self.grid.getNode(ino1).getCoordinates()
        dx = x1 - x0
        dy = y1 - y0
        return math.sqrt(dx*dx + dy*dy)

    def __getTriangleSurface(self, ino0, ino1, ino2):
        x0, y0 = self.grid.getNode(ino0).getCoordinates()
        x1, y1 = self.grid.getNode(ino1).getCoordinates()
        x2, y2 = self.grid.getNode(ino2).getCoordinates()
        y20 = y2 - y0
        x20 = x2 - x0
        y10 = y1 - y0
        x10 = x1 - x0
        jaco = (x10 * y20) - (x20 * y10)
        return 0.5*jaco

    def onMnuEdgeSwap(self, event):
        print('In onMnuEdgeSwap: %s', event)
        self.__debug_validate()

        try:
            self.overlay.set_data([], [])
            self.overlay.remove()
        except ValueError:
            pass

        errMsg = ""
        edIdx = self.edIdx
        if self.edges_cs2ef[edIdx].getSize() < 2:
            errMsg = "Not enough neighbours"

        # ---  Get cavity nodes
        if not errMsg:
            ele0, isd0 = self.edges_cs2ef[edIdx][0]
            ele1, isd1 = self.edges_cs2ef[edIdx][1]
            iel0 = ele0.getLocalIndex()
            iel1 = ele1.getLocalIndex()
            ino0, ino2 = ele0.getEdgeConnectivities(isd0)
            ino2, ino3 = ele0.getEdgeConnectivities((isd0+1) % 3)
            ino_, ino1 = ele1.getEdgeConnectivities((isd1+1) % 3)
            print('swap: ', ele0, isd0, ino0, ino2)
            print('swap: ', ele1, isd1, ino1, ino3, ino_)
            assert ino0 == ino_

        # ---  Check new configuration
        if not errMsg:
            lenOld  = self.__getEdgeLength(ino0, ino2)
            lenNew  = self.__getEdgeLength(ino1, ino3)
            if lenNew < 0.1*lenOld:
                errMsg = "New edge is too short: %.3f vs %.3f" % (lenNew, lenOld)
        if not errMsg:
            srfOld0 = self.__getTriangleSurface(ino0, ino2, ino3)
            srfOld1 = self.__getTriangleSurface(ino0, ino1, ino2)
            srfNew0 = self.__getTriangleSurface(ino0, ino1, ino3)
            srfNew1 = self.__getTriangleSurface(ino1, ino2, ino3)
            srfNew = min(srfNew0, srfNew1)
            srfOld = min(srfOld0, srfOld1)
            if srfNew < 0.1*srfOld:
                errMsg = "New min surface is too small: %.3f vs %.3f" % (srfNew, srfOld)

        # ---  Get cavity ics
        if not errMsg:
            ics1 = self.edges_ef2cs[iel1][(isd1+1) % 3]  # ino0, ino1
            ics2 = self.edges_ef2cs[iel1][(isd1+2) % 3]  # ino1, ino2
            ics3 = self.edges_ef2cs[iel0][(isd0+1) % 3]  # ino2, ino3
            ics4 = self.edges_ef2cs[iel0][(isd0+2) % 3]  # ino3, ino0

        # ---  Modify grid
        if not errMsg:
            elems = self.grid.addElements([(ino0, ino1, ino3), (ino2, ino3, ino1)])
            self.grid.deleteElement(ele0)
            self.grid.deleteElement(ele1)

        # ---  Update edges structure
        if not errMsg:
            ncs = len(self.edges_cs2ef)
            self.edges_cs2ef.append( DACSEdge(ncs+0, elems[0], 1, elems[1], 1) )
            self.edges_cs2ef[ics1].replace(ele1, -1, elems[0], 0)
            self.edges_cs2ef[ics2].replace(ele1, -1, elems[1], 2)
            self.edges_cs2ef[ics3].replace(ele0, -1, elems[1], 0)
            self.edges_cs2ef[ics4].replace(ele0, -1, elems[0], 2)
            self.edges_cs2ef[edIdx].reset()
            #
            self.edges_ef2cs.append( [ics1, ncs+0, ics4])
            self.edges_ef2cs.append( [ics3, ncs+0, ics2])
            self.edges_ef2cs[iel0] = None
            self.edges_ef2cs[iel1] = None

        # ---  Vertex data
        if not errMsg:
            XV, YV = self.csVtx.get_data()

        # ---  Modify edge data
        if not errMsg:
            XE, YE = self.csEdg.get_data()          # [x00, x01, d0, x10, x11, d1, x20, x21, d2 ...]
            XE = np.reshape(XE, (-1,3))
            YE = np.reshape(YE, (-1,3))
            XE[edIdx,:] = np.nan
            YE[edIdx,:] = np.nan
            XE = np.append(XE, [ (XV[ino1], XV[ino3], np.nan) ] )
            YE = np.append(YE, [ (YV[ino1], YV[ino3], np.nan) ] )
            XE = np.ravel(XE)
            YE = np.ravel(YE)

        # ---  Assign back vertex and edges
        if not errMsg:
            self.csEdg.set_data((XE, YE))

        if not errMsg:
            self.canvas.draw()

        if errMsg:
            pub.sendMessage('statusbar.flashmessage', message=errMsg)

        self.ptIdx = -1
        self.edIdx = -1
        self.locked = False
        self.modif  = True
        self.dirty  = True
        self.__debug_validate()

    def onMnuNodeClose(self, event):
        """
        A menu was used, so deactivate move
        """
        # ---  Erase overlay
        try:
            self.overlay.set_data([], [])
            self.overlay.remove()
            self.canvas.draw()
        except ValueError:
            pass
        # ---  Reset locked state
        self.locked = False

    def onMnuNodeCenter1(self, event):
        """
        Center an skin node
        """
        print('In mnuNodeCenter1: %s', event)

        errMsg = ""

        try:
            self.overlay.set_data([], [])
            self.overlay.remove()
        except ValueError:
            pass

        # ---  Get skin edges
        _, IVs = self.__getStarIdx(self.ptIdx)
        IVSkin = [ IV for IV in IVs if self.edges_cs2ef[IV].getSize() == 1 ]

        # ---  Get coordinates
        XE, YE = self.csEdg.get_data()
        XE = np.reshape(XE, (-1,3))
        YE = np.reshape(YE, (-1,3))
        x11, x12 = XE[IVSkin[0],0:2]
        y11, y12 = YE[IVSkin[0],0:2]
        x21, x22 = XE[IVSkin[1],0:2]
        y21, y22 = YE[IVSkin[1],0:2]
        # print((x11, y11), (x12, y12))
        # print((x21, y21), (x22, y22))

        # ---  Get continuous (x0, xc, x1)
        if (x11, y11) == (x21, y21):
            xc, yc = x11, y11
            x0, y0 = x12, y12
            x1, y1 = x22, y22
        else:   # (x11 == x22)
            xc, yc = x22, y22
            x0, y0 = x12, y12
            x1, y1 = x21, y21

        # ---  Check angle
        v0 = np.array([x0-xc, y0-yc])
        v1 = np.array([x1-xc, y1-yc])
        c = np.dot(v0,v1) / np.linalg.norm(v0) / np.linalg.norm(v1)
        alfa = np.arccos(np.clip(c, -1.0, 1.0))
        if alfa < 0: alfa += 2*math.pi
        alfa = math.degrees(alfa)
        if abs(alfa-180) > DALayerGridEditor.TOLERANCE_FLAT:
            errMsg = '\n'.join((
                'Angle not flat enough (> %.1f deg.)' % DALayerGridEditor.TOLERANCE_FLAT,
                'Cowardly refusing to rectify an angle of %.1f deg.' % abs(alfa-180)))

        # ---  Get center
        if not errMsg:
            if (x11, y11) == (x21, y21):
                xc, yc = (x12+x22), (y12+y22)
            else:
                xc, yc = (x12+x21), (y12+y21)
            xc = 0.5*xc
            yc = 0.5*yc

        # ---  Modify vertex data
        if not errMsg:
            X, Y = self.csVtx.get_data()
            x0 = X[self.ptIdx]
            y0 = Y[self.ptIdx]
            X[self.ptIdx] = xc
            Y[self.ptIdx] = yc
            self.csVtx.set_data(X, Y)

        # ---  Modify edge data
        if not errMsg:
            X, Y = self.csEdg.get_data()
            IX = (X == x0)
            IY = (Y == y0)
            I = np.where( np.all((IX, IY), axis=0) )
            X[I] = xc
            Y[I] = yc
            self.csEdg.set_data(X, Y)

        if not errMsg:
            self.canvas.draw()

        if errMsg:
            pub.sendMessage('statusbar.flashmessage', message=errMsg)
            print(errMsg)

        self.ptIdx = -1
        self.edIdx = -1
        self.locked = False
        self.modif  = True

    def onMnuNodeCenter2(self, event):
        """
        Center an internal node
        """
        print('In mnuNodeCenter2: %s', event)

        try:
            self.overlay.set_data([], [])
            self.overlay.remove()
        except ValueError:
            pass

        # ---  Get center
        Xs, Ys = self.__getStarXY(self.ptIdx)
        xc = np.mean(Xs)
        yc = np.mean(Ys)

        # ---  Modify vertex data
        X, Y = self.csVtx.get_data()
        x0 = X[self.ptIdx]
        y0 = Y[self.ptIdx]
        X[self.ptIdx] = xc
        Y[self.ptIdx] = yc
        self.csVtx.set_data(X, Y)

        # ---  Modify edge data
        X, Y = self.csEdg.get_data()
        IX = (X == x0)
        IY = (Y == y0)
        I = np.where( np.all((IX, IY), axis=0) )
        X[I] = xc
        Y[I] = yc
        self.csEdg.set_data(X, Y)

        self.canvas.draw()
        self.ptIdx = -1
        self.edIdx = -1
        self.locked = False
        self.modif  = True

    def onMnuNodeCenter(self, event):
        print('In onMnuNodeCenter: %s', event)
        _, IVs = self.__getStarIdx(self.ptIdx)
        nSkin = 0
        for IV in IVs:
            if self.edges_cs2ef[IV].getSize() == 1:
                nSkin += 1
        if   nSkin == 2:
            return self.onMnuNodeCenter1(event)
        elif nSkin == 0:
            return self.onMnuNodeCenter2(event)
        else:
            errMsg = '\n'.join(('Internal structure corrupted.',
                               'Node detected with %d edges on skin.' %  nSkin))
            raise RuntimeError(errMsg)

    def onMnuNodeDelete(self, event):
        print('In mnuNodeDelete: %s', event)
        self.__debug_validate()

        errMsg = ''

        try:
            self.overlay.set_data([], [])
            self.overlay.remove()
        except ValueError:
            pass

        # ---  Get star idx
        _, IEs = self.__getStarIdx(self.ptIdx)

        # ---  Get cavity edges
        cvtEles = []        #
        cvtEdgI = set()     # cavity internal edges, set of edge idx
        cvtEdgX = set()     # cavity external edges, set of edge idx
        cvtXtrn = []        # cavity external edges, list of vtx idx tuple
        for iedg in IEs:    # loop over star edges
            for ele, _ in self.edges_cs2ef[iedg]:       # loop over ele of edges
                if ele in cvtEles: continue
                for isd in range(3):                    # Uniquer for sides
                    no1, no2 = ele.getEdgeConnectivities(isd)
                    if (no2, no1) in cvtXtrn:
                        cvtEdgI.add(iedg)               # Uniquer for internal edges
                        cvtXtrn.remove((no2, no1))
                    else:
                        cvtXtrn.append((no1, no2))
                cvtEles.append(ele)                     # Keep elements
        # ---  Get cavity external edges
        for ele in cvtEles:                             # Loop over ele of cavity
            iel = ele.getLocalIndex()
            for iedg in self.edges_ef2cs[iel]:          # Edges of element
                if iedg not in cvtEdgI:                 # Skip internal edges
                    cvtEdgX.add(iedg)                   # Uniquer for edges
        # ---  Sort cavity edges for continuity
        iend = len(cvtXtrn)
        for inow in range(1, iend-1):
            ino1 = cvtXtrn[inow-1][1]
            inxt = next(ie_ for ie_ in range(inow,iend) if cvtXtrn[ie_][0] == ino1)
            tmp  = cvtXtrn[inow]
            cvtXtrn[inow] = cvtXtrn[inxt]
            cvtXtrn[inxt] = tmp

        # ---  Build (unclosed) polygon without ptIdx
        XV, YV = self.csVtx.get_data()
        poly = []
        pidx = []
        for edg in cvtXtrn:
            ino = edg[0]
            if ino != self.ptIdx:
                poly.append( (XV[ino], YV[ino]) )
                pidx.append(ino)

        # ---  Ear clip with tripy
        tris = []
        for iel, tri in enumerate(tripy.earclip(poly)):
            ino0 = poly.index(tri[0])
            ino1 = poly.index(tri[1])
            ino2 = poly.index(tri[2])
            tris.append( (pidx[ino0], pidx[ino1], pidx[ino2]) )

        # ---  Get all new edges
        trv1 = {}
        for iel, (ino0, ino1, ino2) in enumerate(tris):
            inos = (ino0, ino1, ino2, ino0)
            for isd, (in0, in1) in enumerate(zip(inos, inos[1:])):
                key = min(in0, in1), max(in0, in1)
                try:
                    trv1[key].append( (iel, isd) )
                except:
                    trv1[key] = [ (iel, isd) ]
        # ---  Flag all external from cavity
        trv2 = {}
        for ics in cvtEdgX:
            ele, isd = self.edges_cs2ef[ics][0]
            in0, in1 = ele.getEdgeConnectivities(isd)
            key = min(in0, in1), max(in0, in1)
            if key in trv1: trv2[key] = ics
        # ---  Add new edges
        newEdgA = {}    # New edges - add
        newEdgM = {}    # New edges - modify
        for key, val in trv1.items():
            if len(val) == 2:
                newEdgA[key] = val
            else:
                if key in trv2:
                    newEdgM[key] = val
                else:
                    newEdgA[key] = val
        print(tris)
        print('newEdgA')
        for key, val in newEdgA.items():
            print(key, val)
        print('newEdgM')
        for key, val in newEdgM.items():
            print(key, val)

        # ---  Modify grid - add
        nel = self.grid.getNbElements()
        if not errMsg:
            elems = self.grid.addElements(tris)

        # ---  Update edges structures
        ncs = len(self.edges_cs2ef)
        if not errMsg:
            ics = 0
            # ---  Add
            for key, val in newEdgA.items():
                if len(val) == 1:
                    (iel0, isd0), = val
                    self.edges_cs2ef.append( DACSEdge(ncs+ics, elems[iel0], isd0) )
                else:
                    (iel0, isd0), (iel1, isd1) = val
                    self.edges_cs2ef.append( DACSEdge(ncs+ics, elems[iel0], isd0, elems[iel1], isd1) )
                ics += 1
            # ---  Modify
            for ele in cvtEles:
                iel = ele.getLocalIndex()
                for isd in range(3):
                    in0, in1 = ele.getEdgeConnectivities(isd)
                    key = min(in0, in1), max(in0, in1)
                    if not key in newEdgM: continue
                    assert len(newEdgM[key]) == 1
                    (niel, nisd), = newEdgM[key]
                    ics = self.edges_ef2cs[iel][isd]
                    self.edges_cs2ef[ics].replace(ele, isd, elems[niel], nisd)
            # ---  Delete
            for iedg in cvtEdgI:
                self.edges_cs2ef[iedg].reset()
            for iedg in cvtEdgX:
                ele, isd = self.edges_cs2ef[iedg][0]
                in0, in1 = ele.getEdgeConnectivities(isd)
                key = min(in0, in1), max(in0, in1)
                if key not in newEdgM:
                    self.edges_cs2ef[iedg].reset()

        if not errMsg:
            # ---  Add
            for ele in elems:
                self.edges_ef2cs.append( [-1, -1, -1] )
            ics = 0
            for key, val in newEdgA.items():
                if len(val) == 1:
                    (iel0, isd0), = val
                    self.edges_ef2cs[nel+iel0][isd0] = ncs+ics
                else:
                    (iel0, isd0), (iel1, isd1) = val
                    self.edges_ef2cs[nel+iel0][isd0] = ncs+ics
                    self.edges_ef2cs[nel+iel1][isd1] = ncs+ics
                ics += 1
            # ---  Modify
            for ele in cvtEles:
                iel = ele.getLocalIndex()
                for isd in range(3):
                    in0, in1 = ele.getEdgeConnectivities(isd)
                    key = min(in0, in1), max(in0, in1)
                    if not key in newEdgM: continue
                    assert len(newEdgM[key]) == 1
                    (niel, nisd), = newEdgM[key]
                    ics = self.edges_ef2cs[iel][isd]
                    self.edges_ef2cs[nel+niel][nisd] = ics
            # ---  Delete
            for ele in cvtEles:
                iel = ele.getLocalIndex()
                self.edges_ef2cs[iel] = None
            for ele in elems:
                iel = ele.getLocalIndex()
                if -1 in self.edges_ef2cs[iel]:
                    print(ele, self.edges_ef2cs[iel])
                    self.edges_ef2cs[iel] = None
                    raise ValueError

        # ---  Modify grid - erase
        if not errMsg:
            for ele in cvtEles:
                self.grid.deleteElement(ele)
            self.grid.deleteNode( self.grid.getNode(self.ptIdx) )

        # ---  Modify vertex data
        if not errMsg:
            XV, YV = self.csVtx.get_data()
            XV[self.ptIdx] = np.nan
            YV[self.ptIdx] = np.nan
            self.csVtx.set_data(XV, YV)

        # ---  Modify edge data
        if not errMsg:
            XE, YE = self.csEdg.get_data()
            XE = np.reshape(XE, (-1,3))
            YE = np.reshape(YE, (-1,3))
            for iedg in cvtEdgI:
                XE[iedg,:] = np.nan
                YE[iedg,:] = np.nan
            for iedg in cvtEdgX:
                if self.edges_cs2ef[iedg].isEmpty():
                    XE[iedg,:] = np.nan
                    YE[iedg,:] = np.nan
            for key, val in newEdgA.items():
                no0, no1 = key
                XE = np.append(XE, [(XV[no0], XV[no1], np.nan)] )
                YE = np.append(YE, [(YV[no0], YV[no1], np.nan)] )
            XE = np.ravel(XE)
            YE = np.ravel(YE)
            self.csEdg.set_data(XE, YE)

        self.__debug_validate()
        self.canvas.draw()
        self.ptIdx = -1
        self.edIdx = -1
        self.locked = False
        self.modif  = True
        self.dirty  = True

    def onPick(self, event):
        """
        Set either ptIdx or edIdx to the picked item
        """
        # print('DALayerGridEditor.onPick', event)
        # print('DALayerGridEditor.onPick', event.artist)
        if event.canvas is not self.canvas: return
        if self.locked: return
        if not isinstance(event.artist, Line2D): return
        if len(event.ind) == 0: return
        if len(event.ind) != 1: return
        artist = event.artist
        marker = artist.get_marker()
        # print('DALayerGridEditor.onPick: marker ', marker)
        # print('DALayerGridEditor.onPick: ind ', event.ind)
        if marker and marker != 'None':
            self.ptIdx = event.ind[0]
            self.ptXY  = artist.get_xydata()[self.ptIdx]
            self.edIdx = -1
            self.locked = True
        else:
            self.edIdx = event.ind[0]
            self.edXY  = artist.get_xydata()[self.edIdx:self.edIdx+2]
            self.edIdx = self.edIdx // 3
            self.ptIdx = -1


if __name__ == "__main__":
    import wx.lib.agw.flatnotebook as fnb
    from DAFrameField import DAFrameField
    from DAPanels.DAPnl2DFigure     import DAPnl2DFigure
    LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer")

    class TestFrame(wx.Frame):
        def __init__(self, *args, **kwds):
            # begin wxGlade: TestFrame.__init__
            kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
            wx.Frame.__init__(self, *args, **kwds)
            self.SetSize((800, 600))
            self.nbk = fnb.FlatNotebook(self, wx.ID_ANY, style=fnb.FNB_X_ON_TAB)

            self.__set_properties()
            self.__do_layout()

            self.fld = self.__genField()

            self.fig = IPRaster.IPFigure()
            self.fig.createFigure(size=self.nbk.GetSize(), use_pyplot=False)
            ax = self.fig.addSubPlot(1, 1, 1) #, facecolor='w')
            self.fig.adjustSubPlots(hspace=0.0, wspace=0.0, bottom=0.0, top=1.0, left=0.0, right=1.0)
            bbx = list(self.fld.getGrid().getBbox(stretchfactor=0.02))
            # bbx[2] = bbx[0] + 0.1*(bbx[2]-bbx[0])
            ax.setup(window=bbx, aspect_equal=True)
            ax.init()
            ax.removeAxis()

            self.win = DAPnl2DFigure(parent=self.nbk, fig=self.fig, axes=ax)
            indx = self.nbk.GetPageCount()
            self.nbk.InsertPage(indx, self.win, 'Edit')
            self.nbk.SetSelection(indx)
            self.win.toolbar.Show()
            self.win.show(True)

            self.lyr = DALayerGridEditor(self.win, self.fld)
            self.lyr.draw()
            # end wxGlade

        def __set_properties(self):
            self.SetTitle("GridEditor Unit Test")

        def __do_layout(self):
            # begin wxGlade: TestFrame.__do_layout
            szr_main = wx.BoxSizer(wx.VERTICAL)
            szr_main.Add(self.nbk, 1, wx.EXPAND, 0)
            self.SetSizer(szr_main)
            self.Layout()
            # end wxGlade

        def __genField(self):
            p = r''
            f = os.path.join(p, 'test.cor'), os.path.join(p, 'test.ele')
            mesh  = DAFrameGrid(files=f, withLocalizer=True)
            field = DAFrameField.as2DFiniteElementField(grid=mesh)
            return field

        def edit(self):
            pass

    class TestApp(wx.App):
        def OnInit(self):
            self.frame = TestFrame(None, wx.ID_ANY, "")
            self.SetTopWindow(self.frame)
            self.frame.Show()
            return True

    formatter = logging.Formatter('%(asctime)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.DEBUG)

    #import time
    #waiting = True
    #while waiting:
    #    time.sleep(1000)

    app = TestApp(0)
    app.MainLoop()
