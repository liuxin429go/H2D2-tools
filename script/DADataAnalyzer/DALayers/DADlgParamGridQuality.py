#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2015-2017
# --- Institut National de la Recherche Scientifique (INRS)
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

if __name__ == "__main__":
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)
    supPath = os.path.normpath(os.path.join(selfDir, '..', '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import collections.abc

import wx

import DADlgColorSpan

try:
    from CTCommon.FEMeshMetricKind    import ELEMENT_QUALITY_METRICS as EQM
except ImportError:
    from CTCommon.FEMeshMetricKind_pp import ELEMENT_QUALITY_METRICS as EQM

criterions = [ kind.name for kind in EQM]

class DADlgParamGridQuality(wx.Dialog):
    def __init__(self, *args, **kwds):
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        super(DADlgParamGridQuality, self).__init__(*args, **kwds)

        self.szr_top_staticbox  = wx.StaticBox(self, -1, "Name")
        self.szr_val_staticbox  = wx.StaticBox(self, -1, "Values")
        self.szr_opts_staticbox = wx.StaticBox(self, -1, "Options")

        bmpUndo = wx.ArtProvider.GetBitmap(wx.ART_UNDO, wx.ART_OTHER) #, (16, 16))

        self.stx_title    = wx.StaticText  (self, wx.ID_ANY, "Layer name ")
        self.txt_title    = wx.TextCtrl    (self, wx.ID_ANY, "")
        self.stx_crtrn    = wx.StaticText  (self, wx.ID_ANY, "Criterion ")
        self.cbx_crtrn    = wx.ComboBox    (self, wx.ID_ANY, choices=criterions)
        self.stx_valmin   = wx.StaticText  (self, wx.ID_ANY, "min ")
        self.txt_valmin   = wx.TextCtrl    (self, wx.ID_ANY, "")
        self.btn_valmin   = wx.BitmapButton(self, wx.ID_ANY, bmpUndo, size=(16,16))
        self.stx_valmax   = wx.StaticText  (self, wx.ID_ANY, "max ")
        self.txt_valmax   = wx.TextCtrl    (self, wx.ID_ANY, "")
        self.btn_valmax   = wx.BitmapButton(self, wx.ID_ANY, bmpUndo, size=(16,16))
        self.stx_valnbr   = wx.StaticText  (self, wx.ID_ANY, "Number ")
        self.spn_valnbr   = wx.SpinCtrl    (self, wx.ID_ANY, "21", min=1, max=100, style=wx.TE_PROCESS_TAB)
        self.btn_valnbr   = wx.BitmapButton(self, wx.ID_ANY, bmpUndo, size=(16,16))
        self.stx_valcmp   = wx.StaticText  (self, wx.ID_ANY, "Color ")
        self.csl_valcmp   = wx.Button      (self, wx.ID_ANY)
        self.btn_valcmp   = wx.BitmapButton(self, wx.ID_ANY, bmpUndo, size=(16,16))
        self.chk_isol     = wx.CheckBox    (self, wx.ID_ANY, "IsoLine ")
        self.chk_log      = wx.CheckBox    (self, wx.ID_ANY, "Log scale ")
        self.chk_opentop  = wx.CheckBox    (self, wx.ID_ANY, "Top open ended ")
        self.chk_openbtm  = wx.CheckBox    (self, wx.ID_ANY, "Bottom open ended ")
        self.chk_colorbar = wx.CheckBox    (self, wx.ID_ANY, "Hide color bar ")
        self.btn_ok       = wx.Button      (self, wx.ID_OK, "")
        self.btn_cancel   = wx.Button      (self, wx.ID_CANCEL, "")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON, self.on_btn_valmin, self.btn_valmin)
        self.Bind(wx.EVT_BUTTON, self.on_btn_valmax, self.btn_valmax)
        self.Bind(wx.EVT_BUTTON, self.on_btn_valnbr, self.btn_valnbr)
        self.Bind(wx.EVT_BUTTON, self.on_btn_valcmp, self.btn_valcmp)
        self.Bind(wx.EVT_BUTTON, self.on_csl_valcmp, self.csl_valcmp)
        self.Bind(wx.EVT_BUTTON, self.on_btn_ok,     self.btn_ok)
        self.Bind(wx.EVT_BUTTON, self.on_btn_cancel, self.btn_cancel)

        self.cb = None

    def __set_properties(self):
        self.SetTitle("Isovalue parameters")
        self.txt_title.SetToolTip   ("Name of the layer that will appear in the display list")
        self.cbx_crtrn.SetToolTip   ("Geometrical criterion to display (with filtering function)")
        self.txt_valmin.SetToolTip  ("Minimum value for the range of iso to draw")
        self.txt_valmax.SetToolTip  ("Maximum value for the range of iso to draw")
        self.csl_valcmp.SetToolTip  ("Color map associated to the values")
        self.chk_isol.SetToolTip    ("Draw lines instead of filling the contour")
        self.chk_log.SetToolTip     ("Values drawn on log scale")
        self.chk_opentop.SetToolTip ("Values at top are open ended")
        self.chk_openbtm.SetToolTip ("Values at bottom are open ended")
        self.chk_colorbar.SetToolTip("Hide the color bar")
        self.btn_ok.SetToolTip      ("Apply the changes")
        self.btn_cancel.SetToolTip  ("Cancel changes and close the dialog box")

        self.cbx_crtrn.SetValue(list(criterions)[0])
        self.btn_ok.SetDefault()

    def __do_layout(self):
        szr_main = wx.BoxSizer(wx.VERTICAL)
        szr_btn  = wx.StdDialogButtonSizer()
        szr_data = wx.BoxSizer(wx.VERTICAL)
        szr_top  = wx.StaticBoxSizer(self.szr_top_staticbox, wx.HORIZONTAL)
        szr_bttm = wx.BoxSizer(wx.HORIZONTAL)
        szr_val  = wx.StaticBoxSizer(self.szr_val_staticbox, wx.VERTICAL)
        szr_opts = wx.StaticBoxSizer(self.szr_opts_staticbox, wx.VERTICAL)

        szr_opts_placeholder = wx.BoxSizer(wx.HORIZONTAL)

        szr_top.Add(self.stx_title, 0, wx.ALIGN_BOTTOM, 0)
        szr_top.Add(self.txt_title, 1, wx.EXPAND, 0)

        szr_crtrn  = wx.BoxSizer(wx.HORIZONTAL)
        szr_valmin = wx.BoxSizer(wx.HORIZONTAL)
        szr_valmax = wx.BoxSizer(wx.HORIZONTAL)
        szr_valnbr = wx.BoxSizer(wx.HORIZONTAL)
        szr_valcmp = wx.BoxSizer(wx.HORIZONTAL)
        szr_crtrn.Add(self.stx_crtrn, 2, wx.ALIGN_BOTTOM, 0)
        szr_crtrn.Add(self.cbx_crtrn, 6, wx.EXPAND, 0)
        szr_val.Add(szr_crtrn,  0, wx.EXPAND, 0)
        szr_valmin.Add(self.stx_valmin, 2, wx.ALIGN_BOTTOM, 0)
        szr_valmin.Add(self.txt_valmin, 6, wx.EXPAND, 0)
        szr_valmin.Add(self.btn_valmin, 1, wx.EXPAND, 0)
        szr_val.Add(szr_valmin, 0, wx.EXPAND, 0)
        szr_valmax.Add(self.stx_valmax, 2, wx.ALIGN_BOTTOM, 0)
        szr_valmax.Add(self.txt_valmax, 6, wx.EXPAND, 0)
        szr_valmax.Add(self.btn_valmax, 1, wx.EXPAND, 0)
        szr_val.Add(szr_valmax, 0, wx.EXPAND, 0)
        szr_valnbr.Add(self.stx_valnbr, 2, wx.ALIGN_BOTTOM, 0)
        szr_valnbr.Add(self.spn_valnbr, 6, wx.EXPAND, 0)
        szr_valnbr.Add(self.btn_valnbr, 1, wx.EXPAND, 0)
        szr_val.Add(szr_valnbr, 0, wx.EXPAND, 0)
        szr_valcmp.Add(self.stx_valcmp, 2, wx.ALIGN_BOTTOM, 0)
        szr_valcmp.Add(self.csl_valcmp, 6, wx.EXPAND, 0)
        szr_valcmp.Add(self.btn_valcmp, 1, wx.EXPAND, 0)
        szr_val.Add(szr_valcmp, 0, wx.EXPAND, 0)

        szr_opts.Add(self.chk_isol, 0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_log,  0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_opentop, 0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_openbtm, 0, wx.EXPAND, 0)
        szr_opts.Add(self.chk_colorbar, 0, wx.EXPAND, 0)
        szr_opts.Add(szr_opts_placeholder, 1, wx.EXPAND, 0)

        szr_bttm.Add(szr_val,  2, wx.EXPAND, 0)
        szr_bttm.Add(szr_opts, 1, wx.EXPAND, 0)

        szr_data.Add(szr_top,  0, wx.EXPAND, 0)
        szr_data.Add(szr_bttm, 1, wx.EXPAND, 0)

        szr_btn.AddButton(self.btn_ok)
        szr_btn.AddButton(self.btn_cancel)
        szr_btn.Realize()

        szr_main.Add(szr_data, 0, wx.EXPAND, 0)
        szr_main.Add(szr_btn,  0, wx.ALIGN_RIGHT | wx.ALL, 4)

        self.SetSizer(szr_main)
        self.Fit()
        # ---  Increase size just a bit
        w, h = self.GetSize()
        self.SetSize( (w+10, h))

    def __setCmapButton(self):
        # ---  Get size
        bmp = self.csl_valcmp.GetBitmap()
        if self.csl_valcmp.GetBitmap().GetSize():
            w, h = bmp.GetSize()
        else:
            w, h = self.csl_valcmp.GetClientSize()[0], 15
        bmp = DADlgColorSpan.getCmapBmp(self.cmap, (w, h))
        self.csl_valcmp.SetBitmap(bmp)

    def get_title(self):
        return self.txt_title.GetValue()

    def set_title(self, t):
        self.txt_title.SetValue(t)

    title = property(get_title, set_title)

    def get_values(self):
        cri = self.cbx_crtrn.GetValue()
        cri = EQM[cri]
        min = eval(self.txt_valmin.GetValue())
        max = eval(self.txt_valmax.GetValue())
        nvl = self.spn_valnbr.GetValue()
        cmap = self.cmap
        cmin = self.cmin
        cmax = self.cmax
        return (cri, min, max, nvl, cmap, cmin, cmax)

    def set_values(self, cri, 
                   min =0.0, max =1.0, nval =11, cmap ='jet', cmin =0.0, cmax =1.0,
                   min0=0.0, max0=1.0, nval0=11, cmap0='jet', cmin0=0.0, cmax0=1.0):
        v = cri.name     # check the value exist
        self.min0  = min0
        self.max0  = max0
        self.nvl0  = nval0
        self.cmap0 = cmap0
        self.cmin0 = cmin0
        self.cmax0 = cmax0

        self.cmap = cmap
        self.cmin = cmin
        self.cmax = cmax

        self.cbx_crtrn.SetValue(v)
        self.txt_valmin.SetValue('%s' % min)
        self.txt_valmax.SetValue('%s' % max)
        self.spn_valnbr.SetValue(nval)
        self.__setCmapButton()

    values = property(get_values, set_values)

    def get_checks(self):
        b1 = self.chk_isol.IsChecked()
        b2 = self.chk_log.IsChecked()
        b3 = self.chk_opentop.IsChecked()
        b4 = self.chk_openbtm.IsChecked()
        b5 = self.chk_colorbar.IsChecked()
        return (b1, b2, b3, b4, b5)

    def set_checks(self, iso_line = False, log_scale = False, open_top = False, open_bottom = False, hide_bar = False):
        self.chk_isol.SetValue(iso_line)
        self.chk_log.SetValue(log_scale)
        self.chk_opentop.SetValue(open_top)
        self.chk_openbtm.SetValue(open_bottom)
        self.chk_colorbar.SetValue(hide_bar)

    options = property(get_checks, set_checks)

    def set_callback(self, cb):
        if not isinstance(cb, collections.abc.Callable): raise TypeError('Callable expected')
        self.cb = cb

    def on_btn_valmin(self, event):
        self.txt_valmin.SetValue('%s' % self.min0)
        
    def on_btn_valmax(self, event):
        self.txt_valmax.SetValue('%s' % self.max0)
        
    def on_btn_valnbr(self, event):
        self.spn_valnbr.SetValue(self.nvl0)
        
    def on_btn_valcmp(self, event):
        self.cmap, self.cmin, self.cmax = self.cmap0, self.cmin0, self.cmax0
        self.__setCmapButton()
        
    def on_csl_valcmp(self, event):
        dlg = DADlgColorSpan.DADlgColorSpan(self)
        if dlg.ShowModal() == wx.ID_OK:
            self.cmap, self.cmin, self.cmax = dlg.getColorSpan()
            self.__setCmapButton()
        dlg.Destroy()
        
    def on_btn_ok(self, event):
        if self.cb:
            self.cb(self)
        else:
            event.Skip()

    def on_btn_cancel(self, event):
        self.Destroy()


if __name__ == "__main__":
    def cb(dlg):
        print(dlg.title)
        print(dlg.values)
        print(dlg.options)

    app = wx.App(False)
    dlg = DADlgParamGridQuality(None, -1, "")
    dlg.set_values(EQM.area, min=-1.0, max=100.0, nval=21)
    dlg.set_checks(False, False, True, False, False)
    dlg.set_callback(cb)
    app.SetTopWindow(dlg)
    dlg.Show()
    app.MainLoop()
