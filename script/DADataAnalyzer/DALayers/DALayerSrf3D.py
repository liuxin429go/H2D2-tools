# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2016-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import wx

from . import DALayer
from . import DADlgParamIso
from IPImageProcessor import IPRaster

LOGGER = logging.getLogger("INRS.H2D2.Tools.DataAnalyzer.Layer.Srf3D")

class DALayerSrf3D(DALayer.DALayer):
    count = 0
    
    def __init__(self, page, field, names):
        super(DALayerSrf3D, self).__init__()
        self.pane  = page
        self.field = field
        self.names = names

        self.fig   = self.pane.get_figure()
        self.ax    = self.pane.get_axes()
        self.items = None

        DALayerSrf3D.count += 1
        self.title = '3D Surfaceo #%03i: %s @ %s' % (DALayerSrf3D.count, names[0], str(field))
        self.vmin, self.vmax, self.nval = (self.field.getDataMin(), self.field.getDataMax(), 21)
        self.iso_line, self.log_scale, self.open_top, self.open_btm, self.hide_bar = (False, False, False, False, True)

        self.min0, self.max0, self.nvl0 = self.vmin, self.vmax, self.nval

    def genIpFig(self):
        kwargs = {}
        #kwargs.update( IPRaster.IPAxes.genKwLimits(self.vmin, self.vmax, self.nval, self.log_scale) )
        
        self.items = []
        print('DALayerSrf3D.genIpFig:')
        print (self.field.getGrid())
        print (self.field.getDataActual())
        cs = self.ax.drawSurface3D(self.field.getGrid(), self.field.getDataActual(), **kwargs)
        self.items.append( DALayer.DALayerItemCS(cs, IPRaster.IPPlotType.SURFACE_3D, 'Surface 3D') )
        if not self.hide_bar:
            pass
        self.pane.add_layer(self)

    def __xeqDraw(self):
        if self.items:
            self.pane.del_layer(self)
        self.genIpFig()
        self.pane.redraw()

    def draw(self):
        dlg = DADlgParamIso.DADlgParamIso(self.pane)
        dlg.set_title (self.title)
        dlg.set_values(self.vmin, self.vmax, self.nval, self.min0, self.max0, self.nvl0)
        dlg.set_checks(self.iso_line, self.log_scale, self.open_top, self.open_btm, self.hide_bar)

        if dlg.ShowModal() == wx.ID_OK:
            self.title = dlg.title
            self.vmin, self.vmax, self.nval = dlg.values
            self.iso_line, self.log_scale, self.open_top, self.open_btm, self.hide_bar = dlg.options
            self.__xeqDraw()

        dlg.Destroy()

    def __str__(self):
        return self.title

if __name__ == "__main__":
    dummy = DALayerIso(None, 1.0, None, [0, 1])
