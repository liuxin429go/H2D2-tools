# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import sys

import wx
import wx.adv as wx_adv
import wx.stc as wx_stc
import wx.lib.wordwrap as wx_wordwrap
import wx.richtext     as wx_rt

from CTCommon import CTWindow
from CTCommon import CTUtil
from CTCommon import FloatSlider as FS
import CTPlot

class DummyMenuItem:
    def Check(self, b): pass
    def GetId(self): return wx.ID_CANCEL

class DropTarget(wx.FileDropTarget):
    def __init__(self, parent):
        super(DropTarget, self).__init__()
        self.parent = parent

    def OnDropFiles(self, x, y, filenames):
        return self.parent.OnDropFiles(x, y, filenames)


class CTFrame(wx.Frame):
    def __init__(self, *args, **kwds):
         # begin wxGlade: CTFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        super(CTFrame, self).__init__(*args, **kwds)

        self.dropTarget = DropTarget(self)
        self.SetDropTarget(self.dropTarget)

        self.split_main = wx.SplitterWindow(self,            -1, style=wx.SP_3D|wx.SP_BORDER)
        self.split_data = wx.SplitterWindow(self.split_main, -1, style=wx.SP_3D|wx.SP_BORDER)

        # Widgets
        style_base = wx.BORDER_STATIC
        self.tracer_plot = CTWindow.CTWindow(proxied_class    = CTPlot.Plot,
                                             do_update        = True,
                                             cb_on_data_change= self.on_tracer_change,
                                             cb_on_data_pick  = self.on_tracer_pick,
                                             cb_on_mouse_move = self.on_mouse_move,
                                             parent = self.split_main)
        self.tracer_data = wx_rt.RichTextCtrl(self.split_data, -1, style = style_base | wx.TE_MULTILINE | wx.ST_NO_AUTORESIZE)
        self.tracer_stat = wx_rt.RichTextCtrl(self.split_data, -1, style = style_base | wx.TE_MULTILINE | wx.ST_NO_AUTORESIZE)
        self.logo   = wx.Panel(self, -1, style = style_base)
        self.slider = FS.FloatSlider(self, -1, style = style_base | wx.SL_HORIZONTAL | wx.SL_LABELS, resolution=1, formater=CTUtil.seconds_to_iso)

        # File history
        self.history= wx.FileHistory(12)
        self.config = wx.Config('H2D2: Convergence Tracer', style=wx.CONFIG_USE_LOCAL_FILE)
        self.history.Load(self.config)
        hist_mnu = wx.Menu()
        self.history.UseMenu(hist_mnu)
        self.history.AddFilesToMenu()

        def addItem(mnu, name, help, style):
            item = wx.MenuItem(mnu, wx.ID_ANY, name, help, style)
            mnu.Append(item)
            return item
        # Menu Bar
        self.menubar = wx.MenuBar()
        self.mnu_file = wx.Menu()
        self.mnu_file_open = addItem(self.mnu_file, "Open...\tCtrl+O", "Open a file", wx.ITEM_NORMAL)
        self.mnu_file_hist = self.mnu_file.Append(wx.ID_ANY, "Add a recent file\tCtrl+R", hist_mnu)
        self.mnu_file.AppendSeparator()
        self.mnu_file_close= addItem(self.mnu_file, "Close\tCtrl+W", "Close all plots", wx.ITEM_NORMAL)
        self.mnu_file_quit = addItem(self.mnu_file, "Quit\tCtrl+Q", "Quit application", wx.ITEM_NORMAL)
        self.menubar.Append(self.mnu_file, "File")

        self.mnu_view = wx.Menu()
        self.mnu_view_rdrw   = addItem(self.mnu_view, "Refresh\tF5", "Redraw the display", wx.ITEM_NORMAL)
        self.mnu_view_pause  = addItem(self.mnu_view, "Pause\tCtrl+P", "Don't update", wx.ITEM_CHECK)
        self.mnu_view_hlght  = DummyMenuItem()  # addItem(self.mnu_view, "Highlight", "Highlight the unconverged time steps", wx.ITEM_CHECK)
        self.mnu_view_x = wx.Menu()
        self.mnu_view_vptx    = addItem(self.mnu_view_x, "x-viewport...", "set the viewport in x", wx.ITEM_NORMAL)
        self.mnu_view_rngx    = addItem(self.mnu_view_x, "x-range...", "set the range in x", wx.ITEM_NORMAL)
        self.mnu_view_ctrx    = addItem(self.mnu_view_x, "x-center...", "set the center in x", wx.ITEM_NORMAL)
        self.mnu_view_x.AppendSeparator()
        self.mnu_view_x_wtime = addItem(self.mnu_view_x, "wtime", "set wall time as x component", wx.ITEM_CHECK)
        self.mnu_view_x_stime = DummyMenuItem()
        self.mnu_view_x_iter  = addItem(self.mnu_view_x, "iter", "set iteration as x component", wx.ITEM_CHECK)
        self.mnu_view.AppendSubMenu(self.mnu_view_x, "x-axis")
        self.mnu_view_y = wx.Menu()
        self.mnu_view_vpty    = addItem(self.mnu_view_y, "y-viewport...", "set the viewport in y", wx.ITEM_NORMAL)
        self.mnu_view_rngy    = addItem(self.mnu_view_y, "y-range...", "set the range in y", wx.ITEM_NORMAL)
        self.mnu_view_ctry    = addItem(self.mnu_view_y, "y-center...", "set the center in y", wx.ITEM_NORMAL)
        self.mnu_view.AppendSubMenu(self.mnu_view_y, "y-axis")
        self.menubar.Append (self.mnu_view, "View")

        self.mnu_stat = wx.Menu()
        self.mnu_stat_entries = [ ]
        mdl_root = 'CTCommon'
        for stat_file in CTUtil.find_files_in_syspath('CTStats*.py', modules=[mdl_root]):
            mdl_name = os.path.split(stat_file)[1]
            mdl_name = os.path.splitext(mdl_name)[0]
            mdl_full = '%s.%s' % (mdl_root, mdl_name)
            m = __import__(mdl_full, globals(), locals(), [mdl_name])
            mnu = addItem(self.mnu_stat, m.CTStats.menu_entry(), m.CTStats.menu_bubble(), wx.ITEM_NORMAL)
            self.mnu_stat_entries.append( (mnu, m) )
        self.menubar.Append (self.mnu_stat, "Statistics")

        self.mnu_help = wx.Menu()
        self.mnu_help_about = self.mnu_help.Append(wx.ID_ABOUT)
        self.menubar.Append(self.mnu_help, "Help")
        self.SetMenuBar(self.menubar)

        # Status Bar
        self.statusbar = self.CreateStatusBar(2)

        # Tool Bar
        self.toolbar = self.tracer_plot.get_toolbar()
        self.toolbar.Reparent(self)
        self.SetToolBar(self.toolbar)

        data_w = 150
        sldr_h = 35
        self.__set_properties(data_w, sldr_h)
        self.__do_layout(data_w, sldr_h)

        self.Bind(wx.EVT_MENU, self.on_mnu_file_open,    self.mnu_file_open)
        self.Bind(wx.EVT_MENU_RANGE, self.on_mnu_file_hist, id=wx.ID_FILE1, id2=wx.ID_FILE9)
        self.Bind(wx.EVT_MENU, self.on_mnu_file_close,   self.mnu_file_close)
        self.Bind(wx.EVT_MENU, self.on_mnu_file_quit,    self.mnu_file_quit)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rdrw,    self.mnu_view_rdrw)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_pause,   self.mnu_view_pause)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_hlght,   self.mnu_view_hlght)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_ctrx,    self.mnu_view_ctrx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rngx,    self.mnu_view_rngx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_vptx,    self.mnu_view_vptx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_wtime, self.mnu_view_x_wtime)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_stime, self.mnu_view_x_stime)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_iter,  self.mnu_view_x_iter)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_ctry,    self.mnu_view_ctry)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rngy,    self.mnu_view_rngy)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_vpty,    self.mnu_view_vpty)
        for m in self.mnu_stat_entries:
            self.Bind(wx.EVT_MENU, self.on_mnu_stat,  m[0])
        self.Bind(wx.EVT_MENU, self.on_mnu_help_about,   self.mnu_help_about)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_ENDSCROLL, self.on_scroll_change)

        self.dirname = ''
        self.fpath   = []

        # ---  Process cmd line files
        if len(sys.argv) > 1: self.OnDropFiles(0, 0, sys.argv[1:])

    def __set_properties(self, data_w, sldr_h):
        self.SetTitle("H2D2 - Convergence Tracer")
        self.SetSize((800, 500))

        statusbar_fields = ['', 'Position']
        for i in range(len(statusbar_fields)):
            self.statusbar.SetStatusText(statusbar_fields[i], i)
        self.toolbar.Realize()

        self.split_main.SetSashGravity(1.0)
        self.split_data.SetSashGravity(0.5)
        self.split_main.SetMinimumPaneSize(20)
        self.split_data.SetMinimumPaneSize(20)

        self.tracer_data.SetMinSize ((data_w, 15))
        self.tracer_data.SetEditable(False)
        self.tracer_stat.SetMinSize ((data_w, 15))
        self.tracer_stat.SetEditable(False)

        self.logo.SetMinSize((data_w, sldr_h))

        self.slider.SetMinSize((100, sldr_h))
        self.slider.SetRange(0, 3600)
        self.slider.SetValue(0)
        self.slider.SetPageSize(900)
        self.slider.Enable(False)

        self.mnu_view_hlght.Check(False)
        self.mnu_view_x_wtime.Check(True)
        self.mnu_view_x_stime.Check(False)
        self.mnu_view_x_iter.Check(False)

    def __do_layout(self, data_w, sldr_h):
        sizer_frame = wx.BoxSizer(wx.VERTICAL)
        sizer_bottm = wx.BoxSizer(wx.HORIZONTAL)
        sizer_frame.Add(self.split_main, 1, wx.EXPAND, 0)
        sizer_frame.Add(sizer_bottm,     0, wx.EXPAND, 0)
        sizer_bottm.Add(self.slider, 1, wx.EXPAND, 0)
        sizer_bottm.Add(self.logo,   0, wx.EXPAND, 0)

        self.split_main.SplitVertically  (self.tracer_plot, self.split_data, -data_w)
        self.split_data.SplitHorizontally(self.tracer_data, self.tracer_stat)

        self.SetSizer(sizer_frame)
        self.Layout()

    def OnDropFiles(self, x, y, fpath):
        if len(fpath) == 1:
            self.dirname = os.path.dirname(fpath[0])
            self.fpath   = fpath

            self.tracer_plot.load_data_from_file(self.fpath)

            file0 = os.path.basename(self.fpath[0])
            self.SetTitle(file0)
            self.slider.Enable(True)
            return True
        else:
            msg = wx.MessageDialog(self, 'Drop only one file', 'Error', wx.OK | wx.ICON_ERROR)
            if (msg.ShowModal() == wx.OK):
                msg.Close(True)
            return False

    def on_mnu_file_open(self, event):
        wildcard = "Tracer files (*.log)|*.log|"     \
                   "All files (*.*)|*.*"
        dlg = wx.FileDialog(self, "Open", self.dirname, "", wildcard, wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            filenames = dlg.GetFilenames()
            dirname   = dlg.GetDirectory()
            if (len(filenames) == 1):
                self.dirname = dirname
                self.fpath   = [ os.path.join(dirname, f) for f in filenames ]

                self.tracer_plot.load_data_from_file(self.fpath)

                file0 = os.path.basename(self.fpath[0])
                self.SetTitle(file0)
                self.slider.Enable(True)

                for p in self.fpath: self.history.AddFileToHistory(p)
                self.history.Save(self.config)
                self.config.Flush()
            else:
                msg = wx.MessageDialog(self, 'Select the files to be ploted', 'Error', wx.OK | wx.ICON_ERROR)
                if (msg.ShowModal() == wx.OK):
                    msg.Destroy()
        dlg.Destroy()

    def on_mnu_file_hist(self, event):
        fileNum = event.GetId() - wx.ID_FILE1
        path = self.history.GetHistoryFile(fileNum)

        try:
            self.fpath.append(path)
            self.tracer_plot.load_data_from_file(self.fpath)

            file0 = os.path.basename(self.fpath[0])
            self.SetTitle(file0)
            self.slider.Enable(True)

            self.history.AddFileToHistory(path)
        except:
            msg = wx.MessageDialog(self, 'Error while reading file %s' % path, 'Error', wx.OK | wx.ICON_ERROR)
            if (msg.ShowModal() == wx.OK):
                msg.Destroy()
                self.history.RemoveFileFromHistory(fileNum)
                self.fpath.pop()
                self.tracer_plot.load_data_from_file(self.fpath)

    def on_mnu_file_close(self, event):
        self.fpath = []

        self.tracer_plot.load_data_from_file(self.fpath)

        self.SetTitle(' ')
        self.slider.Enable(False)
        self.__fill_mnu_dspl(self.fpath)


    def on_mnu_file_quit(self, event):
        dlg = wx.MessageDialog(self, 'Are you sure? \n', 'Quit', wx.YES_NO)
        if (dlg.ShowModal() == wx.ID_YES):
            self.Close(True)

    def on_mnu_view_rdrw(self, event):
        self.tracer_plot.draw_plot()

    def on_mnu_view_pause(self, event):
        self.tracer_plot.pause( self.mnu_view_pause.IsChecked() )

    def on_mnu_view_ctrx(self, event):
        ctr = self.tracer_plot.get_view_ctrx()
        str = '%f' % ctr
        dlg = wx.TextEntryDialog(self, 'Enter the data central value in X', 'Data central value in X', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_ctrx(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_ctry(self, event):
        ctr = self.tracer_plot.get_view_ctry()
        str = '%f' % ctr
        dlg = wx.TextEntryDialog(self, 'Enter the data central value in Y', 'Data central value in Y', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_ctry(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_rngx(self, event):
        rng = self.tracer_plot.get_view_rngx()
        str = '%f' % rng
        dlg = wx.TextEntryDialog(self, 'Enter the displayed data range in X', 'Data range in X', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_rngx(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_rngy(self, event):
        rng = self.tracer_plot.get_view_rngy()
        str = '%f' % rng
        dlg = wx.TextEntryDialog(self, 'Enter the displayed data range in Y', 'Data range in Y', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_rngy(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_vptx(self, event):
        x1, x2 = self.tracer_plot.get_view_vptx()
        str = '%f %f' % (x1, x2)
        dlg = wx.TextEntryDialog(self, 'Enter the x-axis viewport', 'x-axis viewport', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                x1, x2 = dlg.GetValue().split()
                x1 = eval(x1)
                x2 = eval(x2)
                self.tracer_plot.set_view_vptx(float(x1), float(x2))
            except:
                msg = wx.MessageDialog(self, 'Two, blanck separated, floating point values expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_vpty(self, event):
        y1, y2 = self.tracer_plot.get_view_vpty()
        str = '%f %f' % (y1, y2)
        dlg = wx.TextEntryDialog(self, 'Enter the y-axis viewport', 'y-axis viewport', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                y1, y2 = dlg.GetValue().split()
                y1 = eval(y1)
                y2 = eval(y2)
                self.tracer_plot.set_view_vpty(float(y1), float(y2))
            except:
                msg = wx.MessageDialog(self, 'Two, blanck separated, floating point values expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_hlght(self, event):
        pass

    def on_mnu_view_x_wtime(self, event):
        self.mnu_view_x_wtime.Check(True)
        self.mnu_view_x_stime.Check(False)
        self.mnu_view_x_iter.Check(False)
        self.tracer_plot.set_sourcex('wtime')

    def on_mnu_view_x_stime(self, event):
        self.mnu_view_x_wtime.Check(False)
        self.mnu_view_x_stime.Check(True)
        self.mnu_view_x_iter.Check(False)
        self.tracer_plot.set_sourcex('stime')

    def on_mnu_view_x_iter(self, event):
        self.mnu_view_x_wtime.Check(False)
        self.mnu_view_x_stime.Check(False)
        self.mnu_view_x_iter.Check(True)
        self.tracer_plot.set_sourcex('index')

    def on_mnu_stat(self, event):
        mdl = None
        for m in self.mnu_stat_entries:
            if (m[0].GetId() == event.GetId()): mdl = m[1]
        if (not mdl): raise '@¢#&(@@'

        s = mdl.CTStats()
        c = self.tracer_plot.get_figure().get_convergence()
        t = s(c)

        frm = wx.Frame(id=wx.ID_ANY, parent=self, title=mdl.CTStats.menu_entry())
        win = wx_stc.StyledTextCtrl(id=wx.ID_ANY, parent=frm)
        win.SetMarginType (1, wx_stc.STC_MARGIN_NUMBER)
        win.SetMarginWidth(1, 38)
        win.StyleSetFont(wx_stc.STC_STYLE_DEFAULT, wx.Font(10, wx.MODERN, wx.NORMAL, wx.NORMAL))
        win.StyleSetSpec(wx_stc.STC_STYLE_LINENUMBER, "back:#C0C0C0,face:Helvetica,size:9")
        win.SetText('\n'.join(t))
        frm.Show()

    def on_mnu_help_about(self, event):
        info = wx_adv.AboutDialogInfo()
        info.Name = "H2D2 Convergence Tracer"
        info.Version = "18.04rc1"
        info.Copyright = "(C) 2010-2018 INRS"
        info.Description = wx_wordwrap.wordwrap(
            "\nH2D2 Convergence Tracer plots the convergence history of a H2D2 run. "
            "If the simulation is still running it will follow the progress.\n"
            "The program allows for simple graphic interaction such as pan and zoom "
            "as well as data probing.",
            350, wx.ClientDC(self))
        info.WebSite = ("http://www.gre-ehn.ete.inrs.ca/H2D2", "H2D2 home page")
        info.Developers = [ "Yves Secretan" ]
        #info.License = wx_wordwrap.wordwrap(licenseText, 500, wx.ClientDC(self))

        wx_adv.AboutBox(info)

    def on_scroll_change(self, event):
        x = event.GetPosition()
        self.tracer_plot.set_view_ctrx(x)

    def on_tracer_change(self):
        x1, x2 = self.tracer_plot.get_data_spnx()
        xc     = self.tracer_plot.get_view_ctrx()
        v1, v2 = self.tracer_plot.get_view_vptx()
        txt    = self.tracer_plot.get_stats()

        self.slider.SetRange(x1, x2)
        self.slider.SetValue(xc)
        self.slider.SetPageSize((v2-v1) / 4)

        self.tracer_stat.Clear()
        for l in txt: self.tracer_stat.WriteText(l)

    def on_tracer_pick(self, entry = None):
        self.tracer_data.Clear()
        if (entry):
            self.tracer_data.WriteText(entry.as_text())

    def on_mouse_move(self, s):
        self.statusbar.SetStatusText("Pos: %s" % s, 1)

# end of class CTFrame
