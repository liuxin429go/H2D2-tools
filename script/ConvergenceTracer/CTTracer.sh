#!/bin/bash

BIN_DIR=$(readlink -f "$0")
BIN_DIR=`dirname $BIN_DIR`
CFG_DIR=$BIN_DIR/..

if [ -e $BIN_DIR/../script ]
then
   CFG_DIR=$BIN_DIR
   BIN_DIR=$BIN_DIR/../script/ConvergenceTracer
fi

pushd $BIN_DIR >> /dev/null
python CTApp.py $*
popd >> /dev/null
