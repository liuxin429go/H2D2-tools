# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from __future__ import absolute_import
from __future__ import print_function

if __name__ == '__main__':
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import matplotlib
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg

import numpy
import wx

from CTCommon import CTEntry
from CTCommon import CTUtil

class Plot(matplotlib.figure.Figure):
    def __init__(self, parent, *args, **kwargs):
        sp_param  = matplotlib.figure.SubplotParams(left=0.10, bottom=0.15, right=0.98, top=0.96)
        super(Plot, self).__init__(subplotpars=sp_param)
        self.canvas = FigureCanvasWxAgg(parent, -1, self)

        self.ax = self.add_subplot(111)

        self.file    = None
        self.sourcex = 'wtime'
        self.__reset()

    def __reset(self):
        if (self.file): self.file.close()
        self.file = None
        self.convhist = None
        self.entry_last = None

        self.vpx1 = 0.0
        self.vpx2 = 3600
        self.vpy1 = 1.0e-2
        self.vpy2 = 1.0e+6

        self.__init_figure()

    def __get_ticks_size(self):
        nDays = (self.vpx2-self.vpx1) / (24*3600)
        nDays = int(nDays + 0.9)
        if (nDays <= 1):
            tksH = (1.0, 0.25)
        elif (nDays == 2):
            tksH = (2.0, 0.5)
        elif (nDays <= 6):
            tksH = (6.0, 1.0)
        elif (nDays <= 12):
            tksH = (12.0, 3.0)
        else:
            n = int(nDays/6.0 + 0.9)
            tksH = (n*4, n)
        return tksH[0]*900, tksH[1]*900

    def __init_figure(self):
        tmaj, tmin = self.__get_ticks_size()
        self.ax.clear()
        self.ax.xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(CTUtil.seconds_to_iso))
        self.ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(tmaj))
        self.ax.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(tmin))
        self.ax.set_xlim  (self.vpx1, self.vpx2)
        self.ax.set_xlabel('Wall Time')
        self.autofmt_xdate()

        self.ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
        self.ax.axhline(1.0, linestyle='-', color='darkgrey', alpha=0.7)
        self.ax.set_axisbelow(True)
        self.ax.set_yscale('log')
        self.ax.set_ylim  (self.vpy1, self.vpy2)
        self.ax.set_ylabel('Stopping Criteria')

    def __draw_one_entry(self, entry):
        if (not self.entry_last):
            self.entry_last = entry
        if (entry.stime != self.entry_last.stime):
            x = self.entry_last.x
            y = self.vpy2
            self.ax.axvline(x, color='red', linestyle='dashed', label=str(entry.stime), alpha=0.8)
            self.ax.text(x+30, y*0.6, str(entry.stime), size='x-small')
        else:
            if (entry.iter == 1):
                x = self.entry_last.x
                self.ax.axvline(x, 0.9, 1.0, color='blue', linestyle='dashdot', alpha=0.5)

            x = numpy.array( [self.entry_last.x, entry.x] )
            y = numpy.array( [self.entry_last.cria,  entry.cria] )
            mkt, mkc = entry.get_marker()
            stl, clr = entry.get_line()
            self.ax.semilogy(x, y, linestyle=stl, color=clr, marker=mkt, markerfacecolor=mkc, picker=True, pickradius=5)

        self.entry_last = entry

    def __read_one_file(self, file, convhist):
        entry = CTEntry.Entry()
        done = False
        while (not done):
            where = file.tell()
            if entry.read(file):
                convhist.append(entry)
            else:
                done = True
                file.seek(where)

    def load_data_from_file(self, fname):
        self.__reset()

        for f in fname:
            self.file = open(f, 'r')
            self.convhist = CTEntry.AlgoConvergence()
            self.convhist.set_sourcex(self.sourcex)
            self.__read_one_file(self.file, self.convhist)
            self.convhist.count()

        viewCtr = self.get_view_ctrx()
        viewRng = self.get_view_rngx()
        self.set_view_x(viewCtr, viewRng)

    def draw_plot(self):
        self.fill_plot()
        self.redraw()

    def fill_plot(self):
        self.__init_figure()
        if (not self.convhist): return

        self.entry_last = None
        self.convhist.slice(self.__draw_one_entry, self.vpx1, self.vpx2)

    def get_canvas(self):
        return self.canvas

    def get_data_spnx(self):
        """Data span in x"""
        return self.convhist.get_rangex()

    def get_data_spny(self):
        """Data span in y"""
        return self.convhist.get_rangey()

    def __get_entry_cb(self, e):
        if (self.pick_x == e.x): self.pick_e = e

    def get_entry(self, line, x):
        self.pick_x = x
        self.pick_e = None
        if x: self.convhist.slice(self.__get_entry_cb, x-1, x+1)
        return self.pick_e

    def get_convergence(self):
        return self.convhist

    def get_stats(self):
        return self.convhist.get_stats()

    def get_view_ctrx(self):
        return (self.vpx1 + self.vpx2) / 2

    def get_view_ctry(self):
        return (self.vpy1 + self.vpy2) / 2

    def get_view_rngx(self):
        return (self.vpx2 - self.vpx1)

    def get_view_rngy(self):
        return (self.vpy2 - self.vpy1)

    def get_view_vptx(self):
        return (self.vpx1, self.vpx2)

    def get_view_vpty(self):
        return (self.vpy1, self.vpy2)

    def redraw(self):
        tmaj, tmin = self.__get_ticks_size()
        self.ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(tmaj))
        self.ax.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(tmin))
        self.ax.set_xlim(self.vpx1, self.vpx2)
        self.ax.set_ylim(self.vpy1, self.vpy2)
        self.autofmt_xdate()
        self.canvas.draw()

    def set_sourcex(self, attrib):
        self.sourcex = attrib
        if (self.convhist):
            self.convhist.resourcex(self.sourcex)
            self.convhist.count()

            viewCtr = self.get_view_ctrx()
            viewRng = self.get_view_rngx()
            self.set_view_x(viewCtr, viewRng)
            self.draw_plot()

    def set_view_x(self, viewCtr, viewRng):
        try:
            dataSpan = self.get_data_spnx()
            self.vpx1 = max(viewCtr-0.50*viewRng, dataSpan[0])
        except:
            self.vpx1 = viewCtr-0.50*viewRng
        self.vpx2 = self.vpx1 + viewRng
        #self.vpx2 = max(self.vpx1+viewRng, viewRng)

    def set_view_y(self, viewCtr, viewRng):
        try:
            dataSpan = self.get_data_spny()
            self.vpy1 = max(viewCtr-0.50*viewRng, dataSpan[0])
        except:
            self.vpy1 = viewCtr-0.50*viewRng
        self.vpy2 = self.vpy1 + viewRng
        #self.vpy2 = max(self.vpy1+viewRng, viewRng)

    def set_view_ctrx(self, viewCtr):
        viewRng = self.get_view_rngx()
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_ctry(self, viewCtr):
        viewRng = self.get_view_rngy()
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def set_view_rngx(self, viewRng):
        viewCtr = self.get_view_ctrx()
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_rngy(self, viewRng):
        viewCtr = self.get_view_ctry()
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def set_view_vptx(self, xmin, xmax):
        viewCtr = 0.5*(xmax + xmin)
        viewRng = xmax - xmin
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_vpty(self, ymin, ymax):
        viewCtr = 0.5*(ymax + ymin)
        viewRng = ymax - ymin
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def on_update(self):
        modified = False
        if (not self.file): return modified
        entry = CTEntry.Entry()
        where = self.file.tell()
        if entry.read(self.file):
            self.convhist.append(entry)
            self.convhist.count()
            modified = True
        else:
            self.file.seek(where)

        if (modified):
            viewCtr = self.get_view_ctrx()
            viewRng = self.get_view_rngx()
            self.set_view_x(viewCtr, viewRng)
            self.__draw_one_entry(entry)
            self.redraw()
        return modified

if __name__ == '__main__':
    app = wx.App()

    frame = wx.Frame(None, -1, "")
    plot = Plot(frame)
    plot.load_data_from_file(['tracer.log'])
    print(plot.get_view_vptx())
    print(plot.get_view_vpty())
    plot.draw_plot()

    frame.Show()
    app.MainLoop()
