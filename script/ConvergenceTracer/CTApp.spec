# -*- mode: python -*-
import os
import sys
sys.setrecursionlimit(5000)

block_cipher = None

pkg_path = os.path.join(os.environ['INRS_DEV'], 'H2D2', 'tools', 'script', 'ConvergenceTracer')

a = Analysis(['CTApp.py'],
             pathex=[pkg_path],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='CTApp',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='CTApp')
