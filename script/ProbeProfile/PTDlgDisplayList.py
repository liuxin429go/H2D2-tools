# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2003-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import sys
import wx
from wx.lib.mixins.listctrl import CheckListCtrlMixin

class CheckListCtrl(wx.ListCtrl, CheckListCtrlMixin):
    def __init__(self, parent):
        wx.ListCtrl.__init__(self, parent, -1, style=wx.LC_REPORT)
        CheckListCtrlMixin.__init__(self)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnItemActivated)
        self.Bind(wx.EVT_CHAR, self.OnCharEvent)

    def OnItemActivated(self, evt):
        self.ToggleItem(evt.m_itemIndex)

    def OnCharEvent(self, event):
        keycode  = event.GetKeyCode()
        ctrlDown = event.CmdDown()
        altDown  = event.AltDown()
        shiftDown= event.ShiftDown()

        if shiftDown: return
        if altDown: return

        if (ctrlDown and keycode in [1]):   # Ctrl-A
            n = self.GetItemCount()
            for i in range(n):
                self.SetItemState(i, wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)
        else:
            event.Skip()


class PTDlgDisplayList(wx.Dialog):
    def __init__(self, parent):
        self.cb_on_apply = None
        self.cb_args = None

        style = wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER | wx.MAXIMIZE_BOX | wx.MINIMIZE_BOX
        wx.Dialog.__init__(self, parent, style = style)

        self.lst = CheckListCtrl(self)
        self.btn_check   = wx.Button(self, -1, "Check All")
        self.btn_uncheck = wx.Button(self, -1, "Uncheck All")
        self.btn_apply   = wx.Button(self, -1, "Apply")

        self.__set_properties()
        self.__do_layout()

#        aTable = wx.AcceleratorTable([ (wx.ACCEL_CTRL, ord('A'), helpID),
#                                    ])
#        self.SetAcceleratorTable(aTable)

        self.Bind(wx.EVT_BUTTON, self.on_btn_check_all,   self.btn_check)
        self.Bind(wx.EVT_BUTTON, self.on_btn_uncheck_all, self.btn_uncheck)
        self.Bind(wx.EVT_BUTTON, self.on_btn_apply,  self.btn_apply)
        self.Bind(wx.EVT_CONTEXT_MENU, self.on_mnu_popup)

    def __set_properties(self):
        self.SetTitle("Display List")
        self.SetSize((800, 800))
        self.lst.InsertColumn(0, "Item")
        self.lst.InsertColumn(1, "")

    def __do_layout(self):
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(self.lst, 1, wx.ALL | wx.EXPAND, 0)
        sizer_1.Add(sizer_2,  0, wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(self.btn_check,   0, 0, 0)
        sizer_2.Add(self.btn_uncheck, 0, 0, 0)
        sizer_2.Add(self.btn_apply,   0, 0, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        self.Layout()

    def fillList(self, title, data, cb_on_apply, *cb_args):
        self.cb_on_apply = cb_on_apply
        self.cb_args = cb_args

        for i in range( len(data) ):
            v, t, c = data[i]
            try:
                self.lst.SetItem(i, 0, '  %2i' % i)
            except:
                index = self.lst.InsertItem(sys.maxsize, '  %2i' % i)
            self.lst.SetItem(i, 1, '%s' % t)
            item = self.lst.GetItem(i)
            item.SetTextColour(c)
            self.lst.SetItem(item)
            self.lst.CheckItem(i, v)

        self.SetTitle("Display List: %s" % title)
        self.lst.SetColumnWidth(0, wx.LIST_AUTOSIZE)
        self.lst.SetColumnWidth(1, wx.LIST_AUTOSIZE)

    def on_btn_apply(self, event):
        r = []
        n = self.lst.GetItemCount()
        for i in range(n):
            r.append( self.lst.IsChecked(i) )
        self.cb_on_apply(r, self.cb_args)

    def on_btn_uncheck_all(self, event):
        n = self.lst.GetItemCount()
        for i in range(n):
            self.lst.CheckItem(i, False)

    def on_btn_check_all(self, event):
        n = self.lst.GetItemCount()
        for i in range(n):
            self.lst.CheckItem(i)

    def on_mnu_popup(self, event):
        # Only do this part the first time so the events are only bound once
        if not hasattr(self, "id_mnu_check"):
            self.id_mnu_check   = wx.NewId()
            self.id_mnu_uncheck = wx.NewId()
            self.Bind(wx.EVT_MENU, self.on_mnu_check,   id=self.id_mnu_check)
            self.Bind(wx.EVT_MENU, self.on_mnu_uncheck, id=self.id_mnu_uncheck)

        # ---  Create menu
        menu = wx.Menu()
        menu.Append(self.id_mnu_check,   "Check")
        menu.Append(self.id_mnu_uncheck, "Uncheck")

        self.PopupMenu(menu)
        menu.Destroy()

    def on_mnu_uncheck(self, event):
        i = self.lst.GetFirstSelected()
        while i != -1:
            self.lst.CheckItem(i, False)
            i = self.lst.GetNextSelected(i)

    def on_mnu_check(self, event):
        i = self.lst.GetFirstSelected()
        while i != -1:
            self.lst.CheckItem(i)
            i = self.lst.GetNextSelected(i)

if __name__ == "__main__":
    def on_apply(v, *args):
        print(v)
    app = wx.PySimpleApp(0)
    wx.InitAllImageHandlers()
    dialog_1 = PTDlgDisplayList(None)
    lst = ("a", "b", "c")
    args = [ 'arg0', 'arg1' ]
    dialog_1.fillList(lst, on_apply, args)
    app.SetTopWindow(dialog_1)
    dialog_1.Show()
    app.MainLoop()
