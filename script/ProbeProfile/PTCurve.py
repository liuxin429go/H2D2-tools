# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2003-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import numpy

from CTCommon import CTUtil

class Curves:
    @staticmethod
    def reset_counter():
        Curve.reset_counter()

    def __init__(self):
        self.__reset()

    def __reset(self):
        self.file  = None
        self.curves = []
        self.xmin =  1.0e99
        self.xmax = -1.0e99
        self.ymin =  1.0e99
        self.ymax = -1.0e99

        self.vpx1 = -1.0e99
        self.vpx2 =  1.0e99
        self.vpy1 = -1.0e99
        self.vpy2 =  1.0e99

    def __read_one_entry(self):
        line = self.file.readline()
        if (not line): raise EOFError
        nlin, ncol, time = line.split()
        nlin = int(nlin)
        ncol = int(ncol)
        time = float(time)

        if (int(time) % (5*60) == 0):
            crv = Curve()
            for i in range(nlin):
                line = self.file.readline()
                val = float(line)
                crv.add_entry(float(i), val)
                self.ymin = min(self.ymin, val)
                self.ymax = max(self.ymax, val)
            self.curves.append(crv)
        else:
            for i in range(nlin):
                line = self.file.readline()

        self.xmin = min(self.xmin, 0)
        self.xmax = max(self.xmax, nlin)

    def __read_file_to_eof(self):
        try:
            while (True):
                where = self.file.tell()
                self.__read_one_entry()
        except EOFError:
            self.file.seek(where)

    def __read_table_one_entry(self, data):
        raise NotImplementedError()
    """
    time = data[0]
        nlin = len(data[1])
        ncol = len(data[1][0])
        assert(ncol == 1)

        if (len(self.curves) == 0):
            for i in range(nlin):
                self.curves.append( Curve(i) )

        for d in data[1]:
            val = d[0]
            self.curves[i].add_entry(time, val)
            self.ymin = min(self.ymin, val)
            self.ymax = max(self.ymax, val)

        self.xmin = min(self.xmin, time)
        self.xmax = max(self.xmax, time)
    """

    def __read_table_to_eod(self, data):
        for d in data:
            self.__read_table_one_entry(d)

    def load_data_from_file(self, fname):
        if (self.file): self.file.close()
        self.__reset()

        self.dname= os.path.basename(fname)
        self.fname= fname
        self.file = open(fname, 'r')
        self.__read_file_to_eof()

        self.vpx1 = self.xmin
        self.vpx2 = self.xmax
        self.vpy1 = self.ymin
        self.vpy2 = self.ymax

    def load_data_from_table(self, data):
        if (self.file): self.file.close()
        self.__reset()

        self.__read_table_to_eod(data)

        self.vpx1 = self.xmin
        self.vpx2 = self.xmax
        self.vpy1 = self.ymin
        self.vpy2 = self.ymax

    def fill_plot(self, ax):
        for c in self.curves:
            c.fill_plot(ax)

    def get_colors(self):
        clr = []
        clr.append('#000000')
        for c in self.curves:
            clr.append( c.get_color() )
        return clr

    def get_data_rangex(self):
        return self.xmin, self.xmax

    def get_data_rangey(self):
        return self.ymin, self.ymax

    def get_stats(self):
        txt = []
        txt.append( '%s' % self.dname )
        for c in self.curves:
            txt.append( '   ' + c.get_stats() )
        return txt

    def set_data_rangex(self, r):
        self.vpx1 = r[0]
        self.vpx2 = r[1]

    def on_update(self):
        modified = False
        if (not self.file): return modified
        where = self.file.tell()
        try:
            self.__read_file_to_eof()
        except:
            self.file.seek(where)
        modified = where != self.file.tell()
        return modified

    def get_visibility(self):
        r = [ ]
        for c in self.curves:
            r.append( c.get_visibility() )
        return r

    def set_visibility(self, visi):
        for c, v in zip(self.curves, visi):
            c.set_visibility(v)

class Curve:
    counter = 0

    @staticmethod
    def reset_counter():
        Curve.counter = 0

    def __init__(self):
        self.__reset()
        self.clr = CTUtil.get_clr(Curve.counter)
        Curve.counter += 1

    def __reset(self):
        self.dsp  = True
        self.idx  = None
        self.data = []
        self.xmin =  1.0e99
        self.xmax = -1.0e99
        self.ymin =  1.0e99
        self.ymax = -1.0e99

        self.vpx1 = -1.0e99
        self.vpx2 =  1.0e99
        self.vpy1 = -1.0e99
        self.vpy2 =  1.0e99

    def add_entry(self, time, val):
        self.data.append( [time, val] )
        self.ymin = min(self.ymin, val)
        self.ymax = max(self.ymax, val)
        self.xmin = min(self.xmin, time)
        self.xmax = max(self.xmax, time)

    def fill_plot(self, ax):
        if (not self.dsp): return
        def is_ok(e) : return ((e[0] >= self.vpx1 and e[0] <= self.vpx2) and \
                               (e[1] >= self.vpy1 and e[1] <= self.vpy2))
        e_flt = filter(is_ok, self.data)
        x = numpy.array( map(lambda e: e[0], e_flt) )
        y = numpy.array( map(lambda e: e[1], e_flt) )
#        mkt, mkc = entry.get_marker()
        ax.plot(x, y, color = self.clr) #, linestyle=stl, color=clr, marker=mkt, markerfacecolor=mkc, picker=True, pickradius=5)

    def get_color(self):
        return self.clr

    def get_data_rangex(self):
        return self.xmin, self.xmax

    def get_data_rangey(self):
        return self.ymin, self.ymax

    def get_stats(self):
        return 'vmin= %f; vmax = %f' % (self.ymin, self.ymax)

    def set_data_rangex(self, r):
        self.vpx1 = r[0]
        self.vpx2 = r[1]

    def get_visibility(self):
        return self.dsp

    def set_visibility(self, visibility = True):
        self.dsp = visibility

if __name__ == '__main__':
    plot = Curves()
    plot.load_data_from_file('p1.prb')
    plot.fill_plot(None)

