# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2003-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx
import wx.adv as wx_adv
import wx.lib.wordwrap as wx_wordwrap
import wx.richtext     as wx_rt

from CTCommon import CTWindow
from CTCommon import CTUtil
from CTCommon import FloatSlider as FS
import PTPlot
import PTDlgDisplayList
import os

class DummyMenuItem:
    def Check(self, b): pass
    def GetId(self): return wx.ID_CANCEL

class PTFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: CTFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        super(PTFrame, self).__init__(*args, **kwds)

        self.split_main = wx.SplitterWindow(self, -1, style=wx.SP_3D|wx.SP_BORDER)

        # Widgets
        style_base = wx.BORDER_STATIC
        self.tracer_plot = CTWindow.CTWindow(proxied_class    = PTPlot.Plot,
                                             do_update        = True,
                                             cb_on_data_change= self.on_tracer_change,
                                             cb_on_data_pick  = self.on_tracer_pick,
                                             cb_on_mouse_move = self.on_mouse_move,
                                             parent = self.split_main)
        self.tracer_stat = wx_rt.RichTextCtrl(self.split_main, -1, style = style_base | wx.TE_MULTILINE | wx.ST_NO_AUTORESIZE)
        self.logo   = wx.Panel(self, -1, style = style_base)
        self.slider = FS.FloatSlider(self, -1, style = style_base | wx.SL_HORIZONTAL | wx.SL_LABELS, resolution=1, formater=CTUtil.seconds_to_iso)

        def addItem(mnu, name, help, style):
            item = wx.MenuItem(mnu, wx.ID_ANY, name, help, style)
            mnu.Append(item)
            return item
        # Menu Bar
        self.menubar = wx.MenuBar()
        self.mnu_file = wx.Menu()
        self.mnu_file_open = addItem(self.mnu_file, "Open...\tCtrl+O", "Open a file", wx.ITEM_NORMAL)
        self.mnu_file_quit = addItem(self.mnu_file, "Quit\tCtrl+Q", "Quite application", wx.ITEM_NORMAL)
        self.menubar.Append(self.mnu_file, "File")

        self.mnu_view = wx.Menu()
        self.mnu_view_rdrw   = addItem(self.mnu_view, "Refresh\tF5", "Redraw the display", wx.ITEM_NORMAL)
        self.mnu_view_pause  = addItem(self.mnu_view, "Pause\tCtrl+P", "Don't update", wx.ITEM_CHECK)
        self.mnu_view_hlght  = DummyMenuItem()
        self.mnu_view.AppendSeparator()
        self.mnu_view_rst   = addItem(self.mnu_view, "Reset\tCtrl+R", "Reset viewport to data range", wx.ITEM_NORMAL)
        self.mnu_view_rstx  = addItem(self.mnu_view, "Reset x\tCtrl+X", "Reset viewport to x data range", wx.ITEM_NORMAL)
        self.mnu_view_rsty  = addItem(self.mnu_view, "Reset y\tCtrl+Y", "Reset viewport to y data range", wx.ITEM_NORMAL)
        self.mnu_view.AppendSeparator()
        self.mnu_view_x = wx.Menu()
        self.mnu_view_vptx    = addItem(self.mnu_view_x, "x-viewport...", "set the viewport in x", wx.ITEM_NORMAL)
        self.mnu_view_rngx    = addItem(self.mnu_view_x, "x-range...", "set the range in x", wx.ITEM_NORMAL)
        self.mnu_view_ctrx    = addItem(self.mnu_view_x, "x-center...", "set the center in x", wx.ITEM_NORMAL)
        self.mnu_view_x.AppendSeparator()
        self.mnu_view_x_wtime = DummyMenuItem() # addItem(self.mnu_view_x, "wtime", "set wall time as x component", wx.ITEM_CHECK)
        self.mnu_view_x_stime = DummyMenuItem() # addItem(self.mnu_view_x, "stime", "set simulation time as x component", wx.ITEM_CHECK)
        self.mnu_view_x_iter  = DummyMenuItem() # addItem(self.mnu_view_x, "iter", "set iteration as x component", wx.ITEM_CHECK)
        self.mnu_view.AppendSubMenu(self.mnu_view_x, "x-axis")
        self.mnu_view_y = wx.Menu()
        self.mnu_view_vpty    = addItem(self.mnu_view_y, "y-viewport...", "set the viewport in y", wx.ITEM_NORMAL)
        self.mnu_view_rngy    = addItem(self.mnu_view_y, "y-range...", "set the range in y", wx.ITEM_NORMAL)
        self.mnu_view_ctry    = addItem(self.mnu_view_y, "y-center...", "set the center in y", wx.ITEM_NORMAL)
        self.mnu_view.AppendSubMenu(self.mnu_view_y, "y-axis")
        self.menubar.Append (self.mnu_view, "View")

        self.mnu_dspl = wx.Menu()
        self.menubar.Append(self.mnu_dspl, "Display")

        self.mnu_help = wx.Menu()
        self.mnu_help_about = self.mnu_help.Append(wx.ID_ABOUT)
        self.menubar.Append(self.mnu_help, "Help")
        self.SetMenuBar(self.menubar)

        # Status Bar
        self.statusbar = self.CreateStatusBar(2)

        # Tool Bar
        self.toolbar = self.tracer_plot.get_toolbar()
        self.toolbar.Reparent(self)
        self.SetToolBar(self.toolbar)

        # Display list pop-up
        self.dlg_dlist = []

        data_w = 150
        sldr_h = 35
        self.__set_properties(data_w, sldr_h)
        self.__do_layout(data_w, sldr_h)

        self.Bind(wx.EVT_MENU, self.on_mnu_file_open,    self.mnu_file_open)
        self.Bind(wx.EVT_MENU, self.on_mnu_file_quit,    self.mnu_file_quit)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rdrw,    self.mnu_view_rdrw)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_pause,   self.mnu_view_pause)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_hlght,   self.mnu_view_hlght)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rst,     self.mnu_view_rst)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rstx,    self.mnu_view_rstx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rsty,    self.mnu_view_rsty)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_ctrx,    self.mnu_view_ctrx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rngx,    self.mnu_view_rngx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_vptx,    self.mnu_view_vptx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_wtime, self.mnu_view_x_wtime)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_stime, self.mnu_view_x_stime)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_iter,  self.mnu_view_x_iter)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_ctry,    self.mnu_view_ctry)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rngy,    self.mnu_view_rngy)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_vpty,    self.mnu_view_vpty)
        self.Bind(wx.EVT_MENU, self.on_mnu_help_about,   self.mnu_help_about)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_ENDSCROLL, self.on_scroll_change)

        self.dirname  = ''

    def __set_properties(self, data_w, sldr_h):
        self.SetTitle("H2D2 - Probe Tracer")
        self.SetSize((800, 500))

        statusbar_fields = ['', 'Position']
        for i in range(len(statusbar_fields)):
            self.statusbar.SetStatusText(statusbar_fields[i], i)
        self.toolbar.Realize()

        self.split_main.SetSashGravity(1.0)
        self.split_main.SetMinimumPaneSize(20)

        self.tracer_stat.SetMinSize ((data_w, 15))
        self.tracer_stat.SetEditable(False)

        self.logo.SetMinSize((data_w, sldr_h))

        self.slider.SetMinSize((100, sldr_h))
        self.slider.SetRange(0, 3600)
        self.slider.SetValue(0)
        self.slider.SetPageSize(900)
        self.slider.Enable(False)

        self.mnu_view_hlght.Check(False)
        self.mnu_view_x_wtime.Check(True)
        self.mnu_view_x_stime.Check(False)
        self.mnu_view_x_iter.Check(False)

    def __do_layout(self, data_w, sldr_h):
        sizer_frame = wx.BoxSizer(wx.VERTICAL)
        sizer_bottm = wx.BoxSizer(wx.HORIZONTAL)
        sizer_frame.Add(self.split_main, 1, wx.EXPAND, 0)
        sizer_frame.Add(sizer_bottm,     0, wx.EXPAND, 0)
        sizer_bottm.Add(self.slider, 1, wx.EXPAND, 0)
        sizer_bottm.Add(self.logo,   0, wx.EXPAND, 0)

        self.split_main.SplitVertically(self.tracer_plot, self.tracer_stat, -data_w)
        self.SetSizer(sizer_frame)
        self.Layout()

    def __fill_mnu_dspl(self, fpath):
        def callback_factory(i):
            return lambda x: self.on_mnu_dspl(x, i)

        self.dlg_dlist = []
        for i in range(len(fpath)):
            self.dlg_dlist.append(None)

        for m in self.mnu_dspl.GetMenuItems():
            self.mnu_dspl.DestroyItem(m)

        for f, i in zip(fpath, list(range(len(fpath)))):
            mnuItm = wx.MenuItem(self.mnu_dspl, wx.ID_ANY, '%s...' % f, ' ', wx.ITEM_NORMAL)
            self.mnu_dspl.Append(mnuItm)
            self.Bind(wx.EVT_MENU, callback_factory(i), mnuItm)

    def on_mnu_file_open(self, event):
        dlg = wx.FileDialog(self, "Open", self.dirname, "", "*.*", wx.FD_OPEN | wx.FD_MULTIPLE)
        if dlg.ShowModal() == wx.ID_OK:
            filenames = dlg.GetFilenames()
            dirname   = dlg.GetDirectory()
            if (len(filenames) > 0):
                fpath = []
                for f in filenames: fpath.append( os.path.join(dirname, f) )
                self.tracer_plot.load_data_from_file(fpath)

                self.SetTitle(' '.join( (filenames[0], '...') ))
                self.slider.Enable(True)
                self.__fill_mnu_dspl(filenames)
            else:
                dlg = wx.MessageDialog(self, 'Select the files to be ploted', 'Error', wx.OK | wx.ICON_ERROR)
                if (dlg.ShowModal() == wx.OK):
                    self.Close(True)
        dlg.Destroy()

    def on_mnu_file_quit(self, event):
        dlg = wx.MessageDialog(self, 'Are you sure? \n', 'Quit', wx.YES_NO)
        if (dlg.ShowModal() == wx.ID_YES):
            self.Close(True)

    def on_mnu_view_rdrw(self, event):
        self.tracer_plot.draw_plot()

    def on_mnu_view_pause(self, event):
        self.tracer_plot.pause( self.mnu_view_pause.IsChecked() )

    def on_mnu_view_rst(self, event):
        self.tracer_plot.reset_view()

    def on_mnu_view_rstx(self, event):
        self.tracer_plot.reset_viewx()

    def on_mnu_view_rsty(self, event):
        self.tracer_plot.reset_viewy()

    def on_mnu_view_ctrx(self, event):
        ctr = self.tracer_plot.get_view_ctrx()
        str = '%f' % ctr
        dlg = wx.TextEntryDialog(self, 'Enter the data central value in X', 'Data central value in X', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_ctrx(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_ctry(self, event):
        ctr = self.tracer_plot.get_view_ctry()
        str = '%f' % ctr
        dlg = wx.TextEntryDialog(self, 'Enter the data central value in Y', 'Data central value in Y', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_ctry(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_rngx(self, event):
        rng = self.tracer_plot.get_view_rngx()
        str = '%f' % rng
        dlg = wx.TextEntryDialog(self, 'Enter the displayed data range in X', 'Data range in X', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_rngx(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_rngy(self, event):
        rng = self.tracer_plot.get_view_rngy()
        str = '%f' % rng
        dlg = wx.TextEntryDialog(self, 'Enter the displayed data range in Y', 'Data range in Y', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_rngy(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_vptx(self, event):
        x1, x2 = self.tracer_plot.get_view_vptx()
        str = '%f %f' % (x1, x2)
        dlg = wx.TextEntryDialog(self, 'Enter the x-axis viewport', 'x-axis viewport', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                x1, x2 = dlg.GetValue().split()
                x1 = eval(x1)
                x2 = eval(x2)
                self.tracer_plot.set_view_vptx(float(x1), float(x2))
            except:
                msg = wx.MessageDialog(self, 'Two, blanck separated, floating point values expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_vpty(self, event):
        y1, y2 = self.tracer_plot.get_view_vpty()
        str = '%f %f' % (y1, y2)
        dlg = wx.TextEntryDialog(self, 'Enter the y-axis viewport', 'y-axis viewport', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                y1, y2 = dlg.GetValue().split()
                y1 = eval(y1)
                y2 = eval(y2)
                self.tracer_plot.set_view_vpty(float(y1), float(y2))
            except:
                msg = wx.MessageDialog(self, 'Two, blanck separated, floating point values expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_hlght(self, event):
        pass

    def on_mnu_view_x_wtime(self, event):
        self.mnu_view_x_wtime.Check(True)
        self.mnu_view_x_stime.Check(False)
        self.mnu_view_x_iter.Check(False)
        self.tracer_plot.set_sourcex('wtime')

    def on_mnu_view_x_stime(self, event):
        self.mnu_view_x_wtime.Check(False)
        self.mnu_view_x_stime.Check(True)
        self.mnu_view_x_iter.Check(False)
        self.tracer_plot.set_sourcex('stime')

    def on_mnu_view_x_iter(self, event):
        self.mnu_view_x_wtime.Check(False)
        self.mnu_view_x_stime.Check(False)
        self.mnu_view_x_iter.Check(True)
        self.tracer_plot.set_sourcex('index')

    def on_mnu_dspl(self, event, idx):
        vis = self.tracer_plot.get_visibility()
        txt = self.tracer_plot.get_stats()
        clr = self.tracer_plot.get_colors()
        tit = txt[idx][0]
        txt = txt[idx][1:]
        clr = clr[idx][1:]
        vis = vis[idx]

        if (not self.dlg_dlist[idx]):
            self.dlg_dlist[idx] = PTDlgDisplayList.PTDlgDisplayList(self)
        dlg = self.dlg_dlist[idx]
        dlg.fillList(tit, list(zip(vis, txt, clr)), self.on_dlg_dspl_apply, idx)
        dlg.Show()

    def on_dlg_dspl_apply(self, v, args):
        idx = args[0]
        vis = self.tracer_plot.get_visibility()
        vis[idx] = v
        self.tracer_plot.set_visibility(vis)
        self.tracer_plot.draw()

    def on_mnu_help_about(self, event):
        info = wx_adv.AboutDialogInfo()
        info.Name = "H2D2 Probe Tracer"
        info.Version = "18.04rc1"
        info.Copyright = "(C) 2010-2018 INRS"
        info.Description = wx_wordwrap(
            "\nH2D2 Probe Tracer plots the data from simple probes."
            "If the simulation is still running it will follow the progress.\n"
            "The program allows for simple graphic interaction such as pan and zoom.",
            350, wx.ClientDC(self))
        info.WebSite = ("http://www.gre-ehn.ete.inrs.ca/H2D2", "H2D2 home page")
        info.Developers = [ "Yves Secretan" ]
#        info.License = wx.lib.wordwrap.wordwrap(licenseText, 500, wx.ClientDC(self))

        wx_adv.AboutBox(info)

    def on_scroll_change(self, event):
        x = event.GetPosition()
        self.tracer_plot.set_view_ctrx(x)

    def on_tracer_change(self):
        x1, x2 = self.tracer_plot.get_data_spnx()
        xc     = self.tracer_plot.get_view_ctrx()
        v1, v2 = self.tracer_plot.get_view_vptx()
        txt    = self.tracer_plot.get_stats()
        clr    = self.tracer_plot.get_colors()

        self.slider.SetRange(x1, x2)
        self.slider.SetValue(xc)
        self.slider.SetPageSize((v2-v1) / 4)

        self.tracer_stat.Clear()
        for cl, ll in zip(clr, txt):
            for c, l in zip(cl, ll):
                self.tracer_stat.BeginTextColour(c)
                self.tracer_stat.WriteText(l)
                self.tracer_stat.EndTextColour()
                self.tracer_stat.WriteText('\n')

    def on_tracer_pick(self, txt = ()):
        self.tracer_data.Clear()
        self.tracer_data.WriteText(txt)

    def on_mouse_move(self, s):
        self.statusbar.SetStatusText("Pos: %s" % s, 1)

# end of class PTFrame
