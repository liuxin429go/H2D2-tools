@echo off

set BIN_DIR=%~dp0
set BIN_DIR=%BIN_DIR:~0,-1%

if exist "%BIN_DIR%\..\script" (
   set BIN_DIR="%BIN_DIR%\..\script\ProbeProfile"
)

pushd "%BIN_DIR%"
python PTApp.py
popd
