# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2003-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import matplotlib
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg

import numpy
import wx

from CTCommon import CTUtil
import PTCurve

class Plot(matplotlib.figure.Figure):
    def __init__(self, parent, *args, **kwargs):
        sp_param  = matplotlib.figure.SubplotParams(left=0.10, bottom=0.15, right=0.98, top=0.96)
        matplotlib.figure.Figure.__init__(self, subplotpars=sp_param)
        self.canvas = FigureCanvasWxAgg(parent, -1, self)

        self.ax = self.add_subplot(111)

        self.sourcex = 'stime'
        self.__reset()

    def __reset(self):
        self.curves = []
        PTCurve.Curves.reset_counter()

        self.xmin =  1.0e99
        self.xmax = -1.0e99
        self.ymin =  1.0e99
        self.ymax = -1.0e99

        self.vpx1 = 0.0
        self.vpx2 = 3600*12
        self.vpy1 = -10.0
        self.vpy2 =  10.0

        self.__init_figure()
    
    def __init_figure(self):
        self.ax.clear()
        self.ax.xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(CTUtil.seconds_to_iso))
        self.ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(3600))
        self.ax.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(900))
        self.ax.set_xlim  (self.vpx1, self.vpx2)
        self.ax.set_xlabel('Simulation Time')
        self.autofmt_xdate()

        self.ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
        self.ax.axhline(0.0, linestyle='-', color='darkgrey', alpha=0.7)
        self.ax.set_axisbelow(True)
        self.ax.set_ylim  (self.vpy1, self.vpy2)
        self.ax.set_ylabel('h')

    def load_data_from_file(self, fname):
        self.__reset()
        for f in fname:
            crv = PTCurve.Curves()
            self.curves.append(crv)
            crv.load_data_from_file(f)
        
            rx = crv.get_data_rangex()
            ry = crv.get_data_rangey()
            self.xmin = min(self.xmin, rx[0])
            self.xmax = max(self.xmax, rx[1])
            self.ymin = min(self.ymin, ry[0])
            self.ymax = max(self.ymax, ry[1])

        self.reset_view()

    def draw_plot(self):
        self.fill_plot()
        self.redraw()
    
    def fill_plot(self):
        self.__init_figure()
        for c in self.curves:
            c.fill_plot(self.ax)
        
    def get_canvas(self):
        return self.canvas

    def get_colors (self):
        clr = []
        for c in self.curves:
            clr.append( c.get_colors() )
        return clr

    def get_data_spnx(self):
        """Data span in x"""
        return (self.xmin, self.xmax)
        
    def get_data_spny(self):
        """Data span in y"""
        return (self.ymin, self.ymax)

    def get_entry(self, x):
      pass

    def get_stats (self):
        txt = []
        for c in self.curves:
            txt.append( c.get_stats() )
        return txt

    def get_view_ctrx(self):
        return (self.vpx1 + self.vpx2) / 2

    def get_view_ctry(self):
        return (self.vpy1 + self.vpy2) / 2

    def get_view_rngx(self):
        return (self.vpx2 - self.vpx1)

    def get_view_rngy(self):
        return (self.vpy2 - self.vpy1)

    def get_view_vptx(self):
        return (self.vpx1, self.vpx2)

    def get_view_vpty(self):
        return (self.vpy1, self.vpy2)

    def get_visibility(self):
        r = [ ]
        for c in self.curves:
            r.append( c.get_visibility() )
        return r
            
    def redraw(self):
        print((self.vpx2-self.vpx1)/3600.0)
        if (((self.vpx2-self.vpx1)/3600.0) < 100):
            self.ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(3600))
            self.ax.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(900))
        else:
            self.ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(3600*4))
            self.ax.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(900*4))
        self.ax.set_xlim(self.vpx1, self.vpx2)
        self.ax.set_ylim(self.vpy1, self.vpy2)
        self.autofmt_xdate()
        self.canvas.draw()

    def reset_view(self):
        self.reset_viewx()
        self.reset_viewy()
      
    def reset_viewx(self):
        xmin, xmax = self.get_data_spnx()
        self.set_view_vptx(xmin, xmax)
      
    def reset_viewy(self):
        ymin, ymax = self.get_data_spny()
        self.set_view_vpty(ymin, ymax)
      
    def get_data_spny(self):
        """Data span in y"""
        return (self.ymin, self.ymax)
    
    def set_sourcex(self, attrib):
        pass

    def set_view_x(self, viewCtr, viewRng):
        try:
            dataSpan = self.get_data_spnx()
            self.vpx1 = max(viewCtr-0.50*viewRng, dataSpan[0])
        except:
            self.vpx1 = viewCtr-0.50*viewRng
        self.vpx2 = self.vpx1 + viewRng
#        self.vpx2 = max(self.vpx1+viewRng, viewRng)
    
    def set_view_y(self, viewCtr, viewRng):
#        try:
#            dataSpan = self.get_data_spny()
#            self.vpy1 = max(viewCtr-0.50*viewRng, dataSpan[0])
#        except:
#            self.vpy1 = viewCtr-0.50*viewRng
        self.vpy1 = viewCtr-0.50*viewRng
        self.vpy2 = self.vpy1 + viewRng
    
    def set_view_ctrx(self, viewCtr):
        viewRng = self.get_view_rngx()
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_ctry(self, viewCtr):
        viewRng = self.get_view_rngy()
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def set_view_rngx(self, viewRng):
        viewCtr = self.get_view_ctrx()
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_rngy(self, viewRng):
        viewCtr = self.get_view_ctry()
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def set_view_vptx(self, xmin, xmax):
        viewCtr = 0.5*(xmax + xmin)
        viewRng = xmax - xmin
        self.set_view_x(viewCtr, viewRng)
        self.draw_plot()

    def set_view_vpty(self, ymin, ymax):
        viewCtr = 0.5*(ymax + ymin)
        viewRng = ymax - ymin
        self.set_view_y(viewCtr, viewRng)
        self.draw_plot()

    def set_visibility(self, visi):
        for c, v in zip(self.curves, visi):
            c.set_visibility(v)
            
    def on_update(self):
        modified = False
        for c in self.curves:
            mdf = c.on_update()
            if (mdf):
                rx = c.get_data_rangex()
                ry = c.get_data_rangey()
                self.xmin = min(self.xmin, rx[0])
                self.xmax = max(self.xmax, rx[1])
                self.ymin = min(self.ymin, ry[0])
                self.ymax = max(self.ymax, ry[1])
                modified = True
        if (modified):
            viewCtr = self.get_view_ctrx()
            viewRng = self.get_view_rngx()
            self.set_view_x(viewCtr, viewRng)
            self.draw_plot()
        return modified

if __name__ == '__main__':
    app = wx.PySimpleApp()

    frame = wx.Frame(None, -1, "")
    plot = Plot(frame)
    plot.load_data_from_file('p1.prb')
    plot.draw_plot()

    frame.Show()
    app.MainLoop()
