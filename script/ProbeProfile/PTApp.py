#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2003-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import sys
selfDir = os.path.dirname( os.path.abspath(__file__) )
supPath = os.path.normpath(os.path.join(selfDir, '..'))
if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

import PTFrame
import wx

class PTApp(wx.App):
    def OnInit(self):
        tracerFrame = PTFrame.PTFrame(None, -1, "")
        self.SetTopWindow(tracerFrame)
        tracerFrame.Show()
        return 1

if __name__ == "__main__":
    PTApp = PTApp(0)
    PTApp.MainLoop()
