#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from CTCommon import CTCheckListCtrl

import sys
import wx

class PTDlgDisplayList(wx.Dialog):
    def __init__(self, parent):
        self.cb_on_apply = None
        self.cb_args = None

        style = wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER | wx.MAXIMIZE_BOX | wx.MINIMIZE_BOX
        super(PTDlgDisplayList, self).__init__(parent, style = style)

        self.lst = CTCheckListCtrl.CTCheckListCtrl(self)
        self.btn_check   = wx.Button(self, -1, "Check All")
        self.btn_uncheck = wx.Button(self, -1, "Uncheck All")
        self.btn_apply   = wx.Button(self, -1, "Apply")

        self.__set_properties()
        self.__do_layout()

#        aTable = wx.AcceleratorTable([ (wx.ACCEL_CTRL, ord('A'), helpID),
#                                    ])
#        self.SetAcceleratorTable(aTable)

        self.Bind(wx.EVT_BUTTON, self.on_btn_check_all,   self.btn_check)
        self.Bind(wx.EVT_BUTTON, self.on_btn_uncheck_all, self.btn_uncheck)
        self.Bind(wx.EVT_BUTTON, self.on_btn_apply,       self.btn_apply)
        self.Bind(wx.EVT_CONTEXT_MENU, self.on_mnu_popup)

    def __set_properties(self):
        self.SetTitle("Display List")
        self.SetSize((500, 300))
        self.lst.InsertColumn(0, "Item")
        self.lst.InsertColumn(1, "")

    def __do_layout(self):
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(self.lst, 1, wx.ALL | wx.EXPAND, 0)
        sizer_1.Add(sizer_2,  0, wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(self.btn_check,   0, 0, 0)
        sizer_2.Add(self.btn_uncheck, 0, 0, 0)
        sizer_2.Add(self.btn_apply,   0, 0, 0)
        self.SetSizer(sizer_1)
        #sizer_1.Fit(self)
        self.Layout()

    def fillList(self, title, data, cb_on_apply, *cb_args):
        self.cb_on_apply = cb_on_apply
        self.cb_args = cb_args

        for i in range( len(data) ):
            v, t, c = data[i]
            try:
                self.lst.SetItem(i, 0, '  %3i' % (i+1))
            except:
                j = self.lst.GetItemCount()
                _ = self.lst.InsertItem(j, '  %3i' % (i+1))
            self.lst.SetItem(i, 1, '%s' % t)
            item = self.lst.GetItem(i)
            item.SetTextColour(c)
            self.lst.SetItem(item)
            self.lst.CheckItem(i, v)

        self.SetTitle("Display List: %s" % title)
        self.lst.SetColumnWidth(0, wx.LIST_AUTOSIZE)
        self.lst.SetColumnWidth(1, wx.LIST_AUTOSIZE)

    def on_btn_apply(self, event):
        r = []
        n = self.lst.GetItemCount()
        for i in range(n):
            r.append( self.lst.IsChecked(i) )
        self.cb_on_apply(r, self.cb_args)

    def on_btn_uncheck_all(self, event):
        n = self.lst.GetItemCount()
        for i in range(n):
            self.lst.CheckItem(i, False)

    def on_btn_check_all(self, event):
        n = self.lst.GetItemCount()
        for i in range(n):
            self.lst.CheckItem(i)

    def on_mnu_popup(self, event):
        # Only do this part the first time so the events are only bound once
        if not hasattr(self, "id_mnu_check"):
            self.id_mnu_check   = wx.NewId()
            self.id_mnu_uncheck = wx.NewId()
            self.Bind(wx.EVT_MENU, self.on_mnu_check,   id=self.id_mnu_check)
            self.Bind(wx.EVT_MENU, self.on_mnu_uncheck, id=self.id_mnu_uncheck)

        # ---  Create menu
        menu = wx.Menu()
        menu.Append(self.id_mnu_check,   "Check")
        menu.Append(self.id_mnu_uncheck, "Uncheck")

        self.PopupMenu(menu)
        menu.Destroy()

    def on_mnu_uncheck(self, event):
        i = self.lst.GetFirstSelected()
        while i != -1:
            self.lst.CheckItem(i, False)
            i = self.lst.GetNextSelected(i)

    def on_mnu_check(self, event):
        i = self.lst.GetFirstSelected()
        while i != -1:
            self.lst.CheckItem(i)
            i = self.lst.GetNextSelected(i)

if __name__ == "__main__":
    def on_apply(v, *args):
        print(v)
    app = wx.App(False)
    dialog_1 = PTDlgDisplayList(None)
    lst = [ (True, t, 'k') for t in ("a", "b", "c", "d", "e", "f", "g") ]
    args = [ 'arg0', 'arg1' ]
    dialog_1.fillList('Title', lst, on_apply, args)
    app.SetTopWindow(dialog_1)
    dialog_1.Show()
    app.MainLoop()
