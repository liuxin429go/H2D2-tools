# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2017
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx
import wx.adv as wx_adv
import wx.lib.wordwrap as wx_wordwrap
import wx.richtext     as wx_rt

from CTCommon import CTWindow
from CTCommon import CTUtil
from CTCommon import FloatSlider as FS
from CTCommon import CTPlot
import PTPlot
import PTDlgDisplayList
import os
import sys

class DummyMenuItem:
    def Check(self, b): pass
    def GetId(self): return wx.ID_CANCEL

class DropTarget(wx.FileDropTarget):
    def __init__(self, parent):
        super(DropTarget, self).__init__()
        self.parent = parent

    def OnDropFiles(self, x, y, filenames):
        return self.parent.OnDropFiles(x, y, filenames)


class PTFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: CTFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        super(PTFrame, self).__init__(*args, **kwds)

        self.dropTarget = DropTarget(self)
        self.SetDropTarget(self.dropTarget)

        self.split_main = wx.SplitterWindow(self, -1, style=wx.SP_3D|wx.SP_BORDER)

        # Widgets
        style_base = wx.BORDER_STATIC
        self.tracer_plot = CTWindow.CTWindow(proxied_class    = PTPlot.Plot,
                                             do_update        = True,
                                             cb_on_data_change= self.on_tracer_change,
                                             cb_on_data_pick  = self.on_tracer_pick,
                                             cb_on_mouse_move = self.on_mouse_move,
                                             parent = self.split_main,
                                             xformatter = CTPlot.AxisFormatter.epoch_to_isoutc)
        self.tracer_stat = wx_rt.RichTextCtrl(self.split_main, -1, style = style_base | wx.TE_MULTILINE | wx.ST_NO_AUTORESIZE)
        self.logo   = wx.Panel(self, -1, style = style_base)
        self.slider = FS.FloatSlider(self, -1, style = style_base | wx.SL_HORIZONTAL | wx.SL_LABELS, resolution=1, formater=CTUtil.seconds_to_iso)

        # File history
        self.history= wx.FileHistory(12)
        self.config = wx.Config('H2D2: Probe Tracer', style=wx.CONFIG_USE_LOCAL_FILE)
        self.history.Load(self.config)
        hist_mnu = wx.Menu()
        self.history.UseMenu(hist_mnu)
        self.history.AddFilesToMenu()

        def addItem(mnu, name, help, style):
            item = wx.MenuItem(mnu, wx.ID_ANY, name, help, style)
            mnu.Append(item)
            return item
        # Menu Bar
        self.menubar = wx.MenuBar()
        self.mnu_file = wx.Menu()
        self.mnu_file_open = addItem(self.mnu_file, "Open...\tCtrl+O", "Open a file", wx.ITEM_NORMAL)
        self.mnu_file_add  = addItem(self.mnu_file, "Add...\tCtrl+A", "Add a file", wx.ITEM_NORMAL)
        self.mnu_file_hist = self.mnu_file.Append(wx.ID_ANY, "Add a recent file\tCtrl+R", hist_mnu)
        self.mnu_file.AppendSeparator()
        self.mnu_file_close= addItem(self.mnu_file, "Close\tCtrl+W", "Close all plots", wx.ITEM_NORMAL)
        self.mnu_file_quit = addItem(self.mnu_file, "Quit\tCtrl+Q", "Quit application", wx.ITEM_NORMAL)
        self.menubar.Append(self.mnu_file, "File")

        self.mnu_view = wx.Menu()
        self.mnu_view_rdrw   = addItem(self.mnu_view, "Refresh\tF5", "Redraw the display", wx.ITEM_NORMAL)
        self.mnu_view_pause  = addItem(self.mnu_view, "Pause\tCtrl+P", "Don't update", wx.ITEM_CHECK)
        self.mnu_view_hlght  = DummyMenuItem()
        self.mnu_view.AppendSeparator()
        self.mnu_view_rst   = addItem(self.mnu_view, "Reset\tCtrl+R", "Reset viewport to data range", wx.ITEM_NORMAL)
        self.mnu_view_rstx  = addItem(self.mnu_view, "Reset x\tCtrl+X", "Reset viewport to x data range", wx.ITEM_NORMAL)
        self.mnu_view_rsty  = addItem(self.mnu_view, "Reset y\tCtrl+Y", "Reset viewport to y data range", wx.ITEM_NORMAL)
        self.mnu_view.AppendSeparator()
        self.mnu_view_x = wx.Menu()
        self.mnu_view_vptx    = addItem(self.mnu_view_x, "x-viewport...", "set the viewport in x", wx.ITEM_NORMAL)
        self.mnu_view_rngx    = addItem(self.mnu_view_x, "x-range...", "set the range in x", wx.ITEM_NORMAL)
        self.mnu_view_ctrx    = addItem(self.mnu_view_x, "x-center...", "set the center in x", wx.ITEM_NORMAL)
        self.mnu_view_x.AppendSeparator()
        self.mnu_view_x_wtime = DummyMenuItem() # addItem(self.mnu_view_x, "wtime", "set wall time as x component", wx.ITEM_CHECK)
        self.mnu_view_x_stime = DummyMenuItem() # addItem(self.mnu_view_x, "stime", "set simulation time as x component", wx.ITEM_CHECK)
        self.mnu_view_x_iter  = DummyMenuItem() # addItem(self.mnu_view_x, "iter", "set iteration as x component", wx.ITEM_CHECK)
        self.mnu_view.AppendSubMenu(self.mnu_view_x, "x-axis")
        self.mnu_view_y = wx.Menu()
        self.mnu_view_vpty    = addItem(self.mnu_view_y, "y-viewport...", "set the viewport in y", wx.ITEM_NORMAL)
        self.mnu_view_rngy    = addItem(self.mnu_view_y, "y-range...", "set the range in y", wx.ITEM_NORMAL)
        self.mnu_view_ctry    = addItem(self.mnu_view_y, "y-center...", "set the center in y", wx.ITEM_NORMAL)
        self.mnu_view.AppendSubMenu(self.mnu_view_y, "y-axis")
        self.menubar.Append (self.mnu_view, "View")

        self.mnu_dspl = wx.Menu()
        self.mnu_dspl.AppendSeparator()
        self.mnu_dspl_bkgc  = addItem(self.mnu_dspl, "Background color...", "Change the background color", wx.ITEM_NORMAL)
        self.mnu_dspl.AppendSeparator()
        self.mnu_dspl_traw  = addItem(self.mnu_dspl, "Raw time in seconds", "Change the x-axis time format", wx. ITEM_CHECK)
        self.mnu_dspl_tiso  = addItem(self.mnu_dspl, "Time in ISO format", "Change the x-axis time format", wx. ITEM_CHECK)
        self.menubar.Append(self.mnu_dspl, "Display")

        self.mnu_help = wx.Menu()
        self.mnu_help_about = self.mnu_help.Append(wx.ID_ABOUT)
        self.menubar.Append(self.mnu_help, "Help")
        self.SetMenuBar(self.menubar)

        # Status Bar
        self.statusbar = self.CreateStatusBar(2)

        # Tool Bar
        self.toolbar = self.tracer_plot.get_toolbar()
        self.toolbar.Reparent(self)
        self.SetToolBar(self.toolbar)

        # Display list pop-up
        self.dlg_dlist = []

        data_w = 150
        sldr_h = 35
        self.__set_properties(data_w, sldr_h)
        self.__do_layout(data_w, sldr_h)

        self.Bind(wx.EVT_MENU, self.on_mnu_file_open,    self.mnu_file_open)
        self.Bind(wx.EVT_MENU, self.on_mnu_file_add,     self.mnu_file_add)
        self.Bind(wx.EVT_MENU_RANGE, self.on_mnu_file_hist, id=wx.ID_FILE1, id2=wx.ID_FILE9)
        self.Bind(wx.EVT_MENU, self.on_mnu_file_close,   self.mnu_file_close)
        self.Bind(wx.EVT_MENU, self.on_mnu_file_quit,    self.mnu_file_quit)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rdrw,    self.mnu_view_rdrw)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_pause,   self.mnu_view_pause)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_hlght,   self.mnu_view_hlght)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rst,     self.mnu_view_rst)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rstx,    self.mnu_view_rstx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rsty,    self.mnu_view_rsty)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_ctrx,    self.mnu_view_ctrx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rngx,    self.mnu_view_rngx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_vptx,    self.mnu_view_vptx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_wtime, self.mnu_view_x_wtime)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_stime, self.mnu_view_x_stime)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_iter,  self.mnu_view_x_iter)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_ctry,    self.mnu_view_ctry)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rngy,    self.mnu_view_rngy)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_vpty,    self.mnu_view_vpty)
        self.Bind(wx.EVT_MENU, self.on_mnu_dspl_bkgc,    self.mnu_dspl_bkgc)
        self.Bind(wx.EVT_MENU, self.on_mnu_dspl_traw,    self.mnu_dspl_traw)
        self.Bind(wx.EVT_MENU, self.on_mnu_dspl_tiso,    self.mnu_dspl_tiso)
        self.Bind(wx.EVT_MENU, self.on_mnu_help_about,   self.mnu_help_about)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_ENDSCROLL, self.on_scroll_change)

        self.dirname = ''
        self.fpath   = []

        # ---  Process cmd line files
        if len(sys.argv) > 1: self.OnDropFiles(0, 0, sys.argv[1:])

    def __set_properties(self, data_w, sldr_h):
        self.SetTitle("H2D2 - Probe Tracer")
        self.SetSize((800, 500))

        statusbar_fields = ['', 'Position']
        for i in range(len(statusbar_fields)):
            self.statusbar.SetStatusText(statusbar_fields[i], i)
        self.toolbar.Realize()

        self.split_main.SetSashGravity(1.0)
        self.split_main.SetMinimumPaneSize(20)

        self.tracer_stat.SetMinSize ((data_w, 15))
        self.tracer_stat.SetEditable(False)

        self.logo.SetMinSize((data_w, sldr_h))

        self.slider.SetMinSize((100, sldr_h))
        self.slider.SetRange(0, 3600)
        self.slider.SetValue(0)
        self.slider.SetPageSize(900)
        self.slider.Enable(False)

        self.mnu_view_hlght.Check(False)
        self.mnu_view_x_wtime.Check(True)
        self.mnu_view_x_stime.Check(False)
        self.mnu_view_x_iter.Check(False)
        self.mnu_dspl_tiso.Check(True)

    def __do_layout(self, data_w, sldr_h):
        sizer_frame = wx.BoxSizer(wx.VERTICAL)
        sizer_bottm = wx.BoxSizer(wx.HORIZONTAL)
        sizer_frame.Add(self.split_main, 1, wx.EXPAND, 0)
        sizer_frame.Add(sizer_bottm,     0, wx.EXPAND, 0)
        sizer_bottm.Add(self.slider, 1, wx.EXPAND, 0)
        sizer_bottm.Add(self.logo,   0, wx.EXPAND, 0)

        self.split_main.SplitVertically(self.tracer_plot, self.tracer_stat, -data_w)
        self.SetSizer(sizer_frame)
        self.Layout()

    def __fill_mnu_dspl(self, fpath):
        def callback_factory(i):
            return lambda x: self.on_mnu_dspl(x, i)

        self.dlg_dlist = [ None for i in range(len(fpath)) ]

        for i in range(self.mnu_dspl.GetMenuItemCount()-5):
            mnuItm = self.mnu_dspl.FindItemByPosition(0)
            self.mnu_dspl.DestroyItem(mnuItm)

        for i, f in enumerate(fpath):
            mnuItm = wx.MenuItem(self.mnu_dspl, wx.ID_ANY, '%s...' % f, ' ', wx.ITEM_NORMAL)
            self.mnu_dspl.Insert(i, mnuItm)
            self.Bind(wx.EVT_MENU, callback_factory(i), mnuItm)

    def OnDropFiles(self, x, y, fpath):
        if len(fpath) > 0:
            self.dirname = os.path.dirname(fpath[0])
            self.fpath   = fpath

            self.tracer_plot.load_data_from_file(self.fpath)

            file0 = os.path.basename(self.fpath[0])
            self.SetTitle(' '.join( (file0, '...') ))
            self.slider.Enable(True)
            self.__fill_mnu_dspl(self.fpath)
            return True
        else:
            msg = wx.MessageDialog(self, 'Drop one or more files', 'Error', wx.OK | wx.ICON_ERROR)
            if (msg.ShowModal() == wx.OK):
                msg.Close(True)
            return False

    def on_mnu_file_open(self, event):
        wildcard = "Probe files (*.prb)|*.prb|"     \
                   "All files (*.*)|*.*"
        dlg = wx.FileDialog(self, "Open", self.dirname, "", wildcard, wx.FD_OPEN | wx.FD_MULTIPLE)
        if dlg.ShowModal() == wx.ID_OK:
            filenames = dlg.GetFilenames()
            dirname   = dlg.GetDirectory()
            if (len(filenames) > 0):
                self.dirname = dirname
                self.fpath   = [ os.path.join(dirname, f) for f in filenames ]

                self.tracer_plot.load_data_from_file(self.fpath)

                file0 = os.path.basename(self.fpath[0])
                self.SetTitle(' '.join( (file0, '...') ))
                self.slider.Enable(True)
                self.__fill_mnu_dspl(self.fpath)

                for p in self.fpath: self.history.AddFileToHistory(p)
                self.history.Save(self.config)
                self.config.Flush()
            else:
                msg = wx.MessageDialog(self, 'Select the files to be ploted', 'Error', wx.OK | wx.ICON_ERROR)
                if (msg.ShowModal() == wx.OK):
                    msg.Destroy()
        dlg.Destroy()

    def on_mnu_file_add(self, event):
        wildcard = "Probe files (*.prb)|*.prb|"     \
                   "All files (*.*)|*.*"
        dlg = wx.FileDialog(self, "Add", self.dirname, "", wildcard, wx.FD_OPEN | wx.FD_MULTIPLE)
        if dlg.ShowModal() == wx.ID_OK:
            filenames = dlg.GetFilenames()
            dirname   = dlg.GetDirectory()
            if (len(filenames) > 0):
                paths = [ os.path.join(dirname, f) for f in filenames ]
                self.dirname = dirname
                self.fpath.extend(paths)

                self.tracer_plot.load_data_from_file(self.fpath)

                file0 = os.path.basename(self.fpath[0])
                self.SetTitle(' '.join( (file0, '...') ))
                self.slider.Enable(True)
                self.__fill_mnu_dspl(self.fpath)

                for p in paths: self.history.AddFileToHistory(p)
                self.history.Save(self.config)
                self.config.Flush()
            else:
                msg = wx.MessageDialog(self, 'Select the files to be added to the plot', 'Error', wx.OK | wx.ICON_ERROR)
                if (msg.ShowModal() == wx.OK):
                    msg.Destroy()
        dlg.Destroy()

    def on_mnu_file_hist(self, event):
        fileNum = event.GetId() - wx.ID_FILE1
        path = self.history.GetHistoryFile(fileNum)

        try:
            self.fpath.append(path)
            self.tracer_plot.load_data_from_file(self.fpath)

            file0 = os.path.basename(self.fpath[0])
            self.SetTitle(' '.join( (file0, '...') ))
            self.slider.Enable(True)
            self.__fill_mnu_dspl(self.fpath)

            self.history.AddFileToHistory(path)
        except Exception as e:
            msg = wx.MessageDialog(self, 'Error while reading file %s' % path, 'Error', wx.OK | wx.ICON_ERROR)
            if (msg.ShowModal() == wx.OK):
                msg.Destroy()
                self.history.RemoveFileFromHistory(fileNum)
                self.fpath.pop()
                self.tracer_plot.load_data_from_file(self.fpath)

    def on_mnu_file_close(self, event):
        self.fpath = []

        self.tracer_plot.load_data_from_file(self.fpath)

        self.SetTitle(' ')
        self.slider.Enable(False)
        self.__fill_mnu_dspl(self.fpath)

    def on_mnu_file_quit(self, event):
        dlg = wx.MessageDialog(self, 'Are you sure? \n', 'Quit', wx.YES_NO)
        if (dlg.ShowModal() == wx.ID_YES):
            self.Close(True)

    def on_mnu_view_rdrw(self, event):
        self.tracer_plot.draw_plot()

    def on_mnu_view_pause(self, event):
        self.tracer_plot.pause( self.mnu_view_pause.IsChecked() )

    def on_mnu_view_rst(self, event):
        self.tracer_plot.reset_view()

    def on_mnu_view_rstx(self, event):
        self.tracer_plot.reset_viewx()

    def on_mnu_view_rsty(self, event):
        self.tracer_plot.reset_viewy()

    def on_mnu_view_ctrx(self, event):
        ctr = self.tracer_plot.get_view_ctrx()
        str = '%f' % ctr
        dlg = wx.TextEntryDialog(self, 'Enter the data central value in X', 'Data central value in X', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_ctrx(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_ctry(self, event):
        ctr = self.tracer_plot.get_view_ctry()
        str = '%f' % ctr
        dlg = wx.TextEntryDialog(self, 'Enter the data central value in Y', 'Data central value in Y', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_ctry(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_rngx(self, event):
        rng = self.tracer_plot.get_view_rngx()
        str = '%f' % rng
        dlg = wx.TextEntryDialog(self, 'Enter the displayed data range in X', 'Data range in X', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_rngx(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_rngy(self, event):
        rng = self.tracer_plot.get_view_rngy()
        str = '%f' % rng
        dlg = wx.TextEntryDialog(self, 'Enter the displayed data range in Y', 'Data range in Y', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_rngy(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_vptx(self, event):
        x1, x2 = self.tracer_plot.get_view_vptx()
        str = '%f %f' % (x1, x2)
        dlg = wx.TextEntryDialog(self, 'Enter the x-axis viewport', 'x-axis viewport', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                x1, x2 = dlg.GetValue().split()
                x1 = eval(x1)
                x2 = eval(x2)
                self.tracer_plot.set_view_vptx(float(x1), float(x2))
            except:
                msg = wx.MessageDialog(self, 'Two, blanck separated, floating point values expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_vpty(self, event):
        y1, y2 = self.tracer_plot.get_view_vpty()
        str = '%f %f' % (y1, y2)
        dlg = wx.TextEntryDialog(self, 'Enter the y-axis viewport', 'y-axis viewport', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                y1, y2 = dlg.GetValue().split()
                y1 = eval(y1)
                y2 = eval(y2)
                self.tracer_plot.set_view_vpty(float(y1), float(y2))
            except:
                msg = wx.MessageDialog(self, 'Two, blanck separated, floating point values expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_hlght(self, event):
        pass

    def on_mnu_view_x_wtime(self, event):
        self.mnu_view_x_wtime.Check(True)
        self.mnu_view_x_stime.Check(False)
        self.mnu_view_x_iter.Check(False)
        self.tracer_plot.set_sourcex('wtime')

    def on_mnu_view_x_stime(self, event):
        self.mnu_view_x_wtime.Check(False)
        self.mnu_view_x_stime.Check(True)
        self.mnu_view_x_iter.Check(False)
        self.tracer_plot.set_sourcex('stime')

    def on_mnu_view_x_iter(self, event):
        self.mnu_view_x_wtime.Check(False)
        self.mnu_view_x_stime.Check(False)
        self.mnu_view_x_iter.Check(True)
        self.tracer_plot.set_sourcex('index')

    def on_mnu_dspl(self, event, idx):
        vis = self.tracer_plot.get_visibility()
        txt = self.tracer_plot.get_stats()
        clr = self.tracer_plot.get_colors()
        tit = self.fpath[idx]
        txt = txt[idx]
        clr = clr[idx]
        vis = vis[idx]

        if (not self.dlg_dlist[idx]):
            self.dlg_dlist[idx] = PTDlgDisplayList.PTDlgDisplayList(self)
        dlg = self.dlg_dlist[idx]
        dlg.fillList(tit, list(zip(vis, txt, clr)), self.on_dlg_dspl_apply, idx)
        dlg.Show()

    def on_mnu_dspl_bkgc(self, evt):
        dlg = wx.ColourDialog(self)
        dlg.GetColourData().SetChooseFull(True)
        if dlg.ShowModal() == wx.ID_OK:
            clr = dlg.GetColourData().GetColour()
            htm = clr.GetAsString(wx.C2S_HTML_SYNTAX)
            self.tracer_plot.set_background_color(htm)
            self.tracer_plot.redraw()
        dlg.Destroy()

    def on_mnu_dspl_traw(self, evt):
        self.mnu_dspl_traw.Check(True)
        self.mnu_dspl_tiso.Check(False)
        self.tracer_plot.set_formatterx(CTPlot.AxisFormatter.none)
        self.tracer_plot.redraw()

    def on_mnu_dspl_tiso(self, evt):
        self.mnu_dspl_traw.Check(False)
        self.mnu_dspl_tiso.Check(True)
        self.tracer_plot.set_formatterx(CTPlot.AxisFormatter.epoch_to_isoutc)
        self.tracer_plot.redraw()

    def on_mnu_help_about(self, event):
        info = wx_adv.AboutDialogInfo()
        info.Name = "H2D2 Probe Tracer"
        info.Version = "18.04rc1"
        info.Copyright = "(C) 2010-2018 INRS"
        info.Description = wx_wordwrap.wordwrap(
            "\nH2D2 Probe Tracer plots the data from simple probes."
            "If the simulation is still running it will follow the progress.\n"
            "The program allows for simple graphic interaction such as pan and zoom.",
            350, wx.ClientDC(self))
        info.WebSite = ("http://www.gre-ehn.ete.inrs.ca/H2D2", "H2D2 home page")
        info.Developers = [ "Yves Secretan" ]
        #info.License = wx_wordwrap.wordwrap(licenseText, 500, wx.ClientDC(self))

        wx_adv.AboutBox(info)

    def on_dlg_dspl_apply(self, v, args):
        idx = args[0]
        vis = self.tracer_plot.get_visibility()
        vis[idx] = v
        self.tracer_plot.set_visibility(vis)
        self.tracer_plot.draw_plot()
        self.on_tracer_change()

    def on_scroll_change(self, event):
        x = event.GetPosition()
        self.tracer_plot.set_view_ctrx(x)

    def on_tracer_change(self):
        x1, x2 = self.tracer_plot.get_data_spnx()
        xc     = self.tracer_plot.get_view_ctrx()
        v1, v2 = self.tracer_plot.get_view_vptx()
        txt    = self.tracer_plot.get_stats()
        clr    = self.tracer_plot.get_colors()
        vis    = self.tracer_plot.get_visibility()

        self.slider.SetRange(x1, x2)
        self.slider.SetValue(xc)
        self.slider.SetPageSize((v2-v1) / 4)

        self.tracer_stat.Clear()
        for fl, cl, vl, ll in zip(self.fpath, clr, vis, txt):
            self.tracer_stat.BeginTextColour((0,0,0))
            self.tracer_stat.WriteText(fl)
            self.tracer_stat.EndTextColour()
            self.tracer_stat.WriteText('\n')
            for c, v, l in zip(cl, vl, ll):
                if (v):
                    self.tracer_stat.BeginTextColour(c)
                else:
                    self.tracer_stat.BeginTextColour((180, 180, 180))
                self.tracer_stat.WriteText(l)
                self.tracer_stat.EndTextColour()
                self.tracer_stat.WriteText('\n')

    def on_tracer_pick(self, itm):
        #print '~', itm.idx, itm.uid
        pass

    def on_mouse_move(self, s):
        self.statusbar.SetStatusText("Pos: %s" % s, 1)

# end of class PTFrame
