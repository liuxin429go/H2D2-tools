#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2003-2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import matplotlib
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg

import numpy
import wx

import MAMesh
from CTCommon import CTUtil

class Plot(matplotlib.figure.Figure):
    def __init__(self, parent, *args, **kwargs):
        sp_param  = matplotlib.figure.SubplotParams(left=0.02, bottom=0.02, right=0.98, top=0.98)
        matplotlib.figure.Figure.__init__(self, subplotpars=sp_param)
        self.canvas = FigureCanvasWxAgg(parent, -1, self)

        self.__reset()

    def __reset(self):
        MAMesh.MAMeshes.reset_counter()
        self.meshes = MAMesh.MAMeshes()
        self.__init_figure()

    def __init_figure(self):
        self.clear()

    def load_data_from_file(self, fname):
        self.meshes.load_data_from_file(fname)

    def draw_plot(self):
        self.fill_plot()
        self.redraw()

    def fill_plot(self):
        self.__init_figure()
        n = 0
        for c in self.meshes:
            if (c.get_visibility()): n+=1
        nr = (n / 3) + (0, 1, 1)[n % 3]
        nc = min(n, 3)
        i = 0
        for c in self.meshes:
            if (not c.get_visibility()): continue
            p = self.add_subplot(nr, nc, i+1, aspect='equal')
            p.axis('off')
            c.fill_plot(p)
            i += 1

    def on_update(self):
        modified = False
        for c in self.meshes:
            mdf = c.on_update()
            if (mdf):
                rx = c.get_data_rangex()
                ry = c.get_data_rangey()
                self.xmin = min(self.xmin, rx[0])
                self.xmax = max(self.xmax, rx[1])
                self.ymin = min(self.ymin, ry[0])
                self.ymax = max(self.ymax, ry[1])
                modified = True
        if (modified):
            viewCtr = self.get_view_ctrx()
            viewRng = self.get_view_rngx()
            self.set_view_x(viewCtr, viewRng)
            self.draw_plot()
        return modified

    def redraw(self):
        self.canvas.draw()

    def get_canvas(self):
        return self.canvas

    def get_data_spny(self):
        """Data span in y"""
        return (self.ymin, self.ymax)

    def get_background_color(self):
        return self.get_facecolor()

    def set_background_color(self, clr):
        self.set_facecolor(clr)


##-----------------------------------
    def get_color(self):
        return self.meshes.get_color()

    def set_color(self, clrs):
        self.meshes.set_color(clrs)

    def get_entry(self, x):
      pass

    def get_stats (self):
        return self.meshes.get_stats()

    def get_visibility(self):
        return self.meshes.get_visibility()

    def set_visibility(self, visi):
        self.meshes.set_visibility(visi)


if __name__ == '__main__':
    app = wx.PySimpleApp()

    frame = wx.Frame(None, -1, "")
    plot = Plot(frame)
    plot.load_data_from_file('p1.prb')
    plot.draw_plot()

    frame.Show()
    app.MainLoop()
