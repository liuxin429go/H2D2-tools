#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2012
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import wx
import wx.adv as wx_adv
import wx.lib.wordwrap as wx_wordwrap
import wx.richtext     as wx_rt

from CTCommon import CTTextCtrlHandler
from CTCommon import CTWindow
from CTCommon import CTUtil
from CTCommon import H2D2
import MAPlot
import MADlgDisplayList
import MADlgTxtEditor

import codecs
import enum
import fnmatch
import logging
import platform
import os
import imp

States = enum.Enum('States', ('started', 'opened'))

def is_win():
   return (platform.system() == 'Windows')

def is_unx():
   return (platform.system() == 'Linux')

cmd_base = \
"""
h_scenario.clear()
h_scenario.add_as_one_val('%s', 1)

tgt_nelt = -1
tgt_size = 1.0      # Dans la métrique riemanienne
lvl_min  = -1       # Niveau rel. min de déraffinement
lvl_max  =  1       # Niveau rel. max de raffinement
h_remesh.adapt(h_scenario, tgt_nelt, tgt_size, lvl_min, lvl_max)

h_grid_tmp = h_remesh.gen_grid()
h_grid_rnm = h_grid_tmp
#h_renu = scotch()            # Nodes renumbering algo
#h_renu.renum(h_grid_tmp.helem)
#h_numc = h_renu.gen_num()
#del(h_renu)
#h_renu = nm_front()          # Elements renumbering algo
#h_renu.renum(h_grid_tmp.helem, h_numc)
#h_nume = h_renu.gen_num()
#del(h_renu)
#h_grid_rnm = grid(h_grid_tmp.hcoor, h_grid_tmp.helem, h_numc, h_nume)
print(h_grid_rnm)
h_grid_rnm.save('_a.cor', '_a.ele')

h_limt_tmp = h_remesh.gen_limit()
#print(h_limt_tmp)
h_limt_tmp.save(h_grid_rnm.hnumc, '_a.bnd')

del (h_limt_tmp)
del (h_grid_tmp)
"""

class MAFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: CTFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        super(MAFrame, self).__init__(*args, **kwds)

        self.spt_main  = wx.SplitterWindow(self,           -1, style=wx.SP_3D|wx.SP_BORDER)
        self.pane_left = wx.Panel(self.spt_main, -1)
        self.pane_rght = wx.Panel(self.spt_main, -1)
        self.spt_left  = wx.SplitterWindow(self.pane_left, -1, style=wx.SP_3D|wx.SP_BORDER)
        self.pane_left_top  = wx.Panel(self.spt_left, -1)
        self.pane_left_bttm = wx.Panel(self.spt_left, -1)

        inrsDev = None
        try:
            inrsDev = os.environ['INRS_DEV']
        except:
            pass

        # Widgets
        style_base = wx.BORDER_STATIC
        self.tracer_plot = CTWindow.CTWindow(proxied_class    = MAPlot.Plot,
                                             do_update        = False,
                                             #cb_on_data_change= self.on_tracer_change,
                                             #cb_on_data_pick  = self.on_tracer_pick,
                                             cb_on_mouse_move = self.on_mouse_move,
                                             parent = self.pane_left_top)
        self.LOGGER = wx.TextCtrl       (self.pane_left_bttm, -1, style = style_base | wx.TE_MULTILINE | wx.ST_NO_AUTORESIZE)
        self.statts = wx_rt.RichTextCtrl(self.pane_rght,      -1, style = style_base | wx.TE_MULTILINE | wx.ST_NO_AUTORESIZE)
        self.logo   = wx.Panel          (self.pane_rght,      -1, style = style_base)

        def addItem(mnu, name, help, style):
            item = wx.MenuItem(mnu, wx.ID_ANY, name, help, style)
            mnu.Append(item)
            return item
        # Menu Bar
        self.menubar = wx.MenuBar()
        self.mnu_file = wx.Menu()
        self.mnu_file_h2d2  = addItem(self.mnu_file, "H2D2...\tCtrl+H", "Set H2D2 path", wx.ITEM_NORMAL)
        self.mnu_file_hdev  = addItem(self.mnu_file, "H2D2 in INRS_DEV...", "Set H2D2 path", wx.ITEM_NORMAL) if inrsDev else None
        self.mnu_file.AppendSeparator()
        self.mnu_file_start = addItem(self.mnu_file, "Start...\tCtrl+S", "Start a mesh adaptation run, executing the code and waiting for further commands", wx.ITEM_NORMAL)
        self.mnu_file_stop  = addItem(self.mnu_file, "Stop\tCtrl+Z",     "Stop the current mesh adaptation", wx.ITEM_NORMAL)
        self.mnu_file_quit  = addItem(self.mnu_file, "Quit\tCtrl+Q",     "Quit application", wx.ITEM_NORMAL)
        self.menubar.Append(self.mnu_file, "File")

        self.mnu_rmsh = wx.Menu()
        self.mnu_rmsh_swed = addItem(self.mnu_rmsh, "Swap edges\tCtrl+W",        "Swap the edges", wx.ITEM_NORMAL)
        self.mnu_rmsh_cskn = addItem(self.mnu_rmsh, "Cure skin",                 "Cure the skin", wx.ITEM_NORMAL)
        self.mnu_rmsh_covt = addItem(self.mnu_rmsh, "Coarsen by vertex\tCtrl+M", "Coarsen the mesh, based on vertex", wx.ITEM_NORMAL)
        #self.mnu_rmsh_rfel = addItem(self.mnu_rmsh, "Refine by element\tCtrl+L", "Refine the mesh, based on element", wx.ITEM_NORMAL)
        self.mnu_rmsh_rfed = addItem(self.mnu_rmsh, "Refine by edges\tCtrl+K",   "Refine the mesh, based on edges", wx.ITEM_NORMAL)
        #self.mnu_rmsh_smel = addItem(self.mnu_rmsh, "Smooth by element\tCtrl+P", "Smooth the mesh, based on element", wx.ITEM_NORMAL)
        #self.mnu_rmsh_smvt = addItem(self.mnu_rmsh, "Smooth by edges\tCtrl+O",   "Smooth the mesh, based on edges", wx.ITEM_NORMAL)
        self.mnu_rmsh_smba = addItem(self.mnu_rmsh, "Smooth barycentric\tCtrl+I","Smooth the mesh, based on vertex ", wx.ITEM_NORMAL)
        self.mnu_rmsh_smst = addItem(self.mnu_rmsh, "Smooth 'stars'\tCtrl+U",    "Eliminate 'stars' from the mesh", wx.ITEM_NORMAL)
        self.mnu_rmsh.AppendSeparator()
        self.mnu_rmsh_smmt = addItem(self.mnu_rmsh, "Smooth metrics\tCtrl+T",    "Smooth the metrics", wx.ITEM_NORMAL)
        self.mnu_rmsh.AppendSeparator()
        self.mnu_rmsh_edit = addItem(self.mnu_rmsh, "Edit code\tCtrl+E",         "Edit the code snippet used to remesh", wx.ITEM_NORMAL)
        self.menubar.Append (self.mnu_rmsh, "Remesh")

        self.mnu_dspl = wx.Menu()
        self.mnu_dspl_mesh  = addItem(self.mnu_dspl, "Draw mesh...", "Draw a mesh", wx.ITEM_NORMAL)
        self.mnu_dspl.AppendSeparator()
        self.mnu_dspl_mngr  = addItem(self.mnu_dspl, "Display list...", "Display list manager", wx.ITEM_NORMAL)
        self.mnu_dspl_bkgc  = addItem(self.mnu_dspl, "Background color...", "Change the background color", wx.ITEM_NORMAL)
        self.menubar.Append(self.mnu_dspl, "Display")

        self.mnu_help = wx.Menu()
        self.mnu_help_reload= addItem(self.mnu_help, "Reload module...", "Reloads a Python module", wx.ITEM_NORMAL)
        self.mnu_help_logger= addItem(self.mnu_help, "Configure LOGGER...", "Configure the LOGGER", wx.ITEM_NORMAL)
        self.mnu_help.AppendSeparator()
        self.mnu_help_about = self.mnu_help.Append(wx.ID_ABOUT)
        self.menubar.Append(self.mnu_help, "Help")
        self.SetMenuBar(self.menubar)

        # Status Bar
        self.statusbar = self.CreateStatusBar(2)

        # Tool Bar
        self.toolbar = self.tracer_plot.get_toolbar()
        self.toolbar.Reparent(self)
        self.SetToolBar(self.toolbar)

        # Display list pop-up
        self.dlg_dlist = []

        dta_w = 150
        log_h = 150
        self.__set_properties(dta_w, log_h)
        self.__do_layout(dta_w, log_h)

        self.Bind(wx.EVT_MENU, self.on_mnu_file_h2d2, self.mnu_file_h2d2)
        if inrsDev: self.Bind(wx.EVT_MENU, self.on_mnu_file_hdev, self.mnu_file_hdev)
        self.Bind(wx.EVT_MENU, self.on_mnu_file_start,self.mnu_file_start)
        self.Bind(wx.EVT_MENU, self.on_mnu_file_stop, self.mnu_file_stop)
        self.Bind(wx.EVT_MENU, self.on_mnu_file_quit, self.mnu_file_quit)

        self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_swed, self.mnu_rmsh_swed)
        self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_cskn, self.mnu_rmsh_cskn)
        self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_covt, self.mnu_rmsh_covt)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_rfel, self.mnu_rmsh_rfel)
        self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_rfed, self.mnu_rmsh_rfed)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_smel, self.mnu_rmsh_smel)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_smvt, self.mnu_rmsh_smvt)
        self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_smba, self.mnu_rmsh_smba)
        self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_smst, self.mnu_rmsh_smst)
        self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_smmt, self.mnu_rmsh_smmt)
        self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_edit, self.mnu_rmsh_edit)

        self.Bind(wx.EVT_MENU, self.on_mnu_dspl_mesh, self.mnu_dspl_mesh)
        self.Bind(wx.EVT_MENU, self.on_mnu_dspl_mngr, self.mnu_dspl_mngr)
        self.Bind(wx.EVT_MENU, self.on_mnu_dspl_bkgc, self.mnu_dspl_bkgc)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_rsty,    self.mnu_rmsh_rsty)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_ctrx,    self.mnu_rmsh_ctrx)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_rngx,    self.mnu_rmsh_rngx)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_vptx,    self.mnu_rmsh_vptx)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_x_wtime, self.mnu_rmsh_x_wtime)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_x_stime, self.mnu_rmsh_x_stime)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_x_iter,  self.mnu_rmsh_x_iter)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_ctry,    self.mnu_rmsh_ctry)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_rngy,    self.mnu_rmsh_rngy)
        #self.Bind(wx.EVT_MENU, self.on_mnu_rmsh_vpty,    self.mnu_rmsh_vpty)
        self.Bind(wx.EVT_MENU, self.on_mnu_help_reload,  self.mnu_help_reload)
        self.Bind(wx.EVT_MENU, self.on_mnu_help_logger,  self.mnu_help_logger)
        self.Bind(wx.EVT_MENU, self.on_mnu_help_about,   self.mnu_help_about)
        self.LOGGER.Bind(wx.EVT_COMMAND_SCROLL_ENDSCROLL, self.on_scroll_change)

        self.dirh2d2  = ''
        self.pthh2d2  = 'h2d2'
        self.dirname  = ''
        self.h2d2 = None

        self.mnu_rmsh_edit = {
            States.started  : (
                [self.mnu_file,         # off
                 self.mnu_rmsh,
                 self.mnu_dspl,
                 self.mnu_help],
                [self.mnu_file_h2d2,    # on
                 self.mnu_file_hdev,
                 self.mnu_file_start,
                 self.mnu_dspl,
                 self.mnu_rmsh_edit,
                 self.mnu_help]),
            States.opened : (
                [],                     # off
                [self.mnu_file,         # on
                 self.mnu_rmsh,
                 self.mnu_dspl,
                 self.mnu_help])
        }
        self.__set_mnu_state(States.started)

    def __set_properties(self, dta_w, log_h):
        self.SetTitle("H2D2 - Mesh Adaptation")
        self.SetSize((1000, 800))

        statusbar_fields = ['', 'Position']
        for i in range(len(statusbar_fields)):
            self.statusbar.SetStatusText(statusbar_fields[i], i)
        self.toolbar.Realize()

        self.spt_main.SetSashGravity(1.0)
        self.spt_left.SetSashGravity(1.0)

        self.spt_main.SetMinimumPaneSize(20)

        self.statts.SetMinSize ((dta_w, 15))
        self.statts.SetEditable(False)

        self.LOGGER.SetMinSize ((dta_w, 15))
        self.LOGGER.SetEditable(False)

        self.logo.SetMinSize((dta_w, 30))

    def __do_layout(self, dta_w, log_h):
        sizer_main = wx.BoxSizer(wx.VERTICAL)
        sizer_left = wx.BoxSizer(wx.VERTICAL)
        sizer_left_top  = wx.BoxSizer(wx.VERTICAL)
        sizer_left_bttm = wx.BoxSizer(wx.VERTICAL)
        sizer_rght      = wx.BoxSizer(wx.VERTICAL)

        sizer_left.Add     (self.spt_left,    1, wx.EXPAND, 0)
        sizer_left_top.Add (self.tracer_plot, 1, wx.EXPAND, 0)
        sizer_left_bttm.Add(self.LOGGER,      1, wx.EXPAND, 0)
        sizer_rght.Add     (self.statts,      1, wx.EXPAND, 0)
        sizer_rght.Add     (self.logo,        0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_main.Add     (self.spt_main,    1, wx.EXPAND, 0)

        self.pane_left_top.SetSizer (sizer_left_top)
        self.pane_left_bttm.SetSizer(sizer_left_bttm)
        self.pane_left.SetSizer     (sizer_left)
        self.pane_rght.SetSizer     (sizer_rght)

        self.spt_left.SplitHorizontally(self.pane_left_top, self.pane_left_bttm, 400)
        self.spt_main.SplitVertically  (self.pane_left,     self.pane_rght, -dta_w)

        self.SetSizer(sizer_main)
        self.Layout()

    def __set_mnu_state(self, status):
        for it in self.mnu_rmsh_edit[status][0]:
            if isinstance(it, wx.Menu):
                for m in it.GetMenuItems(): m.Enable(False)
            elif isinstance(it, wx.MenuItem):
                it.Enable(False)
        for it in self.mnu_rmsh_edit[status][1]:
            if isinstance(it, wx.Menu):
                for m in it.GetMenuItems(): m.Enable(True)
            elif isinstance(it, wx.MenuItem):
                it.Enable(True)

    def __fill_mnu_dspl(self, fpath):
        def callback_factory(i):
            return lambda x: self.on_mnu_dspl(x, i)

        self.dlg_dlist = []
        for i in range(len(fpath)):
            self.dlg_dlist.append(None)

        for m in self.mnu_dspl.GetMenuItems():
            self.mnu_dspl.DestroyItem(m)

        for f, i in zip(fpath, list(range(len(fpath)))):
            mnuItm = wx.MenuItem(self.mnu_dspl, wx.ID_ANY, '%s...' % f, ' ', wx.ITEM_NORMAL)
            self.mnu_dspl.Append(mnuItm)
            self.Bind(wx.EVT_MENU, callback_factory(i), mnuItm)

    def on_mnu_file_h2d2(self, event):
        if is_win():
            wildcard = "Exe files(*.exe;*.bat)|*.exe;*.bat|"  \
                       "All files (*.*)|*.*"
        else:
            wildcard = "Exe files(*.out;*.sh)|*.out;*.sh|"  \
                       "All files (*.*)|*.*"
        dlg = wx.FileDialog(self, "H2D2", self.dirh2d2, "", wildcard, wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            filenames = dlg.GetFilenames()
            dirname   = os.path.dirname( dlg.GetPath() )
            if (len(filenames) == 1):
                self.pthh2d2 = os.path.join(dirname, filenames[0])
                self.dirh2d2 = dirname
            else:
                dlg = wx.MessageDialog(self, 'Select h2d2', 'Error', wx.OK | wx.ICON_ERROR)
                if (dlg.ShowModal() == wx.OK):
                    self.Close(True)
        dlg.Destroy()

    def on_mnu_file_hdev(self, event):
        if is_win():
            wildcard = "h2d2.exe"
        else:
            wildcard = "h2d2"
        dev = os.environ['INRS_DEV']
        matches = []
        for p in ['Debug', 'Release', '_run']:
            path = os.path.join(dev, p)
            for root, dirnames, filenames in os.walk(path):
                for filename in fnmatch.filter(filenames, wildcard):
                    matches.append( os.path.join(root, filename) )
        dlg = wx.SingleChoiceDialog(self, 'H2D2 in %s' % dev, 'H2D2 executable',  matches, style=wx.CHOICEDLG_STYLE)
        dlg.SetSize( (600,300) )
        if dlg.ShowModal() == wx.ID_OK:
            self.pthh2d2 = dlg.GetStringSelection()
            self.dirh2d2 = os.path.dirname(self.pthh2d2)
        dlg.Destroy()

    def on_mnu_file_start(self, event):
        dlg = wx.FileDialog(self, "Open", self.dirname, "", "*.inp", wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            filenames = dlg.GetFilenames()
            dirname   = os.path.dirname( dlg.GetPath() )
            if (len(filenames) == 1):
                wx.BeginBusyCursor()
                try:
                    self.h2d2 = H2D2.H2D2(args = [self.pthh2d2], cwd = dirname)
                except RuntimeError:
                    wx.EndBusyCursor()
                    raise
                os.chdir(dirname)
                inp = os.path.join(dirname, filenames[0])
                try:
                    f = open(inp, 'r')
                except TypeError:
                    f = codecs.open(inp, mode = 'r', encoding= 'ascii')
                o = self.h2d2.xeq_cmd( f.readlines() )
                wx.EndBusyCursor()

                self.SetTitle(' '.join( (filenames[0], '...') ))

                self.LOGGER.Clear()
                for l in o: self.LOGGER.WriteText(l)
                self.dirname = dirname

                self.__set_mnu_state(States.opened)
            else:
                dlg = wx.MessageDialog(self, 'Select the remeshing command file', 'Error', wx.OK | wx.ICON_ERROR)
                if (dlg.ShowModal() == wx.OK):
                    self.Close(True)
        dlg.Destroy()

    def do_h2d2_adapt_cmd(self, cmd):
        wx.BeginBusyCursor()
        o = self.h2d2.xeq_cmd( cmd.split('\n') )
        for l in o: self.LOGGER.AppendText(l)
        wx.EndBusyCursor()
        self.tracer_plot.load_data_from_file( ['_a.cor', '_a.ele'] )

        self.statts.Clear()
        o = self.tracer_plot.get_stats()
        c = self.tracer_plot.get_color()
        for o_, c_ in zip(o,c):
            self.statts.BeginTextColour(c_)
            self.statts.WriteText(o_)
            self.statts.EndTextColour()
            self.statts.WriteText('\n')

    def on_mnu_file_stop(self, event):
        if (self.h2d2):
            dlg = wx.MessageDialog(self, 'Are you sure? \n', 'Stop adaptation', wx.YES_NO)
            if (dlg.ShowModal() == wx.ID_YES):
                self.h2d2.xeq_cmd('stop()', send_and_forget=True)
                del self.h2d2
                self.h2d2 = None
            dlg.Destroy()
        self.__set_mnu_state(States.started)

    def on_mnu_file_quit(self, event):
        dlg = wx.MessageDialog(self, 'Are you sure? \n', 'Quit', wx.YES_NO)
        if (dlg.ShowModal() == wx.ID_YES):
            del self.h2d2
            self.h2d2 = None
            self.Close(True)

    def on_mnu_rmsh_swed(self, event):
        cmd = cmd_base % 'swap_edges'
        self.do_h2d2_adapt_cmd(cmd)

    def on_mnu_rmsh_cskn(self, event):
        cmd = cmd_base % 'cure_skin'
        self.do_h2d2_adapt_cmd(cmd)

    def on_mnu_rmsh_covt(self, event):
        cmd = cmd_base % 'coarsen_by_vertex'
        self.do_h2d2_adapt_cmd(cmd)

    def on_mnu_rmsh_rfel(self, event):
        cmd = cmd_base % 'refine_by_element'
        self.do_h2d2_adapt_cmd(cmd)

    def on_mnu_rmsh_rfed(self, event):
        cmd = cmd_base % 'refine_by_edges'
        self.do_h2d2_adapt_cmd(cmd)

    def on_mnu_rmsh_smel(self, event):
        cmd = cmd_base % 'smooth_by_element'
        self.do_h2d2_adapt_cmd(cmd)

    def on_mnu_rmsh_smvt(self, event):
        cmd = cmd_base % 'smooth_by_edges'
        self.do_h2d2_adapt_cmd(cmd)

    def on_mnu_rmsh_smba(self, event):
        cmd = cmd_base % 'smooth_barycentric'
        self.do_h2d2_adapt_cmd(cmd)

    def on_mnu_rmsh_smst(self, event):
        cmd = cmd_base % 'smooth_stars'
        self.do_h2d2_adapt_cmd(cmd)

    def on_mnu_rmsh_smmt(self, event):
        cmd = cmd_base % 'smooth_metrics'
        self.do_h2d2_adapt_cmd(cmd)

    def on_mnu_rmsh_edit(self, event):
        global cmd_base
        dlg = MADlgTxtEditor.MADlgTxtEditor(self)
        dlg.setText(cmd_base)
        if dlg.ShowModal() == wx.ID_OK:
            cmd_base = dlg.getText()
        del dlg

    def on_mnu_dspl_mesh(self, event):
        wildcard = "Mesh files(*.cor, *.ele)|*.cor;*.ele|"  \
                   "All files (*.*)|*.*"
        dlg = wx.FileDialog(self, "Display mesh", self.dirname, "", wildcard, wx.FD_OPEN | wx.FD_MULTIPLE)
        if dlg.ShowModal() == wx.ID_OK:
            filenames = dlg.GetFilenames()
            dirname   = os.path.dirname( dlg.GetPath() )
            if (len(filenames) == 1):
                froot = os.path.splitext(filenames[0])[0]
                self.tracer_plot.load_data_from_file( [os.path.join(dirname, froot+'.cor'),
                                                       os.path.join(dirname, froot+'.ele')] )
            elif (len(filenames) == 2):
                ext = os.path.splitext(filenames[0])[1]
                if (ext == '.cor'):
                    self.tracer_plot.load_data_from_file( [os.path.join(dirname, filenames[0]),
                                                           os.path.join(dirname, filenames[1])] )
                else:
                    self.tracer_plot.load_data_from_file( [os.path.join(dirname, filenames[1]),
                                                           os.path.join(dirname, filenames[2])] )
            else:
                dlg = wx.MessageDialog(self, 'Display mesh', 'Error', wx.OK | wx.ICON_ERROR)
                if (dlg.ShowModal() == wx.OK):
                    self.Close(True)
        dlg.Destroy()

    def on_mnu_dspl_mngr(self, event):
        vis = self.tracer_plot.get_visibility()
        txt = self.tracer_plot.get_stats()
        clr = self.tracer_plot.get_color()
        tit = 'Display list manager'

        if (not self.dlg_dlist):
            self.dlg_dlist = MADlgDisplayList.MADlgDisplayList(self)
            self.dlg_dlist.SetSize((400, 400))
        dlg = self.dlg_dlist
        dlg.fillList(tit, list(zip(vis, txt, clr)), self.on_dlg_dspl_apply)
        dlg.Show()

    def on_dlg_dspl_apply(self, vis, clr):
        self.tracer_plot.set_visibility(vis)
        self.tracer_plot.set_color(clr)
        self.tracer_plot.draw_plot()

    def on_dlg_dspl_setclr(self, clr):
        self.tracer_plot.set_color(clr)
        self.tracer_plot.redraw()

    def on_mnu_dspl_bkgc(self, evt):
        dlg = wx.ColourDialog(self)
        dlg.GetColourData().SetChooseFull(True)
        if dlg.ShowModal() == wx.ID_OK:
            clr = dlg.GetColourData().GetColour()
            htm = clr.GetAsString(wx.C2S_HTML_SYNTAX)
            self.tracer_plot.set_background_color(htm)
            self.tracer_plot.redraw()
        dlg.Destroy()

    def on_mnu_help_reload(self, event):
        import sys
        lst = list(sys.modules.keys())
        lst.sort()
        dlg = wx.SingleChoiceDialog(
                self, 'Reload a module', 'Reload a Python module',
                lst,
                wx.CHOICEDLG_STYLE
                )
        if dlg.ShowModal() == wx.ID_OK:
            nam = dlg.GetStringSelection()
            mdl = sys.modules[nam]
            imp.reload(mdl)

        dlg.Destroy()

    def on_mnu_help_logger(self, event):
        if not hasattr(self, 'logHndlr'):
            self.logHndlr = CTTextCtrlHandler.CTTextCtrlHandler(self.LOGGER)
            FORMAT = "%(asctime)s %(levelname)s %(message)s"
            self.logHndlr.setFormatter( logging.Formatter(FORMAT) )

        LOGGER = logging.getLogger("INRS.H2D2.Tools")
        LOGGER.addHandler(self.logHndlr)
        LOGGER.setLevel(logging.DEBUG)

    def on_mnu_help_about(self, event):
        info = wx_adv.AboutDialogInfo()
        info.Name = "H2D2 Mesh Adaptation"
        info.Version = "19.04"
        info.Copyright = "(C) 2012-2015 INRS"
        info.Description = wx_wordwrap.wordwrap(
            "\nH2D2 Mesh adaptation tool is a front-end for the mesh adaptation function incorporated into H2D2.\n"
            "The program allows for simple graphic interaction such as pan and zoom.",
            350, wx.ClientDC(self))
        info.WebSite = ("http://www.gre-ehn.ete.inrs.ca/H2D2", "H2D2 home page")
        info.Developers = [ "Yves Secretan" ]
        #info.License = wx_wordwrap.wordwrap(licenseText, 500, wx.ClientDC(self))

        wx_adv.AboutBox(info)

    def on_scroll_change(self, event):
        x = event.GetPosition()
        self.tracer_plot.set_view_ctrx(x)

    def on_tracer_pick(self, txt = ()):
        self.tracer_data.Clear()
        self.tracer_data.WriteText(txt)

    def on_mouse_move(self, s):
        self.statusbar.SetStatusText("Pos: %s" % s, 1)
