#!/usr/bin/env python
# -*- coding: utf-8 -*-
from distutils.core      import setup
from distutils.extension import Extension
from Cython.Build        import cythonize
from Cython.Distutils    import build_ext

import numpy as np
np_inc = np.get_include()
xt_inc = 'CTCommon/external'

pkg_args = {'language' : 'c++'}
pkg_name = 'CTCommon'
ext_modules=[
    # --- Modules de base
    [
    Extension('CTCommon.external.pyqtree',  ['CTCommon/external/pyqtree.py' ], **pkg_args),
                                            
    Extension('CTCommon.CTException',       ['CTCommon/CTException.py'], **pkg_args),
                                            
    Extension('CTCommon.DTReducOperation',  ['CTCommon/DTReducOperation.py'], **pkg_args),
    Extension('CTCommon.DTDataBloc',        ['CTCommon/DTDataBloc.py'],        include_dirs=[np_inc], **pkg_args),
                                            
    Extension('CTCommon.DTPathError',       ['CTCommon/DTPathError.py'], **pkg_args),
    Extension('CTCommon.FEMeshMetricKind',  ['CTCommon/FEMeshMetricKind.py'], **pkg_args),

    Extension('CTCommon.QTQuadTree',        ['CTCommon/QTQuadTree.pyx', 'CTCommon/QTQuadTree_c.cpp' ], **pkg_args),
    ],

    # --- Modules de base
    [
    Extension('CTCommon.DTDataBloc',        ['CTCommon/DTDataBloc.py'],        include_dirs=[np_inc], **pkg_args),
    Extension('CTCommon.DTDataIO',          ['CTCommon/DTDataIO.py'],          include_dirs=[np_inc], **pkg_args),
    Extension('CTCommon.DTData',            ['CTCommon/DTData.py'],            include_dirs=[np_inc], **pkg_args),
                                            
    Extension('CTCommon.FEMesh',            ['CTCommon/FEMesh.py'],            include_dirs=[np_inc, xt_inc], **pkg_args),
    Extension('CTCommon.FEMeshIO',          ['CTCommon/FEMeshIO.py'],          include_dirs=[np_inc, xt_inc], **pkg_args),
    Extension('CTCommon.FEMeshMetric',      ['CTCommon/FEMeshMetric.py'],      include_dirs=[np_inc, xt_inc], **pkg_args),
                                            
    Extension('CTCommon.DTField',           ['CTCommon/DTField.py'],           include_dirs=[np_inc, xt_inc], **pkg_args),
    Extension('CTCommon.DTPath_v2',         ['CTCommon/DTPath_v2.py'],         include_dirs=[np_inc, xt_inc], **pkg_args),
    Extension('CTCommon.DTDiffusion',       ['CTCommon/DTDiffusion.py'],       include_dirs=[np_inc, xt_inc], **pkg_args),
    ],
]

for mdls in ext_modules:
    setup(
      name = pkg_name,
      cmdclass = {'build_ext': build_ext},
      include_dirs=['CTCommon'],
      ext_modules = cythonize(mdls, 
                              language_level= 3,
                              annotate      = True,
                              #compiler_directives={
                              #    'boundscheck' : False,
                              #    'wraparound'  : False,
                              #    'nonecheck'   : False}
                              ),
    )
