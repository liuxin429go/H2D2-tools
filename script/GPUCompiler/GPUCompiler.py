#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# generated by wxGlade 0.7.0 on Mon Jan 11 09:55:39 2016
#

# This is an automatically generated file.
# Manual changes will be overwritten without warning!

from __future__ import absolute_import
from __future__ import print_function

import optparse
import sys

import wx
import gettext
from GPUCompilerFrame import GPUCompilerFrame

rpdb2_imported = False
try:
   import rpdb2
   rpdb2_imported = True
except:
   pass

class GPUCompiler(wx.App):
    def __init__(self, *args, **kwargs):
        super(GPUCompiler, self).__init__(*args, **kwargs)

    def OnInit(self):
        frm_cplr = GPUCompilerFrame(None, wx.ID_ANY, "")
        self.SetTopWindow(frm_cplr)
        frm_cplr.Show()
        return 1

# end of class GPUCompiler

if __name__ == "__main__":
    def main(opt_args = None):
        # ---  Parse les options
        usage  = '%s [options]' % __package__
        parser = optparse.OptionParser(usage)
        parser.add_option("-d", "--debug", dest="dbg", default=False, action='store_true', help="debug the run", metavar="DEBUG")

        # --- Parse les arguments de la ligne de commande
        if (not opt_args): opt_args = sys.argv[1:]
        (options, args) = parser.parse_args(opt_args)

        # --- Debug si demandé
        if (options.dbg):
            if (not rpdb2_imported):
                raise 'rpdb2 must be installed to debug'
            else:
                rpdb2.start_embedded_debugger("inrs_iehss", fAllowUnencrypted = True)


        gettext.install("GPUCompiler") # replace with the appropriate catalog name
        cmplr = GPUCompiler(0)
        cmplr.MainLoop()

    main()
    