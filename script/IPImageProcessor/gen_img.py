#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

toolsDir = 'e:/dev/H2D2/tools/script'
if os.path.isdir(toolsDir):sys.path.append(toolsDir)

import IPImageProcessor as IP
import numpy as np
import multiprocessing as mp
import logging

#LOGGER = logging.getLogger("INRS.H2D2.Tools")
LOGGER = mp.get_logger()

def main(args = None):
    if (args): pid, pool_size = args

    pdir = 'data' + os.sep      # background info
    gdir = 'data' + os.sep      # geometry/grid dir
    ddir = '.' + os.sep         # data dir
    hdir = '../cd7'+ os.sep     # hydrodynamic

    # ---  Set base Spatial Reference
    sp_proj = IP.IPSpatialReference()
    sp_proj.ImportFromModeleur(gdir+'utm_73W_nad83.pjc')
#    wkt = 'GEOGCS["Lat Long WGS84",DATUM["D_WGS84",SPHEROID["World_Geodetic_System_of_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]]'
#    sp_proj.ImportFromWkt(wkt)

    # ---  Set grid Spatial Reference
    sp_grid = IP.IPSpatialReference()
#    sp_grid.ImportFromModeleur(gdir+'utm_73W_nad83.pjc')
    sp_grid = sp_proj

    # ---  Coordinate transform from grid to project
    grid2proj = IP.IPCoordinateTransformation(sp_grid, sp_proj)

    # --- Mesh & data
    mesh = IP.IPMeshH2D2T6L(gdir+'simul000.cor', gdir+'simul000.ele', projection = grid2proj)
    data = IP.DTData(hdir+'solution.cd2d.pst.sim', cols=(0,))
#    mask = IP.DTData(ddir+'sv2d.pst.sim', cols=(2,))

    # ---  Sizes
#    bbox = mesh.getBbox(stretchfactor=0.02)     # ll, ur in base coord
    bbox = (624900.0, 5173700.0,  638250.0, 5184950.0)
    size = (10, 7)                              # in inches
    dpi  = 120

    # ---  Set up screen Transformation
    screen = IP.IPGeoTransform()
    screen.setWindow(bbox)
    screen.setViewportFromSize(size=size, dpi=dpi)
    screen.resizeWindowToViewport()

    # ---  Base map
    bmap = IP.IPGdalOgr(toSpacialReference=sp_proj, toGeoTransform=screen)
    bmap.addShapeFile(pdir+'hydro_filamentaire.shp')
    bmap.addShapeFile(pdir+'hydro_polygone.shp')
    bmap.addShapeFile(pdir+'reseau_routier.shp')
#    bmap.addGeoTIFF(pdir+'fondImage.tif')
    bImg = bmap.exportAsImage()

    # --- Create Mask
   # fdta = mask.getDataAtTime(0.0)
   # fImg = IP.genMask(mesh, fdta, [-1.0e+99, 0.06], 'w', size=size, screen=screen)
    fImg = None

    # --- Create simple figure
    fig = IP.makeFigureContourFill(mesh,
                        data.getDataAtTime(1245137400),
                        size=size,
                        screen=screen,
                        background=bImg,
                        foreground=fImg)
    fig.getAsPIL().save('img.png')
    fig.showFigure()

if __name__ == '__main__':
    streamHandler = logging.StreamHandler()
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.INFO)

    main()
