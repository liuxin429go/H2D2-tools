#!/usr/bin/env python
# -*- coding: utf-8 -*-
#******************************************************************************
#  $Id$
#
# Project:  Google Summer of Code 2007, 2008 (http://code.google.com/soc/)
# Support:  BRGM (http://www.brgm.fr)
# Purpose:  Convert a raster into TMS (Tile Map Service) tiles in a directory.
#           - generate Google Earth metadata (KML SuperOverlay)
#           - generate simple HTML viewer based on Google Maps and OpenLayers
#           - support of global tiles (Spherical Mercator) for compatibility
#               with interactive web maps a la Google Maps
# Author:   Klokan Petr Pridal, klokan at klokan dot cz
# Web:      http://www.klokan.cz/projects/gdal2tiles/
# GUI:      http://www.maptiler.org/
#
###############################################################################
# Copyright (c) 2008, Klokan Petr Pridal
#
#  Permission is hereby granted, free of charge, to any person obtaining a
#  copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,
#  and/or sell copies of the Software, and to permit persons to whom the
#  Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included
#  in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
#  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#  DEALINGS IN THE SOFTWARE.
#******************************************************************************
#************************************************************************
# --- Copyright (c) INRS 2013
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from .globalmaptiles import GlobalGeodetic

from .KMLMeta   import KMLMetaData
from .KMLWriter import KMLWriter
from .. import IPVector as IP

class KMLTiles:
    # -------------------------------------------------------------------------
    def __init__(self, meta, outDir='', outUrl=''):
        self.tilesize = 256
        self.tileext  = 'png'
        self.geodetic = GlobalGeodetic(self.tilesize)
        self.tminmax  = None
        self.tminz    = 0
        self.tmaxz    = -1
        self.wload    = 0

        self.meta   = meta
        self.outDir = outDir
        self.outUrl = outUrl

    # -------------------------------------------------------------------------
    # Helper methods
    # -------------------------------------------------------------------------
    def makedirs(self, tz, tx):
        d = os.path.join(self.outDir, '%d' % tz, '%d' % tx)
        if not os.path.exists(d): os.makedirs(d)
        return d

    # -------------------------------------------------------------------------
    def get_filename(self, tz, ty, tx, ext = '.png'):
        return os.path.join(self.outDir, '%d' % tz, '%d' % tx, '%s.%s' % (ty, ext))

    # -------------------------------------------------------------------------
    def path_exist(self, tz, ty, tx, ext = '.png'):
        return os.path.exists(self.get_filename(tz, ty, tx, ext))

    # -------------------------------------------------------------------------
    def gen_one_kml(self, fname, tz, ty, tx, children = []):
        """
        Geenrate the KML file for one tile.
        """
        minlodpixels = int(self.tilesize / 2) # default 128
        maxlodpixels = int(self.tilesize * 8) # default 2048 (used to be -1)
        if children == []: maxlodpixels = -1

        tilekml = (tx is not None)

        if tx == 0:
            drawOrder = 2 * tz + 1
        elif tx != None:
            drawOrder = 2 * tz
        else:
            drawOrder = 0

        url = self.outUrl
        if not url:
            if tilekml:
                url = "../../"
            else:
                url = ""

        w = KMLWriter(self.meta, fname)
        w.writeHeader()
        w.writeDocumentHead()
        w.writeDocumentStyle()

        if (tz):
            ymin, xmin, ymax, xmax = self.geodetic.TileLatLonBounds(tx, ty, tz)
            lnkName = '%d.%s' % (ty, self.tileext)
            bbox = (ymax, ymin, xmax, xmin)
            lod  = (minlodpixels, maxlodpixels)
            w.writeDocumentRegion(bbox = bbox, lod = lod)
            w.writeDocumentOverlay(lnkName, bbox, drawOrder)

        for cx, cy, cz in children:
            ymin, xmin, ymax, xmax = self.geodetic.TileLatLonBounds(cx, cy, cz)
            lnkName = '%s%d/%d/%d.%s' % (url, cz, cx, cy, 'kml')
            bbox = (ymax, ymin, xmax, xmin)
            lod  = (minlodpixels, maxlodpixels)
            w.writeNetworkLink(lnkName, bbox = bbox, lod = lod)

        w.writeFooter(closeAll = True)
        w = None

    # -------------------------------------------------------------------------
    def gen_one_bmp(self, fname, bbox):
        """
        Generate the bitmap for one tile.
        """
        raise NotImplementedError('Method gen_one_bmp(self, fname, bbox) mus be implemented by inheritance')

    # -------------------------------------------------------------------------
    def gen_tiles_one_level(self, tz):
        """
        Generate all the tiles for one level, both bitmap and kml files.
        """

        print(("Level: %d" % tz))

        tminx, tminy, tmaxx, tmaxy = self.tminmax[tz]
        for tx in range(tminx, tmaxx+1):
            print('\ttx: %d in (%d, %d)' % (tx, tminx, tmaxx+1))
            dname = self.makedirs(tz, tx)

            for ty in range(tmaxy, tminy-1, -1):

                # ---  Generate the tile
                fname = self.get_filename(tz, ty, tx, self.tileext)
                if not os.path.exists(fname):
                    print('\t\t%s' % (fname))
                    xmin, ymin, xmax, ymax = self.geodetic.TileBounds(tx, ty, tz)
                    bbox = (ymax, ymin, xmax, xmin)
                    self.gen_one_bmp(fname, bbox = bbox)
                else:
                    print('\t\tSkipping %s' % (fname))

                # ---  Get childrens
                if (tz == self.tmaxz):
                    children = []
                else:
                    z = tz + 1
                    children = [ (x, y, z) for x in range(2*tx,2*tx+2) for y in range(2*ty,2*ty+2) if self.path_exist(z, y, x, self.tileext) ]

                # ---  Create the KML file
                fname = self.get_filename(tz, ty, tx, 'kml')
                if True: ## not os.path.exists(fname):
                    print('\t\t%s' % (fname))
                    self.gen_one_kml(fname, tz, ty, tx, children)
                else:
                    print('\t\tSkipping %s' % (fname))

    # -------------------------------------------------------------------------
    #  Main processing methods
    # -------------------------------------------------------------------------
    def process(self):
        """
        The main processing function, runs all the main steps of processing
        """

        # ---  Setup the tile logic (quadtree like)
        self.setup_tiles()

        # ---  Generate the tiles
        self.gen_tiles()

        # ---  Generate the main metadata files
        self.gen_top()

    # -------------------------------------------------------------------------
    def setup_tiles(self):
        """
        Setup the quadtree like structure for the levels, and determine
        min and max levels.
        """
        # ---  Output Bounds
        ominx = self.meta['bbox'][3]   # w
        omaxx = self.meta['bbox'][2]   # e
        omaxy = self.meta['bbox'][0]   # n
        ominy = self.meta['bbox'][1]   # s

        # ---  Generate table with min max tile coordinates for all zoomlevels
        self.tminmax = list(range(0,32))
        for tz in range(0, 32):
            tminx, tminy = self.geodetic.LatLonToTile( ominx, ominy, tz )
            tmaxx, tmaxy = self.geodetic.LatLonToTile( omaxx, omaxy, tz )
            # crop tiles extending world limits (+-180,+-90)
            tminx, tminy = max(0, tminx), max(0, tminy)
            tmaxx, tmaxy = min(2**(tz+1)-1, tmaxx), min(2**tz-1, tmaxy)
            self.tminmax[tz] = (tminx, tminy, tmaxx, tmaxy)

        # ---  Get the minimal zoom level
        for tz in range(0,32):
            tminx, tminy, tmaxx, tmaxy = self.tminmax[tz]
            if (tmaxx > tminx or tmaxy > tminy):
                self.tminz = tz - 1
                break

        # ---  Get the maximal zoom level
        self.tmaxz = self.geodetic.ZoomForPixelSize( self.meta['resolution'] )

        # ---  Get the total work load
        self.wload = 0
        for tz in range(self.tminz, self.tmaxz+1):
            tminx, tminy, tmaxx, tmaxy = self.tminmax[tz]
            self.wload += (tmaxx-tminx+1) * (tmaxy-tminy+1)

    # -------------------------------------------------------------------------
    def gen_top(self):
        """
        Generate the top level kml file.
        """
        assert (self.tminmax)
        assert (self.tmaxz >= self.tminz)

        if not os.path.exists(self.outDir):
            os.makedirs(self.outDir)

        children = []
        tminx, tminy, tmaxx, tmaxy = self.tminmax[self.tminz]
        for tx in range(tminx, tmaxx+1):
            for ty in range(tminy, tmaxy+1):
                children.append( [ tx, ty, self.tminz ] )
        fname = os.path.join(self.outDir, 'doc.kml')
        self.gen_one_kml(fname, None, None, None, children)

    # -------------------------------------------------------------------------
    def gen_tiles(self):
        """
        Generate all the tiles for all the levels.
        """
        assert (self.tminmax)
        assert (self.tmaxz >= self.tminz)

        for tz in range(self.tmaxz, self.tminz-1, -1):
            self.gen_tiles_one_level(tz)


class KMLTilesFromMainKMLFile(KMLTiles):
    # -------------------------------------------------------------------------
    def __init__(self, inFile, meta, outDir='', outUrl=''):
        KMLTiles.__init__(self, meta, outDir, outUrl)
        self.inFile = inFile

    # -------------------------------------------------------------------------
    def gen_one_bmp(self, fname, bbox):
        """
        Generate the bitmap for one tile.
        """

        sp_proj = IP.IPSpatialReference()
        wkt = 'GEOGCS["Lat Long WGS84",DATUM["D_WGS84", SPHEROID["World_Geodetic_System_of_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]]'
        sp_proj.ImportFromWkt(wkt)

        # ---  Set up screen Transformation
        screen = IP.IPGeoTransform()
        screen.setWindow( (bbox[3], bbox[1], bbox[2], bbox[0]) )
        screen.setViewportFromSize(size=(1,1), dpi=self.tilesize)

        # ---  Burn raster
        bmap = IP.IPGdalOgr(toSpacialReference=sp_proj, toGeoTransform=screen, backgroundColor=[255,255,255,0])
        bmap.addKMLFile(self.inFile)
        bImg = bmap.exportAsPILImage()
        bImg.save(fname, 'PNG')
        bmap = None
        bImg = None


# =============================================================================
# =============================================================================
# =============================================================================

if __name__=='__main__':
    meta = KMLMetaData()
    meta['authority']= 'KMLWriter'
    meta['title']    = 'Test'
    meta['variable'] = 'Temperature'
    meta['bbox']     = [ 2.0, -2.0, 3.0, -3.0 ]   # n, s, e, w
    meta['quality']  = KMLMeta.Quality.low
    meta['resolution'] = 1.0e-5

    k = KMLTilesFromMainKMLFile('bb', meta)
    k.process()

