#!/usr/bin/env python
# -*- coding: utf-8 -*-
## cython: profile=True
## cython: linetrace=True
#************************************************************************
# --- Copyright (c) Yves Secretan
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************
"""
Transfer 1 column of H2D2 post-treatment data to LandXML TIN format
"""

# ---  System imports
from datetime import date, datetime
import logging
import os
import sys

from deprecation import deprecated

import numpy as np

if __name__ == '__main__':
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '../..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

from CTCommon.FEMesh      import FEElementQ4
from CTCommon.XMLWriter   import XMLWriter
from IPImageProcessor.KML import KMLMeta

LOGGER = logging.getLogger("H2D2.Tools.GIS.LandXML")

# https://github.com/rsignell-usgs/dap2arc/blob/master/src/dap2arc.py


class LandXMLWriter(XMLWriter):
    def __init__(self, meta : KMLMeta, fname = ''):
        self.meta  = meta
        self.fname = fname
        super(LandXMLWriter, self).__init__(self.fname)

    def writeHeader(self):
        atts_LandXML = {
            'version'   : '1.1', 
            'date'      : '%s' % date.today(),
            'time'      : '%s' % datetime.now().time().replace(microsecond=0),
            'xmlns'     : 'http://www.landxml.org/schema/LandXML-1.1',
            'xmlns:xsi' : 'http://www.w3.org/2001/XMLSchema-instance',
            'xsi:schemaLocation' : 'http://www.landxml.org/schema/LandXML-1.1 http://www.landxml.org/schema/LandXML-1.1/LandXML-1.1.xsd',
        }
        atts_Metric = {
            'areaUnit'          : 'squareMeter',
            'linearUnit'        : 'meter',
            'volumeUnit'        : 'cubicMeter',
            'temperatureUnit'   : 'celsius',
            'pressureUnit'      : 'milliBars',
            'elevationUnit'     : 'meter',
        }
        atts_Application = {
            'name'              : 'H2D2',
            'version'           : '21.04',
            'manufacturer'      : 'INRS',
            'manufacturerURL'   : 'http://gre-ehn.ete.inrs.ca/h2d2',
            #'timestamp'         : '%s' % datetime.utcnow(),
        }
        atts_cs = { 
            'name'          : '%s' % self.meta['srs_name'], 
        }
        if self.meta['srs_epsg']: atts_cs['epsgCode']   = '%d' % self.meta['srs_epsg']
        if self.meta['srs_wkt']:  atts_cs['ogcWktCode'] = self.meta['srs_wkt']

        super(LandXMLWriter, self).writeHeader()
        self.openElem('LandXML', atts_LandXML)

        self.openElem ('Units')
        self.writeElem('Metric', atts_Metric)
        self.closeElem()        # Units

        self.openElem ('Application', atts_Application)
        self.writeElem('Author', {'createdBy' : 'secretyv'})
        self.closeElem()        # Application
        self.writeElem('CoordinateSystem', atts_cs)

        self.openElem('Surfaces')

    def writeOneTIN(self, grid, H, name='Water Level'):
        surfType = 'grid' if isinstance(grid.getElement(0), FEElementQ4) else 'TIN'

        igs = [ nod.getGlobalIndex() for nod in grid.iterNodes() ]

        # ---  Header
        Hr = H[igs]                 # restriction to grid nodes
        F = np.logical_not( np.isnan(Hr) )
        Hr = Hr[F]                  # restriction to not nan values
        self.openElem('Surface', {'name' : '%s' % name})
        self.openElem('Definition', {'surfType' : surfType,
                                     'elevMin'  : '%f' % min(Hr),
                                     'elevMax'  : '%f' % max(Hr)})

        # ---  Mask for nan values
        F = np.logical_not( np.isnan(H) )

        # ---  Points
        self.openElem('Pnts')
        for nod in grid.iterNodes():
            ig = nod.getGlobalIndex()
            if not F[ig]: continue
            il = nod.getLocalIndex()
            x, y = nod.getCoordinates()
            self.writeTag('P id="%d"' % (il+1,), ' %f %f %f' % (y, x, H[ig]))
        self.closeElem()    # Pnts

        # ---  Faces
        self.openElem('Faces')
        for ele in grid.iterElements():
            c = ele.getConnectivities()
            if not all(F[c]): continue
            self.writeTag('F', ' '.join(['%d' % (n+1,) for n in c]))
        self.closeElem()    # Faces

        self.closeElem()    # Definition
        self.closeElem()    # Surface

    def writeFooter(self):        
        super(LandXMLWriter, self).writeFooter(closeAll=True)
        self.close()

def write(fout, meta, grid, vals, name):
    """
    write a TIN to fout in LandXML format

    Args:
        fout (str): output file name
        meta (LandXMLMetaData): meta data
        grid (FEMesh): finite element grid for the TIN
        vals (ndarray): values for the TIN
        name (str): name for the values
    """
    xmlWriter = LandXMLWriter(meta, fout)
    xmlWriter.writeHeader()
    xmlWriter.writeOneTIN(grid, vals, name)
    xmlWriter.writeFooter()

@deprecated('now')
def writeLandXML(fout, grid, H, name='Water Level'):

    X, Y = grid.getCoordinates()

    xml_header = [
        '<?xml version="1.0"?>',
        '<LandXML', 
        '   version="1.1"', 
        '   date="2008-06-09"',
        '   time="08:33:4"',
        '   xmlns="http://www.landxml.org/schema/LandXML-1.1"',
        '   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"',
        '   xsi:schemaLocation="http://www.landxml.org/schema/LandXML-1.1 http://www.landxml.org/schema/LandXML-1.1/LandXML-1.1.xsd">',
        '',
        ]
    xml_units = [
        '<Units>',
        '   <Metric areaUnit="squareMeter"',
        '           linearUnit="meter"',
        '           volumeUnit="cubicMeter"',
        '           temperatureUnit="celsius"', 
        '           pressureUnit="milliBars"/>',
        '</Units>',
        '',
    ]
    xml_appl = [
        '<Application',
        '   name="H2D2"',
        '   version="19.04"',
        '   manufacturer="INRS"',
        '   manufacturerURL="http://gre-ehn.ete.inrs.ca/h2d2">',
        '   <Author createdBy="secretyv"/>',
        '</Application>',
        '',
    ]     
    xml_proj = [
        '<CoordinateSystem',
        '   name="MTM - Zone 8"', 
        '   epsgCode="32188">',
        '</CoordinateSystem>',
        '',
    ]     

    f = open(fout, 'w')

    f.write('\n'.join(xml_header))
    f.write('\n'.join(xml_units))
    f.write('\n'.join(xml_appl))
    f.write('\n'.join(xml_proj))

    igs = [ nod.getGlobalIndex() for nod in grid.iterNodes() ]
    f.write('<Surfaces>\n')
    f.write('   <Surface name=%s>\n' % quoteattr(name))
    f.write('      <Definition surfType=\"TIN\" elevMin=\"%f\" elevMax=\"%f\">\n' % (min(H[igs]), max(H[igs])))
    #f.write('      <Definition surfType=\"TIN\">\n')

    f.write('         <Pnts>\n')
    for nod in grid.iterNodes():
        ig = nod.getGlobalIndex()
        il = nod.getLocalIndex()
        x, y = nod.getCoordinates()
        f.write('            <P id=\"%d\"> %f %f %f </P>\n' % (il, y, x, H[ig]))
    f.write('         </Pnts>\n')

    f.write('         <Faces>\n')
    for ele in grid.iterElements():
        c = ele.getConnectivities()
        f.write('            <F> %d %d %d </F>\n' % (c[0], c[1], c[2]))
    f.write('         </Faces>\n')
    f.write('      </Definition>\n')
    f.write('   </Surface>\n')
    f.write('</Surfaces>\n')

    xml_trailer = [
        '</LandXML>',
        '',
    ]
    f.write('\n'.join(xml_trailer))

    f.close()
