# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2021
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
from matplotlib.ticker import ScalarFormatter
from matplotlib.ticker import FormatStrFormatter
from matplotlib.transforms import Bbox
import numpy as np

class IPDraggableColorBarWithFrame:
    """
    Colorbar in a "frame" and draggable

    ctrl+shift+h to hide
    ctrl+shift+r to reset position
    """
    # https://stackoverflow.com/questions/2539477/how-to-create-a-draggable-legend-in-matplotlib
    # Note: Changing the cursor is a getting in a fight with th toolbar
    # settle on changing frame color

    def __init__(self, mappable, ax, **kw):
        """
        mappable: The matplotlib.cm.ScalarMappable (i.e., AxesImage, ContourSet, etc.) described by this colorbar. This argument is mandatory.
        ax: Parent axes into which the new colorbar axes will be drawn. This argument is mandatory.
        height: "auto"
        loc: Unsuported
        """
        kwCBDef = {
            'format'   : ScalarFormatter(),
            'ticks'    : getattr(mappable, 'levels', np.linspace(0.0, 1.0, 11)),
            'label'    : ''
        }
        kwIADef = {
            'mappable' : mappable,
            'fontsize' : 8,
            'height'   : 'auto',
            'bgcolor'  : 'white',
        }

        self.axesParent    = None
        self.axesInsetMain = None
        self.axesInsetCB   = None
        self.cbar   = None
        self.pos    = None
        self.width  = -1
        self.height = -1
        self.cbWidth= -1

        self.doMove = kw.pop('movable',   True)
        self.doKey  = kw.pop('keyAction', True)

        self.kwCB = kwCBDef | { k: v for k, v in kw.items() if k not in kwIADef }
        self.kwIA = kwIADef | { k: v for k, v in kw.items() if k not in kwCBDef }
        
        if isinstance(self.kwCB['format'], str):
            self.kwCB['format'] = FormatStrFormatter(self.kwCB['format'])

        self.axesParent = ax
        self.figure     = self.axesParent.figure
        self.canvas     = self.figure.canvas
        self.trfInv     = self.axesParent.transAxes.inverted()

        self.__getdimensions()
        self.__createCB()

        self.isHidden = False
        self.isMoving = False
        self.isHoover = False
        if self.doMove:
            self.canvas.mpl_connect('motion_notify_event', self.__onMouseMove)
            self.canvas.mpl_connect('button_press_event',  self.__onMouseDown)
            self.canvas.mpl_connect('button_release_event',self.__onMouseUp)
            self.canvas.mpl_connect('axes_enter_event',    self.__onAxesEnter)
        if self.doKey:
            self.canvas.mpl_connect('key_release_event',   self.__onKey)

    # https://stackoverflow.com/questions/24581194/matplotlib-text-bounding-box-dimensions
    def __getTextDims(self, txt, fontsize=8):
        """
        __getTextDims return the sizes of text `txt` in normalized coordinates.

        Args:
            txt (str): the text to be measured
            fontsize (int, optional): Font size to use. Defaults to 8.

        Returns:
            tuple: (width, height) in normalized coordinates ([0,1]).
        """
        mplTxt = self.axesParent.text(0, 0, txt, fontsize=fontsize)
        bb = mplTxt.get_window_extent(renderer=self.canvas.get_renderer())
        mplTxt.remove()
        bb = bb.transformed(self.trfInv)
        return bb.x1-bb.x0, bb.y1-bb.y0

    def __getMaxTickWidth(self, ticks, format, fontsize=8):
        """
        __getMaxTickWidth return the maximum extend for the ticks.

        Args:
            ticks (iterable): [description]
            format (Formatter): [description]
            fontsize (int, optional): Font size to use. Defaults to 8.

        Returns:
            float: extend in normalized coordinates ([0,1]).
        """
        txts = [ format.format_data(tck) for tck in ticks ]
        imax = max( [(len(txt), it) for it, txt in enumerate(txts)])[1]
        return self.__getTextDims(txts[imax], fontsize)[0]

    def __getdimensions(self):
        """
        __getdimensions computes all the dimensions

        sets:
            self.width      # in norm of parent
            self.height     # in norm coord of parent
            self.cbwitdh    # in norm of parent (i.e. self.width)
        """
        kwCB = self.kwCB
        kwIA = self.kwIA

        # ---  Sizes in figure coordinates
        tkWidth  = self.__getMaxTickWidth(kwCB['ticks'], kwCB['format'], kwIA['fontsize'])
        cbWidth, cbHeight = self.__getTextDims('mm')
        lbWidth, lbHeight = self.__getTextDims(kwCB['label']+'m') if kwCB['label'] else (0,0)

        w1 = 2*cbWidth + tkWidth + 1.4*lbHeight  # % of main window, factor 2 is for margins
        if kwIA['height'] == 'auto':
            h1 = (1.1*cbHeight) * (2 + len(kwCB['ticks']))
        elif isinstance(kwIA['height'], str):
            h1 = eval(kwIA['height'][:-1])/100
        else:
            h1 = kwIA['height']
        self.width  = w1
        self.height = max(h1, min(1.5*h1, lbWidth))
        self.cbWidth = cbWidth / self.width     # % of axesInsetMain

    def __createCB(self):
        """
        __createCB creates the colorbar with 2 nested inset_axes, the
        external one playing the role of fram, while the innermost holds
        the colorbar.

        Args:
            cs ([type]): [description]
            kwCB (dict): keyword arguments for the colorbar
            kwIA (dict): keyword arguments for the inset_axes
        """
        kwCB = self.kwCB
        kwIA = self.kwIA

        # ---  Main inset-axes
        ww = "%.2f%%" % (self.width*100)
        hh = "%.2f%%" % (self.height*100)
        self.axesInsetMain = inset_axes(self.axesParent, width=ww, height=hh, loc='upper right')
        self.axesInsetMain.get_xaxis().set_visible(False)
        self.axesInsetMain.get_yaxis().set_visible(False)
        self.axesInsetMain.set_facecolor(kwIA['bgcolor'])
        if self.pos:
            self.axesInsetMain.set_axes_locator(self.pos)
        
        # ---  Colorbar inset-axes
        ww = "%.2f%%" % (self.cbWidth*100)
        self.axesInsetCB = inset_axes(self.axesInsetMain, width=ww, height="90%", loc='center left')
        self.axesInsetCB.yaxis.set_ticks_position("right")

        # ---  Colorbar
        self.cbar = self.figure.colorbar(kwIA['mappable'], cax=self.axesInsetCB, orientation="vertical", **kwCB)
        self.cbar.ax.tick_params(labelsize=kwIA['fontsize'])
        self.cbar.ax.yaxis.label.set_size(kwIA['fontsize'])

    def __onAxesEnter(self, event):
        if self.isHidden: return
        if self.isHoover:
            if event.inaxes not in (self.axesInsetMain, self.axesInsetCB):
                for s in self.axesInsetMain.spines.values():
                    s.set_color('black')
                    s.set_linewidth(1)
                self.canvas.draw()
                self.isHoover = False
        else:
            if event.inaxes == self.axesInsetMain:
                for s in self.axesInsetMain.spines.values():
                    s.set_color('salmon')
                    s.set_linewidth(3)
                self.canvas.draw()
                self.isHoover = True

    def __onMouseMove(self, event):
        """
        __onMouseMove is the call-back for a mouse move event. It will update
        the colorbar position

        Args:
            event (MouseEvent): mouse event with the position

        Sets:
            self.pos (InsetPosition): The actual position
        """
        if self.isMoving:
            dx = event.x - self.mouse_x
            dy = event.y - self.mouse_y
            xp, yp = self.frame_x + dx, self.frame_y + dy       # px coord
            xn, yn = self.trfInv.transform_point( (xp, yp) )    # normalized coord
            self.pos = InsetPosition(self.axesParent, [xn, yn, self.width, self.height])
            self.axesInsetMain.set_axes_locator(self.pos)
            self.canvas.draw()

    def __onMouseDown(self, event):
        if event.inaxes in (self.axesInsetMain, self.axesInsetCB):
            bbox = self.axesInsetMain.get_window_extent()
            self.mouse_x = event.x
            self.mouse_y = event.y
            self.frame_x = bbox.xmin
            self.frame_y = bbox.ymin
            self.isMoving = True

    def __onMouseUp(self, event):
        if self.isMoving:
            self.isMoving = False

    def __onKey(self, event):
        if event.inaxes in (self.axesInsetMain, self.axesInsetCB) or self.isHidden:
            if event.key == 'ctrl+H':
                self.hide(not self.isHidden)
                self.canvas.draw()
            elif event.key == 'ctrl+R':
                self.reset()
                self.canvas.draw()
            
    def hide(self, doHide):
        """
        hide 

        Caller is responsible for calling canvas.draw().
        """
        if doHide:
            if self.cbar: self.cbar.remove()
            if self.axesInsetMain: self.axesInsetMain.remove()
            self.cbar = self.axesInsetCB = self.axesInsetMain = None
            self.isHidden = True
        else:
            self.__createCB()
            self.isHidden = False

    def hidden(self):
        return self.isHidden

    def reset(self):
        """
        reset [summary]

        Caller is responsible for calling canvas.draw()
        """
        # ---  Remove
        self.hide(True)
        # ---  Re-create
        self.pos  = None
        self.__createCB()
        # ---  Re-draw
        self.isHidden = False
        self.isMoving = False
        self.isHoover = False

if __name__ == "__main__":
    import numpy as np
    import matplotlib
    import matplotlib.pyplot as plt

    print(matplotlib.get_backend())

    fig, ax = plt.subplots(1, 1, figsize=[4, 4])

    # Default delta is large because that makes it fast, and it illustrates
    # the correct registration between image and contours.
    delta = 0.5
    extent = (-3, 4, -4, 3)

    x = np.arange(-3.0, 4.001, delta)
    y = np.arange(-4.0, 3.001, delta)
    X, Y = np.meshgrid(x, y)
    Z1 = np.exp(-X**2 - Y**2)
    Z2 = np.exp(-(X - 1)**2 - (Y - 1)**2)
    Z = (Z1 - Z2) * 2

    # Boost the upper limit to avoid truncation errors.
    levels = np.arange(-2.0, 1.601, 0.4)
    cset   = ax.contourf(X, Y, Z, levels)

    cbar = IPDraggableColorBarWithFrame(cset, ax, format='%.2f', height='auto', label='Label 012345678 colorbar blabla')

    plt.show()
