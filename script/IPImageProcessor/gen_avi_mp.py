#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

import IPRaster as IP

import numpy as np
import multiprocessing as mp

import logging
#LOGGER = logging.getLogger("INRS.H2D2.Tools")
LOGGER = mp.log_to_stderr()

def main(args = None):
    if (args): pid, pool_size = args

    pdir = 'data' + os.sep      # background info
    gdir = 'data' + os.sep      # geometry/grid dir
    ddir = '.' + os.sep         # data dir
    hdir = 'data' + os.sep      # hydrodynamic

    # ---  Set project Spatial Reference
    sp_proj = IP.IPSpatialReference()
    sp_proj.ImportFromModeleur(gdir+'utm_73W_nad83.pjc')
#    wkt = 'GEOGCS["Lat Long WGS84",DATUM["D_WGS84",SPHEROID["World_Geodetic_System_of_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]]'
#    sp_proj.ImportFromWkt(wkt)

    # ---  Set grid Spatial Reference
    sp_grid = IP.IPSpatialReference()
#    sp_grid.ImportFromModeleur(gdir+'utm_73W_nad83.pjc')
    sp_grid = sp_proj

    # ---  Coordinate transform from grid to project
    grid2proj = IP.IPCoordinateTransformation(sp_grid, sp_proj)

    # --- Mesh & data
    mesh = IP.FEMesh( [gdir+'simul000.cor', gdir+'simul000.ele'], projection = grid2proj)
    data = IP.DTData(hdir+'solution.cd2d.pst.sim', cols=(0,))
#    mask = IP.DTData(ddir+'sv2d.pst.sim', cols=(2,))

    # ---  Sizes
#    bbox = mesh.getBbox(stretchfactor=0.02)     # ll, ur in base coord
    bbox = (624900.0, 5173700.0,  638250.0, 5184950.0)
    size = (10, 7)                              # in inches
    dpi  = 120

    # ---  Set up screen Transformation
    screen = IP.IPGeoTransform()
    screen.setWindow(bbox)
    screen.setViewportFromSize(size=size, dpi=dpi)
    screen.resizeWindowToViewport()

    # ---  Base map
    bmap = IP.IPGdalOgr(toSpacialReference=sp_proj, toGeoTransform=screen)
    bmap.addShapeFile(pdir+'hydro_filamentaire.shp')
    bmap.addShapeFile(pdir+'hydro_polygone.shp')
    bmap.addShapeFile(pdir+'reseau_routier.shp')
#    bmap.addGeoTIFF(pdir+'fondImage.tif')
    bImg = bmap.exportAsImage()

    # --- Create Mask
   # fdta = mask.getDataAtTime(0.0)
   # fImg = IP.genMask(mesh, fdta, [-1.0e+99, 0.06], 'w', size=size, screen=screen)
    fImg = None

    # ---  Create animation
    vmin = 0.00
    vmax = 0.05
    nval = 21
    lvls = np.linspace(vmin, vmax, nval)
    tini = 1245137400.0 # le début
#    tini = 1245507600.0 - 5*3600 # le pic
    tfin = tini + 5*(24*3600.0)
    delt = 3600.0/12

    npas = 1 + int((tfin - tini) / delt)        # Total number of time steps
    npap = int(npas / float(pool_size))         # Number of time steps per process
    t0 = tini + delt*npap*pid                   # Initial time of the process
    t1 = t0   + delt*npap - 0.1*delt            # Final time of the process
    if (pid == pool_size-1): t1 = tfin

    hp = IP.IPAnimation()
    hp.createFigure(size=size, dpi=dpi, title = 'Concentration - ')
    hp.addSubPlot(1, 1, 1, window=screen.getWindow(), background=bImg, foreground=fImg)
    hp.adjustSubPlot(hspace=0.09, wspace=0.01, bottom=0.05, top=0.97, left=0.07, right=0.95)
    hp.createFrames(mesh, data, levels=lvls, tini=t0, tfin=t1, delt=delt, colorbar=True)

def init_worker():
    import signal
    signal.signal(signal.SIGINT, signal.SIG_IGN)

if __name__ == '__main__':
#    streamHandler = logging.StreamHandler()
#    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.INFO)

    POOL_SIZE = 7
    po = mp.Pool(POOL_SIZE, init_worker)
    try:
        args = ((i, POOL_SIZE) for i in range(POOL_SIZE))
        r = po.map_async(main, args)
        po.close()
        r.get(timeout=0xFFFF)
        po.terminate()
        po.join()
        IP.IPAnimation.createAnimationFromFrames()

    except KeyboardInterrupt:
        po.terminate()
        po.join()
