# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2014
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import collections.abc
import matplotlib
import math

from ..MPL import MPLSticher
from ..MPL import MPLSimplifier

class MPLUtil:
    PathCodes = {
         0 : "STOP",
         1 : "MOVETO",
         2 : "LINETO",
         3 : "CURVE3",
         4 : "CURVE4",
        79 : "CLOSEPOLY",
         }

    sticher    = MPLSticher.Sticher()
    simplifier = MPLSimplifier.Simplifier()
    tol_angle  = math.radians(20.0)
    tol_length = 50 / (111111 * math.cos(math.radians(45)))    # m at latitude 45deg
    tol_area   = tol_length * tol_length
    tol_position = tol_length

    @staticmethod
    def __appendRing(rings, ring, stich=True, simplify=False):
        '''
        Rings are normalized (closed, min point first), and CCW
        '''
        if len(ring) >= 3:
            poly = ring
            if not MPLUtil.isRingCCW(poly): poly.reverse()

            if (stich):
                poly = MPLUtil.sticher.stich(poly)
            if (simplify):
                if len(poly) >= 3:
                    if (MPLUtil.getArea(poly) < MPLUtil.tol_area): poly = []
                if len(poly) >= 3:
                    poly = MPLUtil.normalizeRing(poly)
                    poly = MPLUtil.simplifier.simplify(poly, tol_angle=MPLUtil.tol_angle, tol_length=MPLUtil.tol_length)
                if len(poly) >= 3:
                    if (MPLUtil.getArea(poly) < MPLUtil.tol_area): poly = []
                print('appendPolygon: %6d %6d  %.6e' % (len(poly), len(ring), MPLUtil.getArea(poly)))
            if len(poly) >= 3:
                poly = MPLUtil.normalizeRing(poly)
                rings.append(poly)

    @staticmethod
    def rgba2kml(color):
        rgba = matplotlib.colors.to_rgba(color)
        r = int( max(min(rgba[0]*255, 255), 0) )
        g = int( max(min(rgba[1]*255, 255), 0) )
        b = int( max(min(rgba[2]*255, 255), 0) )
        a = int( max(min(rgba[3]*255, 255), 0) )
        return '%02x%02x%02x%02x' % (a, b, g, r)

    @staticmethod
    def rgba2html(color):
        rgb = matplotlib.colors.to_rgb(color)
        r = int( max(min(rgb[0]*255, 255), 0) )
        g = int( max(min(rgb[1]*255, 255), 0) )
        b = int( max(min(rgb[2]*255, 255), 0) )
        return '#%02x%02x%02x' % (r, g, b)

    @staticmethod
    def kml2html(color):
        a, b, g, r = ( color[i:i+2] for i in range(0, len(color), 2) )
        return '#%s%s%s' % (r, g, b), int(a, 16)

    @staticmethod
    def isSameVertex(v1, v2):
        if v1[0] != v2[0]: return False
        if v1[1] != v2[1]: return False
        return True

    @staticmethod
    def getRingsFromPath(path, simplify=False):
        '''
        Get the rings from a path
        Rings are normalized (closed, min point first), and CCW
        '''
        rings = []
        ring  = []
        v0 = None
        for (vertex, code) in path.iter_segments(simplify=False):
            if (code == 1):     # MOVETO
                MPLUtil.__appendRing(rings, ring, simplify)
                ring = []
                ring.append(vertex)
                v0 = vertex
            elif not MPLUtil.isSameVertex(vertex, v0):
                ring.append(vertex)
                v0 = vertex
        MPLUtil.__appendRing(rings, ring, simplify)
        return rings

    @staticmethod
    def getPolygonsFromContourSet(cs, simplify = False):
        '''
        Return a list with the following structure
            [   # collection
                [   # polygons
                    [ outerRing, innnerRing, innnerRing, ... ]
                    ...
                ]
                    ...
            ]
        where xxxRing is a list or nparray of vertices.
        outerRing is CCW, innerRing are CW
        '''

        cset = []
        for collection in cs.collections:
            if not isinstance(collection, matplotlib.collections.PathCollection): continue
            coll = []
            for path in collection.get_paths():
                rings = MPLUtil.getRingsFromPath(path)
                for r in rings: assert MPLUtil.isSameVertex(r[0],r[-1])
                polys = MPLUtil.makePolygonsFromRings(rings)
                coll.extend(polys)
            cset.append(coll)
        return cset

    @staticmethod
    def getPolylinesFromContourSet(cs, simplify = False):
        '''
        Return a list with the following structure
            [   # collection
                [   # polyline
                ]
                    ...
            ]
        '''

        cset = []
        for collection in cs.collections:
            if not isinstance(collection, matplotlib.collections.LineCollection): continue
            coll = []
            for path in collection.get_paths():
                pline = []
                v0 = None
                for (vertex, code) in path.iter_segments(simplify=False):
                    assert len(vertex) == 2
                    if (code == 1):     # MOVETO
                        if len(pline) > 1: coll.append(pline)
                        pline = []
                        pline.append(vertex)
                        v0 = vertex
                    elif not MPLUtil.isSameVertex(vertex, v0):
                        pline.append(vertex)
                        v0 = vertex
                if len(pline) > 1: coll.append(pline)
            cset.append(coll)
        return cset

    @staticmethod
    def getLines2DFromContourSet(cs, simplify = False):
        '''
        Return a list with the following structure
            [   # collection
                [   # polyline
                ]
                    ...
            ]
        '''
        cset = []
        for line2D in cs:
            if not isinstance(line2D, matplotlib.lines.Line2D): return cset
            coll = []
            path = line2D.get_path()
            pline = []
            v0 = None
            for (vertex, code) in path.iter_segments(simplify=False):
                assert len(vertex) == 2
                if (code == 1):     # MOVETO
                    if len(pline) > 1: coll.append(pline)
                    pline = []
                    pline.append(vertex)
                    v0 = vertex
                elif not MPLUtil.isSameVertex(vertex, v0):
                    pline.append(vertex)
                    v0 = vertex
            if len(pline) > 1: coll.append(pline)
            cset.append(coll)
        return cset

    @staticmethod
    def getLinesFromContourSet(cs, simplify = False):
        '''
        Return a list with the following structure
            [   # collection
                [   # polyline
                ]
                    ...
            ]
        '''
        cset = []
        for collection in cs:
            if not isinstance(collection, matplotlib.collections.LineCollection): return cset
            coll = []
            v0 = None
            for path in collection.get_paths():
                pline = []
                for (vertex, code) in path.iter_segments(simplify=False):
                    assert len(vertex) == 2
                    if (code == 1):     # MOVETO
                        if len(pline) > 1: coll.append(pline)
                        pline = []
                        pline.append(vertex)
                        v0 = vertex
                    elif not MPLUtil.isSameVertex(vertex, v0):
                        pline.append(vertex)
                        v0 = vertex
                if len(pline) > 1: coll.append(pline)
            cset.append(coll)
        return cset

    @staticmethod
    def getQuiverFromContourSet(cs):
        '''
        Return a list with the following structure
            [   # collection
                [   # polygone
                ]
                    ...
            ]
        '''
        if not isinstance(cs, matplotlib.collections.PolyCollection): return
        cset = []
        for offset, path in zip(cs.get_offsets(), cs.get_paths()):
            pline = []
            v0 = None
            for (vertex, code) in path.iter_segments(simplify=False):
                if (code == 1):     # MOVETO
                    if len(pline) > 1: cset.append(pline)
                    pline = []
                    pline.append(offset+vertex)
                    v0 = vertex
                elif not MPLUtil.isSameVertex(vertex, v0):
                    pline.append(offset+vertex)
                    v0 = vertex
            if len(pline) > 1: cset.append(pline)
        return cset

    @staticmethod
    def getEllipsesFromContourSet(cs):
        '''
        Return a list with the following structure
            [   # collection
                [   # polygone
                ]
                    ...
            ]
        '''
        if not isinstance(cs, matplotlib.collections.PatchCollection): return
        cset = []
        for path in cs.get_paths():
            pline = []
            for (vertexes, code) in path.iter_segments(simplify=False):
                v0 = None
                for vertex in zip(vertexes[::2],vertexes[1::2]):
                    if (code == 1):     # MOVETO
                        if len(pline) > 1: cset.append(pline)
                        pline = []
                        pline.append(vertex)
                        v0 = vertex
                    elif not MPLUtil.isSameVertex(vertex, v0):
                        pline.append(vertex)
                        v0 = vertex
            if len(pline) > 1: cset.append(pline)
        return cset

    @staticmethod
    def getAnnotationsFromContourSet(cs):
        '''
        Return a list with the following structure
            [   # collection
                [   # polygone
                ]
                    ...
            ]
        '''
        if not isinstance(cs, collections.abc.Iterable): return
        cset = []
        for ann in cs:
            X, Y = ann.get_position()
            txt = ann.get_text()
            try:
                for x, y in zip(X, Y):
                    cset.append( (x, y, txt) )
            except TypeError:
                cset.append( (X, Y, txt) )
        return cset

    @staticmethod
    def getColorsFromContourSet(cs):
        if hasattr(cs, 'collections'):
            collections = cs.collections
        elif isinstance(cs, (list, tuple)):
            collections = cs
        elif isinstance(cs, matplotlib.collections.LineCollection):
            collections = [cs]
        elif isinstance(cs, matplotlib.collections.PathCollection):
            collections = [cs]
        elif isinstance(cs, matplotlib.collections.PatchCollection):
            collections = [cs]
        elif isinstance(cs, matplotlib.collections.PolyCollection):
            collections = [cs]
        else:
            raise ValueError('Invalid ContourSet')
        if (len(collections) <= 0): return []
            
        # ---  Process
        if   isinstance(collections[0], matplotlib.collections.LineCollection):
            colors = [ [ MPLUtil.rgba2kml(c) for c in col.get_color() ] for col in collections ]
        elif isinstance(collections[0], matplotlib.collections.PathCollection):
            colors = [ MPLUtil.rgba2kml(c.get_facecolor()[0]) for c in collections ]
        elif isinstance(collections[0], matplotlib.collections.PatchCollection):
            colors = [ [ MPLUtil.rgba2kml(c) for c in col.get_facecolor() ] for col in collections ]
        elif isinstance(collections[0], matplotlib.collections.PolyCollection):
            colors = [ [ MPLUtil.rgba2kml(c) for c in col.get_facecolor() ] for col in collections ]
        elif isinstance(collections[0], matplotlib.lines.Line2D):
            colors = [ MPLUtil.rgba2kml(c.get_color()) for c in collections ]
        else:
            raise ValueError('Unhandled type: %s' % type(collections[0]))
        return colors

    @staticmethod
    def getLevelsFromContourSet(cs):
        return cs.levels

    @staticmethod
    def makePathFromRing(vertices):
        '''
        Return a matplotlib path build from the vertices
        '''
        codes = [2]*len(vertices)   # Lineto
        codes[0] = 1                # Moveto
        return matplotlib.path.Path(vertices, codes)

    @staticmethod
    def makePolygonsFromRings(rings):
        '''
        Reorganizes the rings as a list of outer an inner rings
            [   # polygons
                [ outerRing, innnerRing, innnerRing, ... ]
                ...
            ]
        where xxxRing is a list or nparray of vertices.
        outerRing is CCW, innerRing are CW
        '''
        class Item:
            def __init__(self, ring):
                self.ring = ring
                self.path = MPLUtil.makePathFromRing(ring)
                self.bbox = MPLUtil.getBbox(ring)
                self.poly = [ring]

        #print 'makePolygonsFromRings: nrings = ', len(rings)
        psrtd = sorted(rings, key=lambda p: len(p))
        items = [ Item(ring) for ring in psrtd ]
        poly  = [ [ring] for ring in psrtd ]
        for io in items:
            if (io.poly is None): continue
            assert (len(io.poly) == 1)
            for ii in items:
                if (ii.poly is None): continue
                if (len(ii.poly) != 1): continue
                if (ii == io): continue
                if (not MPLUtil.doBboxIntersect(io.bbox, ii.bbox)): continue
                if (not io.path.contains_point (ii.ring[0])): continue
                if (not io.path.contains_points(ii.ring).all()): continue   # contains_path is more precise, but more costly
                #if (not io.path.contains_path(ii.path)): continue          # on tests, contains_points produces the same result
                #print io, ' contains ', ii
                ii.ring.reverse()                                           # from CCW to CW
                io.poly.append(ii.ring)
                ii.poly = None
        #print 'poly done'
        return [ it.poly for it in items if it.poly ]

    @staticmethod
    def normalizeRing(vertices):
        '''
        Return a closed ring, starting from the lowest-rightmost vertex
        '''
        if MPLUtil.isSameVertex(vertices[0], vertices[-1]):
            vtcs = vertices[:-1]
        else:
            vtcs = vertices
        if (len(vtcs) < 3): return []
        i1 = -1
        v1 = (-1.0e99, 1.0e99)
        for i, v in enumerate(vtcs):
            if (v[1] < v1[1]) or (v[1] == v1[1] and v[0] > v1[0]):
                i1, v1 = i, v
        assert (i1 >= 0)
        r = vtcs[i1:]
        r.extend( vtcs[:i1] )
        if not MPLUtil.isSameVertex(r[0],r[-1]): r.append(r[0])
        return r

    @staticmethod
    def isRingCCW(vertices):
        '''
        Return True if the ring is CounterClockWise ordered, False otherwise
        '''
        if (len(vertices) < 3): return False
        i1 = -1
        v1 = (-1.0e99, 1.0e99)
        for i, v in enumerate(vertices):
            if (v[1] < v1[1]) or (v[1] == v1[1] and v[0] > v1[0]):
                i1, v1 = i, v
        assert (i1 >= 0)

        v2 = vertices[(i1+1) % len(vertices)]
        v3 = vertices[(i1+2) % len(vertices)]
        x21 = v2[0] - v1[0]
        y21 = v2[1] - v1[1]
        x31 = v3[0] - v1[0]
        y31 = v3[1] - v1[1]

        return (x21*y31 - x31*y21) > 0

    @staticmethod
    def getArea(v):
        '''
        Return the oriented area of the ring
        # http://stackoverflow.com/questions/451426/how-do-i-calculate-the-surface-area-of-a-2d-polygon
        '''
        a = sum( x0*y1 - x1*y0 for ((x0, y0), (x1, y1)) in zip(v, v[1:] + [v[0]]) )
        return 0.5 * abs(a)

    @staticmethod
    def getBbox(vertices):
        '''
        Return the bounding box of the ring
        '''
        vmin = [ 1.0e99, 1.0e99]
        vmax = [-1.0e99,-1.0e99]
        for v in vertices:
            vmin[0] = min(vmin[0], v[0])
            vmin[1] = min(vmin[1], v[1])
            vmax[0] = max(vmax[0], v[0])
            vmax[1] = max(vmax[1], v[1])
        return (vmin, vmax)

    @staticmethod
    def doBboxIntersect(bb1, bb2):
        '''
        Return True if the 2 bounding boxes intersect
        '''
        if (bb2[0][0] > bb1[1][0]): return False    # bb2.xmin > bb1.xmax
        if (bb2[1][0] < bb1[0][0]): return False    # bb2.ymin > bb1.ymax
        if (bb2[0][1] > bb1[1][1]): return False    # bb2.xmax < bb1.xmin
        if (bb2[1][1] < bb1[0][1]): return False    # bb2.ymax > bb1.ymin
        return True

if __name__ == '__main__':
    polygon = [ (-1, -1), (1, -1), (1, 1), (-1, 1) ]
    print(MPLUtil.isCCW(polygon))
