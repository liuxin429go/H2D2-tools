#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2013
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Creating and plotting unstructured triangular grids.
"""

#import pour décimer le maillage
#from FEFem import FEMesh

import enum
import logging
import math
#import multiprocessing as mp
import subprocess

import PIL
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as mpl_plt
import matplotlib.tri    as mpl_tri
from matplotlib.patches  import Ellipse
#from mpl_toolkits.mplot3d import Axes3D

from .           import IPUtil
from .GDAL       import GDLBasemap
#from MPL.MPLUtil import MPLUtil
#from external    import images2gif
from .external   import basic_units

from CTCommon.CTException import CTException
from CTCommon import FEMesh

LOGGER = logging.getLogger("INRS.H2D2.Tools.ImageProcessor")
#LOGGER = mp.get_logger()

IPUnits    = enum.Enum('IPUnits',    ('meter', 'degree'))
IPPlotType = enum.Enum('IPPlotType', 
                       ('CONTOUR_FILL', 'CONTOUR_LINE', 
                        'QUIVER',
                        'MESH', 'SUB_MESH', 'NODES',
                        'PARTICLE_PATH', 'ELLIPSE',
                        'SURFACE_3D',
                        'GRAPHE',
                        'TEXT', 'IMAGE'))

def compressKML(elements, outF = 'Simulation.kmz'):
    import zipfile

    try:
        compression = zipfile.ZIP_DEFLATED
    except:
        compression = zipfile.ZIP_STORED

    zf = zipfile.ZipFile(outF, mode='w')

    for e in elements:
        if os.path.isdir(e):
            for f in os.listdir(e):
                zf.write(os.path.join(e,f), arcname = os.path.join(os.path.basename(e),f), compress_type=compression)
        else:
            zf.write(e, arcname = os.path.basename(e), compress_type=compression)

    zf.close()


#---------------------------------------------------------------
#   Classe utilitaire de dimensionnement des axes
#---------------------------------------------------------------
class IPAxisSizes:
    def __init__(self, ll=None, ur=None, wh=None):
        if ll and ur:
            self.ll = ll
            self.wh = (ur[0]-ll[0], ur[1]-ll[1])
        else:
            self.ll = ll
            self.wh = wh

    def setPos(self, parent, wh=None, pos='ll', margin=(0,0)):
        if pos == 'll':
            pLL = parent.getLL()
            self.ll = pLL[0]+margin[0], pLL[1]+margin[1]
            self.wh = wh
        elif pos == 'lr':
            pLR = parent.getLR()
            self.ll = pLR[0]-wh[0]-margin[0], pLR[1]+margin[1]
            self.wh = wh
        elif pos == 'ur':
            pUR = parent.getUR()
            self.ll = pUR[0]-wh[0]-margin[0], pUR[1]-wh[1]-margin[1]
            self.wh = wh
        elif pos == 'ul':
            pUL = parent.getUL()
            self.ll = pUL[0]+margin[0], pUL[1]-wh[1]-margin[1]
            self.wh = wh
        else:
            raise ValueError
        return self

    def getW(self):
        return self.wh[0]

    def getH(self):
        return self.wh[1]

    def getWH(self):
        return self.wh

    def getLL(self):
        return self.ll

    def getLR(self):
        return self.ll[0]+self.wh[0], self.ll[1]

    def getUL(self):
        return self.ll[0], self.ll[1]+self.wh[1]

    def getUR(self):
        return self.ll[0]+self.wh[0], self.ll[1]+self.wh[1]

    def asLLUR(self):
        return self.getLL()+self.getUR()

    def asLLWH(self):
        return self.getLL()+self.getWH()


#---------------------------------------------------------------
#---------------------------------------------------------------
class IPFigure:
    """
    Classe faisant les images de plusieurs jeux de données.
    """

    def __init__(self):
        self.fig = None

    #def __del__(self):
    #    if (self.fig):
    #        if mpl_plt: mpl_plt.close(self.fig)
    #        self.fig = None

    def __getattr__(self, name):
        attr = getattr(self.fig, name)
        if hasattr(attr, '__call__'):
            def newfunc(*args, **kwargs):
                result = attr(*args, **kwargs)
                return result
            return newfunc
        else:
            return attr

    #----------------------------------------------------
    #   Fonctions de gestion de la figure
    #----------------------------------------------------
    def createFigure(self, use_pyplot=True, size=(10, 8), dpi=120, **kwargs):
        """
        Crée une figure aux dimensions voulues. Par défaut, crée
        une figure pour utilisation avec pyplot. Pour une figure
        sans pyplot, utiliser use_pyplot=False.
        INPUT: - size : tuple (largeur,hauteur) en pouces
               - dpi  : résolution (dots per inch)
        une figure comprend 1 ou des "axes" qui sont soit créés
        spécifiquement par addAxes, soit par addSubPlot.
        """
        if use_pyplot:
            self.fig = mpl_plt.figure(figsize=size, dpi=dpi, **kwargs)
        else:
            self.fig = mpl.figure.Figure(figsize=size, dpi=dpi, **kwargs)
        return self.fig

    def addAxes(self, ll_wh = (0, 0, 1, 1), **kwargs):
        ax = self.fig.add_axes(ll_wh, **kwargs)
        return IPAxes(ax)

    def addSubPlot(self, numRows=1, numCols=1, plotNum=1, **kwargs):
        """
        Ajoute un subplot à l'endroit spécifié (indices en base 1).
        """
        ax = self.fig.add_subplot(numRows, numCols, plotNum, aspect='equal', **kwargs)
        return IPAxes(ax)

    def adjustSubPlots(self, hspace=0.09, wspace=0.01, bottom=0.05, top=0.97, left=0.05, right=0.98):
        """
        Ajustement des cadres de la figure
        INPUT - hspace : espace horizontal
              - wspace : '' vertical
              - bottom : '' en bas
              - top    : '' en haut
              - left   : '' coté gauche
              - right  : '' côté droit
        """
        self.fig.subplots_adjust(hspace=hspace,
                                 wspace=wspace,
                                 bottom=bottom,
                                 top=top,
                                 left=left,
                                 right=right)

    def addColorBar(self, cs, shrink=0.75, format='%6.2f', **kwargs):
        """
        Ajoute la colorbar correspondant aux contour cs.
        INPUT: shrink : facteur échelle à appliquer à la colorbar
        """
        self.colorbar(cs, shrink=shrink, format=format, fraction=0.05, **kwargs)

    def clear(self):
        self.fig.clf()

    #----------------------------------------------------
    #   The underlying matplotlib figure
    #----------------------------------------------------
    def getFigure(self):
        return self.fig

    #----------------------------------------------------
    #   Fonctions de sauvegarde
    #----------------------------------------------------
    def saveFigure(self, filename, **kwargs):
        """
        Enregistre la figure dans le fichier filename
        INPUT: - filename : le chemin complet du fichier image
        """
        self.fig.savefig(filename, **kwargs)

    def getAsNumpyArray(self):
        # --- Draw the figure
        self.fig.canvas.draw()

        # --- Get the RGBA buffer from the figure
        w,h = self.fig.canvas.get_width_height()
        buf = np.fromstring(self.fig.canvas.tostring_argb(), dtype=np.uint8)
        buf.shape = (w, h, 4)

        # ---  canvas.tostring_argb give pixmap in ARGB mode. Roll the ALPHA channel to have it in RGBA mode
        buf = np.roll(buf, 3, axis=2)
        return buf

    def getAsPIL(self):
        buf = self.getAsNumpyArray()
        w, h, d = buf.shape
        img = PIL.Image.frombytes("RGBA", (w, h), buf.tostring())
        return img

    def showFigure(self):
        """
        Affiche le plot
        """
        mpl_plt.show()

#---------------------------------------------------------------
#---------------------------------------------------------------
class IPAxes:
    def __init__(self, axes):
        """
        Constructeur
        """
        self.wndw = (0, 0, 1, 1)
        self.bgnd = None
        self.equl = False
        self.unit = basic_units.m
        self.axes = axes

    #def __del__(self):
    #    if (self.axes):
    #        try:
    #            self.contourSets = [ ]
    #        except:
    #            pass
    #        self.axes = None

    def __getattr__(self, name):
        attr = getattr(self.axes, name)
        if hasattr(attr, '__call__'):
            def newfunc(*args, **kwargs):
                result = attr(*args, **kwargs)
                return result
            return newfunc
        else:
            return attr

    @staticmethod
    def genKwExtend(open_top = True, open_bottom = True):
        kw = {}
        if open_top:
            if open_bottom:
                kw['extend'] = 'both'
            else:
                kw['extend'] = 'max'
        else:
            if open_bottom:
                kw['extend'] = 'min'
        return kw

    @staticmethod
    def genKwLimits(vmin, vmax, nval, log):
        kw = {}
        if log:
            kw['levels'] = np.logspace(np.log10(vmin), np.log10(vmax), nval)
            kw['norm']   = mpl.colors.LogNorm()
        else:
            kw['levels'] = np.linspace(vmin, vmax, nval)
        return kw

    @staticmethod
    def genKwNorm(vmin, vmax):
        kw = {}
        kw['norm'] = mpl.colors.Normalize(vmin, vmax)
        return kw

    @staticmethod
    def genKwCmap(cname, cmin, cmax):
        #https://stackoverflow.com/questions/18926031/how-to-extract-a-subset-of-a-colormap-as-a-new-colormap-in-matplotlib
        cmap = mpl.cm.get_cmap(cname)
        if cmin > 0.0 or cmax < 1.0:
            cmap = mpl.colors.LinearSegmentedColormap.from_list(
                'trunc({n},{a:.2f},{b:.2f})'.format(n=cname, a=cmin, b=cmax),
                cmap(np.linspace(cmin, cmax, 100)))
        kw = {}
        kw['cmap'] = cmap
        return kw


        kw = {}
        kw['norm'] = mpl.colors.Normalize(vmin, vmax)
        return kw

    #----------------------------------------------------
    #   The underlying matplotlib axes
    #----------------------------------------------------
    def getAxes(self):
        return self.axes

    #----------------------------------------------------
    #   Gestion du subplot(Axes)
    #----------------------------------------------------
    def init(self):
        self.clear()

        w = self.wndw
        self.set_autoscale_on(False)
        self.update_datalim( ((w[0],w[1]), (w[2],w[3])) )
        self.set_xlim(w[0], w[2])
        self.set_ylim(w[1], w[3])
        if self.equl:
            self.set_aspect('equal', 'box', anchor='C')
        else:
            self.set_aspect('auto', 'box', anchor='C')
        #self.xaxis.set_units(self.units)
        #self.yaxis.set_units(self.units)

        if (self.bgnd is not None): self.drawImage(self.bgnd)

    def close(self, title = None, foreground = None):
        self.grid(True)
        if foreground: self.paste(foreground)
        if title: self.set_title(title)

    def setup(self,
              window = (0, 0, 1, 1),
              background = None,
              aspect_equal = False,
              units = IPUnits.meter):
        """
        Assigne les paramètres de subplot(Axes) courant.
        window :    Dimension du dessin en coord utilisateur
        background: Image d'arrière fond
        Il faut encore initialiser avec init() pour les rendre actifs
        """
        self.wndw = window
        self.bgnd = background
        self.equl = aspect_equal
        if (units == IPUnits.meter):
            self.units = basic_units.m
        elif (units == IPUnits.degree):
            self.units = basic_units.degrees

    def removeAxis(self):
        """
        Enlève les axes du sub-plot courant
        """
        self.axis('off')

    def draw(self, type, *args, **kwargs):
        cs = None
        if   (type == IPPlotType.contourLine):
            cs = self.drawContourLine(*args, **kwargs)
        elif (type == IPPlotType.contourFill):
            cs = self.drawContourFill(*args, **kwargs)
        elif (type == IPPlotType.vectors):
            cs = self.drawVectors(*args, **kwargs)
        elif (type == IPPlotType.mesh):
            cs = self.drawMesh(*args, **kwargs)
        elif (type == IPPlotType.meshSkin):
            cs = self.drawMeshSkin(*args, **kwargs)
        elif (type == IPPlotType.image):
            cs = self.drawImage(*args, **kwargs)
        return cs

    def addTitle(self, title='', **kwargs):
        """
        Ajoute un titre au sub-plot courant
        """
        self.set_title(title, **kwargs)

    def paste(self, ax_src):
        PathColl = mpl.collections.PathCollection
        ax_dst = self.axes
        #for d in ax_src.artists:     ax_dst.add_artist(d)
        for d in ax_src.collections:
            c = PathColl(d.get_paths(),
                         edgecolors=d.get_edgecolors(),
                         facecolors=d.get_facecolors(),
                         linewidths=d.get_linewidths())
            ax_dst.add_collection(c)
        ##for d in ax_src.containers:  ax_dst.add_container(d)
        #for d in ax_src.lines:       ax_dst.add_line(d)
        #for d in ax_src.patches:     ax_dst.add_patch(d)
        #for d in ax_src.tables:      ax_dst.add_table(d)

    #----------------------------------------------------
    #   Fonctions de dessins
    #----------------------------------------------------
    def drawEllipses(self, ellps, **kwargs):
        """
        Plot ellipses:
        [ (x,y,w,h,a), ... ]
        """
        if 'color' not in kwargs and 'cmap' not in kwargs: 
            kwargs['cmap'] = mpl_plt.cm.jet
        if 'alpha' not in kwargs: kwargs['alpha'] = 0.4
        ells = [ Ellipse(e[0:2], width=2*e[2], height=2*e[3], angle=math.degrees(e[4])) for e in ellps ]
        pc = mpl.collections.PatchCollection(ells, **kwargs)
        #pc = mpl.collections.PatchCollection(ells, array=T, **kwargs)
        CS = self.add_collection(pc)    # add the collection to the plot
        return CS

    def drawLine(self, points, data, color='g', linewidth=0.5):
        """
        Plot the line
        INPUT: - linewidth : largeur des lignes
        """
        s = [ s_ for s_,d_ in zip(points.asRunLength(), data) if not math.isnan(d_) ]
        d = [ d_ for d_ in data if not math.isnan(d_) ]
        CS = self.plot(s,
                       d,
                       color     = color,
                       linewidth = linewidth)
        return CS

    def drawOnePath(self, T, X, Y, **kwargs):
        """
        Plot a path in the form:
        [ (t,x,y), ... ]
        """
        # Path must be decomposed in segments, each segment can then
        # be attributed a value for coloring
        # inspired from http://matplotlib.org/examples/pylab_examples/multicolored_line.html
        if 'color' not in kwargs and 'cmap' not in kwargs: 
            kwargs['cmap'] = mpl_plt.cm.jet
        XY = np.column_stack((X,Y)).reshape(-1,1,2)
        XY = np.concatenate([XY[:-1],XY[1:]], axis=1)
        lc = mpl.collections.LineCollection(XY, array=T, **kwargs)
        CS = self.add_collection(lc)    # add the collection to the plot
        return CS

    def drawPaths(self, paths, **kwargs):
        """
        Plot a list of paths. Each path is in the form:
        [ (t,x,y[,...]), ... ]
        """
        # Path must be decomposed in segments, each segment can then
        # be attributed a value for coloring
        # inspired from http://matplotlib.org/examples/pylab_examples/multicolored_line.html
        if 'cmap' not in kwargs: kwargs['cmap'] = mpl_plt.cm.jet
        XY = []
        T  = []
        for path in paths:
            xyt = np.array(path)
            t  = xyt[:,0]
            xy = xyt[:,1:3].reshape(-1,1,2)
            segs = np.concatenate([xy[:-1],xy[1:]], axis=1)
            XY.extend(segs)
            T.extend (t)
        T = np.array(T)
        lc = mpl.collections.LineCollection(XY, array=T, **kwargs)
        CS = self.add_collection(lc)    # add the collection to the plot
        return CS

        # Version avec une couleur par path
        # Avec cette version, l'information sur le temps est perdue.
        # et datacursor ne peut plus retourner d'info intéressante.
    #def drawPaths(self, paths, **kwargs):
    #    """
    #    Plot a list of paths. Each path is in the form:
    #    [ (x,y,t), ... ]
    #    """
    #    if 'cmap' not in kwargs: kwargs['cmap'] = mpl_plt.cm.jet
    #    XY = []
    #    T  = []
    #    for path in paths:
    #        xyt = np.array(path)
    #        xy = xyt[:,0:2]
    #        XY.append(xy)
    #        T.append (xyt[0,2])
    #    T = np.array(T)
    #    lc = mpl.collections.LineCollection(XY, array=T, **kwargs)
    #    CS = self.add_collection(lc)    # add the collection to the plot
    #    return CS

    def drawMesh(self, mesh, **kwargs):
        """
        Draw the mesh
        """
        if 'color'     not in kwargs: kwargs['color']     = 'g'
        if 'linewidth' not in kwargs: kwargs['linewidth'] = 0.5

        CS = None
        try:
            ele0 = mesh.getElement(0)
            if isinstance(ele0, FEMesh.FEElementT3):
                kwargs['triangles'] = mesh.getT3XtrnConnectivities()
                X, Y = mesh.getCoordinates()
                CS = self.triplot(X, Y, **kwargs)
            elif isinstance(ele0, FEMesh.FEElementT6L):
                kwargs['triangles'] = mesh.getT3XtrnConnectivities()
                X, Y = mesh.getCoordinates()
                CS = self.triplot(X, Y, **kwargs)
            elif isinstance(ele0, FEMesh.FEElementQ4):
                CS = self.__drawElem1B1(mesh, close=True, **kwargs)
            else:
                CS = self.__drawElem1B1(mesh, close=False, **kwargs)
        except Exception as e:
            print(e)
            pass
        return CS

    def __drawElem1B1(self, mesh, close=False, **kwargs):
        """
        Draw a sub-mesh like a skin
        """
        if 'color'     not in kwargs: kwargs['color']     = 'g'
        if 'linewidth' not in kwargs: kwargs['linewidth'] = 0.5
        X, Y = mesh.getCoordinates()
        xs,ys = [], []
        for e in mesh.iterElements():
            connec = e.getConnectivities()
            for inod in connec:
                xs.append(X[inod])
                ys.append(Y[inod])
            if close:
                inod = connec[0]
                xs.append(X[inod])
                ys.append(Y[inod])
            xs.append(None)
            ys.append(None)
        CS = self.plot(xs, ys, **kwargs)
        return CS

    def drawNodesMesh(self, mesh, **kwargs):
        """
        Draw the nodes of a mesh
        """
        if 'color'  not in kwargs: kwargs['color']  = 'k'   # black
        if 'marker' not in kwargs: kwargs['marker'] = ''    # point
        kwargs['linestyle'] = 'None'

        X, Y = mesh.getCoordinates()
        CS = self.plot(X, Y, **kwargs)
        return CS

    def drawNodeNumbersMesh(self, mesh, **kwargs):
        """
        """
        if 'color'  not in kwargs: kwargs['color']  = 'k'  # black

        CSS = []
        X, Y = mesh.getCoordinates()
        for node in mesh.iterNodes():
            iExt = node.getGlobalIndex()
            iInt = node.getLocalIndex()
            CS = self.annotate('%i' % (iExt+1), (X[iInt], Y[iInt]), **kwargs)
            CSS.append(CS)
        return CSS

    def drawElemNumbersMesh(self, mesh, **kwargs):
        """
        """
        doProj = False
        try:
            doProj = mesh.hasProjection()
        except AttributeError:
            pass

        if 'color'  not in kwargs: kwargs['color']  = 'k'  # black
    
        CSS = []
        for elem in mesh.iterElements():
            xy = elem.getCenter()
            if doProj: xy = mesh.projectToProj(*xy)
            iExt = elem.getGlobalIndex()
            CS = self.annotate('%i' % (iExt+1), xy, **kwargs)
            CSS.append(CS)
        return CSS

    def drawContourLine(self, mesh, data, **kwargs):
        """
        Dessine les lignes de contour
        INPUT: - linewidths : largeur des lignes
               - fill_value : valeur de remplissage
        """
        if 'color' not in kwargs and 'cmap' not in kwargs: 
            kwargs['cmap'] = mpl_plt.cm.jet
        if isinstance(mesh, (list, tuple)):
            CS = self.contour(mesh[0], mesh[1], data, **kwargs)
        else:
            tri = mesh.getT3SplitConnectivities()
            #  https://github.com/matplotlib/matplotlib/issues/10167
            dOK = ~np.isfinite(data)                # Filter out nan
            tri_mask = np.any(dOK[tri], axis=1)     # Build mask
            X, Y = mesh.getCoordinates()
            T = mpl_tri.Triangulation(X, Y, triangles=tri, mask=tri_mask)
            CS = self.tricontour(T, data, **kwargs)
        return CS

    def drawContourFill(self, mesh, data, **kwargs):
        """
        Rempli les zones de contour avec des couleurs par défaut
        Les niveaux sont fixés par l'objet
        """
        if 'color' not in kwargs and 'cmap' not in kwargs: 
            kwargs['cmap'] = mpl_plt.cm.jet
        if isinstance(mesh, (list, tuple)):
            CS = self.contourf(mesh[0], mesh[1], data, **kwargs)
        else:
            tri = mesh.getT3SplitConnectivities()
            #  https://github.com/matplotlib/matplotlib/issues/10167
            dOK = ~np.isfinite(data)                # Filter out nan
            tri_mask = np.any(dOK[tri], axis=1)     # Build mask
            X, Y = mesh.getCoordinates()
            T = mpl_tri.Triangulation(X, Y, triangles=tri, mask=tri_mask)
            CS = self.tricontourf(T, data, **kwargs)
        return CS

    def drawVectors(self, mesh, data, **kwargs):
        if 'color' not in kwargs and 'cmap' not in kwargs: 
            kwargs['cmap'] = mpl_plt.cm.jet
        if 'units' not in kwargs: kwargs['units'] = 'xy'
        if 'scale' not in kwargs: kwargs['scale'] = 2.0
        if 'scale_units' not in kwargs: kwargs['scale_units'] = 'inches'
        if isinstance(mesh, (list, tuple)):
            X = mesh[0]
            Y = mesh[1]
            U = data[:,0]
            V = data[:,1]
        else:
            X, Y = mesh.getCoordinates()
            U = data[:,0]
            V = data[:,1]
        F = np.logical_not( np.logical_or(np.isnan(U), np.isnan(V)) )
        C = np.hypot(U, V)
        CS = self.quiver(X[F], Y[F], U[F], V[F], C[F], **kwargs)
        return CS

    def drawSurface3D(self, mesh, data, **kwargs):
        """
        Dessine les lignes de contour
        INPUT: - linewidths : largeur des lignes
               - fill_value : valeur de remplissage
        """
        if 'color' not in kwargs and 'cmap' not in kwargs: 
            kwargs['cmap'] = mpl_plt.cm.jet

        try:
            print('drawSurface3D:')
            ele0 = mesh.getElement(0)
            if isinstance(ele0, FEMesh.FEElementT3):
                kwargs['triangles'] = mesh.getT3SplitConnectivities()
                X, Y = mesh.getCoordinates()
                CS = self.plot_trisurf(X, Y, data, **kwargs)
                print(CS)
            elif isinstance(ele0, FEMesh.FEElementT6L):
                kwargs['triangles'] = mesh.getT3SplitConnectivities()
                X, Y = mesh.getCoordinates()
                CS = self.plot_trisurf(X, Y, data, **kwargs)
            else:
                raise CTException('Unsupported element type for drawSurface3D')
        except Exception as e:
            print(e)
            CS = None
        return CS

    def drawImage(self, img, window=None):
        """
        window is [left, bottom, rignt, up]
        """
        if   (type(img) == type(' ')):
            img_ = self.imread(img)
        elif isinstance(img, PIL.Image.Image):
            img_ = img
        else:
            w, h, _ = img.shape
            img_ = PIL.Image.frombytes("RGBA", (w, h), img.tostring( ))
        w = window if window else self.wndw
        xtnt = (w[0], w[2], w[1], w[3])
        CS = self.imshow(img_,
                         interpolation='bilinear',
                         aspect='equal',
                         extent=xtnt,
                         alpha=1.0)
        return CS

    ####----------------------------------------------------
    ####   Fonctions de sauvegarde
    ####----------------------------------------------------
    ###def saveAsSHPOneCS(self, CS, wSHP, wSPL):
    ###    """
    ###    Save the content of a contour set as a ShapeFile
    ###    """
    ###    from SHP import SHPWriter
    ###    Kind = enum.Enum('Kind', 'NIL Polygone Polyline Line2D')
    ###    poly, kind = [], Kind.NIL
    ###    try:
    ###        if not poly: poly, kind = MPLUtil.getPolygonsFromContourSet(CS), Kind.Polygone
    ###    except:
    ###        pass
    ###    try:
    ###        if not poly: poly, kind = MPLUtil.getPolylinesFromContourSet(CS), Kind.Polyline
    ###    except:
    ###        pass
    ###    try:
    ###        if not poly: poly, kind = MPLUtil.getLines2DFromContourSet(CS), Kind.Line2D
    ###    except:
    ###        pass
    ###    # print len(poly), kind
    ###    lvls, clrs = None, None
    ###    if kind in (Kind.Polygone, Kind.Polyline):
    ###        lvls = MPLUtil.getLevelsFromContourSet(CS)
    ###        clrs = MPLUtil.getColorsFromContourSet(CS)
    ###    if poly and kind is Kind.Polygone:
    ###        wSHP.writePolyCollection(poly, lvls, clrs)
    ###        wSPL.writeDocument(SHPWriter.isoType.Isovalue, lvls, clrs)
    ###    if poly and kind is Kind.Polyline:
    ###        wSHP.writeLineCollection(poly, lvls, clrs)
    ###        wSPL.writeDocument(SHPWriter.isoType.Isoline, lvls, clrs)
    ###    if poly and kind is Kind.Line2D:
    ###        lvls = [0] * len(poly)
    ###        clrs = [MPLUtil.rgba2kml([1.0,0.0,0.0,1.0])] * len(poly)
    ###        wSHP.writeLineCollection(poly, lvls, clrs)
    ###        wSPL.writeDocument(SHPWriter.isoType.Isoline, lvls, clrs)
    ###
    ###def saveAsSHP(self, meta, filename):
    ###    """
    ###    Save the content of the axes as a ShapeFile
    ###    """
    ###    from SHP import SHPWriter
    ###    wSHP = SHPWriter.SHPWriter(meta, filename)
    ###    wSPL = SHPWriter.SLDWriter(meta, filename+'.sld')
    ###    wSHP.writeHeader()
    ###    wSPL.writeHeader()
    ###    for CS in self.contourSets:
    ###        self.saveAsSHPOneCS(CS, wSHP, wSPL)
    ###    wSHP.writeFooter()
    ###    wSPL.writeFooter()
    ###    del wSHP
    ###    del wSPL
    ###
    ###def __getOneCSComponents(self, CS):
    ###    """
    ###    Save the content of one contour set
    ###    """
    ###    poly, kind = [], CSKind.NIL
    ###    lvls, clrs = None, None
    ###    if not poly:
    ###        try:
    ###            LOGGER.debug ('__getOneCSComponents: try Polygone')
    ###            poly, kind = MPLUtil.getPolygonsFromContourSet(CS), CSKind.Polygone
    ###            lvls = MPLUtil.getLevelsFromContourSet(CS)
    ###            clrs = MPLUtil.getColorsFromContourSet(CS)
    ###            LOGGER.debug ('__getOneCSComponents: Polygone OK')
    ###        except:
    ###            poly, kind = [], CSKind.NIL
    ###    if not poly: 
    ###        try:
    ###            LOGGER.debug ('__getOneCSComponents: try Polyline')
    ###            poly, kind = MPLUtil.getPolylinesFromContourSet(CS), CSKind.Polyline
    ###            lvls = MPLUtil.getLevelsFromContourSet(CS)
    ###            clrs = MPLUtil.getColorsFromContourSet(CS)
    ###            LOGGER.debug ('__getOneCSComponents: Polyline OK')
    ###        except:
    ###            poly, kind = [], CSKind.NIL
    ###    if not poly: 
    ###        try:
    ###            LOGGER.debug ('__getOneCSComponents: try Lines2D')
    ###            poly, kind = MPLUtil.getLines2DFromContourSet(CS), CSKind.LINE2D
    ###            lvls = [0.0] * len(poly)
    ###            try:
    ###                clrs = MPLUtil.getColorsFromContourSet(CS)
    ###                print clrs
    ###            except Exception as e:
    ###                import traceback
    ###                errMsg = '%s\n%s' % (str(e), traceback.format_exc())
    ###                print errMsg
    ###                clrs = [MPLUtil.rgba2kml([0.0,0.0,1.0,1.0])] * len(poly)
    ###            LOGGER.debug ('__getOneCSComponents: Lines2D OK')
    ###        except:
    ###            poly, kind = [], CSKind.NIL
    ###    if not poly: 
    ###        try:
    ###            LOGGER.debug ('__getOneCSComponents: try Lines')
    ###            poly, kind = MPLUtil.getLinesFromContourSet([CS]), CSKind.Line
    ###            lvls = [0.0] * len(poly)
    ###            #clrs = MPLUtil.getColorsFromContourSet([CS])
    ###            clrs = [MPLUtil.rgba2kml([0.8,1.0,0.1,1.0])] * len(poly)
    ###            LOGGER.debug ('__getOneCSComponents: Lines OK')
    ###        except Exception as e:
    ###            import traceback
    ###            errMsg = '%s\n%s' % (str(e), traceback.format_exc())
    ###            print errMsg
    ###            poly, kind = [], CSKind.NIL
    ###    return kind, poly, lvls, clrs
    ###
    ###def saveAs(self, writer, *args, **kwargs):
    ###    """
    ###    Add the content of the axes to the writer
    ###    """
    ###    title = kwargs.pop('title', '')
    ###    writer.writeHeader()
    ###    for CS in self.contourSets:
    ###        kind, poly, lvls, clrs = self.__getOneCSComponents(CS)
    ###        if poly:
    ###            if   kind is CSKind.Polygone:
    ###                kwargs['title'] = title if title else 'Isovalue'
    ###                writer.writePolyCollection(poly, lvls, clrs, *args, **kwargs)
    ###            elif kind is CSKind.Polyline:
    ###                kwargs['title'] = title if title else 'Isoline'
    ###                writer.writeLineCollection(poly, lvls, clrs, *args, **kwargs)
    ###            elif kind is CSKind.LINE2D:
    ###                kwargs['title'] = title if title else 'Geometry'
    ###                writer.writeLineCollection(poly, lvls, clrs, *args, **kwargs)
    ###            elif kind is CSKind.Line:
    ###                kwargs['title'] = title if title else 'Particle Path'
    ###                writer.writeLineCollection(poly, lvls, clrs, *args, **kwargs)
    ###    writer.writeFooter()
    ###
    ###def getLevels(self):
    ###    contourSets = self.contourSets
    ###    if len(contourSets) == 0: raise ValueError('No contours')
    ###    lvls = contourSets[0].levels
    ###    for CS in contourSets[1:]:
    ###        if (lvls != CS.levels): raise ValueError('Incoherent levels')
    ###    return lvls
    ###
    ###def getColors(self):
    ###    contourSets = self.contourSets
    ###    if len(contourSets) == 0: raise ValueError('No contours')
    ###    clrs = contourSets[0].colors
    ###    for CS in contourSets[1:]:
    ###        if (clrs != CS.colors): raise ValueError('Incoherent levels')
    ###    return clrs



class IPAnimation:
    @staticmethod
    def createFrames(fig, ax,
                     mesh,
                     data,
                     tini=0.0, tfin=-1.0, delt=1.0,
                     fileroot='ZYX_ipframe_',
                     **kwargs):
        """
        Crée les fichiers d'une animation à partir d'un fichier de données
        """
        titleroot = ax.get_title() if ax.title else None
        addclrbar = kwargs.pop("colorbar", True)
        shrink    = kwargs.pop("shrink", 0.7)
        format    = kwargs.pop("format", '%6.1e')
        cols      = kwargs.pop("cols", None)

        t = tini
        i = 1
        imax = 1 + int((tfin-tini)/delt)
        while t <= tfin:
            d = data.getDataAtTime(t, cols = cols)

            LOGGER.info ('Frame (%5i/%5i) at time %f (%s)' % (i, imax, t, IPUtil.getDatetimeFromEpoch(t)))
            LOGGER.debug('    data: (%f, %f)' % (min(d), max(d)))
            #if (levels is not None): LOGGER.debug('    lvls: (%f, %f)' % (levels[0], levels[-1]))

            if titleroot:
                title = '%s %s' % (titleroot, IPUtil.getDatetimeFromEpoch(t))
            else:
                title = None
            ax.init ()
            cs = ax.draw (IPPlotType.contourFill, mesh, d, **kwargs)
            ax.close(title=title)
            if addclrbar:
                fig.addColorBar(cs, shrink=shrink, format=format)
                addclrbar = False

            img = fig.getAsPIL()
            fname = '%s_%012i.png' % (fileroot, int(t*1000))
            img.save(fname)

            del (img)
            del (d)
            t = t + delt
            i = i + 1

    @staticmethod
    def createLines(fig, ax,
                    points,
                    data,
                    tini=0.0, tfin=-1.0, delt=1.0,
                    color='b',
                    fileroot='ZYX_ipframe_',
                    **kwargs):
        """
        Crée les fichiers d'une animation à partir d'un fichier de données
        """
        titleroot = ax.get_title() if ax.title else None
        cols      = kwargs.pop("cols", None)

        t = tini
        i = 1
        imax = 1 + int((tfin-tini)/delt)
        while t <= tfin:
            t_, d = data.getPointsAtTime(t, points)

            LOGGER.info ('Frame (%5i/%5i) at time %f (%s)' % (i, imax, t, IPUtil.getDatetimeFromEpoch(t)))
            LOGGER.debug('    data: (%f, %f)' % (min(d), max(d)))

            if titleroot:
                title = '%s %s' % (titleroot, IPUtil.getDatetimeFromEpoch(t))
            else:
                title = None
            ax.init    ()
            ax.drawLine(points, d, color=color)
            ax.close   (title=title)

            img = fig.getAsPIL()
            fname = '%s_%012i.png' % (fileroot, int(t*1000))
            img.save(fname)
            t = t + delt
            i = i + 1

    @staticmethod
    def createKMLSequence(fig, ax,
                          mesh,
                          data,
                          levels=[],
                          tini=0.0, tfin=-1.0, delt=1.0,
                          meta = None,
                          fileroot='ZYX_kmlseq_'):
        """
        Crée les fichiers d'une séquence KML à partir d'un fichier de données
        """
        import KMLWriter

        t = tini
        i = 1
        imax = 1 + int((tfin-tini)/delt)
        while t <= tfin:
            d = data.getDataAtTime(t)

            LOGGER.info ('Item (%5i/%5i) at time %f (%s)' % (i, imax, t, IPUtil.getDatetimeFromEpoch(t)))
            LOGGER.debug('    data: (%f, %f)' % (min(d), max(d)))
            if (levels is not None): LOGGER.debug('    lvls: (%f, %f)' % (levels[0], levels[-1]))

            meta['time'] = IPUtil.getDatetimeFromEpoch(t)

            if fig.title:
                title = '%s %s' % (fig.title, IPUtil.getDatetimeFromEpoch(t))
            else:
                title = None
            meta['time'] = IPUtil.getDatetimeFromEpoch(t)
            ax.init ()
            ax.draw (IPPlotType.contourFill, mesh, d, levels=levels)
            ax.close(title=title)
            fname = '%s_%012i.kml' % (fileroot, int(t*1000))
            writer = KMLWriter.KMLWriter(meta, fname)
            ax.saveAs(writer)
            t = t + delt
            i = i + 1

    @staticmethod
    def createKMZSequence(fig,
                          mesh,
                          data,
                          levels=[],
                          tini=0.0, tfin=-1.0, delt=1.0,
                          meta = None,
                          fileroot='ZYX_kmlseq_',
                          filename = "Simulation2.kml"):
        """
        Crée les fichiers d'une séquence KML à partir d'un fichier de données et
        les combine dans un fichier KMZ
        """
        import KMLWriter
        from tempfile import mkdtemp
        from shutil import rmtree

        fKMZ = []

        subdir = mkdtemp(dir = os.getcwd())
        fileroot = os.path.join(os.path.basename(subdir), fileroot)
        fKMZ.append(subdir)

        writer = KMLWriter.KMLWriter(meta, filename)
        writer.writeHeader()
        writer.writeDocumentHead()

        fKMZ.append(filename)

        t = tini
        i = 1
        imax = 1 + int((tfin-tini)/delt)
        tmax = t+1*delt
        while t <= tmax: #tfin:
            d = data.getDataAtTime(t)

            LOGGER.info ('Item (%5i/%5i) at time %f (%s)' % (i, imax, t, IPUtil.getDatetimeFromEpoch(t)))
            LOGGER.debug('    data: (%f, %f)' % (min(d), max(d)))
            if (levels is not None): LOGGER.debug('    lvls: (%f, %f)' % (levels[0], levels[-1]))

            if fig.title:
                title = '%s %s' % (fig.title, IPUtil.getDatetimeFromEpoch(t))
            else:
                title = None
            meta['time_start'] = IPUtil.getDatetimeFromEpoch(t)
            meta['time_end'] = IPUtil.getDatetimeFromEpoch(t + delt-1)
            fig.draw(IPPlotType.contourFill, mesh, d, levels=levels, title=title)

            fname = '%s_%012i.kml' % (fileroot, int(t*1000))

            writer1 = KMLWriter.KMLWriter(meta, fname)
            writer1.writeHeader()
            fig.saveAs(writer1)
            writer1.writeFooter()

            #writer.writeTimeSpan()
            writer.writeNetworkLink(fname, os.path.basename(fname), tspan = True)
            t = t + delt
            i = i + 1

        writer.writeFooter(closeAll = True)
        del writer
        compressKML(fKMZ, filename.replace('.kml', '.kmz'))
        ##rmtree(subdir, ignore_errors=True)

    @staticmethod
    def createAnimationFromFrames(fileroot='ZYX_ipframe_', outfile='output.avi'):
        def do_call_or_raise(command):
            try:
                ret = subprocess.check_call(' '.join(command))
            except subprocess.CalledProcessError as e:
                raise CTException("Command '%s' returned non-zero exit status %i" % (command, e.returncode))

        cmd = ('mencoder',
                'mf://%s*.png' % fileroot,
                '-nosound',
                #'-mf',
                #'type=png:w=800:h=600:fps=25'
               )
        out = ('-o', outfile)
        # ---  codecs
        # https://bbs.archlinux.org/viewtopic.php?id=100548
        x264 = ('-ovc', 'x264', '-x264encopts', 'preset=slow:tune=film')
        # http://matplotlib.sourceforge.net/examples/animation/movie_demo.html#animation-movie-demo
        lavc = ('-ovc', 'lavc', '-lavcopts', 'vcodec=mpeg4')

        command = cmd + x264 + out
        print("\n\nabout to execute:\n%s\n\n" % ' '.join(command))
        ret = do_call_or_raise(command)


#----------------------------------------------------
#   Fonctions d'automatisation
#----------------------------------------------------
def genDefaultScreen(mesh, size):
    bbox = mesh.getBbox(stretchfactor=0.02)     # in base coord
    screen = GDLBasemap.IPGeoTransform()
    screen.setWindow(bbox)
    screen.setViewportFromSize(size=size, dpi=80)
    screen.resizeWindowToViewport()
    return screen

def genMask(mesh, data, levels=[], colors='w',
            size=(8,8), screen=None):
    """
        Return the mask as an Axes instance
    """
    if (not screen):
        screen = genDefaultScreen(mesh, size)

    LOGGER.info('Generate mask: data  (%15g, %15g)' % (min(data), max(data)))
    LOGGER.info('               levels(%15g, %15g)' % (levels[0], levels[-1]))
    LOGGER.info('               window(%15g, %15g) (%15g, %15g)' % (screen.getWindow()))

    f = IPFigure()
    f.createFigure(size=size)
    ax = f.addSubPlot(1, 1, 1)
    ax.setup(window=screen.getWindow())
    ax.init()
    ax.removeAxis()
    ax.drawContourFill(mesh, data, levels=levels, colors=colors)
    ax.close()
    return ax

def makeFigureMesh(mesh,
                   size = (10,8),
                   screen = None,
                   background = None,
                   foreground = None,
                   title = ''):
    """
    Ajoute un maillage
    """
    if (screen is None):
        screen = genDefaultScreen(mesh, size)

    LOGGER.info ('make mesh: window(%15g, %15g) (%15g, %15g)' % (screen.getWindow()))

    f = IPFigure()
    f.createFigure(size=size)
    ax = f.addSubPlot(1, 1, 1, facecolor='w')
    f.adjustSubPlots()
    ax.setup(window=screen.getWindow(), background=background)
    ax.init()
    ax.removeAxis()
    ax.drawMesh(mesh)
    ax.close(title='%s' % (title), foreground=foreground)
    return f, ax

def makeFigureContourLine(mesh, data,
                          size = (10,8),
                          screen = None,
                          background = None,
                          foreground = None,
                          levels     = None,
                          colorbar = True,
                          title = ''):
    """
    Crée une figure à partir de données
    """
    if (screen is None):
        screen = genDefaultScreen(mesh, size)
    if (levels is None):
        levels = (min(data), max(data), 21, False)

    LOGGER.info ('make isoLin: data  (%15g, %15g)' % (min(data), max(data)))
    LOGGER.debug('             levels(%15g, %15g)' % (levels[0], levels[-1]))
    LOGGER.debug('             window(%15g, %15g) (%15g, %15g)' % (screen.getWindow()))

    kwargs = {}
    kwargs.update( IPAxes.genKwLimits(levels[0], levels[1], levels[2], levels[3]) )

    f = IPFigure()
    f.createFigure(size=size)
    ax = f.addSubPlot(1, 1, 1, facecolor='w')
    f.adjustSubPlots()
    ax.setup(window=screen.getWindow(), background=background)
    ax.init()
    ax.removeAxis()
    cs = ax.drawContourLine(mesh, data, **kwargs)
    ax.close(title='%s' % (title), foreground=foreground)
    if colorbar: f.addColorBar(cs)
    return f, ax

def makeFigureContourFill(mesh, data,
                          size = (10,8),
                          screen = None,
                          background = None,
                          foreground = None,
                          levels     = None,
                          open_ends  = (True, True),
                          colorbar   = True,
                          title      = ''):
    """
    Crée une figure à partir de données
    """
    if (screen is None):
        screen = genDefaultScreen(mesh, size)
    if (levels is None):
        levels = (min(data), max(data), 21, False)

    LOGGER.info ('make isoSrf: data  (%15g, %15g)' % (min(data), max(data)))
    LOGGER.debug('             levels(%15g, %15g)' % (levels[0], levels[-1]))
    LOGGER.debug('             window(%15g, %15g) (%15g, %15g)' % (screen.getWindow()))

    kwargs = {}
    kwargs.update( IPAxes.genKwExtend(open_ends[0], open_ends[1]) )
    kwargs.update( IPAxes.genKwLimits(levels[0], levels[1], levels[2], levels[3]) )

    f = IPFigure()
    f.createFigure(size=size)
    ax = f.addSubPlot(1, 1, 1, facecolor='w')
    f.adjustSubPlots()
    ax.setup(window=screen.getWindow(), background=background)
    ax.init()
    ax.removeAxis()
    cs = ax.drawContourFill(mesh, data, **kwargs)
    ax.close(title='%s' % (title), foreground=foreground)
    if colorbar: f.addColorBar(cs)
    return f, ax

def makeFigureQuiver(mesh, data,
                     size = (10,8),
                     screen = None,
                     background = None,
                     foreground = None,
                     levels = None,
                     colors = None,
                     colorbar = True,
                     title = ''):
    """
    Crée une figure à partir de données
    """
    if (screen is None):
        screen = genDefaultScreen(mesh, size)
    if (levels is None):
        dnrm = np.apply_along_axis(np.linalg.norm, 1, data)
        levels = np.linspace(min(dnrm), max(dnrm), 21)

    LOGGER.info ('make quiver: data  ')
    LOGGER.debug('             levels(%15g, %15g)' % (levels[0], levels[-1]))
    LOGGER.debug('             window(%15g, %15g) (%15g, %15g)' % (screen.getWindow()))

    kwargs = {}
    kwargs.update( IPAxes.genKwLimits(levels[0], levels[1], levels[2], levels[3]) )

    f = IPFigure()
    f.createFigure(size=size)
    ax = f.addSubPlot(1, 1, 1, facecolor='w')
    f.adjustSubPlots()
    ax.setup(window=screen.getWindow(), background=background)
    ax.init()
    ax.removeAxis()
    cs = ax.drawVectors(mesh, data, **kwargs)
    ax.close(title='%s' % (title), foreground=foreground)
    if colorbar: f.addColorBar(cs)
    return f, ax

if __name__ == '__main__':
    import os
    import sys
    selfDir = os.path.dirname( os.path.abspath(__file__) )
    supPath = os.path.normpath(os.path.join(selfDir, '..'))
    if os.path.isdir(supPath) and supPath not in sys.path: sys.path.append(supPath)

    def dataCursorFormatter(**kwargs):
        print(kwargs)
        t = 't={t:,.3f}'.format(t=kwargs['c'])
        x = 'x={x:,.3f}'.format(x=kwargs['x'])
        y = 'y={y:,.3f}'.format(y=kwargs['y'])
        return '\n'.join((t, x, y))

    #import mpldatacursor
    f = IPFigure()
    f.createFigure()
    ax = f.addSubPlot(1, 1, 1, facecolor='w')
    f.adjustSubPlots()

    #pth0 = [ (0.0,0.0,0.0), (0.1,0.1,0.1), (0.2,0.3,0.3) ]
    #pth1 = [ (0.5,0.5,0.5), (0.6,0.6,0.6), (0.7,0.8,0.8) ]
    #cs = ax.drawOnePath(pth0, label='xxx_0')
    #mpldatacursor.datacursor(cs, hover=True, formatter=dataCursorFormatter, xytext=(15,15))
    #cs = ax.drawOnePath(pth1, label='xxx_1')
    #mpldatacursor.datacursor(cs, hover=True, formatter=dataCursorFormatter, xytext=(15,15))

    #ell = [ [0,0, 1, 1, 0], [1, 1, 1, 0.5, 0.5] ]
    #cs = ax.drawEllipses(ell)

    
    from KML.KMLWriter   import KMLWriter
    from KML.KMLMeta     import KMLMetaData
    from CTCommon.FEMesh import FEMesh
    d = 'E:/Projets_simulation/LacErie/1x/'
    mesh = FEMesh(files=[d+'erie_t6l.cor', d+'erie_t6l.ele'], withLocalizer=True)
    skin = mesh.genSkin()
    ax.drawSubMesh(skin)
    
    meta = KMLMetaData()
    wrtr = KMLWriter(meta, fname='a.kml')
    ax.saveAs(wrtr)

    #f.showFigure()
