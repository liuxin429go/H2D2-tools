# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) INRS 2009-2018
# --- Institut National de la Recherche Scientifique (INRS)
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import sys

import wx
import wx.adv as wx_adv
import wx.lib.wordwrap as wx_wordwrap
import wx.richtext     as wx_rt

from CTCommon import CTWindow
from CTCommon import CTUtil
from CTCommon import FloatSlider as FS
import CCPlot

class DummyMenuItem:
    def Check(self, b): pass
    def GetId(self): return wx.ID_CANCEL


class DropTarget(wx.FileDropTarget):
    def __init__(self, parent):
        super(DropTarget, self).__init__()
        self.parent = parent

    def OnDropFiles(self, x, y, filenames):
        return self.parent.OnDropFiles(x, y, filenames)


class CCFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: CTFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        super(CCFrame, self).__init__(*args, **kwds)

        self.dropTarget = DropTarget(self)
        self.SetDropTarget(self.dropTarget)

        self.split_main = wx.SplitterWindow(self,            -1, style=wx.SP_3D|wx.SP_BORDER)
        self.split_data = wx.SplitterWindow(self.split_main, -1, style=wx.SP_3D|wx.SP_BORDER)

        # Widgets
        style_base = wx.BORDER_STATIC
        self.tracer_plot = CTWindow.CTWindow(proxied_class    = CCPlot.Plot,
                                             do_update        = False,
                                             cb_on_data_change= self.on_tracer_change,
                                             cb_on_data_pick  = self.on_tracer_pick,
                                             cb_on_mouse_move = self.on_mouse_move,
                                             parent = self.split_main)
        self.tracer_data  = wx.Panel(self.split_data, -1, style = style_base)
        self.tracer_data1 = wx_rt.RichTextCtrl(self.tracer_data, -1, style = style_base | wx.TE_MULTILINE | wx.ST_NO_AUTORESIZE)
        self.tracer_data2 = wx_rt.RichTextCtrl(self.tracer_data, -1, style = style_base | wx.TE_MULTILINE | wx.ST_NO_AUTORESIZE)
        self.tracer_stat  = wx.Panel(self.split_data, -1, style = style_base)
        self.tracer_stat1 = wx_rt.RichTextCtrl(self.tracer_stat, -1, style = style_base | wx.TE_MULTILINE | wx.ST_NO_AUTORESIZE)
        self.tracer_stat2 = wx_rt.RichTextCtrl(self.tracer_stat, -1, style = style_base | wx.TE_MULTILINE | wx.ST_NO_AUTORESIZE)
        self.logo   = wx.Panel(self, -1, style = style_base)
        self.slider = FS.FloatSlider(self, -1, style = style_base | wx.SL_HORIZONTAL | wx.SL_LABELS, resolution=1, formater=FS.IntegerFormater)

        def addItem(mnu, name, help, style):
            item = wx.MenuItem(mnu, wx.ID_ANY, name, help, style)
            mnu.Append(item)
            return item
        # Menu Bar
        self.menubar = wx.MenuBar()
        self.mnu_file = wx.Menu()
        self.mnu_file_open = addItem(self.mnu_file, "Open...", "Open a file", wx.ITEM_NORMAL)
        self.mnu_file_quit = addItem(self.mnu_file, "Quit", "Quite application", wx.ITEM_NORMAL)
        self.menubar.Append(self.mnu_file, "File")

        self.mnu_view = wx.Menu()
        self.mnu_view_rdrw   = addItem(self.mnu_view, "Refresh\tF5", "Redraw the display", wx.ITEM_NORMAL)
#        self.mnu_view_pause  = addItem(self.mnu_view, "Pause", "Don't update", wx.ITEM_CHECK)
        self.mnu_view_hlght  = addItem(self.mnu_view, "Highlight", "Highlight the unconverged time steps", wx.ITEM_CHECK)
        self.mnu_view_x = wx.Menu()
        self.mnu_view_vptx    = addItem(self.mnu_view_x, "x-viewport...", "set the viewport in x", wx.ITEM_NORMAL)
        self.mnu_view_rngx    = addItem(self.mnu_view_x, "x-range...", "set the range in x", wx.ITEM_NORMAL)
        self.mnu_view_ctrx    = addItem(self.mnu_view_x, "x-center...", "set the center in x", wx.ITEM_NORMAL)
#        self.mnu_view_x.AppendSeparator()
#        self.mnu_view_x_wtime = addItem(self.mnu_view_x, "wtime", "set wall time as x component", wx.ITEM_CHECK)
#        self.mnu_view_x_stime = addItem(self.mnu_view_x, "stime", "set simulation time as x component", wx.ITEM_CHECK)
#        self.mnu_view_x_iter  = addItem(self.mnu_view_x, "iter", "set iteration as x component", wx.ITEM_CHECK)
        self.mnu_view.AppendSubMenu(self.mnu_view_x, "x-axis")
        self.mnu_view_y = wx.Menu()
        self.mnu_view_vpty    = addItem(self.mnu_view_y, "y-viewport...", "set the viewport in y", wx.ITEM_NORMAL)
        self.mnu_view_rngy    = addItem(self.mnu_view_y, "y-range...", "set the range in y", wx.ITEM_NORMAL)
        self.mnu_view_ctry    = addItem(self.mnu_view_y, "y-center...", "set the center in y", wx.ITEM_NORMAL)
        self.mnu_view.AppendSubMenu(self.mnu_view_y, "y-axis")
        self.menubar.Append (self.mnu_view, "View")

        self.mnu_help = wx.Menu()
        self.mnu_help_about = self.mnu_help.Append(wx.ID_ABOUT)
        self.menubar.Append(self.mnu_help, "Help")
        self.SetMenuBar(self.menubar)

        # Status Bar
        self.statusbar = self.CreateStatusBar(2)

        # Tool Bar
        self.toolbar = self.tracer_plot.get_toolbar()
        self.toolbar.Reparent(self)
        self.SetToolBar(self.toolbar)

        data_w = 150
        sldr_h = 35
        self.__set_properties(data_w, sldr_h)
        self.__do_layout(data_w, sldr_h)

        self.Bind(wx.EVT_MENU, self.on_mnu_file_open,    self.mnu_file_open)
        self.Bind(wx.EVT_MENU, self.on_mnu_file_quit,    self.mnu_file_quit)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rdrw,    self.mnu_view_rdrw)
#        self.Bind(wx.EVT_MENU, self.on_mnu_view_pause,   self.mnu_view_pause)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_hlght,   self.mnu_view_hlght)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_ctrx,    self.mnu_view_ctrx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rngx,    self.mnu_view_rngx)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_vptx,    self.mnu_view_vptx)
#        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_wtime, self.mnu_view_x_wtime)
#        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_stime, self.mnu_view_x_stime)
#        self.Bind(wx.EVT_MENU, self.on_mnu_view_x_iter,  self.mnu_view_x_iter)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_ctry,    self.mnu_view_ctry)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_rngy,    self.mnu_view_rngy)
        self.Bind(wx.EVT_MENU, self.on_mnu_view_vpty,    self.mnu_view_vpty)
        self.Bind(wx.EVT_MENU, self.on_mnu_help_about,   self.mnu_help_about)
        self.slider.Bind(wx.EVT_COMMAND_SCROLL_ENDSCROLL, self.on_scroll_change)

        self.dirname = ''
        self.fpath   = []

        # ---  Process cmd line files
        if len(sys.argv) > 1: self.OnDropFiles(0, 0, sys.argv[1:])

    def __set_properties(self, data_w, sldr_h):
        self.SetTitle("H2D2 - Convergence Compare")
        self.SetSize((800, 500))

        statusbar_fields = ['', 'Position']
        for i in range(len(statusbar_fields)):
            self.statusbar.SetStatusText(statusbar_fields[i], i)
        self.toolbar.Realize()

        self.split_main.SetSashGravity(1.0)
        self.split_data.SetSashGravity(0.5)
        self.split_main.SetMinimumPaneSize(20)
        self.split_data.SetMinimumPaneSize(20)

        self.tracer_data1.SetMinSize ((data_w, 15))
        self.tracer_data1.SetEditable(False)
        self.tracer_data2.SetMinSize ((data_w, 15))
        self.tracer_data2.SetEditable(False)
        self.tracer_stat1.SetMinSize ((data_w, 15))
        self.tracer_stat1.SetEditable(False)
        self.tracer_stat2.SetMinSize ((data_w, 15))
        self.tracer_stat2.SetEditable(False)

        self.logo.SetMinSize((data_w, sldr_h))

        self.slider.SetMinSize((100, sldr_h))
        self.slider.SetRange(0, 400)
        self.slider.SetValue(0)
        self.slider.SetPageSize(100)
        self.slider.Enable(False)

        self.mnu_view_hlght.Check(False)
        self.mnu_view_hlght.Enable(False)
#        self.mnu_view_x_wtime.Check(True)
#        self.mnu_view_x_stime.Check(False)
#        self.mnu_view_x_iter.Check(False)

    def __do_layout(self, data_w, sldr_h):
        sizer_data  = wx.BoxSizer(wx.HORIZONTAL)
        sizer_stat  = wx.BoxSizer(wx.HORIZONTAL)
        sizer_data.Add (self.tracer_data1, 1, wx.EXPAND, 0)
        sizer_data.Add (self.tracer_data2, 1, wx.EXPAND, 0)
        sizer_stat.Add (self.tracer_stat1, 1, wx.EXPAND, 0)
        sizer_stat.Add (self.tracer_stat2, 1, wx.EXPAND, 0)
        self.tracer_data.SetSizer(sizer_data)
        self.tracer_stat.SetSizer(sizer_stat)

        sizer_frame = wx.BoxSizer(wx.VERTICAL)
        sizer_bottm = wx.BoxSizer(wx.HORIZONTAL)
        sizer_frame.Add(self.split_main, 1, wx.EXPAND, 0)
        sizer_frame.Add(sizer_bottm,     0, wx.EXPAND, 0)
        sizer_bottm.Add(self.slider, 1, wx.EXPAND, 0)
        sizer_bottm.Add(self.logo,   0, wx.EXPAND, 0)

        self.split_main.SplitVertically  (self.tracer_plot, self.split_data, -data_w)
        self.split_data.SplitHorizontally(self.tracer_data, self.tracer_stat)

        self.SetSizer(sizer_frame)
        self.Layout()

    def OnDropFiles(self, x, y, fpath):
        if len(fpath) == 2:
            self.dirname = os.path.dirname(fpath[0])
            self.fpath   = fpath

            self.tracer_plot.load_data_from_file(self.fpath)

            file0 = os.path.basename(self.fpath[0])
            file1 = os.path.basename(self.fpath[1])
            self.SetTitle(' / '.join( (file0, file1) ))
            self.slider.Enable(True)
            return True
        else:
            msg = wx.MessageDialog(self, 'Drop exactly two files', 'Error', wx.OK | wx.ICON_ERROR)
            if (msg.ShowModal() == wx.OK):
                msg.Close(True)
            return False

    def on_mnu_file_open(self, event):
        dlg = wx.FileDialog(self, "Open", self.dirname, "", "*.*", wx.FD_OPEN | wx.FD_MULTIPLE)
        if dlg.ShowModal() == wx.ID_OK:
            filenames = dlg.GetFilenames()
            dirname   = dlg.GetDirectory()
            if (len(filenames) == 2):
                self.dirname = dirname
                self.fpath   = [ os.path.join(dirname, f) for f in filenames ]

                self.tracer_plot.load_data_from_file(self.fpath)

                file0 = os.path.basename(self.fpath[0])
                file1 = os.path.basename(self.fpath[1])
                self.SetTitle(' / '.join( (file0, file1) ))
                self.slider.Enable(True)
            else:
                dlg = wx.MessageDialog(self, 'Exactly 2 files are required', 'Error', wx.OK | wx.ICON_ERROR)
                if (dlg.ShowModal() == wx.OK):
                    self.Close(True)
        dlg.Destroy()

    def on_mnu_file_quit(self, event):
        dlg = wx.MessageDialog(self, 'Are you sure? \n', 'Quit', wx.YES_NO)
        if (dlg.ShowModal() == wx.ID_YES):
            self.Close(True)

    def on_mnu_view_rdrw(self, event):
        self.tracer_plot.draw_plot()

    def on_mnu_view_pause(self, event):
        self.tracer_plot.pause( self.mnu_view_pause.IsChecked() )

    def on_mnu_view_ctrx(self, event):
        ctr = self.tracer_plot.get_view_ctrx()
        str = '%f' % ctr
        dlg = wx.TextEntryDialog(self, 'Enter the data central value in X', 'Data central value in X', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_ctrx(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_ctry(self, event):
        ctr = self.tracer_plot.get_view_ctry()
        str = '%f' % ctr
        dlg = wx.TextEntryDialog(self, 'Enter the data central value in Y', 'Data central value in Y', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_ctry(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_rngx(self, event):
        rng = self.tracer_plot.get_view_rngx()
        str = '%f' % rng
        dlg = wx.TextEntryDialog(self, 'Enter the displayed data range in X', 'Data range in X', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_rngx(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_rngy(self, event):
        rng = self.tracer_plot.get_view_rngy()
        str = '%f' % rng
        dlg = wx.TextEntryDialog(self, 'Enter the displayed data range in Y', 'Data range in Y', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                v = dlg.GetValue()
                v = eval(v)
                self.tracer_plot.set_view_rngy(float(v))
            except:
                msg = wx.MessageDialog(self, 'Floating point value expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_vptx(self, event):
        x1, x2 = self.tracer_plot.get_view_vptx()
        str = '%f %f' % (x1, x2)
        dlg = wx.TextEntryDialog(self, 'Enter the x-axis viewport', 'x-axis viewport', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                x1, x2 = dlg.GetValue().split()
                x1 = eval(x1)
                x2 = eval(x2)
                self.tracer_plot.set_view_vptx(float(x1), float(x2))
            except:
                msg = wx.MessageDialog(self, 'Two, blanck separated, floating point values expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_vpty(self, event):
        y1, y2 = self.tracer_plot.get_view_vpty()
        str = '%f %f' % (y1, y2)
        dlg = wx.TextEntryDialog(self, 'Enter the y-axis viewport', 'y-axis viewport', str)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                y1, y2 = dlg.GetValue().split()
                y1 = eval(y1)
                y2 = eval(y2)
                self.tracer_plot.set_view_vpty(float(y1), float(y2))
            except:
                msg = wx.MessageDialog(self, 'Two, blanck separated, floating point values expected', 'Error', wx.OK | wx.ICON_ERROR)
                msg.ShowModal()
                msg.Destroy()
        dlg.Destroy()

    def on_mnu_view_hlght(self, event):
        pass

    def on_mnu_view_x_wtime(self, event):
        self.mnu_view_x_wtime.Check(True)
        self.mnu_view_x_stime.Check(False)
        self.mnu_view_x_iter.Check(False)
        self.tracer_plot.set_sourcex('wtime')

    def on_mnu_view_x_stime(self, event):
        self.mnu_view_x_wtime.Check(False)
        self.mnu_view_x_stime.Check(True)
        self.mnu_view_x_iter.Check(False)
        self.tracer_plot.set_sourcex('stime')

    def on_mnu_view_x_iter(self, event):
        self.mnu_view_x_wtime.Check(False)
        self.mnu_view_x_stime.Check(False)
        self.mnu_view_x_iter.Check(True)
        self.tracer_plot.set_sourcex('index')

    def on_mnu_help_about(self, event):
        info = wx_adv.AboutDialogInfo()
        info.Name = "H2D2 Convergence Compare"
        info.Version = "18.04rc1"
        info.Copyright = "(C) 2009-2018 INRS"
        info.Description = wx_wordwrap.wordwrap(
            "\nH2D2 Convergence Compare plots in parallel the convergence history of two H2D2 runs."
            "The program allows for simple graphic interaction such as pan and zoom"
            "as well as data probing.",
            350, wx.ClientDC(self))
        info.WebSite = ("http://www.gre-ehn.ete.inrs.ca/H2D2", "H2D2 home page")
        info.Developers = [ "Yves Secretan" ]
#        info.License = wx_wordwrap.wordwrap(licenseText, 500, wx.ClientDC(self))

        wx_adv.AboutBox(info)

    def on_scroll_change(self, event):
        x = event.GetPosition()
        self.tracer_plot.set_view_ctrx(x)

    def on_tracer_change(self):
        x1, x2 = self.tracer_plot.get_data_spnx()
        xc     = self.tracer_plot.get_view_ctrx()
        v1, v2 = self.tracer_plot.get_view_vptx()
        txt    = self.tracer_plot.get_stats()

        self.slider.SetRange(x1, x2)
        self.slider.SetValue(xc)
        self.slider.SetPageSize((v2-v1) / 4)

        lines = txt[0].split('\n')
        self.tracer_stat1.Clear()
        self.tracer_stat1.BeginTextColour( CTUtil.get_clr(0) )
        self.tracer_stat1.WriteText(lines[0])
        self.tracer_stat1.Newline()
        self.tracer_stat1.BeginTextColour( (0,0,0) )
        for l in lines[1:-1]:
            self.tracer_stat1.WriteText(l)
            self.tracer_stat1.Newline()
        self.tracer_stat1.WriteText(lines[-1])

        lines = txt[1].split('\n')
        self.tracer_stat2.Clear()
        self.tracer_stat2.BeginTextColour( CTUtil.get_clr(1) )
        self.tracer_stat2.WriteText(lines[0])
        self.tracer_stat2.Newline()
        self.tracer_stat2.BeginTextColour( (0,0,0) )
        for l in lines[1:-1]:
            self.tracer_stat2.WriteText(l)
            self.tracer_stat2.Newline()
        self.tracer_stat2.WriteText(lines[-1])

    def on_tracer_pick(self, txt = ('', '')):
        lines = txt[0].split('\n')
        self.tracer_data1.Clear()
        self.tracer_data1.BeginTextColour( CTUtil.get_clr(0) )
        self.tracer_data1.WriteText(lines[0])
        self.tracer_data1.Newline()
        self.tracer_data1.BeginTextColour( (0,0,0) )
        for l in lines[1:-1]:
            self.tracer_data1.WriteText(l)
            self.tracer_data1.Newline()
        self.tracer_data1.WriteText(lines[-1])

        lines = txt[1].split('\n')
        self.tracer_data2.Clear()
        self.tracer_data2.BeginTextColour( CTUtil.get_clr(1) )
        self.tracer_data2.WriteText(lines[0])
        self.tracer_data2.Newline()
        self.tracer_data2.BeginTextColour( (0,0,0) )
        for l in lines[1:-1]:
            self.tracer_data2.WriteText(l)
            self.tracer_data2.Newline()
        self.tracer_data2.WriteText(lines[-1])

    def on_mouse_move(self, s):
        self.statusbar.SetStatusText("Pos: %s" % s, 1)

# end of class CTFrame
