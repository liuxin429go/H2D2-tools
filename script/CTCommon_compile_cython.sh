#! /bin/bash

pushd CTCommon
rm -r *.pyd
rm -r *.so
rm -r *.c
rm -r *.html
popd

$INRS_BLD/compile_cython.sh CTCommon.pxd . CTCommon/cython-build
