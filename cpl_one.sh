#!/bin/bash
[ -z "$TMP" ] && echo 'TMP must be defined' && exit
[ -z "$INRS_LXT" ] && echo 'INRS_LXT must be defined' && exit
[ -z "$INRS_DEV" ] && echo 'INRS_DEV must be defined' && exit
[ -z "$INRS_BLD" ] && echo 'INRS_BLD must be defined' && exit

LCL_CPL=$1 && shift
LCL_MPI=$1 && shift
if [ -z $LCL_CPL ]; then LCL_ISZ=cython; fi
if [ -z $LCL_MPI ]; then LCL_ISZ=openmpi; fi

SCONSTRUCT=$INRS_BLD/MUC_SCons/SConstruct
scons -Q --debug=explain --file=$SCONSTRUCT compilers=$LCL_CPL mpilibs=$LCL_MPI  builds=release
